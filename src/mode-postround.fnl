
(local fennel (require :lib.fennel))
(local clj (require :lib.cljlib))

(local util (require :src.util))
(local fonts (require :src.fonts))
(local music (require :src.music))
(local drawing (require :src.drawing))
(local debug (require :src.debug))
(local interim (require :src.mode-interim))

(var debug-state {})
(var counter 0)


{:activate (fn activate [{: job-value : balance : current-level &as old-state} reset?]
             (let [take-home (* job-value .88)] 
               (-> old-state
                   (clj.assoc :balance (+ (or balance 100.0) take-home))
                   (clj.assoc :job-take-home take-home)
                   (clj.assoc :current-level (+ current-level 1)))))
 :fun-draw (fn fun-draw [{: current-level : current-mouse : job-value : job-take-home : balance &as  state}]
             (local (w h _flags) (love.window.getMode))
             (love.graphics.setColor 1 1 1 1)
             (drawing.draw-fax "From: Paladin Accounting, Inc.\nTo: Spooks-Be-Gone, LLC\nSubject: Cashflow Update"
                               (: "Hello,\nCongrats on your latest job! You earned $%.2f and will take home $%.2f after taxes.\nYour new balance is $%.2f.\n\nLet me know if you have any questions!\nSincerely,\nN Paladin, CPA" 
                                  :format job-value (or job-take-home 0.0) balance)
                               :middle)

             (debug.draw-debug debug-state state w h))
 :fun-update (fn fun-update [{: current-level :current-mouse [mx my]  &as state} dt set-mode]
               (if (< counter 65535)
                 (set counter (+ counter 1))
                 (set counter 0))
               (print current-level)

               (if (= current-level (# interim.levels)) ;Use equal because if we're on the last level, we're about to change levels.
                 (set-mode :end true))
               (clj.assoc state :time (+ (or (. state :time) 0) dt)))
 :fun-keypressed (fn fun-keypressed [{: current-level &as state} key set-mode]
                   (let  [ (w h _flags) (love.window.getMode)]
                     (if (= "f5" key) (activate state)
                       (= "return" key) (set-mode :interim false)
                       (= "\\" key) (clj.assoc state :show-debug (not (. state :show-debug)))

                       (= "pageup" key) (clj.update state :debug-scroll
                                                    (partial + (/ h 2)))
                       (= "pagedown" key) (clj.update state :debug-scroll
                                                      (fn [scroll] (- scroll (/ h 2))))
                       :else (do (print key) state))))
 :fun-mousemoved (fn fun-mousemoved [state x y dx dy set-mode]
                  (clj.assoc state :current-mouse [x y]))
 :fun-mousepressed (fn fun-mousepressed [state x y button istouch presses set-mode]
                     (let [(w _h _flags) (love.window.getMode)
                           left (- (/ w 2) 140)
                           right (+ left 280)]
                       (if (and (= button 1) (< left x right) (< 10 y 400))
                       (set-mode :interim false)
                       state)))}
