
(fn calculate-line-length [x1 y1 x2 y2]
  (math.sqrt (+ (math.pow (- x2 x1) 2)
                (math.pow (- y2 y1) 2))))

; (calculate-line-length 10 110 10 10)
; 100
; 
;(calculate-line-length 0 14.14 14.14 0)
; 19.996979771956
; 

{: calculate-line-length}
