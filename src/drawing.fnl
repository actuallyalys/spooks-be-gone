(local fonts (require :src.fonts))

(fn draw-outline [x y w h b] ; b = border
  (love.graphics.setColor 1.0 1.0 0.0 1)
  (love.graphics.rectangle :line (- x b) (- y b) (+ 310 (* b 2) ) (+ 400 (* b 2))))

(fn draw-email [top-text bottom-text side active]
  (let [(w h _flags) (love.window.getMode)
        x (case side 
                :left 20 
                :right 350
                :middle (- (/ w 2) 140))
        y (+ x 280)
        x-inner (+ x 5)
        y-inner (- y 5)]

    (when active
      (draw-outline x 15 310 400 2))
    (love.graphics.setColor 1.0 1.0 1.0 1)
    (love.graphics.rectangle :fill x 15 310 400)
    (love.graphics.setColor 0.05 0.05 0.05 1)

    (love.graphics.printf "GHOST MAIL ELECTRONIC MAIL" fonts.small-mono 
                          x-inner 390 300 :center)
    (love.graphics.printf (: "%s\n==========================================\n%s" :format 
                             top-text
                             bottom-text) fonts.small-mono 
                          x-inner 30 300 :left)))

(fn draw-letter [text [r g b a &as ink-color] side active]
  (let [x (case side
                :left 20 
                :right 350)
        y (+ x 300)
        x-inner (+ x 5)]
    (when active
      (draw-outline x 15 310 400 2))
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.rectangle :fill x 15 310 400)
    ; (love.graphics.setColor 0.1 0.1 0.5 1)
    (love.graphics.setColor r g b a)
    (love.graphics.printf text fonts.small-handwriting 
                          x-inner 30 300 :left)))

(fn draw-fax [top-text bottom-text side active]
  (let [(w h _flags) (love.window.getMode)
        x (case side 
                :left 20 
                :right 350
                :middle (- (/ w 2) 140))
        y (+ x 280)
        x-inner (+ x 5)
        y-inner (- y 5)]

    (when active
      (draw-outline x 15 310 400 2))
    (love.graphics.setColor 1.0 1.0 1.0 1)
    (love.graphics.rectangle :fill x 15 310 400)
    (love.graphics.setColor 0.3 0.3 0.3 1)

    (love.graphics.printf "FAX" fonts.large-display 
                          x-inner 30 300 :left)
    (love.graphics.printf top-text fonts.small-display 
                          x-inner 70 300 :left) 
    (love.graphics.line x-inner 145 y-inner 145)
    (love.graphics.line x-inner 142 y-inner 142)
    (love.graphics.printf bottom-text fonts.small-display 
                          x-inner 150 300 :left)))

(fn draw-correspondence [{: top-text : bottom-text : correspondence-type : ink-color &as level} side active]
  (case correspondence-type
    :fax (draw-fax top-text bottom-text side active)
    :letter (draw-letter top-text ink-color side active)
    :email (draw-email top-text bottom-text side active)))

(fn within-correspondence? [side mx my]
  (match side 
    :left  (and (< 20 mx 330) (< 15 my 415) )
    :right (and (< 350 mx 660) (< 15 my 415) )))

{: draw-fax : draw-letter : draw-correspondence : within-correspondence?}
