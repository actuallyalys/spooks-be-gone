(import-macros {: incf} :sample-macros)
(local fennel (require :lib.fennel))
(local clj (require :lib.cljlib))
(local itable (require :lib.itable))

(local util (require :src.util))
(local fonts (require :src.fonts))
(local music (require :src.music))
(local debug (require :src.debug))

(var counter 0)

(love.graphics.setNewFont 30)

(local (major minor revision) (love.getVersion))

; (love.getHeight)

(var debug-state {})


(fn can-resume? [state]
  (. state :characters))


{:activate (fn activate [{:music old-music &as old-state} reset?]
             (let [obsidian4 (love.audio.newSource "content/22G18_Obsidian004_PianoLoop_OGGV_VBR060.ogg" :stream)
                   Storm  (love.audio.newSource "content/Storm.ogg" :stream)
                   loaded-music (if old-music old-music {: obsidian4 : Storm})]
               (print (fennel.view (. old-state :current-source)))

               (-> old-state 
                   (clj.assoc :current-source (music.change-music (or (. old-state :current-source) {}) loaded-music :playing :obsidian4))
                   (clj.assoc :music loaded-music))))
 :fun-draw (fn fun-draw [state]
             (local (w h _flags) (love.window.getMode))
             (love.graphics.setColor 1 1 1 1)
             (love.graphics.printf
               (: "Love Version: %s.%s.%s"
                  :format  major minor revision)
               fonts.small-sans
               0 (- h 15) w :right)
             (love.graphics.printf
               "0.2.2023-09-03-1 [AFTERJAM]"
               fonts.small-sans
               0 (- h 15) w :left)
             (love.graphics.printf
               "Spooks-Be-Gone, LLC"
               fonts.large-display
               0 (- (/ h 2) 15) w :center)
             (love.graphics.printf
               "\"See a spooky thing? Give us a ring!\""
               fonts.med-display
               0 (- (+ (/ h 2) 36) 15) w :center)
             (love.graphics.printf
               "by actuallyalys<3"
               fonts.small-display
               0 (- (+ (/ h 2) 64) 15) w :center)
             (love.graphics.printf
               (if (can-resume? state) 
                 "Press Enter to restart or Space to resume." 
                 "Press Enter to start.")
               fonts.med-sans
               0 (- (* h 0.75) 15) w :center)

             (debug.draw-debug debug-state state w h))
 :fun-update (fn fun-update [state dt set-mode]
             (if (< counter 65535)
                 (set counter (+ counter 1))
                 (set counter 0))
             ; (when (> (. state :time) (. state :max-time))
             ;   (love.event.quit))
             (-> state 
                 (clj.assoc :current-source (music.advance-music dt (. state :music) (. state :current-source))) 
                 (clj.assoc :time (+ (or (. state :time) 0) dt))))
 :fun-keypressed (fn fun-keypressed [state key set-mode]
                   (let  [ (w h _flags) (love.window.getMode)]
                     (if (= "f5" key) (activate state)
                       (= "return" key) (set-mode :interim true)
                       (= "space" key) (if (can-resume? state)
                                         (set-mode :play false)
                                         state)
                       (= "\\" key) (clj.assoc state :show-debug (not (. state :show-debug)))
                       (= "pageup" key) (clj.update state :debug-scroll
                                                    (partial + (/ h 2)))
                       (= "pagedown" key) (clj.update state :debug-scroll
                                                      (fn [scroll] (- scroll (/ h 2))))

                       :else (do (print key) state))))
 :mousepressed (fn mousepressed [x y button istouch presses]
                 (comment (set max-time (+ max-time 60)))) }
