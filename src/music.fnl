(local clj (require :lib.cljlib))

; (fn advance-music [dt music {: remaining-time : next : current  &as current-sources}]
;   (let [current-source (. music current)
;         current-stopped? (or (and remaining-time (<= remaining-time 0)) (not current) (not (: current-source :isPlaying)))
;         next-source (. music next)
;         new-dt (if next dt 0)
;         new-next (when (not current-stopped?) next)]
;     (when current-stopped?
;       (print "playing")
;       (love.audio.play next-source :stream))
;     ; (when next 
;     ;   (: current-source  :setVolume (- (: current-source :getVolume) 0.05)))
;     ; (-> current-sources
;     ;     ; (clj.assoc :remaining-time (- remaining-time new-dt))
;     ;     (clj.assoc :next new-next)
;     ;     (clj.assoc :current (if current-stopped? next current))
;     ;     )
;     (clj.assoc current-sources :current next)
;     ))
(fn advance-music [dt music {: status : next : current  &as current-sources}]
  (case status
    :stopped current-sources
    :playing current-sources
    :fading-out (let [current-source (. music current) 
                      old-volume (: current-source :getVolume)] 
                    (: current-source :setVolume (- old-volume (* dt .125)))
                    current-sources)
    _ current-sources))


(fn change-music [{: status : next : current  &as current-sources} music new-status new-song]
  (let  [current-source (. music current)
         new-source (. music new-song)]
    (case new-status
      :stopped (do (love.audio.stop current-source)
                 (-> current-sources
                     (clj.assoc :status new-status)
                     (clj.assoc :current nil)))
      :playing (do 
                 (when current 
                   (love.audio.stop current-source))
                 ;(love.audio.stop)
                 (love.audio.play new-source)
                 (: new-source :setLooping true)
                 (-> current-sources 
                     (clj.assoc :status new-status)
                     (clj.assoc :current new-song)))
      :fading-out 

      (clj.assoc current-sources :status new-status)
      _ current-sources
      )))


{: advance-music : change-music}
