
(local extra-small-sans (love.graphics.newFont 9 "normal" (love.graphics.getDPIScale)))
(local small-sans (love.graphics.newFont 12 "normal" (love.graphics.getDPIScale)))
(local med-sans (love.graphics.newFont 15 "normal" (love.graphics.getDPIScale)))
(local large-sans (love.graphics.newFont 30 "normal" (love.graphics.getDPIScale)))

(local debug-small-display (love.graphics.newFont "content/Old_Fax.ttf" 11 "normal" (love.graphics.getDPIScale)))
(local extra-small-display (love.graphics.newFont "content/Old_Fax.ttf" 12 "normal" (love.graphics.getDPIScale)))
(local small-display (love.graphics.newFont "content/Old_Fax.ttf" 16 "normal" (love.graphics.getDPIScale)))
(local med-display (love.graphics.newFont "content/Old_Fax.ttf" 24 "normal" (love.graphics.getDPIScale)))
(local large-display (love.graphics.newFont "content/Old_Fax.ttf" 40 "normal" (love.graphics.getDPIScale)))

(local small-handwriting (love.graphics.newFont "content/TheGirlNextDoor-Regular.ttf" 16 "normal" (love.graphics.getDPIScale)))
(local med-handwriting (love.graphics.newFont "content/TheGirlNextDoor-Regular.ttf" 24 "normal" (love.graphics.getDPIScale)))

(local small-mono (love.graphics.newFont "content/Inconsolata_SemiCondensed-Regular.ttf" 16 "normal" (love.graphics.getDPIScale)))
(local med-mono (love.graphics.newFont "content/Inconsolata_SemiCondensed-Regular.ttf" 24 "normal" (love.graphics.getDPIScale)))

{: extra-small-sans : small-sans : med-sans : large-sans : small-display : debug-small-display : extra-small-display : med-display : large-display : small-handwriting : med-handwriting : small-mono : med-mono}
