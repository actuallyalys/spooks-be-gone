
(local fennel (require :lib.fennel))
(local clj (require :lib.cljlib))

(local util (require :src.util))
(local fonts (require :src.fonts))
(local music (require :src.music))
(local drawing (require :src.drawing))
(local debug (require :src.debug))

(var debug-state {})
(var counter 0)


{:activate (fn activate [{: job-value : balance : current-level &as old-state} reset?]
             old-state)
 :fun-draw (fn fun-draw [{: balance :player-character {: hp} &as  state}]
             (local (w h _flags) (love.window.getMode))
             (love.graphics.setColor 1 1 1 1)
             (if (= hp 0) 
               (drawing.draw-fax "From: Paladin Accounting, Inc.\nTo: Spooks-Be-Gone, LLC\nSubject: Update"
                               ("To whom it may concern,

                               Our records show a balance of $%.2f.

                               Our condolences,
                               N Paladin
                               
                               " :format balance )
                               :middle)
               (drawing.draw-fax "From: Paladin Accounting, Inc.\nTo: Spooks-Be-Gone, LLC\nSubject: Update"
                               (: "Congrats on your retirement! You retired with $%.2f." :format balance )
                               :middle))

             (debug.draw-debug debug-state state w h))
 :fun-update (fn fun-update [{:current-mouse [mx my]  &as state} dt set-mode]
               (if (< counter 65535)
                 (set counter (+ counter 1))
                 (set counter 0))
               (clj.assoc state :time (+ (or (. state :time) 0) dt)))
 :fun-keypressed (fn fun-keypressed [{: current-level &as state} key set-mode]
                   (let  [ (w h _flags) (love.window.getMode)]
                     (if (= "f5" key) (activate state)
                       (= "return" key) (set-mode :intro false)
                       (= "\\" key) (clj.assoc state :show-debug (not (. state :show-debug)))

                       (= "pageup" key) (clj.update state :debug-scroll
                                                    (partial + (/ h 2)))
                       (= "pagedown" key) (clj.update state :debug-scroll
                                                      (fn [scroll] (- scroll (/ h 2))))
                       :else (do (print key) state))))
 :fun-mousemoved (fn fun-mousemoved [state x y dx dy set-mode]
                  (clj.assoc state :current-mouse [x y]))
 :fun-mousepressed (fn fun-mousepressed [state x y button istouch presses set-mode]
                     (let [(w _h _flags) (love.window.getMode)
                           left (- (/ w 2) 140)
                           right (+ left 280)]
                       (if (and (= button 1) (< left x right) (< 10 y 400))
                       (set-mode :intro false)
                       state)))}
