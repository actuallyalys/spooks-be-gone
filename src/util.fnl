
(local fennel (require :lib.fennel))
(local itable (require :lib.itable))
(local clj (require :lib.cljlib))

; (fn swap! [v f]
;   (set v (f v)))


(fn update [tbl key f]
  (let  [old-val (. tbl key)
         new-val (f old-val)]
    (tset tbl key new-val)
    tbl))

(fn when-update [tbl key f]
  (let  [old-val (. tbl key)
         new-val (f old-val)]
    (when new-val
      (tset tbl key new-val))
    tbl))

(fn assoc [tbl key val] 
  (tset tbl key val)
  tbl)

(fn when-assoc [tbl key val] 
  (when val
    (tset tbl key val))
  tbl)

(fn sign [x]
  (if (> x 0) 1
    (= x 0) 0
    -1))


(fn inc [x] (+ x 1))

(fn print-every [counter interval val ...]
  (when (= (% counter interval) 0)
    (print val ...)))

(fn union [s1 s2]
  (let [new-set {}]
    (each [item exists? (pairs s1)]
      (when exists?
        (tset new-set item true)))
    (each [item exists? (pairs s2)]
      (when exists?
        (tset new-set item true)))
    new-set))

(fn difference [s1 s2]
  (let [new-set {}]
    (each [item exists? (pairs s1)]
      (when (and exists? (not (. s2 item)))
        (tset new-set item true)))
    new-set))


; (difference {:a true} {:b true})
; => {:a true}
;
;(difference (:a true} {:b false})
; => {:b false}
;(difference {:a true} {:b true :a true})
; {}
; 

(fn intersection [s1 s2]
  (let [new-set {}]
    (each [item exists? (pairs s1)]
      (when (and exists? (. s2 item))
        (tset new-set item true)))
    new-set))

; (intersection {:a true} {:b true :a true})
; {:a true}

;(intersection {:a true} {:b true})
; {}
; 


(fn count-set [s] 
  (var count 0)
  (each [_k v (pairs s)]
    (when v
      (set count (+ count 1)) ))
  count) 


(fn shuffle [coll] 
  (var new-coll (collect [k v (pairs coll)] k v))
  (fcollect [i 1 (table.getn coll)]
            (let [i (math.random 1 (table.getn new-coll))] 
              (table.remove new-coll i))))

(fn remove-from-array-by-value [coll v]
  (each [i item (ipairs coll)] 
    (when (= item v) (table.remove coll i)))
  coll)

;(remove-from-array-by-value [1 9 2 3] 2)
; [1 9 3]
; 

(fn merge [t1 t2]
  (var new-table {})
  (each [k v (pairs t1)]
    (tset new-table k v))
  (each [k v (pairs t2)]
    (tset new-table k v))
  new-table)

(fn merge-all [t1 t2]
  (var new-table {})
  (each [k v (pairs t1)]
    (tset new-table k v))
  (each [k v (itable.pairs t1)]
    (tset new-table k v))
  (each [k v (pairs t2)]
    (tset new-table k v))
  (each [k v (itable.pairs t2)]
    (tset new-table k v))
  new-table)

;(merge {:a 1} {:b 2})
; {:a 1 :b 2}
; 
;(merge {:a 1} {:b 2 :a 3})
; {:a 3 :b 2}
; 

(fn concat [t1 t2]
  (var new-table [])
  (each [_n item (ipairs t1)]
    (table.insert new-table item))
  (each [_n item (ipairs t2)]
    (table.insert new-table item))
  (print (fennel.view new-table))
  new-table)


(fn assoc!
  [map k & ks]
  (match (getmetatable map)
    {:cljlib/type :transient :cljlib/assoc! f} (clj.apply f map k ks)
    {:cljlib/type :transient} (error "unsupported transient operation" 2)
    _ (error "expected transient collection" 2))
  map)

(fn update!
  [map k f & args]
  (let [old-value (. map k)
        new-value (clj.apply f old-value args)]
    (assoc! map k new-value
            )))


{:update update :inc inc :assoc assoc :sign sign : when-update : when-assoc : print-every : union : difference : count-set : shuffle : remove-from-array-by-value : merge : merge-all : concat : assoc! : update!} 

