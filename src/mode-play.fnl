(local fennel (require :lib.fennel))
(local clj (require :lib.cljlib))
(local bump (require :lib.bump))
(local table (require :table))

(local util (require :src.util))
(local fonts (require :src.fonts))
(local util-math (require :src.math))
(local music (require :src.music))
(local debug (require :src.debug))

; (import-macros cljm :lib.cljlib)

(local (major minor revision) (love.getVersion))

(var counter 0)

(var key-presses [])

(var debug-state {:key-presses []})

(local enemy-stats {:skeleton {:hp 1}
                    :revenant {:hp 2}})

(fn create-quads [image w h]
  (let [quads []
        (d1 d2) (: image :getDimensions)]
    (for [y 0 (- d2 h)]
      (for [x 0 (- d1 w) w]
        (table.insert quads (love.graphics.newQuad x y w h d1 d2))))
    quads))

(fn advance-animation 
  [{: image : frame : mode : rate : next-mode &as  animation} progress]
  "Advance current animation by another frame."
  (let [duration (. image mode :duration)
        new-frame (+ 1 (% (math.floor (* progress rate)) (- duration 0)))
        rolled-over? (< new-frame frame)]
    (-> animation 
        (clj.assoc :frame new-frame)
        (clj.assoc :mode (if (and rolled-over? next-mode) next-mode mode)))))

(var world (bump.newWorld))

(var animation-duration 8)
(var animation-height 32)
(var animation-width 24)

(fn create-animation-image [image-name w h]
  (let [image (love.graphics.newImage image-name)
        quads (create-quads image w h)]
    {:image image
     :duration (length quads)
     :quads quads}))

(fn create-animation 
  [image {:offset [x y &as offset]
          : mob-offset
          : scale
          : enemy-type 
          : rotate} mode]
  {: enemy-type
   :offset (or offset [0 0])
   :mob-offset (or mob-offset [0 0])
   :scale (or scale [1 1])
   :rotate (or rotate 0)
   :image  image ;going to create from scratch each time now
   :frame 1
   :rate (if (= mode :walk) 15 10)
   :move-rate 50
   :mode (or mode :idle)})

(fn move-character 
  [{:offset [x y &as offset] :mob-offset [mobx moby] : scale : move-rate &as animation} dt {:animation {:offset [px py]} &as player-character}]
  "Moves character"
  (let  [new-scale [(if (> px x) 1 -1) 1]
         distance (math.sqrt (+ (math.pow (- px x) 2) 
                                (math.pow (- py y) 2)))
         vx (* (math.random) (/ (math.sqrt distance) 20)
               (if (> px x) 1.0 -1.0))
         vy (* (math.random) (/ (math.sqrt distance) 20)
               (if (> py y) 1.0 -1.0))
         dx (if (> distance 30)
              (+ vx (* (if (> px x) 1.0 -1.0) dt move-rate))
              0) ;delta x
         dy (if (or (> distance 30) (> (math.abs (- y py)) 10))
              (+ vy (* (if (> py y) 1.0 -1.0) dt move-rate))
              0)
         new-offset [(+ x dx) (+ y dy)]]
    (-> animation
        (clj.assoc :next-mode (if (> distance 40) :walk :idle))
        (clj.assoc :offset new-offset )
        (clj.assoc :scale new-scale))))

(fn update-world [{: id :animation {:offset [x y] :mob-offset [mobx moby] &as animation} &as character}]
  (: world :update id (math.floor (+ x mobx)) (math.floor (+ y moby)))
  character)

(fn move-player-character 
  [{:offset [x y &as offset] : scale : mode : next-mode : frame &as animation} dt [mx my &as current-mouse]]
  (let [player-move-rate 100 
        distance (math.sqrt (+ (math.pow (- mx x) 2) 
                               (math.pow (- my y) 2)))
        new-scale [(if (> mx x) 1 -1) 1]
        dx (if (> distance 5) (* (if (> mx x) 1.0 -1.0) dt player-move-rate) 0) ;delta x
        dy (if (> distance 5) (* (if (> my y) 1.0 -1.0) dt player-move-rate) 0)
        [new-mode new-next-mode] (case [mode next-mode]
                                   [:attack _] [:attack :idle]
                                   [current :attack]  [:attack current]
                                   (where [current _] (> distance 5)) [current :walk]
                                   [a a] [a :idle]
                                   [current current_next] [current current_next])]
    (-> animation 
        (clj.assoc :mode new-mode)
        (clj.assoc :next-mode new-next-mode)
        (clj.assoc :frame (if (not (= mode new-mode)) (do (print "resetting frame") 1) frame))
        (clj.assoc :scale new-scale)
        (clj.assoc :offset [(+ x dx) (+ y dy)]))))

(fn register-attack [{: id : hp :animation {:offset [ex ey] :mob-offset [mobx moby] &as animation} &as character} 
                     attack-items
                     {: frame : mode :offset [x y] :scale [sx sy]  &as player-character}] ;e for "enemy"
  (let  [distance (math.sqrt (+ (math.pow (- (math.floor (+ mobx ex)) x) 2) 
                                (math.pow (- (math.floor (+ moby ey)) y) 2)))]
    (if (not (and (= mode :attack)
                (= frame 1)
             (accumulate [hit false 
                          _n item (ipairs attack-items)]
               (or hit (= item id)))))
      character
      (clj.assoc character :hp (- hp 1)))))

(fn setup-attack [{: id : hp :animation {: mode :offset [ex ey] :mob-offset [mobx moby] &as animation} : attack-timer &as enemy}
                  dt
                  {:animation {: frame :offset [x y] :scale [sx sy]}  &as player}]
  (let  [distance (math.sqrt (+ (math.pow (- (math.floor (+ mobx ex)) x) 2) 
                                (math.pow (- (math.floor (+ moby ey)) y) 2)))]
    (if (and (not attack-timer)
             (< distance 25))
      (clj.assoc enemy :attack-timer 1)

      (= mode :attack)
      (-> enemy
          (clj.assoc :attack-timer nil)
          (clj.assoc-in [:animation :next-mode] :idle)) ;TODO handle better, probably

      (and attack-timer (<= attack-timer 0))
      (clj.assoc-in enemy [:animation :next-mode] :attack)

      attack-timer
      (clj.assoc enemy :attack-timer (- attack-timer dt))


      :else
      enemy)))

;should be the same code as what we use for player's attacks on enemies, really
(fn register-attack-on-player [{: hp &as player} characters]
  (let [hits (accumulate [hits 0
                          _n {:animation {: mode : frame :scale [sx _sy] :offset [x y] :mob-offset [mobx moby]}  &as character} (ipairs characters)]
               (let [new-x (if (= sx -1) (- (+ x mobx) 64)
                             (= sx 1) (+ x mobx 32))
                     (_attack-hits len) (: world :queryRect new-x (+ moby y 32) 32 32 (fn [id] (= id 1001)))]
                 (if (and (= mode :attack) (= frame 1) (> len 0))
                   (+ 1 hits) ;change to have actually variable damage.
                   hits)))]
      (clj.assoc player :hp (- hp hits))))

(fn mark-dead [{: hp :animation {: mode : next-mode} &as character}]
  (if (and (<= hp 0) 
           (not (= mode :dead))) ;don't remark as dead
    (-> character 
        (clj.assoc-in [:animation :mode] :dead)
        (clj.assoc-in [:animation :frame] 1))
    character))

(fn cull-dead [{:animation {: mode : frame} &as character}]
  (when (not (and (= mode :dead) 
                  (or (>= frame 11)
                      (>= frame (length (. character :animation :image :dead :quads)))
                      )))
    character))

{:activate (fn activate [old-state force-reset {: skeletons : value : revenants &as characters}]
             (set world (bump.newWorld))
             (let [modes {"Idle" 24 "Attack" 43  "Dead" 33 "React" 22 "Walk" 22}  
                       (w h _flags) (love.window.getMode)
                       mode-names (icollect [name _v (pairs modes)]
                                    (: name :lower))
                       skeleton-sprites (collect [state-name w (pairs modes)]
                                          (: state-name :lower) (create-animation-image (.. "content/Skeleton_" state-name ".png") w animation-height))
                       main-character-sprites (collect [state-number state-name (pairs {"00" :idle "01" :walk "03" :run "11" :attack "25" :dead })]
                                                state-name (create-animation-image (.. "content/Fire_Warrior-Sheet-00-" state-number ".png") 144 63))
                       revenant-sprites (collect [_n state-name (ipairs ["Dead" "Attack" "Walk" "Idle"])] 
                                          (: state-name :lower) (create-animation-image (.. "content/Skeleton2_" state-name "_recut.png") 80 57))
                       animations (when force-reset
                                    (util.concat  (fcollect [i 1 (* animation-height (or revenants 0)) animation-height]
                                                            (create-animation revenant-sprites {:enemy-type :revenant
                                                                                                :offset [i i] 
                                                                                                :mob-offset [(* (- (math.random) 0.5) 30) (* (- (math.random) 0.5) 30)] 
                                                                                                :scale [-1 1]} :idle))
                                                 (fcollect [i 1 (* animation-height skeletons) animation-height]
                                                           (create-animation skeleton-sprites {:enemy-type :skeleton
                                                                                               :offset [i i] 
                                                                                               :mob-offset [(* (- (math.random) 0.5) 30) (* (- (math.random) 0.5) 30)] 
                                                                                               :scale [-1 1]} :idle))))]
                   (: world :add 1001 (/ w 2) (+ (/ h 2) 8) 
                      (* 64 0.75) ;144 
                      (* 63 0.75))
                   (-> old-state 
                       (clj.assoc :current-source (music.change-music (or (. old-state :current-source) {}) (. old-state :music) :playing :Storm))
                       (clj.assoc :debug-scroll 0)
                       (clj.assoc :debug-depth 2)
                       (clj.assoc :skeletons skeleton-sprites)
                       (clj.assoc :player-character {:id 1001 ;for now :(
                                                                         :hp 5
                                                                         :animation (create-animation main-character-sprites {:offset [(/ w 2) (/ h 2)]})})
                                  (clj.assoc :job-value value)
                                  (clj.assoc :animation-progress 0)
                                  (clj.assoc :quads (create-quads (love.graphics.newImage "content/Skeleton_Idle.png") 24 32))
                                  (clj.assoc :image (love.graphics.newImage "content/Skeleton_Idle.png"))
                                  (clj.assoc :characters (if force-reset 
                                                           (icollect [n {: enemy-type :offset [x y] :mob-offset [mobx moby] &as animation} (ipairs animations)]
                                                             (let [_ (print "enemy-type: " enemy-type)
                                                                   hp (. enemy-stats enemy-type :hp)
                                                                   character {:animation animation
                                                                              :hp hp
                                                                              :id n}]
                                                               (: world :add n (+ x mobx) (+ y moby) animation-width animation-height)
                                                               character))
                                                           (. old-state :characters))))))
 :fun-draw (fn fun-draw [state]
             (local (w h _flags) (love.window.getMode))
             (var start-time (os.clock))
             (love.graphics.printf
               (:  "FPS: %d"
                  :format (love.timer.getFPS))
               fonts.small-display
               0 (- h 15) w :left)
             (love.graphics.printf
               (:  "HP: %d"
                  :format (. state :player-character :hp) )
               fonts.small-display
               0 (- h 15) w :center)
             (debug.draw-debug debug-state state w h)
             ;debug
             ; (love.graphics.setColor 0.95 0 0 1)
             ; (let [{:offset [x y] :scale [sx sy]} (. state :player-character :animation)
             ;       new-x (if (= sx -1) (- x 32)
             ;               (= sx 1) (+ x 32))]
             ;   (love.graphics.rectangle :line new-x (- (+ y 32) 8) 48 48))
             ; (love.graphics.setColor 1 1 1 1)
             ; (let [(items len) (: world :getItems) ]
             ;   (each [_n id (ipairs items)]
             ;     (let [(x y w h) (: world :getRect id)]
             ;       (love.graphics.rectangle :line x y w h))))
             (love.graphics.translate 25 25)

             (love.graphics.origin)
             (each [_n {:animation {: image : frame : mode :scale [sx sy] :offset [x y] :mob-offset [mobx moby] :rotate r &as animation}} (ipairs (. state :characters))]
               (let  [selected-image (. image mode)
                      scale-adjusted-x x]
                 (love.graphics.draw (. selected-image :image) (. selected-image :quads frame) (math.floor (+ scale-adjusted-x mobx)) (math.floor (+ y moby)) r sx sy)))
             (let  [{: frame : mode :offset [x y] :scale [sx sy] :rotate r} (. state :player-character :animation)
                    selected-image (. state :player-character :animation :image mode)
                    scale-adjusted-x (if (= -1 sx) (+ x 36) x)]
               (love.graphics.draw (. selected-image :image) (. selected-image :quads frame) (math.floor scale-adjusted-x) (math.floor y) r sx sy))
             (tset debug-state :time-draw (: "%5.2f" :format (* (- (os.clock) start-time) 1000))))
 :fun-update (fn fun-update [{: player-character : pc-animations : characters &as state} dt set-mode]
               (var start-time (os.clock))
               (if (< counter 65535)
                 (set counter (+ counter 1))
                 (set counter 0))
               (if (not characters)
                 state
                 (= 0 (length characters))
                 (set-mode :postround true)

                 (and (= (. player-character :animation :mode ) :dead)
                      (>= (. player-character :animation :frame ) 11))
                 (set-mode :end true)
                 (-> state
                     (clj.assoc :characters (let [{:animation {:scale [sx _sy] :offset [x y]}} player-character
                                                  new-x (if (= sx -1) (- x 32)
                                                          (= sx 1) (+ x 32))
                                                  (attack-hits _len) (: world :queryRect new-x (- (+ y 32) 8) 48 48)]
                                              (icollect [_n {: animation &as character} (ipairs characters)]
                                                (-> character 
                                                    (clj.assoc :animation (-> animation
                                                                              (advance-animation (. state :animation-progress))
                                                                              (move-character dt (or (. state :player-character) [0 0]))))
                                                    (update-world)
                                                    (setup-attack dt (. state :player-character))
                                                    (register-attack attack-hits (. state :player-character :animation))
                                                    (mark-dead)
                                                    (cull-dead)))))
                     (clj.assoc-in [:player-character :animation] (-> (. state :player-character :animation)
                                                                      (advance-animation (. state :animation-progress))
                                                                      (move-player-character dt (or (. state :current-mouse) [0 0]))))
                     (clj.update :player-character #(register-attack-on-player $ (. state :characters)))
                     (clj.update :player-character mark-dead)
                     (clj.update :player-character update-world)
                     (clj.assoc :current-source (music.advance-music dt (. state :music) (. state :current-source))) 
                     (clj.assoc :animation-progress (+ dt (. state :animation-progress))))))
:fun-keypressed (fn fun-keypressed [state key set-mode]
                  ;don't love this , but...
                  (local (w h _flags) (love.window.getMode))
                  (table.insert (. debug-state :key-presses) key)
                  (if (= "f5" key) (activate state true)
                    (= "\\" key) (clj.assoc state :show-debug (not (. state :show-debug)))
                    (= "pageup" key) (clj.update state :debug-scroll
                                                 (partial + (/ h 2)))
                    (= "pagedown" key) (clj.update state :debug-scroll
                                                   (fn [scroll] (- scroll (/ h 2))))
                    (= "escape" key) (set-mode :intro)
                    (= "backspace" key) (-> state
                            (clj.assoc  :mouse [])
                            (clj.assoc-in [:ui :moving-name] nil)
                            (clj.assoc-in [:ui :moving-inhabitant] nil))
                    (= "]" key) (set-mode :postround true)
                    (= "-" key) (clj.update state :debug-depth #(math.max 1 (- $ 1)))
                    (= "=" key) (clj.update state :debug-depth #(+ $ 1)) ;+ 
                    :else (do (print key) state)))
:fun-mousemoved (fn fun-mousemoved [state x y dx dy set-mode]
                  (clj.assoc state :current-mouse [x y]))
:fun-mousepressed (fn fun-mousepressed [state x y button istouch presses set-mode]
                    ;or is needed to prevent issues destructuring from nil:
                    (if (= button 1) 
                      (-> state
                          (clj.assoc-in [:player-character :animation :next-mode] :attack))
                      state))}
