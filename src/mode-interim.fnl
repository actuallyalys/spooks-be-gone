
(local fennel (require :lib.fennel))
(local clj (require :lib.cljlib))

(local util (require :src.util))
(local fonts (require :src.fonts))
(local music (require :src.music))
(local drawing (require :src.drawing))
(local debug (require :src.debug))

(var debug-state {})
(var counter 0)



(local levels 
  [[{:skeletons 10 :value 500 :correspondence-type :fax 
     :top-text "TO: SPOOK-BE-GONE LLC\nFROM: HANK FURY\nSUBJECT: Skeletons!!"
     :bottom-text "We have a SERIOUS problem with skeletons. Please come soon. I will pay $500!\nHANK"}
    {:skeletons 1 :revenants 1 :value 10 :correspondence-type :letter :ink-color [0.5 0 0.5 1]
     :top-text "May 1\nHello,\nI have found myself positively vexed by a skeleton. I know it's a rather trivial undertaking for such a skilled exterminator as yourself, and I cannot pay very much, but I am out of other options.\nThank you very much,\nMary"}]
   [{:skeletons 20 :revenants 4 :value 1000 :correspondence-type :fax
     :top-text "TO: SPOOK-BE-GONE LLC\nFROM: Redd Walker\nSUBJECT: Spook Remediation Requested"
     :bottom-text "To Whom It May Concern:\nPlease arrive as soon as you can. Thank you."}
    {:skeletons 5 :value 50 :correspondence-type :letter :top-text "Hi, Can you come soon? My dearest Lucy and I can't open our bed and breakfast until these ghastly hauntings are dealt with.\nCarrie" :ink-color [0.1 0.1 0.5]}]
   [{:skeletons 25 :value 750 :correspondence-type :fax
     :top-text "TO: SPOOK-BE-GONE LLC\nFROM: Spectral Used Books \nSUBJECT: Help"
     :bottom-text "Hi, are you available? We've heard good things and were hoping you could clear out our store room.\nAsh"}
    {:skeletons 60 :revenants 4 :value 1500 :correspondence-type :fax
     :top-text "TO: SPOOK-BE-GONE LLC\nFROM: Redd Walker\nSUBJECT: Spook Remediation Requested"
     :bottom-text "To Whom It May Concern:\nWe have another storefront with a major spook infestation. Please arrive as soon as you can. Thank you."} ]
   [{:skeletons 90 :revenants 9 :value 2500 :correspondence-type :email
     :top-text "To: Spooks-Be-Gone \nFrom: C X Levett <cxl@example.com> \nSubject: Help! Ghosts! Help!\nPriority: HIGH"
     :bottom-text "Hi!\nPlease help!\nty, C X L"}
    {:revenant 10 :value 1000 :correspondence-type :letter :top-text "Hi, Can you come soon? My dearest Lucy and I opened our B&B, but these huge skeletons showed up! We're afraid we're going to get terrible reviews. The Yelp crowd is unforgiving.\nCarrie" :ink-color [0.1 0.1 0.5]}]])


{:levels levels
 :activate (fn activate [{: current-level : balance &as  old-state} reset?]
             (let [current-level (if reset? 1
                                   current-level)
                   balance (if reset? 100.0
                             balance)]
               (print balance)
               (-> old-state
                   (clj.assoc :current-mouse [0 0])
                   (clj.assoc :balance balance)
                   (clj.assoc :current-level current-level)
                   (clj.assoc :current-options (. levels current-level))
                   (clj.assoc :current-source (music.change-music (or (. old-state :current-source) {}) (. old-state :music) :fading-out :obsidian4)))))
 :fun-draw (fn fun-draw [{: current-level :current-mouse [mx my] : current-options &as state}]
             ; (print (fennel.view state))
             (local (w h _flags) (love.window.getMode))
             (love.graphics.setColor 1 1 1 1)
             (drawing.draw-correspondence (. current-options 1) :left 
                                          ; current-mouse
                                          (drawing.within-correspondence? :left mx my))
             (drawing.draw-correspondence (. current-options 2) :right 
                                          ; current-mouse
                                          (drawing.within-correspondence? :right mx my))
             (debug.draw-debug debug-state state w h))
 :fun-update (fn fun-update [{: current-level &as state} dt set-mode]
               (if (< counter 65535)
                 (set counter (+ counter 1))
                 (set counter 0))

               (-> state 
                   (clj.assoc :current-source (music.advance-music dt (. state :music) (. state :current-source))) 
                   ; (set-mode :play true (. state :current-options 2))
                   (clj.assoc :time (+ (or (. state :time) 0) dt))))
 :fun-keypressed (fn fun-keypressed [state key set-mode]
                   (let  [ (w h _flags) (love.window.getMode)]
                     (if (= "f5" key) (activate state)
                       (= "a" key) (set-mode :play true (. state :current-options 1))
                       (= "b" key) (set-mode :play true (. state :current-options 2))
                       ( = "\\" key) (clj.assoc state :show-debug (not (. state :show-debug)))

                       (= "pageup" key) (clj.update state :debug-scroll
                                                    (partial + (/ h 2)))
                       (= "pagedown" key) (clj.update state :debug-scroll
                                                      (fn [scroll] (- scroll (/ h 2))))
                       :else (do (print key) state))))
 :fun-mousemoved (fn fun-mousemoved [state x y dx dy set-mode]
                  (clj.assoc state :current-mouse [x y]))
 :fun-mousepressed (fn fun-mousepressed [state x y button istouch presses set-mode]
                     (if 
                       (and (= button 1) (drawing.within-correspondence? :left x y))
                       (set-mode :play true (. state :current-options 1))
                       (and (= button 1) (drawing.within-correspondence? :right x y))
                       (set-mode :play true (. state :current-options 2))
                       state))}
