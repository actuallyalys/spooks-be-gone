(local fennel (require :lib.fennel))
(local clj (require :lib.cljlib))

(local fonts (require :src.fonts))

(fn draw-debug [{: key-presses &as debug-state} {: debug-depth : debug-scroll : show-debug &as state} w h]
  (when show-debug 
    (love.graphics.setColor 0.25 0.25 0.25 0.5)
    (love.graphics.rectangle :fill 0 0 w h )
    (love.graphics.setColor 1 1 1 1)

    (love.graphics.printf
      (:  "Debug: %s\nFPS: %d\nkeypresses: %s\nstate: %s"
         :format 
         (fennel.view debug-state {:depth (or debug-depth 99)})
         (love.timer.getFPS)
         (fennel.view (clj.take-last 10 key-presses))
         (fennel.view state {:depth (or debug-depth 99)}))
      fonts.small-sans
      0 (or debug-scroll 0) w :left)))

{: draw-debug}
