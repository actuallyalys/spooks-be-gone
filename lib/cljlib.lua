--[[ This is a self-contained version of the fennel-cljlib library
     meant to be used directly from Lua, or embedded into other
     applications. It doesn't include macros, given that Lua doesn't
     support Fennel's macro system, but all other features, like
     laziness, and immutability are available in the same way as if
     this library was used from Fennel. ]]
local function _2_()
  return "#<namespace: core>"
end
package.preload["lazy-seq"] = package.preload["lazy-seq"] or function(...)
  utf8 = _G.utf8
  local function pairs_2a(t)
    local mt = getmetatable(t)
    if (("table" == mt) and mt.__pairs) then
      return mt.__pairs(t)
    else
      return pairs(t)
    end
  end
  pcall(function() require("fennel").metadata:setall(pairs_2a, "fnl/arglist", {"t"}) end)
  local function ipairs_2a(t)
    local mt = getmetatable(t)
    if (("table" == mt) and mt.__ipairs) then
      return mt.__ipairs(t)
    else
      return ipairs(t)
    end
  end
  pcall(function() require("fennel").metadata:setall(ipairs_2a, "fnl/arglist", {"t"}) end)
  local function rev_ipairs(t)
    local function next(t0, i)
      local i0 = (i - 1)
      local _5_ = i0
      if (_5_ == 0) then
        return nil
      elseif true then
        local _ = _5_
        return i0, (t0)[i0]
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(next, "fnl/arglist", {"t", "i"}) end)
    return next, t, (1 + #t)
  end
  pcall(function() require("fennel").metadata:setall(rev_ipairs, "fnl/arglist", {"t"}) end)
  local function length_2a(t)
    local mt = getmetatable(t)
    if (("table" == mt) and mt.__len) then
      return mt.__len(t)
    else
      return #t
    end
  end
  pcall(function() require("fennel").metadata:setall(length_2a, "fnl/arglist", {"t"}) end)
  local function table_pack(...)
    local _8_ = {...}
    _8_["n"] = select("#", ...)
    return _8_
  end
  pcall(function() require("fennel").metadata:setall(table_pack, "fnl/arglist", {"..."}) end)
  local table_unpack = (table.unpack or _G.unpack)
  local seq = nil
  local cons_iter = nil
  local function first(s)
    local _9_ = seq(s)
    if (nil ~= _9_) then
      local s_2a = _9_
      return s_2a(true)
    elseif true then
      local _ = _9_
      return nil
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(first, "fnl/arglist", {"s"}, "fnl/docstring", "Return first element of a sequence.") end)
  local function empty_cons_view()
    return "@seq()"
  end
  pcall(function() require("fennel").metadata:setall(empty_cons_view, "fnl/arglist", {}) end)
  local function empty_cons_len()
    return 0
  end
  pcall(function() require("fennel").metadata:setall(empty_cons_len, "fnl/arglist", {}) end)
  local function empty_cons_index()
    return nil
  end
  pcall(function() require("fennel").metadata:setall(empty_cons_index, "fnl/arglist", {}) end)
  local function cons_newindex()
    return error("cons cell is immutable")
  end
  pcall(function() require("fennel").metadata:setall(cons_newindex, "fnl/arglist", {}) end)
  local function empty_cons_next(s)
    return nil
  end
  pcall(function() require("fennel").metadata:setall(empty_cons_next, "fnl/arglist", {"s"}) end)
  local function empty_cons_pairs(s)
    return empty_cons_next, nil, s
  end
  pcall(function() require("fennel").metadata:setall(empty_cons_pairs, "fnl/arglist", {"s"}) end)
  local function gettype(x)
    local _11_
    do
      local t_12_ = getmetatable(x)
      if (nil ~= t_12_) then
        t_12_ = (t_12_)["__lazy-seq/type"]
      else
      end
      _11_ = t_12_
    end
    if (nil ~= _11_) then
      local t = _11_
      return t
    elseif true then
      local _ = _11_
      return type(x)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(gettype, "fnl/arglist", {"x"}) end)
  local function realize(c)
    if ("lazy-cons" == gettype(c)) then
      c()
    else
    end
    return c
  end
  pcall(function() require("fennel").metadata:setall(realize, "fnl/arglist", {"c"}) end)
  local empty_cons = {}
  local function empty_cons_call(tf)
    if tf then
      return nil
    else
      return empty_cons
    end
  end
  pcall(function() require("fennel").metadata:setall(empty_cons_call, "fnl/arglist", {"tf"}) end)
  local function empty_cons_fennelrest()
    return empty_cons
  end
  pcall(function() require("fennel").metadata:setall(empty_cons_fennelrest, "fnl/arglist", {}) end)
  local function empty_cons_eq(_, s)
    return rawequal(getmetatable(empty_cons), getmetatable(realize(s)))
  end
  pcall(function() require("fennel").metadata:setall(empty_cons_eq, "fnl/arglist", {"_", "s"}) end)
  setmetatable(empty_cons, {__call = empty_cons_call, __len = empty_cons_len, __fennelview = empty_cons_view, __fennelrest = empty_cons_fennelrest, ["__lazy-seq/type"] = "empty-cons", __newindex = cons_newindex, __index = empty_cons_index, __name = "cons", __eq = empty_cons_eq, __pairs = empty_cons_pairs})
  local function rest(s)
    local _17_ = seq(s)
    if (nil ~= _17_) then
      local s_2a = _17_
      return s_2a(false)
    elseif true then
      local _ = _17_
      return empty_cons
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(rest, "fnl/arglist", {"s"}, "fnl/docstring", "Return the tail of a sequence.\n\nIf the sequence is empty, returns empty sequence.") end)
  local function seq_3f(x)
    local tp = gettype(x)
    return ((tp == "cons") or (tp == "lazy-cons") or (tp == "empty-cons"))
  end
  pcall(function() require("fennel").metadata:setall(seq_3f, "fnl/arglist", {"x"}, "fnl/docstring", "Check if object is a sequence.") end)
  local function empty_3f(x)
    return not seq(x)
  end
  pcall(function() require("fennel").metadata:setall(empty_3f, "fnl/arglist", {"x"}, "fnl/docstring", "Check if sequence is empty.") end)
  local function next(s)
    return seq(realize(rest(seq(s))))
  end
  pcall(function() require("fennel").metadata:setall(next, "fnl/arglist", {"s"}, "fnl/docstring", "Return the tail of a sequence.\n\nIf the sequence is empty, returns nil.") end)
  local function view_seq(list, options, view, indent, elements)
    table.insert(elements, view(first(list), options, indent))
    do
      local tail = next(list)
      if ("cons" == gettype(tail)) then
        view_seq(tail, options, view, indent, elements)
      else
      end
    end
    return elements
  end
  pcall(function() require("fennel").metadata:setall(view_seq, "fnl/arglist", {"list", "options", "view", "indent", "elements"}) end)
  local function pp_seq(list, view, options, indent)
    local items = view_seq(list, options, view, (indent + 5), {})
    local lines
    do
      local tbl_17_auto = {}
      local i_18_auto = #tbl_17_auto
      for i, line in ipairs(items) do
        local val_19_auto
        if (i == 1) then
          val_19_auto = line
        else
          val_19_auto = ("     " .. line)
        end
        if (nil ~= val_19_auto) then
          i_18_auto = (i_18_auto + 1)
          do end (tbl_17_auto)[i_18_auto] = val_19_auto
        else
        end
      end
      lines = tbl_17_auto
    end
    lines[1] = ("@seq(" .. (lines[1] or ""))
    do end (lines)[#lines] = (lines[#lines] .. ")")
    return lines
  end
  pcall(function() require("fennel").metadata:setall(pp_seq, "fnl/arglist", {"list", "view", "options", "indent"}) end)
  local drop = nil
  local function cons_fennelrest(c, i)
    return drop((i - 1), c)
  end
  pcall(function() require("fennel").metadata:setall(cons_fennelrest, "fnl/arglist", {"c", "i"}) end)
  local allowed_types = {cons = true, ["empty-cons"] = true, ["lazy-cons"] = true, ["nil"] = true, string = true, table = true}
  local function cons_next(_, s)
    if (empty_cons ~= s) then
      local tail = next(s)
      local _22_ = gettype(tail)
      if (_22_ == "cons") then
        return tail, first(s)
      elseif true then
        local _0 = _22_
        return empty_cons, first(s)
      else
        return nil
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(cons_next, "fnl/arglist", {"_", "s"}) end)
  local function cons_pairs(s)
    return cons_next, nil, s
  end
  pcall(function() require("fennel").metadata:setall(cons_pairs, "fnl/arglist", {"s"}) end)
  local function cons_eq(s1, s2)
    if rawequal(s1, s2) then
      return true
    else
      if (not rawequal(s2, empty_cons) and not rawequal(s1, empty_cons)) then
        local s10, s20, res = s1, s2, true
        while (res and s10 and s20) do
          res = (first(s10) == first(s20))
          s10 = next(s10)
          s20 = next(s20)
        end
        return res
      else
        return false
      end
    end
  end
  pcall(function() require("fennel").metadata:setall(cons_eq, "fnl/arglist", {"s1", "s2"}) end)
  local function cons_len(s)
    local s0, len = s, 0
    while s0 do
      s0, len = next(s0), (len + 1)
    end
    return len
  end
  pcall(function() require("fennel").metadata:setall(cons_len, "fnl/arglist", {"s"}) end)
  local function cons_index(s, i)
    if (i > 0) then
      local s0, i_2a = s, 1
      while ((i_2a ~= i) and s0) do
        s0, i_2a = next(s0), (i_2a + 1)
      end
      return first(s0)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(cons_index, "fnl/arglist", {"s", "i"}) end)
  local function cons(head, tail)
    do local _ = {head, tail} end
    local tp = gettype(tail)
    assert(allowed_types[tp], ("expected nil, cons, table, or string as a tail, got: %s"):format(tp))
    local function _28_(_241, _242)
      if _242 then
        return head
      else
        local _29_ = tail
        if (nil ~= _29_) then
          local s = _29_
          return s
        elseif (_29_ == nil) then
          return empty_cons
        else
          return nil
        end
      end
    end
    return setmetatable({}, {__call = _28_, ["__lazy-seq/type"] = "cons", __index = cons_index, __newindex = cons_newindex, __len = cons_len, __pairs = cons_pairs, __name = "cons", __eq = cons_eq, __fennelview = pp_seq, __fennelrest = cons_fennelrest})
  end
  pcall(function() require("fennel").metadata:setall(cons, "fnl/arglist", {"head", "tail"}, "fnl/docstring", "Construct a cons cell.\nPrepends new `head' to a `tail', which must be either a table,\nsequence, or nil.\n\n# Examples\n\n``` fennel\n(assert-eq [0 1] (cons 0 [1]))\n(assert-eq (list 0 1 2 3) (cons 0 (cons 1 (list 2 3))))\n```") end)
  local function _32_(s)
    local _33_ = gettype(s)
    if (_33_ == "cons") then
      return s
    elseif (_33_ == "lazy-cons") then
      return seq(realize(s))
    elseif (_33_ == "empty-cons") then
      return nil
    elseif (_33_ == "nil") then
      return nil
    elseif (_33_ == "table") then
      return cons_iter(s)
    elseif (_33_ == "string") then
      return cons_iter(s)
    elseif true then
      local _ = _33_
      return error(("expected table, string or sequence, got %s"):format(_), 2)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(_32_, "fnl/arglist", {"s"}, "fnl/docstring", "Construct a sequence out of a table, string or another sequence `s`.\nReturns `nil` if given an empty sequence or an empty table.\n\nSequences are immutable and persistent, but their contents are not\nimmutable, meaning that if a sequence contains mutable references, the\ncontents of a sequence can change.  Unlike iterators, sequences are\nnon-destructive, and can be shared.\n\nSequences support two main operations: `first`, and `rest`.  Being a\nsingle linked list, sequences have linear access time complexity..\n\n# Examples\n\nTransform sequential table to a sequence:\n\n``` fennel\n(local nums [1 2 3 4 5])\n(local num-seq (seq nums))\n\n(assert-eq nums num-seq)\n```\n\nIterating through a sequence:\n\n```fennel\n(local s (seq [1 2 3 4 5]))\n\n(fn reverse [s]\n  ((fn reverse [s res]\n     (match (seq s)\n       s* (reverse (rest s*) (cons (first s*) res))\n       _ res))\n   s nil))\n\n(assert-eq [5 4 3 2 1]\n           (reverse s))\n```\n\n\nSequences can also be created manually by using `cons` function.") end)
  seq = _32_
  local function lazy_seq(f)
    local lazy_cons = cons(nil, nil)
    local realize0
    local function _35_()
      local s = seq(f())
      if (nil ~= s) then
        return setmetatable(lazy_cons, getmetatable(s))
      else
        return setmetatable(lazy_cons, getmetatable(empty_cons))
      end
    end
    pcall(function() require("fennel").metadata:setall(_35_, "fnl/arglist", {}) end)
    realize0 = _35_
    local function _37_(_241, _242)
      return realize0()(_242)
    end
    local function _38_(_241, _242)
      return (realize0())[_242]
    end
    local function _39_(...)
      realize0()
      return pp_seq(...)
    end
    local function _40_()
      return length_2a(realize0())
    end
    local function _41_()
      return pairs_2a(realize0())
    end
    local function _42_(_241, _242)
      return (realize0() == _242)
    end
    return setmetatable(lazy_cons, {__call = _37_, __index = _38_, __newindex = cons_newindex, __fennelview = _39_, __fennelrest = cons_fennelrest, __len = _40_, __pairs = _41_, __name = "lazy cons", __eq = _42_, ["__lazy-seq/type"] = "lazy-cons"})
  end
  pcall(function() require("fennel").metadata:setall(lazy_seq, "fnl/arglist", {"f"}, "fnl/docstring", "Create lazy sequence from the result of calling a function `f`.\nDelays execution of `f` until sequence is consumed.\n\nSee `lazy-seq` macro from init-macros.fnl for more convenient usage.") end)
  local function list(...)
    local args = table_pack(...)
    local l = empty_cons
    for i = args.n, 1, -1 do
      l = cons(args[i], l)
    end
    return l
  end
  pcall(function() require("fennel").metadata:setall(list, "fnl/arglist", {"&", "args"}, "fnl/docstring", "Create eager sequence of provided `args'.\n\n# Examples\n\n``` fennel\n(local l (list 1 2 3 4 5))\n(assert-eq [1 2 3 4 5] l)\n```") end)
  local function spread(arglist)
    local arglist0 = seq(arglist)
    if (nil == arglist0) then
      return nil
    elseif (nil == next(arglist0)) then
      return seq(first(arglist0))
    elseif "else" then
      return cons(first(arglist0), spread(next(arglist0)))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(spread, "fnl/arglist", {"arglist"}) end)
  local function list_2a(...)
    local _44_, _45_, _46_, _47_, _48_ = select("#", ...), ...
    if ((_44_ == 1) and true) then
      local _3fargs = _45_
      return seq(_3fargs)
    elseif ((_44_ == 2) and true and true) then
      local _3fa = _45_
      local _3fargs = _46_
      return cons(_3fa, seq(_3fargs))
    elseif ((_44_ == 3) and true and true and true) then
      local _3fa = _45_
      local _3fb = _46_
      local _3fargs = _47_
      return cons(_3fa, cons(_3fb, seq(_3fargs)))
    elseif ((_44_ == 4) and true and true and true and true) then
      local _3fa = _45_
      local _3fb = _46_
      local _3fc = _47_
      local _3fargs = _48_
      return cons(_3fa, cons(_3fb, cons(_3fc, seq(_3fargs))))
    elseif true then
      local _ = _44_
      return spread(list(...))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(list_2a, "fnl/arglist", {"&", "args"}, "fnl/docstring", "Creates a new sequence containing the `args' prepended to the rest,\nthe last of which will be treated as a sequence.\n\n# Examples\n\n``` fennel\n(local l (list* 1 2 3 [4 5]))\n(assert-eq [1 2 3 4 5] l)\n```") end)
  local function kind(t)
    local _50_ = type(t)
    if (_50_ == "table") then
      local len = length_2a(t)
      local nxt, t_2a, k = pairs_2a(t)
      local function _51_()
        if (len == 0) then
          return k
        else
          return len
        end
      end
      if (nil ~= nxt(t_2a, _51_())) then
        return "assoc"
      elseif (len > 0) then
        return "seq"
      else
        return "empty"
      end
    elseif (_50_ == "string") then
      local len
      if utf8 then
        len = utf8.len(t)
      else
        len = #t
      end
      if (len > 0) then
        return "string"
      else
        return "empty"
      end
    elseif true then
      local _ = _50_
      return "else"
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(kind, "fnl/arglist", {"t"}) end)
  local function rseq(rev)
    local _56_ = gettype(rev)
    if (_56_ == "table") then
      local _57_ = kind(rev)
      if (_57_ == "seq") then
        local function wrap(nxt, t, i)
          local i0, v = nxt(t, i)
          if (nil ~= i0) then
            local function _58_()
              return wrap(nxt, t, i0)
            end
            return cons(v, lazy_seq(_58_))
          else
            return empty_cons
          end
        end
        pcall(function() require("fennel").metadata:setall(wrap, "fnl/arglist", {"nxt", "t", "i"}) end)
        return wrap(rev_ipairs(rev))
      elseif (_57_ == "empty") then
        return nil
      elseif true then
        local _ = _57_
        return error("can't create an rseq from a non-sequential table")
      else
        return nil
      end
    elseif true then
      local _ = _56_
      return error(("can't create an rseq from a " .. _))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(rseq, "fnl/arglist", {"rev"}, "fnl/docstring", "Returns, in possibly-constant time, a seq of the items in `rev` in reverse order.\nInput must be traversable with `ipairs`.  Doesn't work in constant\ntime if `rev` implements a linear-time `__len` metamethod, or invoking\nLua `#` operator on `rev` takes linar time.  If `t` is empty returns\n`nil`.\n\n# Examples\n\n``` fennel\n(local v [1 2 3])\n(local r (rseq v))\n\n(assert-eq (reverse v) r)\n```") end)
  local function _62_(t)
    local _63_ = kind(t)
    if (_63_ == "assoc") then
      local function wrap(nxt, t0, k)
        local k0, v = nxt(t0, k)
        if (nil ~= k0) then
          local function _64_()
            return wrap(nxt, t0, k0)
          end
          return cons({k0, v}, lazy_seq(_64_))
        else
          return empty_cons
        end
      end
      pcall(function() require("fennel").metadata:setall(wrap, "fnl/arglist", {"nxt", "t", "k"}) end)
      return wrap(pairs_2a(t))
    elseif (_63_ == "seq") then
      local function wrap(nxt, t0, i)
        local i0, v = nxt(t0, i)
        if (nil ~= i0) then
          local function _66_()
            return wrap(nxt, t0, i0)
          end
          return cons(v, lazy_seq(_66_))
        else
          return empty_cons
        end
      end
      pcall(function() require("fennel").metadata:setall(wrap, "fnl/arglist", {"nxt", "t", "i"}) end)
      return wrap(ipairs_2a(t))
    elseif (_63_ == "string") then
      local char
      if utf8 then
        char = utf8.char
      else
        char = string.char
      end
      local function wrap(nxt, t0, i)
        local i0, v = nxt(t0, i)
        if (nil ~= i0) then
          local function _69_()
            return wrap(nxt, t0, i0)
          end
          return cons(char(v), lazy_seq(_69_))
        else
          return empty_cons
        end
      end
      pcall(function() require("fennel").metadata:setall(wrap, "fnl/arglist", {"nxt", "t", "i"}) end)
      local function _71_()
        if utf8 then
          return utf8.codes(t)
        else
          return ipairs_2a({string.byte(t, 1, #t)})
        end
      end
      return wrap(_71_())
    elseif (_63_ == "empty") then
      return nil
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(_62_, "fnl/arglist", {"t"}) end)
  cons_iter = _62_
  local function every_3f(pred, coll)
    local _73_ = seq(coll)
    if (nil ~= _73_) then
      local s = _73_
      if pred(first(s)) then
        local _74_ = next(s)
        if (nil ~= _74_) then
          local r = _74_
          return every_3f(pred, r)
        elseif true then
          local _ = _74_
          return true
        else
          return nil
        end
      else
        return false
      end
    elseif true then
      local _ = _73_
      return false
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(every_3f, "fnl/arglist", {"pred", "coll"}, "fnl/docstring", "Check if `pred` is true for every element of a sequence `coll`.") end)
  local function some_3f(pred, coll)
    local _78_ = seq(coll)
    if (nil ~= _78_) then
      local s = _78_
      local function _79_()
        local _80_ = next(s)
        if (nil ~= _80_) then
          local r = _80_
          return some_3f(pred, r)
        elseif true then
          local _ = _80_
          return nil
        else
          return nil
        end
      end
      return (pred(first(s)) or _79_())
    elseif true then
      local _ = _78_
      return nil
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(some_3f, "fnl/arglist", {"pred", "coll"}, "fnl/docstring", "Check if `pred` returns logical true for any element of a sequence\n`coll`.") end)
  local function pack(s)
    local res = {}
    local n = 0
    do
      local _83_ = seq(s)
      if (nil ~= _83_) then
        local s_2a = _83_
        for _, v in pairs_2a(s_2a) do
          n = (n + 1)
          do end (res)[n] = v
        end
      else
      end
    end
    res["n"] = n
    return res
  end
  pcall(function() require("fennel").metadata:setall(pack, "fnl/arglist", {"s"}, "fnl/docstring", "Pack sequence into sequential table with size indication.") end)
  local function count(s)
    local _85_ = seq(s)
    if (nil ~= _85_) then
      local s_2a = _85_
      return length_2a(s_2a)
    elseif true then
      local _ = _85_
      return 0
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(count, "fnl/arglist", {"s"}, "fnl/docstring", "Count amount of elements in the sequence.") end)
  local function unpack(s)
    local t = pack(s)
    return table_unpack(t, 1, t.n)
  end
  pcall(function() require("fennel").metadata:setall(unpack, "fnl/arglist", {"s"}, "fnl/docstring", "Unpack sequence items to multiple values.") end)
  local function concat(...)
    local _87_ = select("#", ...)
    if (_87_ == 0) then
      return empty_cons
    elseif (_87_ == 1) then
      local x = ...
      local function _88_()
        return x
      end
      return lazy_seq(_88_)
    elseif (_87_ == 2) then
      local x, y = ...
      local function _89_()
        local _90_ = seq(x)
        if (nil ~= _90_) then
          local s = _90_
          return cons(first(s), concat(rest(s), y))
        elseif (_90_ == nil) then
          return y
        else
          return nil
        end
      end
      return lazy_seq(_89_)
    elseif true then
      local _ = _87_
      local function _94_(...)
        local _92_, _93_ = ...
        return _92_, _93_
      end
      return concat(concat(_94_(...)), select(3, ...))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(concat, "fnl/arglist", {"([x])", "([x y])", "([x y & zs])"}, "fnl/docstring", "Return a lazy sequence of concatenated sequences.") end)
  local function reverse(s)
    local function helper(s0, res)
      local _96_ = seq(s0)
      if (nil ~= _96_) then
        local s_2a = _96_
        return helper(rest(s_2a), cons(first(s_2a), res))
      elseif true then
        local _ = _96_
        return res
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(helper, "fnl/arglist", {"s", "res"}) end)
    return helper(s, empty_cons)
  end
  pcall(function() require("fennel").metadata:setall(reverse, "fnl/arglist", {"s"}, "fnl/docstring", "Returns an eager reversed sequence.") end)
  local function map(f, ...)
    local _98_ = select("#", ...)
    if (_98_ == 0) then
      return nil
    elseif (_98_ == 1) then
      local col = ...
      local function _99_()
        local _100_ = seq(col)
        if (nil ~= _100_) then
          local x = _100_
          return cons(f(first(x)), map(f, seq(rest(x))))
        elseif true then
          local _ = _100_
          return nil
        else
          return nil
        end
      end
      return lazy_seq(_99_)
    elseif (_98_ == 2) then
      local s1, s2 = ...
      local function _102_()
        local s10 = seq(s1)
        local s20 = seq(s2)
        if (s10 and s20) then
          return cons(f(first(s10), first(s20)), map(f, rest(s10), rest(s20)))
        else
          return nil
        end
      end
      return lazy_seq(_102_)
    elseif (_98_ == 3) then
      local s1, s2, s3 = ...
      local function _104_()
        local s10 = seq(s1)
        local s20 = seq(s2)
        local s30 = seq(s3)
        if (s10 and s20 and s30) then
          return cons(f(first(s10), first(s20), first(s30)), map(f, rest(s10), rest(s20), rest(s30)))
        else
          return nil
        end
      end
      return lazy_seq(_104_)
    elseif true then
      local _ = _98_
      local s = list(...)
      local function _106_()
        local function _107_(_2410)
          return (nil ~= seq(_2410))
        end
        if every_3f(_107_, s) then
          return cons(f(unpack(map(first, s))), map(f, unpack(map(rest, s))))
        else
          return nil
        end
      end
      return lazy_seq(_106_)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(map, "fnl/arglist", {"f", "..."}, "fnl/docstring", "Map function `f` over every element of a collection `col`.\n`f` should accept as many arguments as there are collections supplied to `map`.\nReturns a lazy sequence.\n\n# Examples\n\n```fennel\n(map #(+ $ 1) [1 2 3]) ;; => @seq(2 3 4)\n(map #(+ $1 $2) [1 2 3] [4 5 6]) ;; => @seq(5 7 9)\n(local res (map #(+ $ 1) [:a :b :c])) ;; will raise an error only when realized\n```") end)
  local function map_indexed(f, coll)
    local mapi
    local function mapi0(idx, coll0)
      local function _110_()
        local _111_ = seq(coll0)
        if (nil ~= _111_) then
          local s = _111_
          return cons(f(idx, first(s)), mapi0((idx + 1), rest(s)))
        elseif true then
          local _ = _111_
          return nil
        else
          return nil
        end
      end
      return lazy_seq(_110_)
    end
    pcall(function() require("fennel").metadata:setall(mapi0, "fnl/arglist", {"idx", "coll"}) end)
    mapi = mapi0
    return mapi(1, coll)
  end
  pcall(function() require("fennel").metadata:setall(map_indexed, "fnl/arglist", {"f", "coll"}, "fnl/docstring", "Returns a lazy sequence consisting of the result of applying `f` to 1\nand the first item of `coll`, followed by applying `f` to 2 and the second\nitem in `coll`, etc, until `coll` is exhausted.") end)
  local function mapcat(f, ...)
    local step
    local function step0(colls)
      local function _113_()
        local _114_ = seq(colls)
        if (nil ~= _114_) then
          local s = _114_
          local c = first(s)
          return concat(c, step0(rest(colls)))
        elseif true then
          local _ = _114_
          return nil
        else
          return nil
        end
      end
      return lazy_seq(_113_)
    end
    pcall(function() require("fennel").metadata:setall(step0, "fnl/arglist", {"colls"}) end)
    step = step0
    return step(map(f, ...))
  end
  pcall(function() require("fennel").metadata:setall(mapcat, "fnl/arglist", {"f", "..."}, "fnl/docstring", "Apply `concat` to the result of calling `map` with `f` and\ncollections.") end)
  local function take(n, coll)
    local function _116_()
      if (n > 0) then
        local _117_ = seq(coll)
        if (nil ~= _117_) then
          local s = _117_
          return cons(first(s), take((n - 1), rest(s)))
        elseif true then
          local _ = _117_
          return nil
        else
          return nil
        end
      else
        return nil
      end
    end
    return lazy_seq(_116_)
  end
  pcall(function() require("fennel").metadata:setall(take, "fnl/arglist", {"n", "coll"}, "fnl/docstring", "Take `n` elements from the collection `coll`.\nReturns a lazy sequence of specified amount of elements.\n\n# Examples\n\nTake 10 element from a sequential table\n\n```fennel\n(take 10 [1 2 3]) ;=> @seq(1 2 3)\n(take 5 [1 2 3 4 5 6 7 8 9 10]) ;=> @seq(1 2 3 4 5)\n```") end)
  local function take_while(pred, coll)
    local function _120_()
      local _121_ = seq(coll)
      if (nil ~= _121_) then
        local s = _121_
        local v = first(s)
        if pred(v) then
          return cons(v, take_while(pred, rest(s)))
        else
          return nil
        end
      elseif true then
        local _ = _121_
        return nil
      else
        return nil
      end
    end
    return lazy_seq(_120_)
  end
  pcall(function() require("fennel").metadata:setall(take_while, "fnl/arglist", {"pred", "coll"}, "fnl/docstring", "Take the elements from the collection `coll` until `pred` returns logical\nfalse for any of the elemnts.  Returns a lazy sequence.") end)
  local function _124_(n, coll)
    local step
    local function step0(n0, coll0)
      local s = seq(coll0)
      if ((n0 > 0) and s) then
        return step0((n0 - 1), rest(s))
      else
        return s
      end
    end
    pcall(function() require("fennel").metadata:setall(step0, "fnl/arglist", {"n", "coll"}) end)
    step = step0
    local function _126_()
      return step(n, coll)
    end
    return lazy_seq(_126_)
  end
  pcall(function() require("fennel").metadata:setall(_124_, "fnl/arglist", {"n", "coll"}, "fnl/docstring", "Drop `n` elements from collection `coll`, returning a lazy sequence\nof remaining elements.") end)
  drop = _124_
  local function drop_while(pred, coll)
    local step
    local function step0(pred0, coll0)
      local s = seq(coll0)
      if (s and pred0(first(s))) then
        return step0(pred0, rest(s))
      else
        return s
      end
    end
    pcall(function() require("fennel").metadata:setall(step0, "fnl/arglist", {"pred", "coll"}) end)
    step = step0
    local function _128_()
      return step(pred, coll)
    end
    return lazy_seq(_128_)
  end
  pcall(function() require("fennel").metadata:setall(drop_while, "fnl/arglist", {"pred", "coll"}, "fnl/docstring", "Drop the elements from the collection `coll` until `pred` returns logical\nfalse for any of the elemnts.  Returns a lazy sequence.") end)
  local function drop_last(...)
    local _129_ = select("#", ...)
    if (_129_ == 0) then
      return empty_cons
    elseif (_129_ == 1) then
      return drop_last(1, ...)
    elseif true then
      local _ = _129_
      local n, coll = ...
      local function _130_(x)
        return x
      end
      pcall(function() require("fennel").metadata:setall(_130_, "fnl/arglist", {"x"}) end)
      return map(_130_, coll, drop(n, coll))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(drop_last, "fnl/arglist", {"([])", "([coll])", "([n coll])"}, "fnl/docstring", "Return a lazy sequence from `coll` without last `n` elements.") end)
  local function take_last(n, coll)
    local function loop(s, lead)
      if lead then
        return loop(next(s), next(lead))
      else
        return s
      end
    end
    pcall(function() require("fennel").metadata:setall(loop, "fnl/arglist", {"s", "lead"}) end)
    return loop(seq(coll), seq(drop(n, coll)))
  end
  pcall(function() require("fennel").metadata:setall(take_last, "fnl/arglist", {"n", "coll"}, "fnl/docstring", "Return a sequence of last `n` elements of the `coll`.") end)
  local function take_nth(n, coll)
    local function _133_()
      local _134_ = seq(coll)
      if (nil ~= _134_) then
        local s = _134_
        return cons(first(s), take_nth(n, drop(n, s)))
      else
        return nil
      end
    end
    return lazy_seq(_133_)
  end
  pcall(function() require("fennel").metadata:setall(take_nth, "fnl/arglist", {"n", "coll"}, "fnl/docstring", "Return a lazy sequence of every `n` item in `coll`.") end)
  local function split_at(n, coll)
    return {take(n, coll), drop(n, coll)}
  end
  pcall(function() require("fennel").metadata:setall(split_at, "fnl/arglist", {"n", "coll"}, "fnl/docstring", "Return a table with sequence `coll` being split at `n`") end)
  local function split_with(pred, coll)
    return {take_while(pred, coll), drop_while(pred, coll)}
  end
  pcall(function() require("fennel").metadata:setall(split_with, "fnl/arglist", {"pred", "coll"}, "fnl/docstring", "Return a table with sequence `coll` being split with `pred`") end)
  local function filter(pred, coll)
    local function _136_()
      local _137_ = seq(coll)
      if (nil ~= _137_) then
        local s = _137_
        local x = first(s)
        local r = rest(s)
        if pred(x) then
          return cons(x, filter(pred, r))
        else
          return filter(pred, r)
        end
      elseif true then
        local _ = _137_
        return nil
      else
        return nil
      end
    end
    return lazy_seq(_136_)
  end
  pcall(function() require("fennel").metadata:setall(filter, "fnl/arglist", {"pred", "coll"}, "fnl/docstring", "Returns a lazy sequence of the items in the `coll` for which `pred`\nreturns logical true.") end)
  local function keep(f, coll)
    local function _140_()
      local _141_ = seq(coll)
      if (nil ~= _141_) then
        local s = _141_
        local _142_ = f(first(s))
        if (nil ~= _142_) then
          local x = _142_
          return cons(x, keep(f, rest(s)))
        elseif (_142_ == nil) then
          return keep(f, rest(s))
        else
          return nil
        end
      elseif true then
        local _ = _141_
        return nil
      else
        return nil
      end
    end
    return lazy_seq(_140_)
  end
  pcall(function() require("fennel").metadata:setall(keep, "fnl/arglist", {"f", "coll"}, "fnl/docstring", "Returns a lazy sequence of the non-nil results of calling `f` on the\nitems of the `coll`.") end)
  local function keep_indexed(f, coll)
    local keepi
    local function keepi0(idx, coll0)
      local function _145_()
        local _146_ = seq(coll0)
        if (nil ~= _146_) then
          local s = _146_
          local x = f(idx, first(s))
          if (nil == x) then
            return keepi0((1 + idx), rest(s))
          else
            return cons(x, keepi0((1 + idx), rest(s)))
          end
        else
          return nil
        end
      end
      return lazy_seq(_145_)
    end
    pcall(function() require("fennel").metadata:setall(keepi0, "fnl/arglist", {"idx", "coll"}) end)
    keepi = keepi0
    return keepi(1, coll)
  end
  pcall(function() require("fennel").metadata:setall(keep_indexed, "fnl/arglist", {"f", "coll"}, "fnl/docstring", "Returns a lazy sequence of the non-nil results of (f index item) in\nthe `coll`.  Note, this means false return values will be included.\n`f` must be free of side-effects.") end)
  local function remove(pred, coll)
    local function _149_(_241)
      return not pred(_241)
    end
    return filter(_149_, coll)
  end
  pcall(function() require("fennel").metadata:setall(remove, "fnl/arglist", {"pred", "coll"}, "fnl/docstring", "Returns a lazy sequence of the items in the `coll` without elements\nfor wich `pred` returns logical true.") end)
  local function cycle(coll)
    local function _150_()
      return concat(seq(coll), cycle(coll))
    end
    return lazy_seq(_150_)
  end
  pcall(function() require("fennel").metadata:setall(cycle, "fnl/arglist", {"coll"}, "fnl/docstring", "Create a lazy infinite sequence of repetitions of the items in the\n`coll`.") end)
  local function _repeat(x)
    local function step(x0)
      local function _151_()
        return cons(x0, step(x0))
      end
      return lazy_seq(_151_)
    end
    pcall(function() require("fennel").metadata:setall(step, "fnl/arglist", {"x"}) end)
    return step(x)
  end
  pcall(function() require("fennel").metadata:setall(_repeat, "fnl/arglist", {"x"}, "fnl/docstring", "Takes a value `x` and returns an infinite lazy sequence of this value.\n\n# Examples\n\n``` fennel\n(assert-eq 10 (accumulate [res 0\n                           _ x (pairs (take 10 (repeat 1)))]\n                (+ res x)))\n```") end)
  local function repeatedly(f, ...)
    local args = table_pack(...)
    local f0
    local function _152_()
      return f(table_unpack(args, 1, args.n))
    end
    pcall(function() require("fennel").metadata:setall(_152_, "fnl/arglist", {}) end)
    f0 = _152_
    local function step(f1)
      local function _153_()
        return cons(f1(), step(f1))
      end
      return lazy_seq(_153_)
    end
    pcall(function() require("fennel").metadata:setall(step, "fnl/arglist", {"f"}) end)
    return step(f0)
  end
  pcall(function() require("fennel").metadata:setall(repeatedly, "fnl/arglist", {"f", "..."}, "fnl/docstring", "Takes a function `f` and returns an infinite lazy sequence of\nfunction applications.  Rest arguments are passed to the function.") end)
  local function iterate(f, x)
    local x_2a = f(x)
    local function _154_()
      return iterate(f, x_2a)
    end
    return cons(x, lazy_seq(_154_))
  end
  pcall(function() require("fennel").metadata:setall(iterate, "fnl/arglist", {"f", "x"}, "fnl/docstring", "Returns an infinete lazy sequence of x, (f x), (f (f x)) etc.") end)
  local function nthnext(coll, n)
    local function loop(n0, xs)
      local _155_ = xs
      local function _156_()
        local xs_2a = _155_
        return (n0 > 0)
      end
      if ((nil ~= _155_) and _156_()) then
        local xs_2a = _155_
        return loop((n0 - 1), next(xs_2a))
      elseif true then
        local _ = _155_
        return xs
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(loop, "fnl/arglist", {"n", "xs"}) end)
    return loop(n, seq(coll))
  end
  pcall(function() require("fennel").metadata:setall(nthnext, "fnl/arglist", {"coll", "n"}, "fnl/docstring", "Returns the nth next of `coll`, (seq coll) when `n` is 0.") end)
  local function nthrest(coll, n)
    local function loop(n0, xs)
      local _158_ = seq(xs)
      local function _159_()
        local xs_2a = _158_
        return (n0 > 0)
      end
      if ((nil ~= _158_) and _159_()) then
        local xs_2a = _158_
        return loop((n0 - 1), rest(xs_2a))
      elseif true then
        local _ = _158_
        return xs
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(loop, "fnl/arglist", {"n", "xs"}) end)
    return loop(n, coll)
  end
  pcall(function() require("fennel").metadata:setall(nthrest, "fnl/arglist", {"coll", "n"}, "fnl/docstring", "Returns the nth rest of `coll`, `coll` when `n` is 0.") end)
  local function dorun(s)
    local _161_ = seq(s)
    if (nil ~= _161_) then
      local s_2a = _161_
      return dorun(next(s_2a))
    elseif true then
      local _ = _161_
      return nil
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(dorun, "fnl/arglist", {"s"}, "fnl/docstring", "Realize whole sequence for side effects.\n\nWalks whole sequence, realizing each cell.  Use at your own risk on\ninfinite sequences.") end)
  local function doall(s)
    dorun(s)
    return s
  end
  pcall(function() require("fennel").metadata:setall(doall, "fnl/arglist", {"s"}, "fnl/docstring", "Realize whole lazy sequence.\n\nWalks whole sequence, realizing each cell.  Use at your own risk on\ninfinite sequences.") end)
  local function partition(...)
    local _163_ = select("#", ...)
    if (_163_ == 2) then
      local n, coll = ...
      return partition(n, n, coll)
    elseif (_163_ == 3) then
      local n, step, coll = ...
      local function _164_()
        local _165_ = seq(coll)
        if (nil ~= _165_) then
          local s = _165_
          local p = take(n, s)
          if (n == length_2a(p)) then
            return cons(p, partition(n, step, nthrest(s, step)))
          else
            return nil
          end
        elseif true then
          local _ = _165_
          return nil
        else
          return nil
        end
      end
      return lazy_seq(_164_)
    elseif (_163_ == 4) then
      local n, step, pad, coll = ...
      local function _168_()
        local _169_ = seq(coll)
        if (nil ~= _169_) then
          local s = _169_
          local p = take(n, s)
          if (n == length_2a(p)) then
            return cons(p, partition(n, step, pad, nthrest(s, step)))
          else
            return list(take(n, concat(p, pad)))
          end
        elseif true then
          local _ = _169_
          return nil
        else
          return nil
        end
      end
      return lazy_seq(_168_)
    elseif true then
      local _ = _163_
      return error("wrong amount arguments to 'partition'")
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(partition, "fnl/arglist", {"([n coll])", "([n step coll])", "([n step pad coll])"}, "fnl/docstring", "Given a `coll' returns a lazy sequence of lists of `n` items each, at\noffsets `step`apart. If `step` is not supplied, defaults to `n`,\ni.e. the partitions do not overlap. If a `pad` collection is supplied,\nuse its elements as necessary to complete last partition upto `n`\nitems. In case there are not enough padding elements, return a\npartition with less than `n` items.") end)
  local function partition_by(f, coll)
    local function _173_()
      local _174_ = seq(coll)
      if (nil ~= _174_) then
        local s = _174_
        local v = first(s)
        local fv = f(v)
        local run
        local function _175_(_2410)
          return (fv == f(_2410))
        end
        run = cons(v, take_while(_175_, next(s)))
        local function _176_()
          return drop(length_2a(run), s)
        end
        return cons(run, partition_by(f, lazy_seq(_176_)))
      else
        return nil
      end
    end
    return lazy_seq(_173_)
  end
  pcall(function() require("fennel").metadata:setall(partition_by, "fnl/arglist", {"f", "coll"}, "fnl/docstring", "Applies `f` to each value in `coll`, splitting it each time `f`\n   returns a new value.  Returns a lazy seq of partitions.") end)
  local function partition_all(...)
    local _178_ = select("#", ...)
    if (_178_ == 2) then
      local n, coll = ...
      return partition_all(n, n, coll)
    elseif (_178_ == 3) then
      local n, step, coll = ...
      local function _179_()
        local _180_ = seq(coll)
        if (nil ~= _180_) then
          local s = _180_
          local p = take(n, s)
          return cons(p, partition_all(n, step, nthrest(s, step)))
        elseif true then
          local _ = _180_
          return nil
        else
          return nil
        end
      end
      return lazy_seq(_179_)
    elseif true then
      local _ = _178_
      return error("wrong amount arguments to 'partition-all'")
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(partition_all, "fnl/arglist", {"([n coll])", "([n step coll])"}, "fnl/docstring", "Given a `coll' returns a lazy sequence of lists like `partition`, but\nmay include partitions with fewer than `n' items at the end. Optional\n`step' argument is accepted similarly to `partition`.") end)
  local function reductions(...)
    local _183_ = select("#", ...)
    if (_183_ == 2) then
      local f, coll = ...
      local function _184_()
        local _185_ = seq(coll)
        if (nil ~= _185_) then
          local s = _185_
          return reductions(f, first(s), rest(s))
        elseif true then
          local _ = _185_
          return list(f())
        else
          return nil
        end
      end
      return lazy_seq(_184_)
    elseif (_183_ == 3) then
      local f, init, coll = ...
      local function _187_()
        local _188_ = seq(coll)
        if (nil ~= _188_) then
          local s = _188_
          return reductions(f, f(init, first(s)), rest(s))
        else
          return nil
        end
      end
      return cons(init, lazy_seq(_187_))
    elseif true then
      local _ = _183_
      return error("wrong amount arguments to 'reductions'")
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(reductions, "fnl/arglist", {"([f coll])", "([f init coll])"}, "fnl/docstring", "Returns a lazy seq of the intermediate values of the reduction (as\nper reduce) of `coll` by `f`, starting with `init`.") end)
  local function contains_3f(coll, elt)
    local _191_ = gettype(coll)
    if (_191_ == "table") then
      local _192_ = kind(coll)
      if (_192_ == "seq") then
        local res = false
        for _, v in ipairs_2a(coll) do
          if res then break end
          if (elt == v) then
            res = true
          else
            res = false
          end
        end
        return res
      elseif (_192_ == "assoc") then
        if coll[elt] then
          return true
        else
          return false
        end
      else
        return nil
      end
    elseif true then
      local _ = _191_
      local function loop(coll0)
        local _196_ = seq(coll0)
        if (nil ~= _196_) then
          local s = _196_
          if (elt == first(s)) then
            return true
          else
            return loop(rest(s))
          end
        elseif (_196_ == nil) then
          return false
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(loop, "fnl/arglist", {"coll"}) end)
      return loop(coll)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(contains_3f, "fnl/arglist", {"coll", "elt"}, "fnl/docstring", "Test if `elt` is in the `coll`.  May be a linear search depending on the type of the collection.") end)
  local function distinct(coll)
    local function step(xs, seen)
      local loop
      local function loop0(_200_, seen0)
        local _arg_201_ = _200_
        local f = _arg_201_[1]
        local xs0 = _arg_201_
        local _202_ = seq(xs0)
        if (nil ~= _202_) then
          local s = _202_
          if (seen0)[f] then
            return loop0(rest(s), seen0)
          else
            local function _203_()
              seen0[f] = true
              return seen0
            end
            return cons(f, step(rest(s), _203_()))
          end
        elseif true then
          local _ = _202_
          return nil
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(loop0, "fnl/arglist", {"[f &as xs]", "seen"}) end)
      loop = loop0
      local function _206_()
        return loop(xs, seen)
      end
      return lazy_seq(_206_)
    end
    pcall(function() require("fennel").metadata:setall(step, "fnl/arglist", {"xs", "seen"}) end)
    return step(coll, {})
  end
  pcall(function() require("fennel").metadata:setall(distinct, "fnl/arglist", {"coll"}, "fnl/docstring", "Returns a lazy sequence of the elements of the `coll` without\nduplicates.  Comparison is done by equality.") end)
  local function inf_range(x, step)
    local function _207_()
      return cons(x, inf_range((x + step), step))
    end
    return lazy_seq(_207_)
  end
  pcall(function() require("fennel").metadata:setall(inf_range, "fnl/arglist", {"x", "step"}) end)
  local function fix_range(x, _end, step)
    local function _208_()
      if (((step >= 0) and (x < _end)) or ((step < 0) and (x > _end))) then
        return cons(x, fix_range((x + step), _end, step))
      elseif ((step == 0) and (x ~= _end)) then
        return cons(x, fix_range(x, _end, step))
      else
        return nil
      end
    end
    return lazy_seq(_208_)
  end
  pcall(function() require("fennel").metadata:setall(fix_range, "fnl/arglist", {"x", "end", "step"}) end)
  local function range(...)
    local _210_ = select("#", ...)
    if (_210_ == 0) then
      return inf_range(0, 1)
    elseif (_210_ == 1) then
      local _end = ...
      return fix_range(0, _end, 1)
    elseif (_210_ == 2) then
      local x, _end = ...
      return fix_range(x, _end, 1)
    elseif true then
      local _ = _210_
      return fix_range(...)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(range, "fnl/arglist", {"([])", "([end])", "([start end])", "([start end step])"}, "fnl/docstring", "Create a possibly infinite sequence of numbers.\n\nIf `end' argument is specified, returns a finite sequence from 0 up to\nthis argument.  If two arguments `start' and `end' were specified,\nreturns a finite sequence from lower to, but not included, upper\nbound.  A third argument `step' provides a step interval.\n\nIf no arguments were specified, returns an infinite sequence starting at 0.\n\n# Examples\n\nVarious ranges:\n\n```fennel\n(range 10) ;; => @seq(0 1 2 3 4 5 6 7 8 9)\n(range 4 8) ;; => @seq(4 5 6 7)\n(range 0 -5 -2) ;; => @seq(0 -2 -4)\n(take 10 (range)) ;; => @seq(0 1 2 3 4 5 6 7 8 9)\n```") end)
  local function realized_3f(s)
    local _212_ = gettype(s)
    if (_212_ == "lazy-cons") then
      return false
    elseif (_212_ == "empty-cons") then
      return true
    elseif (_212_ == "cons") then
      return true
    elseif true then
      local _ = _212_
      return error(("expected a sequence, got: %s"):format(_))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(realized_3f, "fnl/arglist", {"s"}, "fnl/docstring", "Check if sequence's first element is realized.") end)
  local function line_seq(file)
    local next_line = file:lines()
    local function step(f)
      local line = f()
      if ("string" == type(line)) then
        local function _214_()
          return step(f)
        end
        return cons(line, lazy_seq(_214_))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(step, "fnl/arglist", {"f"}) end)
    return step(next_line)
  end
  pcall(function() require("fennel").metadata:setall(line_seq, "fnl/arglist", {"file"}, "fnl/docstring", "Accepts a `file` handle, and creates a lazy sequence of lines using\n`lines` metamethod.\n\n# Examples\n\nLazy sequence of file lines may seem similar to an iterator over a\nfile, but the main difference is that sequence can be shared onve\nrealized, and iterator can't.  Lazy sequence can be consumed in\niterator style with the `doseq` macro.\n\nBear in mind, that since the sequence is lazy it should be realized or\ntruncated before the file is closed:\n\n```fennel\n(let [lines (with-open [f (io.open \"init.fnl\" :r)]\n              (line-seq f))]\n  ;; this errors because only first line was realized, but the file\n  ;; was closed before the rest of lines were cached\n  (assert-not (pcall next lines)))\n```\n\nSequence is realized with `doall` before file was closed and can be shared:\n\n``` fennel\n(let [lines (with-open [f (io.open \"init.fnl\" :r)]\n              (doall (line-seq f)))]\n  (assert-is (pcall next lines)))\n```\n\nInfinite files can't be fully realized, but can be partially realized\nwith `take`:\n\n``` fennel\n(let [lines (with-open [f (io.open \"/dev/urandom\" :r)]\n              (doall (take 3 (line-seq f))))]\n  (assert-is (pcall next lines)))\n```") end)
  local function tree_seq(branch_3f, children, root)
    local function walk(node)
      local function _216_()
        local function _217_()
          if branch_3f(node) then
            return mapcat(walk, children(node))
          else
            return nil
          end
        end
        return cons(node, _217_())
      end
      return lazy_seq(_216_)
    end
    pcall(function() require("fennel").metadata:setall(walk, "fnl/arglist", {"node"}) end)
    return walk(root)
  end
  pcall(function() require("fennel").metadata:setall(tree_seq, "fnl/arglist", {"branch?", "children", "root"}, "fnl/docstring", "Returns a lazy sequence of the nodes in a tree, via a depth-first walk.\n\n`branch?` must be a function of one arg that returns true if passed a\nnode that can have children (but may not).  `children` must be a\nfunction of one arg that returns a sequence of the children.  Will\nonly be called on nodes for which `branch?` returns true.  `root` is\nthe root node of the tree.\n\n# Examples\n\nFor the given tree `[\"A\" [\"B\" [\"D\"] [\"E\"]] [\"C\" [\"F\"]]]`:\n\n        A\n       / \\\n      B   C\n     / \\   \\\n    D   E   F\n\nCalling `tree-seq` with `next' as the `branch?` and `rest' as the\n`children` returns a flat representation of a tree:\n\n``` fennel\n(assert-eq (map first (tree-seq next rest [\"A\" [\"B\" [\"D\"] [\"E\"]] [\"C\" [\"F\"]]]))\n           [\"A\" \"B\" \"D\" \"E\" \"C\" \"F\"])\n```") end)
  local function interleave(...)
    local _218_, _219_, _220_ = select("#", ...), ...
    if (_218_ == 0) then
      return empty_cons
    elseif ((_218_ == 1) and true) then
      local _3fs = _219_
      local function _221_()
        return _3fs
      end
      return lazy_seq(_221_)
    elseif ((_218_ == 2) and true and true) then
      local _3fs1 = _219_
      local _3fs2 = _220_
      local function _222_()
        local s1 = seq(_3fs1)
        local s2 = seq(_3fs2)
        if (s1 and s2) then
          return cons(first(s1), cons(first(s2), interleave(rest(s1), rest(s2))))
        else
          return nil
        end
      end
      return lazy_seq(_222_)
    elseif true then
      local _ = _218_
      local cols = list(...)
      local function _224_()
        local seqs = map(seq, cols)
        local function _225_(_2410)
          return (nil ~= seq(_2410))
        end
        if every_3f(_225_, seqs) then
          return concat(map(first, seqs), interleave(unpack(map(rest, seqs))))
        else
          return nil
        end
      end
      return lazy_seq(_224_)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(interleave, "fnl/arglist", {"([])", "([s])", "([s1 s2])", "([s1 s2 & ss])"}, "fnl/docstring", "Returns a lazy sequence of the first item in each sequence, then the\nsecond one, until any sequence exhausts.") end)
  local function interpose(separator, coll)
    return drop(1, interleave(_repeat(separator), coll))
  end
  pcall(function() require("fennel").metadata:setall(interpose, "fnl/arglist", {"separator", "coll"}, "fnl/docstring", "Returns a lazy sequence of the elements of `coll` separated by `separator`.") end)
  local function keys(t)
    assert(("assoc" == kind(t)), "expected an associative table")
    local function _228_(_241)
      return (_241)[1]
    end
    return map(_228_, t)
  end
  pcall(function() require("fennel").metadata:setall(keys, "fnl/arglist", {"t"}, "fnl/docstring", "Return a sequence of keys in table `t`.") end)
  local function vals(t)
    assert(("assoc" == kind(t)), "expected an associative table")
    local function _229_(_241)
      return (_241)[2]
    end
    return map(_229_, t)
  end
  pcall(function() require("fennel").metadata:setall(vals, "fnl/arglist", {"t"}, "fnl/docstring", "Return a sequence of values in table `t`.") end)
  local function zipmap(keys0, vals0)
    local t = {}
    local function loop(s1, s2)
      if (s1 and s2) then
        t[first(s1)] = first(s2)
        return loop(next(s1), next(s2))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(loop, "fnl/arglist", {"s1", "s2"}) end)
    loop(seq(keys0), seq(vals0))
    return t
  end
  pcall(function() require("fennel").metadata:setall(zipmap, "fnl/arglist", {"keys", "vals"}, "fnl/docstring", "Return an associative table with the `keys` mapped to the\ncorresponding `vals`.") end)
  local reduced_mt
  local function _231_(x, view, options, indent)
    return ("#<reduced: " .. view(x.value, options, (11 + indent)) .. ">")
  end
  pcall(function() require("fennel").metadata:setall(_231_, "fnl/arglist", {"x", "view", "options", "indent"}) end)
  local function _232_(_241)
    return ("reduced: " .. tostring(_241.value))
  end
  reduced_mt = {__fennelview = _231_, __name = "reduced", __tostring = _232_}
  local function reduced(value)
    return setmetatable({value = value}, reduced_mt)
  end
  pcall(function() require("fennel").metadata:setall(reduced, "fnl/arglist", {"value"}, "fnl/docstring", "Terminates the `reduce` early with a given `value`.\n\n# Examples\n\n``` fennel\n(assert-eq :NaN\n           (reduce (fn [acc x]\n                     (if (not= :number (type x))\n                         (reduced :NaN)\n                         (+ acc x)))\n                   [1 2 :3 4 5]))\n```") end)
  local function reduced_3f(x)
    local _233_ = getmetatable(x)
    if (_233_ == reduced_mt) then
      return true
    elseif true then
      local _ = _233_
      return false
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(reduced_3f, "fnl/arglist", {"x"}, "fnl/docstring", "Returns `true` if `x` is the result of a call to `reduced'") end)
  local function reduce(f, ...)
    local _235_, _236_, _237_ = select("#", ...), ...
    if (_235_ == 0) then
      return error("expected a collection")
    elseif ((_235_ == 1) and true) then
      local _3fcoll = _236_
      local _238_ = count(_3fcoll)
      if (_238_ == 0) then
        return f()
      elseif (_238_ == 1) then
        return first(_3fcoll)
      elseif true then
        local _ = _238_
        return reduce(f, first(_3fcoll), rest(_3fcoll))
      else
        return nil
      end
    elseif ((_235_ == 2) and true and true) then
      local _3fval = _236_
      local _3fcoll = _237_
      local _240_ = seq(_3fcoll)
      if (nil ~= _240_) then
        local coll = _240_
        local done_3f = false
        local res = _3fval
        for _, v in pairs_2a(coll) do
          if done_3f then break end
          local res0 = f(res, v)
          if rawequal(getmetatable(res0), reduced_mt) then
            done_3f = true
            res = res0.value
          else
            res = res0
          end
        end
        return res
      elseif true then
        local _ = _240_
        return _3fval
      else
        return nil
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(reduce, "fnl/arglist", {"([f coll])", "([f val coll])"}, "fnl/docstring", "`f` should be a function of 2 arguments. If `val` is not supplied,\nreturns the result of applying `f` to the first 2 items in `coll`,\nthen applying `f` to that result and the 3rd item, etc. If `coll`\ncontains no items, f must accept no arguments as well, and reduce\nreturns the result of calling `f` with no arguments.  If `coll` has\nonly 1 item, it is returned and `f` is not called.  If `val` is\nsupplied, returns the result of applying `f` to `val` and the first\nitem in `coll`, then applying `f` to that result and the 2nd item,\netc. If `coll` contains no items, returns `val` and `f` is not\ncalled. Early termination is supported via `reduced`.\n\n# Examples\n\n``` fennel\n(fn add [...]\n  \"Addition function with multiple arities.\"\n  (match (values (select \"#\" ...) ...)\n    (0) 0\n    (1 ?a) ?a\n    (2 ?a ?b) (+ ?a ?b)\n    (3 ?a ?b) (add (+ ?a ?b) (select 3 ...))))\n;; no initial value\n(assert-eq 10 (reduce add [1 2 3 4]))\n;; initial value\n(assert-eq 10 (reduce add 1 [2 3 4]))\n;; empty collection - function is called with 0 args\n(assert-eq 0 (reduce add []))\n(assert-eq 10.3 (reduce math.floor 10.3 []))\n;; collection with a single element doesn't call a function unless the\n;; initial value is supplied\n(assert-eq 10.3 (reduce math.floor [10.3]))\n(assert-eq 7 (reduce add 3 [4]))\n```") end)
  return {first = first, rest = rest, nthrest = nthrest, next = next, nthnext = nthnext, cons = cons, seq = seq, rseq = rseq, ["seq?"] = seq_3f, ["empty?"] = empty_3f, ["lazy-seq"] = lazy_seq, list = list, ["list*"] = list_2a, ["every?"] = every_3f, ["some?"] = some_3f, pack = pack, unpack = unpack, count = count, concat = concat, map = map, ["map-indexed"] = map_indexed, mapcat = mapcat, take = take, ["take-while"] = take_while, ["take-last"] = take_last, ["take-nth"] = take_nth, drop = drop, ["drop-while"] = drop_while, ["drop-last"] = drop_last, remove = remove, ["split-at"] = split_at, ["split-with"] = split_with, partition = partition, ["partition-by"] = partition_by, ["partition-all"] = partition_all, filter = filter, keep = keep, ["keep-indexed"] = keep_indexed, ["contains?"] = contains_3f, distinct = distinct, cycle = cycle, ["repeat"] = _repeat, repeatedly = repeatedly, reductions = reductions, iterate = iterate, range = range, ["realized?"] = realized_3f, dorun = dorun, doall = doall, ["line-seq"] = line_seq, ["tree-seq"] = tree_seq, reverse = reverse, interleave = interleave, interpose = interpose, keys = keys, vals = vals, zipmap = zipmap, reduce = reduce, reduced = reduced, ["reduced?"] = reduced_3f}
end
package.preload["itable"] = package.preload["itable"] or function(...)
  -- -*- buffer-read-only: t -*-
  local _local_1_ = table
  local t_2fsort = _local_1_["sort"]
  local t_2fconcat = _local_1_["concat"]
  local t_2fremove = _local_1_["remove"]
  local t_2fmove = _local_1_["move"]
  local t_2finsert = _local_1_["insert"]
  local t_2funpack = (table.unpack or _G.unpack)
  local t_2fpack
  local function _2_(...)
    local _3_ = {...}
    _3_["n"] = select("#", ...)
    return _3_
  end
  t_2fpack = _2_
  local function pairs_2a(t)
    local _5_
    do
      local _4_ = getmetatable(t)
      if ((_G.type(_4_) == "table") and (nil ~= (_4_).__pairs)) then
        local p = (_4_).__pairs
        _5_ = p
      elseif true then
        local _ = _4_
        _5_ = pairs
      else
        _5_ = nil
      end
    end
    return _5_(t)
  end
  local function ipairs_2a(t)
    local _10_
    do
      local _9_ = getmetatable(t)
      if ((_G.type(_9_) == "table") and (nil ~= (_9_).__ipairs)) then
        local i = (_9_).__ipairs
        _10_ = i
      elseif true then
        local _ = _9_
        _10_ = ipairs
      else
        _10_ = nil
      end
    end
    return _10_(t)
  end
  local function length_2a(t)
    local _15_
    do
      local _14_ = getmetatable(t)
      if ((_G.type(_14_) == "table") and (nil ~= (_14_).__len)) then
        local l = (_14_).__len
        _15_ = l
      elseif true then
        local _ = _14_
        local function _18_(...)
          return #...
        end
        _15_ = _18_
      else
        _15_ = nil
      end
    end
    return _15_(t)
  end
  local function copy(t)
    if t then
      local tbl_12_auto = {}
      for k, v in pairs_2a(t) do
        local _20_, _21_ = k, v
        if ((nil ~= _20_) and (nil ~= _21_)) then
          local k_13_auto = _20_
          local v_14_auto = _21_
          tbl_12_auto[k_13_auto] = v_14_auto
        else
        end
      end
      return tbl_12_auto
    else
      return nil
    end
  end
  local function eq(...)
    local _24_, _25_, _26_ = select("#", ...), ...
    local function _27_(...)
      return true
    end
    if ((_24_ == 0) and _27_(...)) then
      return true
    else
      local function _28_(...)
        return true
      end
      if ((_24_ == 1) and _28_(...)) then
        return true
      elseif ((_24_ == 2) and true and true) then
        local _3fa = _25_
        local _3fb = _26_
        if (_3fa == _3fb) then
          return true
        elseif (function(_29_,_30_,_31_) return (_29_ == _30_) and (_30_ == _31_) end)(type(_3fa),type(_3fb),"table") then
          local res, count_a, count_b = true, 0, 0
          for k, v in pairs_2a(_3fa) do
            if not res then break end
            local function _32_(...)
              local res0 = nil
              for k_2a, v0 in pairs_2a(_3fb) do
                if res0 then break end
                if eq(k_2a, k) then
                  res0 = v0
                else
                end
              end
              return res0
            end
            res = eq(v, _32_(...))
            count_a = (count_a + 1)
          end
          if res then
            for _, _0 in pairs_2a(_3fb) do
              count_b = (count_b + 1)
            end
            res = (count_a == count_b)
          else
          end
          return res
        else
          return false
        end
      elseif (true and true and true) then
        local _ = _24_
        local _3fa = _25_
        local _3fb = _26_
        return (eq(_3fa, _3fb) and eq(select(2, ...)))
      else
        return nil
      end
    end
  end
  local function deep_index(tbl, key)
    local res = nil
    for k, v in pairs_2a(tbl) do
      if res then break end
      if eq(k, key) then
        res = v
      else
        res = nil
      end
    end
    return res
  end
  local function deep_newindex(tbl, key, val)
    local done = false
    if ("table" == type(key)) then
      for k, _ in pairs_2a(tbl) do
        if done then break end
        if eq(k, key) then
          rawset(tbl, k, val)
          done = true
        else
        end
      end
    else
    end
    if not done then
      return rawset(tbl, key, val)
    else
      return nil
    end
  end
  local function immutable(t, opts)
    local t0
    if (opts and opts["fast-index?"]) then
      t0 = t
    else
      t0 = setmetatable(t, {__index = deep_index, __newindex = deep_newindex})
    end
    local len = length_2a(t0)
    local proxy = {}
    local __len
    local function _42_()
      return len
    end
    __len = _42_
    local __index
    local function _43_(_241, _242)
      return (t0)[_242]
    end
    __index = _43_
    local __newindex
    local function _44_()
      return error((tostring(proxy) .. " is immutable"), 2)
    end
    __newindex = _44_
    local __pairs
    local function _45_()
      local function _46_(_, k)
        return next(t0, k)
      end
      return _46_, nil, nil
    end
    __pairs = _45_
    local __ipairs
    local function _47_()
      local function _48_(_, k)
        return next(t0, k)
      end
      return _48_
    end
    __ipairs = _47_
    local __call
    local function _49_(_241, _242)
      return (t0)[_242]
    end
    __call = _49_
    local __fennelview
    local function _50_(_241, _242, _243, _244)
      return _242(t0, _243, _244)
    end
    __fennelview = _50_
    local __fennelrest
    local function _51_(_241, _242)
      return immutable({t_2funpack(t0, _242)})
    end
    __fennelrest = _51_
    return setmetatable(proxy, {__index = __index, __newindex = __newindex, __len = __len, __pairs = __pairs, __ipairs = __ipairs, __call = __call, __metatable = {__len = __len, __pairs = __pairs, __ipairs = __ipairs, __call = __call, __fennelrest = __fennelrest, __fennelview = __fennelview, ["itable/type"] = "immutable"}})
  end
  local function insert(t, ...)
    local t0 = copy(t)
    do
      local _52_, _53_, _54_ = select("#", ...), ...
      if (_52_ == 0) then
        error("wrong number of arguments to 'insert'")
      elseif ((_52_ == 1) and true) then
        local _3fv = _53_
        t_2finsert(t0, _3fv)
      elseif (true and true and true) then
        local _ = _52_
        local _3fk = _53_
        local _3fv = _54_
        t_2finsert(t0, _3fk, _3fv)
      else
      end
    end
    return immutable(t0)
  end
  local move
  if t_2fmove then
    local function _56_(src, start, _end, tgt, dest)
      local src0 = copy(src)
      local dest0 = copy(dest)
      return immutable(t_2fmove(src0, start, _end, tgt, dest0))
    end
    move = _56_
  else
    move = nil
  end
  local function pack(...)
    local function _59_(...)
      local _58_ = {...}
      _58_["n"] = select("#", ...)
      return _58_
    end
    return immutable(_59_(...))
  end
  local function remove(t, key)
    local t0 = copy(t)
    local v = t_2fremove(t0, key)
    return immutable(t0), v
  end
  local function concat(t, sep, start, _end, serializer, opts)
    local serializer0 = (serializer or tostring)
    local _60_
    do
      local tbl_15_auto = {}
      local i_16_auto = #tbl_15_auto
      for _, v in ipairs_2a(t) do
        local val_17_auto = serializer0(v, opts)
        if (nil ~= val_17_auto) then
          i_16_auto = (i_16_auto + 1)
          do end (tbl_15_auto)[i_16_auto] = val_17_auto
        else
        end
      end
      _60_ = tbl_15_auto
    end
    return t_2fconcat(_60_, sep, start, _end)
  end
  local function unpack(t, ...)
    return t_2funpack(copy(t), ...)
  end
  local function assoc(t, key, val, ...)
    local len = select("#", ...)
    if (0 ~= (len % 2)) then
      error(("no value supplied for key " .. tostring(select(len, ...))), 2)
    else
    end
    local t0
    do
      local _63_ = copy(t)
      do end (_63_)[key] = val
      t0 = _63_
    end
    for i = 1, len, 2 do
      local k, v = select(i, ...)
      do end (t0)[k] = v
    end
    return immutable(t0)
  end
  local function assoc_in(t, _64_, val)
    local _arg_65_ = _64_
    local k = _arg_65_[1]
    local ks = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_arg_65_, 2)
    local t0 = (t or {})
    if next(ks) then
      return assoc(t0, k, assoc_in(((t0)[k] or {}), ks, val))
    else
      return assoc(t0, k, val)
    end
  end
  local function update(t, key, f)
    local function _68_()
      local _67_ = copy(t)
      do end (_67_)[key] = f(t[key])
      return _67_
    end
    return immutable(_68_())
  end
  local function update_in(t, _69_, f)
    local _arg_70_ = _69_
    local k = _arg_70_[1]
    local ks = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_arg_70_, 2)
    local t0 = (t or {})
    if next(ks) then
      return assoc(t0, k, update_in((t0)[k], ks, f))
    else
      return update(t0, k, f)
    end
  end
  local function deepcopy(x)
    local function deepcopy_2a(x0, seen)
      local _72_ = type(x0)
      if (_72_ == "table") then
        local _73_ = seen[x0]
        if (_73_ == true) then
          return error("immutable tables can't contain self reference", 2)
        elseif true then
          local _ = _73_
          seen[x0] = true
          local function _74_()
            local tbl_12_auto = {}
            for k, v in pairs_2a(x0) do
              local _75_, _76_ = deepcopy_2a(k, seen), deepcopy_2a(v, seen)
              if ((nil ~= _75_) and (nil ~= _76_)) then
                local k_13_auto = _75_
                local v_14_auto = _76_
                tbl_12_auto[k_13_auto] = v_14_auto
              else
              end
            end
            return tbl_12_auto
          end
          return immutable(_74_())
        else
          return nil
        end
      elseif true then
        local _ = _72_
        return x0
      else
        return nil
      end
    end
    return deepcopy_2a(x, {})
  end
  local function first(_80_)
    local _arg_81_ = _80_
    local x = _arg_81_[1]
    return x
  end
  local function rest(t)
    local _82_ = remove(t, 1)
    return _82_
  end
  local function nthrest(t, n)
    local t_2a = {}
    for i = (n + 1), length_2a(t) do
      t_2finsert(t_2a, t[i])
    end
    return immutable(t_2a)
  end
  local function last(t)
    return t[length_2a(t)]
  end
  local function butlast(t)
    local _83_ = remove(t, length_2a(t))
    return _83_
  end
  local function join(...)
    local _84_, _85_, _86_ = select("#", ...), ...
    if (_84_ == 0) then
      return nil
    elseif ((_84_ == 1) and true) then
      local _3ft = _85_
      return immutable(copy(_3ft))
    elseif ((_84_ == 2) and true and true) then
      local _3ft1 = _85_
      local _3ft2 = _86_
      local to = copy(_3ft1)
      local from = (_3ft2 or {})
      for _, v in ipairs_2a(from) do
        t_2finsert(to, v)
      end
      return immutable(to)
    elseif (true and true and true) then
      local _ = _84_
      local _3ft1 = _85_
      local _3ft2 = _86_
      return join(join(_3ft1, _3ft2), select(3, ...))
    else
      return nil
    end
  end
  local function take(n, t)
    local t_2a = {}
    for i = 1, n do
      t_2finsert(t_2a, t[i])
    end
    return immutable(t_2a)
  end
  local function drop(n, t)
    return nthrest(t, n)
  end
  local function partition(...)
    local res = {}
    local function partition_2a(...)
      local _88_, _89_, _90_, _91_, _92_ = select("#", ...), ...
      local function _93_(...)
        return true
      end
      if ((_88_ == 0) and _93_(...)) then
        return error("wrong amount arguments to 'partition'")
      else
        local function _94_(...)
          return true
        end
        if ((_88_ == 1) and _94_(...)) then
          return error("wrong amount arguments to 'partition'")
        elseif ((_88_ == 2) and true and true) then
          local _3fn = _89_
          local _3ft = _90_
          return partition_2a(_3fn, _3fn, _3ft)
        elseif ((_88_ == 3) and true and true and true) then
          local _3fn = _89_
          local _3fstep = _90_
          local _3ft = _91_
          local p = take(_3fn, _3ft)
          if (_3fn == length_2a(p)) then
            t_2finsert(res, p)
            return partition_2a(_3fn, _3fstep, {t_2funpack(_3ft, (_3fstep + 1))})
          else
            return nil
          end
        elseif (true and true and true and true and true) then
          local _ = _88_
          local _3fn = _89_
          local _3fstep = _90_
          local _3fpad = _91_
          local _3ft = _92_
          local p = take(_3fn, _3ft)
          if (_3fn == length_2a(p)) then
            t_2finsert(res, p)
            return partition_2a(_3fn, _3fstep, _3fpad, {t_2funpack(_3ft, (_3fstep + 1))})
          else
            return t_2finsert(res, take(_3fn, join(p, _3fpad)))
          end
        else
          return nil
        end
      end
    end
    partition_2a(...)
    return immutable(res)
  end
  local function keys(t)
    local function _98_()
      local tbl_15_auto = {}
      local i_16_auto = #tbl_15_auto
      for k, _ in pairs_2a(t) do
        local val_17_auto = k
        if (nil ~= val_17_auto) then
          i_16_auto = (i_16_auto + 1)
          do end (tbl_15_auto)[i_16_auto] = val_17_auto
        else
        end
      end
      return tbl_15_auto
    end
    return immutable(_98_())
  end
  local function vals(t)
    local function _100_()
      local tbl_15_auto = {}
      local i_16_auto = #tbl_15_auto
      for _, v in pairs_2a(t) do
        local val_17_auto = v
        if (nil ~= val_17_auto) then
          i_16_auto = (i_16_auto + 1)
          do end (tbl_15_auto)[i_16_auto] = val_17_auto
        else
        end
      end
      return tbl_15_auto
    end
    return immutable(_100_())
  end
  local function group_by(f, t)
    local res = {}
    local ungroupped = {}
    for _, v in pairs_2a(t) do
      local k = f(v)
      if (nil ~= k) then
        local _102_ = res[k]
        if (nil ~= _102_) then
          local t_2a = _102_
          t_2finsert(t_2a, v)
        elseif true then
          local _0 = _102_
          res[k] = {v}
        else
        end
      else
        t_2finsert(ungroupped, v)
      end
    end
    local function _105_()
      local tbl_12_auto = {}
      for k, t0 in pairs_2a(res) do
        local _106_, _107_ = k, immutable(t0)
        if ((nil ~= _106_) and (nil ~= _107_)) then
          local k_13_auto = _106_
          local v_14_auto = _107_
          tbl_12_auto[k_13_auto] = v_14_auto
        else
        end
      end
      return tbl_12_auto
    end
    return immutable(_105_()), immutable(ungroupped)
  end
  local function frequencies(t)
    local res = setmetatable({}, {__index = deep_index, __newindex = deep_newindex})
    for _, v in pairs_2a(t) do
      local _109_ = res[v]
      if (nil ~= _109_) then
        local a = _109_
        res[v] = (a + 1)
      elseif true then
        local _0 = _109_
        res[v] = 1
      else
      end
    end
    return immutable(res)
  end
  local itable
  local function _111_(t, f)
    local function _113_()
      local _112_ = copy(t)
      t_2fsort(_112_, f)
      return _112_
    end
    return immutable(_113_())
  end
  itable = {sort = _111_, pack = pack, unpack = unpack, concat = concat, insert = insert, move = move, remove = remove, pairs = pairs_2a, ipairs = ipairs_2a, length = length_2a, eq = eq, deepcopy = deepcopy, assoc = assoc, ["assoc-in"] = assoc_in, update = update, ["update-in"] = update_in, keys = keys, vals = vals, ["group-by"] = group_by, frequencies = frequencies, first = first, rest = rest, nthrest = nthrest, last = last, butlast = butlast, join = join, partition = partition, take = take, drop = drop}
  local function _114_(_, t, opts)
    local _115_ = getmetatable(t)
    if ((_G.type(_115_) == "table") and ((_115_)["itable/type"] == "immutable")) then
      return t
    elseif true then
      local _0 = _115_
      return immutable(copy(t), opts)
    else
      return nil
    end
  end
  return setmetatable(itable, {__call = _114_})
end
--[[ "MIT License

Copyright (c) 2022 Andrey Listopadov

Permission is hereby granted‚ free of charge‚ to any person obtaining a copy
of this software and associated documentation files (the “Software”)‚ to deal
in the Software without restriction‚ including without limitation the rights
to use‚ copy‚ modify‚ merge‚ publish‚ distribute‚ sublicense‚ and/or sell
copies of the Software‚ and to permit persons to whom the Software is
furnished to do so‚ subject to the following conditions：

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”‚ WITHOUT WARRANTY OF ANY KIND‚ EXPRESS OR
IMPLIED‚ INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY‚
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM‚ DAMAGES OR OTHER
LIABILITY‚ WHETHER IN AN ACTION OF CONTRACT‚ TORT OR OTHERWISE‚ ARISING FROM‚
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE." ]]
local _local_1_ = {setmetatable({}, {__fennelview = _2_, __name = "namespace"}), require("lazy-seq"), require("itable")}, nil
local core = _local_1_[1]
local lazy = _local_1_[2]
local itable = _local_1_[3]
local function unpack_2a(x, ...)
  if core["seq?"](x) then
    return lazy.unpack(x)
  else
    return itable.unpack(x, ...)
  end
end
pcall(function() require("fennel").metadata:setall(unpack_2a, "fnl/arglist", {"x", "..."}) end)
local function pack_2a(...)
  local _245_ = {...}
  _245_["n"] = select("#", ...)
  return _245_
end
pcall(function() require("fennel").metadata:setall(pack_2a, "fnl/arglist", {"..."}) end)
local function pairs_2a(t)
  local _246_ = getmetatable(t)
  if ((_G.type(_246_) == "table") and (nil ~= (_246_).__pairs)) then
    local p = (_246_).__pairs
    return p(t)
  elseif true then
    local _ = _246_
    return pairs(t)
  else
    return nil
  end
end
pcall(function() require("fennel").metadata:setall(pairs_2a, "fnl/arglist", {"t"}) end)
local function ipairs_2a(t)
  local _248_ = getmetatable(t)
  if ((_G.type(_248_) == "table") and (nil ~= (_248_).__ipairs)) then
    local i = (_248_).__ipairs
    return i(t)
  elseif true then
    local _ = _248_
    return ipairs(t)
  else
    return nil
  end
end
pcall(function() require("fennel").metadata:setall(ipairs_2a, "fnl/arglist", {"t"}) end)
local function length_2a(t)
  local _250_ = getmetatable(t)
  if ((_G.type(_250_) == "table") and (nil ~= (_250_).__len)) then
    local l = (_250_).__len
    return l(t)
  elseif true then
    local _ = _250_
    return #t
  else
    return nil
  end
end
pcall(function() require("fennel").metadata:setall(length_2a, "fnl/arglist", {"t"}) end)
local apply
package.preload["init"] = package.preload["init"] or function(...)
  local function _255_()
    return "#<namespace: core>"
  end
  --[[ "MIT License
  
  Copyright (c) 2022 Andrey Listopadov
  
  Permission is hereby granted‚ free of charge‚ to any person obtaining a copy
  of this software and associated documentation files (the “Software”)‚ to deal
  in the Software without restriction‚ including without limitation the rights
  to use‚ copy‚ modify‚ merge‚ publish‚ distribute‚ sublicense‚ and/or sell
  copies of the Software‚ and to permit persons to whom the Software is
  furnished to do so‚ subject to the following conditions：
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  
  THE SOFTWARE IS PROVIDED “AS IS”‚ WITHOUT WARRANTY OF ANY KIND‚ EXPRESS OR
  IMPLIED‚ INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY‚
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM‚ DAMAGES OR OTHER
  LIABILITY‚ WHETHER IN AN ACTION OF CONTRACT‚ TORT OR OTHERWISE‚ ARISING FROM‚
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE." ]]
  local _local_254_ = {setmetatable({}, {__fennelview = _255_, __name = "namespace"}), require("lazy-seq"), require("itable")}, nil
  local core = _local_254_[1]
  local lazy = _local_254_[2]
  local itable = _local_254_[3]
  local function unpack_2a(x, ...)
    if core["seq?"](x) then
      return lazy.unpack(x)
    else
      return itable.unpack(x, ...)
    end
  end
  pcall(function() require("fennel").metadata:setall(unpack_2a, "fnl/arglist", {"x", "..."}) end)
  local function pack_2a(...)
    local _257_ = {...}
    _257_["n"] = select("#", ...)
    return _257_
  end
  pcall(function() require("fennel").metadata:setall(pack_2a, "fnl/arglist", {"..."}) end)
  local function pairs_2a(t)
    local _258_ = getmetatable(t)
    if ((_G.type(_258_) == "table") and (nil ~= (_258_).__pairs)) then
      local p = (_258_).__pairs
      return p(t)
    elseif true then
      local _ = _258_
      return pairs(t)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(pairs_2a, "fnl/arglist", {"t"}) end)
  local function ipairs_2a(t)
    local _260_ = getmetatable(t)
    if ((_G.type(_260_) == "table") and (nil ~= (_260_).__ipairs)) then
      local i = (_260_).__ipairs
      return i(t)
    elseif true then
      local _ = _260_
      return ipairs(t)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(ipairs_2a, "fnl/arglist", {"t"}) end)
  local function length_2a(t)
    local _262_ = getmetatable(t)
    if ((_G.type(_262_) == "table") and (nil ~= (_262_).__len)) then
      local l = (_262_).__len
      return l(t)
    elseif true then
      local _ = _262_
      return #t
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(length_2a, "fnl/arglist", {"t"}) end)
  local apply
  do
    local v_33_auto
    local function apply0(...)
      local _265_ = select("#", ...)
      if (_265_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "apply"))
      elseif (_265_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "apply"))
      elseif (_265_ == 2) then
        local f, args = ...
        return f(unpack_2a(args))
      elseif (_265_ == 3) then
        local f, a, args = ...
        return f(a, unpack_2a(args))
      elseif (_265_ == 4) then
        local f, a, b, args = ...
        return f(a, b, unpack_2a(args))
      elseif (_265_ == 5) then
        local f, a, b, c, args = ...
        return f(a, b, c, unpack_2a(args))
      elseif true then
        local _ = _265_
        local core_48_auto = require("init")
        local _let_266_ = core_48_auto.list(...)
        local f = _let_266_[1]
        local a = _let_266_[2]
        local b = _let_266_[3]
        local c = _let_266_[4]
        local d = _let_266_[5]
        local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_266_, 6)
        local flat_args = {}
        local len = (length_2a(args) - 1)
        for i = 1, len do
          flat_args[i] = args[i]
        end
        for i, a0 in pairs_2a(args[(len + 1)]) do
          flat_args[(i + len)] = a0
        end
        return f(a, b, c, d, unpack_2a(flat_args))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(apply0, "fnl/arglist", {"([f args])", "([f a args])", "([f a b args])", "([f a b c args])", "([f a b c d & args])"}, "fnl/docstring", "Apply `f` to the argument list formed by prepending intervening\narguments to `args`, and `f` must support variadic amount of\narguments.\n\n# Examples\nApplying `add` to different amount of arguments:\n\n``` fennel\n(assert-eq (apply add [1 2 3 4]) 10)\n(assert-eq (apply add 1 [2 3 4]) 10)\n(assert-eq (apply add 1 2 3 4 5 6 [7 8 9]) 45)\n```") end)
    v_33_auto = apply0
    core["apply"] = v_33_auto
    apply = v_33_auto
  end
  local add
  do
    local v_33_auto
    local function add0(...)
      local _268_ = select("#", ...)
      if (_268_ == 0) then
        return 0
      elseif (_268_ == 1) then
        local a = ...
        return a
      elseif (_268_ == 2) then
        local a, b = ...
        return (a + b)
      elseif (_268_ == 3) then
        local a, b, c = ...
        return (a + b + c)
      elseif (_268_ == 4) then
        local a, b, c, d = ...
        return (a + b + c + d)
      elseif true then
        local _ = _268_
        local core_48_auto = require("init")
        local _let_269_ = core_48_auto.list(...)
        local a = _let_269_[1]
        local b = _let_269_[2]
        local c = _let_269_[3]
        local d = _let_269_[4]
        local rest = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_269_, 5)
        return apply(add0, (a + b + c + d), rest)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(add0, "fnl/arglist", {"([])", "([a])", "([a b])", "([a b c])", "([a b c d])", "([a b c d & rest])"}, "fnl/docstring", "Sum arbitrary amount of numbers.") end)
    v_33_auto = add0
    core["add"] = v_33_auto
    add = v_33_auto
  end
  local sub
  do
    local v_33_auto
    local function sub0(...)
      local _271_ = select("#", ...)
      if (_271_ == 0) then
        return 0
      elseif (_271_ == 1) then
        local a = ...
        return ( - a)
      elseif (_271_ == 2) then
        local a, b = ...
        return (a - b)
      elseif (_271_ == 3) then
        local a, b, c = ...
        return (a - b - c)
      elseif (_271_ == 4) then
        local a, b, c, d = ...
        return (a - b - c - d)
      elseif true then
        local _ = _271_
        local core_48_auto = require("init")
        local _let_272_ = core_48_auto.list(...)
        local a = _let_272_[1]
        local b = _let_272_[2]
        local c = _let_272_[3]
        local d = _let_272_[4]
        local rest = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_272_, 5)
        return apply(sub0, (a - b - c - d), rest)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(sub0, "fnl/arglist", {"([])", "([a])", "([a b])", "([a b c])", "([a b c d])", "([a b c d & rest])"}, "fnl/docstring", "Subtract arbitrary amount of numbers.") end)
    v_33_auto = sub0
    core["sub"] = v_33_auto
    sub = v_33_auto
  end
  local mul
  do
    local v_33_auto
    local function mul0(...)
      local _274_ = select("#", ...)
      if (_274_ == 0) then
        return 1
      elseif (_274_ == 1) then
        local a = ...
        return a
      elseif (_274_ == 2) then
        local a, b = ...
        return (a * b)
      elseif (_274_ == 3) then
        local a, b, c = ...
        return (a * b * c)
      elseif (_274_ == 4) then
        local a, b, c, d = ...
        return (a * b * c * d)
      elseif true then
        local _ = _274_
        local core_48_auto = require("init")
        local _let_275_ = core_48_auto.list(...)
        local a = _let_275_[1]
        local b = _let_275_[2]
        local c = _let_275_[3]
        local d = _let_275_[4]
        local rest = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_275_, 5)
        return apply(mul0, (a * b * c * d), rest)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(mul0, "fnl/arglist", {"([])", "([a])", "([a b])", "([a b c])", "([a b c d])", "([a b c d & rest])"}, "fnl/docstring", "Multiply arbitrary amount of numbers.") end)
    v_33_auto = mul0
    core["mul"] = v_33_auto
    mul = v_33_auto
  end
  local div
  do
    local v_33_auto
    local function div0(...)
      local _277_ = select("#", ...)
      if (_277_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "div"))
      elseif (_277_ == 1) then
        local a = ...
        return (1 / a)
      elseif (_277_ == 2) then
        local a, b = ...
        return (a / b)
      elseif (_277_ == 3) then
        local a, b, c = ...
        return (a / b / c)
      elseif (_277_ == 4) then
        local a, b, c, d = ...
        return (a / b / c / d)
      elseif true then
        local _ = _277_
        local core_48_auto = require("init")
        local _let_278_ = core_48_auto.list(...)
        local a = _let_278_[1]
        local b = _let_278_[2]
        local c = _let_278_[3]
        local d = _let_278_[4]
        local rest = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_278_, 5)
        return apply(div0, (a / b / c / d), rest)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(div0, "fnl/arglist", {"([a])", "([a b])", "([a b c])", "([a b c d])", "([a b c d & rest])"}, "fnl/docstring", "Divide arbitrary amount of numbers.") end)
    v_33_auto = div0
    core["div"] = v_33_auto
    div = v_33_auto
  end
  local le
  do
    local v_33_auto
    local function le0(...)
      local _280_ = select("#", ...)
      if (_280_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "le"))
      elseif (_280_ == 1) then
        local a = ...
        return true
      elseif (_280_ == 2) then
        local a, b = ...
        return (a <= b)
      elseif true then
        local _ = _280_
        local core_48_auto = require("init")
        local _let_281_ = core_48_auto.list(...)
        local a = _let_281_[1]
        local b = _let_281_[2]
        local _let_282_ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_281_, 3)
        local c = _let_282_[1]
        local d = _let_282_[2]
        local more = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_282_, 3)
        if (a <= b) then
          if d then
            return apply(le0, b, c, d, more)
          else
            return (b <= c)
          end
        else
          return false
        end
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(le0, "fnl/arglist", {"([a])", "([a b])", "([a b & [c d & more]])"}, "fnl/docstring", "Returns true if nums are in monotonically non-decreasing order") end)
    v_33_auto = le0
    core["le"] = v_33_auto
    le = v_33_auto
  end
  local lt
  do
    local v_33_auto
    local function lt0(...)
      local _286_ = select("#", ...)
      if (_286_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "lt"))
      elseif (_286_ == 1) then
        local a = ...
        return true
      elseif (_286_ == 2) then
        local a, b = ...
        return (a < b)
      elseif true then
        local _ = _286_
        local core_48_auto = require("init")
        local _let_287_ = core_48_auto.list(...)
        local a = _let_287_[1]
        local b = _let_287_[2]
        local _let_288_ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_287_, 3)
        local c = _let_288_[1]
        local d = _let_288_[2]
        local more = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_288_, 3)
        if (a < b) then
          if d then
            return apply(lt0, b, c, d, more)
          else
            return (b < c)
          end
        else
          return false
        end
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(lt0, "fnl/arglist", {"([a])", "([a b])", "([a b & [c d & more]])"}, "fnl/docstring", "Returns true if nums are in monotonically decreasing order") end)
    v_33_auto = lt0
    core["lt"] = v_33_auto
    lt = v_33_auto
  end
  local ge
  do
    local v_33_auto
    local function ge0(...)
      local _292_ = select("#", ...)
      if (_292_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "ge"))
      elseif (_292_ == 1) then
        local a = ...
        return true
      elseif (_292_ == 2) then
        local a, b = ...
        return (a >= b)
      elseif true then
        local _ = _292_
        local core_48_auto = require("init")
        local _let_293_ = core_48_auto.list(...)
        local a = _let_293_[1]
        local b = _let_293_[2]
        local _let_294_ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_293_, 3)
        local c = _let_294_[1]
        local d = _let_294_[2]
        local more = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_294_, 3)
        if (a >= b) then
          if d then
            return apply(ge0, b, c, d, more)
          else
            return (b >= c)
          end
        else
          return false
        end
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(ge0, "fnl/arglist", {"([a])", "([a b])", "([a b & [c d & more]])"}, "fnl/docstring", "Returns true if nums are in monotonically non-increasing order") end)
    v_33_auto = ge0
    core["ge"] = v_33_auto
    ge = v_33_auto
  end
  local gt
  do
    local v_33_auto
    local function gt0(...)
      local _298_ = select("#", ...)
      if (_298_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "gt"))
      elseif (_298_ == 1) then
        local a = ...
        return true
      elseif (_298_ == 2) then
        local a, b = ...
        return (a > b)
      elseif true then
        local _ = _298_
        local core_48_auto = require("init")
        local _let_299_ = core_48_auto.list(...)
        local a = _let_299_[1]
        local b = _let_299_[2]
        local _let_300_ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_299_, 3)
        local c = _let_300_[1]
        local d = _let_300_[2]
        local more = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_300_, 3)
        if (a > b) then
          if d then
            return apply(gt0, b, c, d, more)
          else
            return (b > c)
          end
        else
          return false
        end
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(gt0, "fnl/arglist", {"([a])", "([a b])", "([a b & [c d & more]])"}, "fnl/docstring", "Returns true if nums are in monotonically increasing order") end)
    v_33_auto = gt0
    core["gt"] = v_33_auto
    gt = v_33_auto
  end
  local inc
  do
    local v_33_auto
    local function inc0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "inc"))
        else
        end
      end
      return (x + 1)
    end
    pcall(function() require("fennel").metadata:setall(inc0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Increase number `x` by one") end)
    v_33_auto = inc0
    core["inc"] = v_33_auto
    inc = v_33_auto
  end
  local dec
  do
    local v_33_auto
    local function dec0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "dec"))
        else
        end
      end
      return (x - 1)
    end
    pcall(function() require("fennel").metadata:setall(dec0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Decrease number `x` by one") end)
    v_33_auto = dec0
    core["dec"] = v_33_auto
    dec = v_33_auto
  end
  local class
  do
    local v_33_auto
    local function class0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "class"))
        else
        end
      end
      local _307_ = type(x)
      if (_307_ == "table") then
        local _308_ = getmetatable(x)
        if ((_G.type(_308_) == "table") and (nil ~= (_308_)["cljlib/type"])) then
          local t = (_308_)["cljlib/type"]
          return t
        elseif true then
          local _ = _308_
          return "table"
        else
          return nil
        end
      elseif (nil ~= _307_) then
        local t = _307_
        return t
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(class0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Return cljlib type of the `x`, or lua type.") end)
    v_33_auto = class0
    core["class"] = v_33_auto
    class = v_33_auto
  end
  local constantly
  do
    local v_33_auto
    local function constantly0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "constantly"))
        else
        end
      end
      local function _312_()
        return x
      end
      pcall(function() require("fennel").metadata:setall(_312_, "fnl/arglist", {}) end)
      return _312_
    end
    pcall(function() require("fennel").metadata:setall(constantly0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Returns a function that takes any number of arguments and returns `x`.") end)
    v_33_auto = constantly0
    core["constantly"] = v_33_auto
    constantly = v_33_auto
  end
  local complement
  do
    local v_33_auto
    local function complement0(...)
      local f = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "complement"))
        else
        end
      end
      local function fn_314_(...)
        local _315_ = select("#", ...)
        if (_315_ == 0) then
          return not f()
        elseif (_315_ == 1) then
          local a = ...
          return not f(a)
        elseif (_315_ == 2) then
          local a, b = ...
          return not f(a, b)
        elseif true then
          local _ = _315_
          local core_48_auto = require("init")
          local _let_316_ = core_48_auto.list(...)
          local a = _let_316_[1]
          local b = _let_316_[2]
          local cs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_316_, 3)
          return not apply(f, a, b, cs)
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(fn_314_, "fnl/arglist", {"([])", "([a])", "([a b])", "([a b & cs])"}) end)
      return fn_314_
    end
    pcall(function() require("fennel").metadata:setall(complement0, "fnl/arglist", {"[f]"}, "fnl/docstring", "Takes a function `f` and returns the function that takes the same\namount of arguments as `f`, has the same effect, and returns the\nopposite truth value.") end)
    v_33_auto = complement0
    core["complement"] = v_33_auto
    complement = v_33_auto
  end
  local identity
  do
    local v_33_auto
    local function identity0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "identity"))
        else
        end
      end
      return x
    end
    pcall(function() require("fennel").metadata:setall(identity0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Returns its argument.") end)
    v_33_auto = identity0
    core["identity"] = v_33_auto
    identity = v_33_auto
  end
  local comp
  do
    local v_33_auto
    local function comp0(...)
      local _319_ = select("#", ...)
      if (_319_ == 0) then
        return identity
      elseif (_319_ == 1) then
        local f = ...
        return f
      elseif (_319_ == 2) then
        local f, g = ...
        local function fn_320_(...)
          local _321_ = select("#", ...)
          if (_321_ == 0) then
            return f(g())
          elseif (_321_ == 1) then
            local x = ...
            return f(g(x))
          elseif (_321_ == 2) then
            local x, y = ...
            return f(g(x, y))
          elseif (_321_ == 3) then
            local x, y, z = ...
            return f(g(x, y, z))
          elseif true then
            local _ = _321_
            local core_48_auto = require("init")
            local _let_322_ = core_48_auto.list(...)
            local x = _let_322_[1]
            local y = _let_322_[2]
            local z = _let_322_[3]
            local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_322_, 4)
            return f(apply(g, x, y, z, args))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_320_, "fnl/arglist", {"([])", "([x])", "([x y])", "([x y z])", "([x y z & args])"}) end)
        return fn_320_
      elseif true then
        local _ = _319_
        local core_48_auto = require("init")
        local _let_324_ = core_48_auto.list(...)
        local f = _let_324_[1]
        local g = _let_324_[2]
        local fs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_324_, 3)
        return core.reduce(comp0, core.cons(f, core.cons(g, fs)))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(comp0, "fnl/arglist", {"([])", "([f])", "([f g])", "([f g & fs])"}, "fnl/docstring", "Compose functions.") end)
    v_33_auto = comp0
    core["comp"] = v_33_auto
    comp = v_33_auto
  end
  local eq
  do
    local v_33_auto
    local function eq0(...)
      local _326_ = select("#", ...)
      if (_326_ == 0) then
        return true
      elseif (_326_ == 1) then
        local _ = ...
        return true
      elseif (_326_ == 2) then
        local a, b = ...
        if ((a == b) and (b == a)) then
          return true
        elseif (function(_327_,_328_,_329_) return (_327_ == _328_) and (_328_ == _329_) end)("table",type(a),type(b)) then
          local res, count_a = true, 0
          for k, v in pairs_2a(a) do
            if not res then break end
            local function _330_(...)
              local res0, done = nil, nil
              for k_2a, v0 in pairs_2a(b) do
                if done then break end
                if eq0(k_2a, k) then
                  res0, done = v0, true
                else
                end
              end
              return res0
            end
            res = eq0(v, _330_(...))
            count_a = (count_a + 1)
          end
          if res then
            local count_b
            do
              local res0 = 0
              for _, _0 in pairs_2a(b) do
                res0 = (res0 + 1)
              end
              count_b = res0
            end
            res = (count_a == count_b)
          else
          end
          return res
        else
          return false
        end
      elseif true then
        local _ = _326_
        local core_48_auto = require("init")
        local _let_334_ = core_48_auto.list(...)
        local a = _let_334_[1]
        local b = _let_334_[2]
        local cs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_334_, 3)
        return (eq0(a, b) and apply(eq0, b, cs))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(eq0, "fnl/arglist", {"([])", "([_])", "([a b])", "([a b & cs])"}, "fnl/docstring", "Comparison function.\n\nAccepts arbitrary amount of values, and does the deep comparison.  If\nvalues implement `__eq` metamethod, tries to use it, by checking if\nfirst value is equal to second value, and the second value is equal to\nthe first value.  If values are not equal and are tables does the deep\ncomparison.  Tables as keys are supported.") end)
    v_33_auto = eq0
    core["eq"] = v_33_auto
    eq = v_33_auto
  end
  local function deep_index(tbl, key)
    local res = nil
    for k, v in pairs_2a(tbl) do
      if res then break end
      if eq(k, key) then
        res = v
      else
        res = nil
      end
    end
    return res
  end
  pcall(function() require("fennel").metadata:setall(deep_index, "fnl/arglist", {"tbl", "key"}, "fnl/docstring", "This function uses the `eq` function to compare keys of the given\ntable `tbl` and the given `key`.  Several other functions also reuse\nthis indexing method, such as sets.") end)
  local function deep_newindex(tbl, key, val)
    local done = false
    if ("table" == type(key)) then
      for k, _ in pairs_2a(tbl) do
        if done then break end
        if eq(k, key) then
          rawset(tbl, k, val)
          done = true
        else
        end
      end
    else
    end
    if not done then
      return rawset(tbl, key, val)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(deep_newindex, "fnl/arglist", {"tbl", "key", "val"}, "fnl/docstring", "This function uses the `eq` function to compare keys of the given\ntable `tbl` and the given `key`. If the key is found it's being\nset, if not a new key is set.") end)
  local memoize
  do
    local v_33_auto
    local function memoize0(...)
      local f = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "memoize"))
        else
        end
      end
      local memo = setmetatable({}, {__index = deep_index})
      local function fn_341_(...)
        local core_48_auto = require("init")
        local _let_342_ = core_48_auto.list(...)
        local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_342_, 1)
        local _343_ = memo[args]
        if (nil ~= _343_) then
          local res = _343_
          return unpack_2a(res, 1, res.n)
        elseif true then
          local _ = _343_
          local res = pack_2a(f(...))
          do end (memo)[args] = res
          return unpack_2a(res, 1, res.n)
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(fn_341_, "fnl/arglist", {"[& args]"}) end)
      return fn_341_
    end
    pcall(function() require("fennel").metadata:setall(memoize0, "fnl/arglist", {"[f]"}, "fnl/docstring", "Returns a memoized version of a referentially transparent function.\nThe memoized version of the function keeps a cache of the mapping from\narguments to results and, when calls with the same arguments are\nrepeated often, has higher performance at the expense of higher memory\nuse.") end)
    v_33_auto = memoize0
    core["memoize"] = v_33_auto
    memoize = v_33_auto
  end
  local deref
  do
    local v_33_auto
    local function deref0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "deref"))
        else
        end
      end
      local _346_ = getmetatable(x)
      if ((_G.type(_346_) == "table") and (nil ~= (_346_)["cljlib/deref"])) then
        local f = (_346_)["cljlib/deref"]
        return f(x)
      elseif true then
        local _ = _346_
        return error("object doesn't implement cljlib/deref metamethod", 2)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(deref0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Dereference an object.") end)
    v_33_auto = deref0
    core["deref"] = v_33_auto
    deref = v_33_auto
  end
  local empty
  do
    local v_33_auto
    local function empty0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "empty"))
        else
        end
      end
      local _349_ = getmetatable(x)
      if ((_G.type(_349_) == "table") and (nil ~= (_349_)["cljlib/empty"])) then
        local f = (_349_)["cljlib/empty"]
        return f()
      elseif true then
        local _ = _349_
        local _350_ = type(x)
        if (_350_ == "table") then
          return {}
        elseif (_350_ == "string") then
          return ""
        elseif true then
          local _0 = _350_
          return error(("don't know how to create empty variant of type " .. _0))
        else
          return nil
        end
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(empty0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Get an empty variant of a given collection.") end)
    v_33_auto = empty0
    core["empty"] = v_33_auto
    empty = v_33_auto
  end
  local nil_3f
  do
    local v_33_auto
    local function nil_3f0(...)
      local _353_ = select("#", ...)
      if (_353_ == 0) then
        return true
      elseif (_353_ == 1) then
        local x = ...
        return (x == nil)
      elseif true then
        local _ = _353_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "nil?"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(nil_3f0, "fnl/arglist", {"([])", "([x])"}, "fnl/docstring", "Test if `x` is nil.") end)
    v_33_auto = nil_3f0
    core["nil?"] = v_33_auto
    nil_3f = v_33_auto
  end
  local zero_3f
  do
    local v_33_auto
    local function zero_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "zero?"))
        else
        end
      end
      return (x == 0)
    end
    pcall(function() require("fennel").metadata:setall(zero_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is equal to zero.") end)
    v_33_auto = zero_3f0
    core["zero?"] = v_33_auto
    zero_3f = v_33_auto
  end
  local pos_3f
  do
    local v_33_auto
    local function pos_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "pos?"))
        else
        end
      end
      return (x > 0)
    end
    pcall(function() require("fennel").metadata:setall(pos_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is greater than zero.") end)
    v_33_auto = pos_3f0
    core["pos?"] = v_33_auto
    pos_3f = v_33_auto
  end
  local neg_3f
  do
    local v_33_auto
    local function neg_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "neg?"))
        else
        end
      end
      return (x < 0)
    end
    pcall(function() require("fennel").metadata:setall(neg_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is less than zero.") end)
    v_33_auto = neg_3f0
    core["neg?"] = v_33_auto
    neg_3f = v_33_auto
  end
  local even_3f
  do
    local v_33_auto
    local function even_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "even?"))
        else
        end
      end
      return ((x % 2) == 0)
    end
    pcall(function() require("fennel").metadata:setall(even_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is even.") end)
    v_33_auto = even_3f0
    core["even?"] = v_33_auto
    even_3f = v_33_auto
  end
  local odd_3f
  do
    local v_33_auto
    local function odd_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "odd?"))
        else
        end
      end
      return not even_3f(x)
    end
    pcall(function() require("fennel").metadata:setall(odd_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is odd.") end)
    v_33_auto = odd_3f0
    core["odd?"] = v_33_auto
    odd_3f = v_33_auto
  end
  local string_3f
  do
    local v_33_auto
    local function string_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "string?"))
        else
        end
      end
      return (type(x) == "string")
    end
    pcall(function() require("fennel").metadata:setall(string_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a string.") end)
    v_33_auto = string_3f0
    core["string?"] = v_33_auto
    string_3f = v_33_auto
  end
  local boolean_3f
  do
    local v_33_auto
    local function boolean_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "boolean?"))
        else
        end
      end
      return (type(x) == "boolean")
    end
    pcall(function() require("fennel").metadata:setall(boolean_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a Boolean") end)
    v_33_auto = boolean_3f0
    core["boolean?"] = v_33_auto
    boolean_3f = v_33_auto
  end
  local true_3f
  do
    local v_33_auto
    local function true_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "true?"))
        else
        end
      end
      return (x == true)
    end
    pcall(function() require("fennel").metadata:setall(true_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is `true`") end)
    v_33_auto = true_3f0
    core["true?"] = v_33_auto
    true_3f = v_33_auto
  end
  local false_3f
  do
    local v_33_auto
    local function false_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "false?"))
        else
        end
      end
      return (x == false)
    end
    pcall(function() require("fennel").metadata:setall(false_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is `false`") end)
    v_33_auto = false_3f0
    core["false?"] = v_33_auto
    false_3f = v_33_auto
  end
  local int_3f
  do
    local v_33_auto
    local function int_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "int?"))
        else
        end
      end
      return ((type(x) == "number") and (x == math.floor(x)))
    end
    pcall(function() require("fennel").metadata:setall(int_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a number without floating point data.\n\nNumber is rounded with `math.floor` and compared with original number.") end)
    v_33_auto = int_3f0
    core["int?"] = v_33_auto
    int_3f = v_33_auto
  end
  local pos_int_3f
  do
    local v_33_auto
    local function pos_int_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "pos-int?"))
        else
        end
      end
      return (int_3f(x) and pos_3f(x))
    end
    pcall(function() require("fennel").metadata:setall(pos_int_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a positive integer.") end)
    v_33_auto = pos_int_3f0
    core["pos-int?"] = v_33_auto
    pos_int_3f = v_33_auto
  end
  local neg_int_3f
  do
    local v_33_auto
    local function neg_int_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "neg-int?"))
        else
        end
      end
      return (int_3f(x) and neg_3f(x))
    end
    pcall(function() require("fennel").metadata:setall(neg_int_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a negative integer.") end)
    v_33_auto = neg_int_3f0
    core["neg-int?"] = v_33_auto
    neg_int_3f = v_33_auto
  end
  local double_3f
  do
    local v_33_auto
    local function double_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "double?"))
        else
        end
      end
      return ((type(x) == "number") and (x ~= math.floor(x)))
    end
    pcall(function() require("fennel").metadata:setall(double_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a number with floating point data.") end)
    v_33_auto = double_3f0
    core["double?"] = v_33_auto
    double_3f = v_33_auto
  end
  local empty_3f
  do
    local v_33_auto
    local function empty_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "empty?"))
        else
        end
      end
      local _369_ = type(x)
      if (_369_ == "table") then
        local _370_ = getmetatable(x)
        if ((_G.type(_370_) == "table") and ((_370_)["cljlib/type"] == "seq")) then
          return nil_3f(core.seq(x))
        elseif ((_370_ == nil) or ((_G.type(_370_) == "table") and ((_370_)["cljlib/type"] == nil))) then
          local next_2a = pairs_2a(x)
          return (next_2a(x) == nil)
        else
          return nil
        end
      elseif (_369_ == "string") then
        return (x == "")
      elseif (_369_ == "nil") then
        return true
      elseif true then
        local _ = _369_
        return error("empty?: unsupported collection")
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(empty_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check if collection is empty.") end)
    v_33_auto = empty_3f0
    core["empty?"] = v_33_auto
    empty_3f = v_33_auto
  end
  local not_empty
  do
    local v_33_auto
    local function not_empty0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "not-empty"))
        else
        end
      end
      if not empty_3f(x) then
        return x
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(not_empty0, "fnl/arglist", {"[x]"}, "fnl/docstring", "If `x` is empty, returns `nil`, otherwise `x`.") end)
    v_33_auto = not_empty0
    core["not-empty"] = v_33_auto
    not_empty = v_33_auto
  end
  local map_3f
  do
    local v_33_auto
    local function map_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "map?"))
        else
        end
      end
      if ("table" == type(x)) then
        local _376_ = getmetatable(x)
        if ((_G.type(_376_) == "table") and ((_376_)["cljlib/type"] == "hash-map")) then
          return true
        elseif ((_G.type(_376_) == "table") and ((_376_)["cljlib/type"] == "sorted-map")) then
          return true
        elseif ((_376_ == nil) or ((_G.type(_376_) == "table") and ((_376_)["cljlib/type"] == nil))) then
          local len = length_2a(x)
          local nxt, t, k = pairs_2a(x)
          local function _377_(...)
            if (len == 0) then
              return k
            else
              return len
            end
          end
          return (nil ~= nxt(t, _377_(...)))
        elseif true then
          local _ = _376_
          return false
        else
          return nil
        end
      else
        return false
      end
    end
    pcall(function() require("fennel").metadata:setall(map_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check whether `x` is an associative table.\n\nNon-empty tables are tested by calling `next`. If the length of the\ntable is greater than zero, the last integer key is passed to the\n`next`, and if `next` returns a key, the table is considered\nassociative. If the length is zero, `next` is called with what `paris`\nreturns for the table, and if the key is returned, table is considered\nassociative.\n\nEmpty tables can't be analyzed with this method, and `map?` will\nalways return `false`.  If you need this test pass for empty table,\nsee `hash-map` for creating tables that have additional metadata\nattached for this test to work.\n\n# Examples\nNon-empty map:\n\n``` fennel\n(assert-is (map? {:a 1 :b 2}))\n```\n\nEmpty tables don't pass the test:\n\n``` fennel\n(assert-not (map? {}))\n```\n\nEmpty tables created with `hash-map` will pass the test:\n\n``` fennel\n(assert-is (map? (hash-map)))\n```") end)
    v_33_auto = map_3f0
    core["map?"] = v_33_auto
    map_3f = v_33_auto
  end
  local vector_3f
  do
    local v_33_auto
    local function vector_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "vector?"))
        else
        end
      end
      if ("table" == type(x)) then
        local _381_ = getmetatable(x)
        if ((_G.type(_381_) == "table") and ((_381_)["cljlib/type"] == "vector")) then
          return true
        elseif ((_381_ == nil) or ((_G.type(_381_) == "table") and ((_381_)["cljlib/type"] == nil))) then
          local len = length_2a(x)
          local nxt, t, k = pairs_2a(x)
          local function _382_(...)
            if (len == 0) then
              return k
            else
              return len
            end
          end
          if (nil ~= nxt(t, _382_(...))) then
            return false
          elseif (len > 0) then
            return true
          else
            return false
          end
        elseif true then
          local _ = _381_
          return false
        else
          return nil
        end
      else
        return false
      end
    end
    pcall(function() require("fennel").metadata:setall(vector_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check whether `tbl` is a sequential table.\n\nNon-empty sequential tables are tested for two things:\n- `next` returns the key-value pair,\n- key, that is returned by the `next` is equal to `1`.\n\nEmpty tables can't be analyzed with this method, and `vector?` will\nalways return `false`.  If you need this test pass for empty table,\nsee `vector` for creating tables that have additional\nmetadata attached for this test to work.\n\n# Examples\nNon-empty vector:\n\n``` fennel\n(assert-is (vector? [1 2 3 4]))\n```\n\nEmpty tables don't pass the test:\n\n``` fennel\n(assert-not (vector? []))\n```\n\nEmpty tables created with `vector` will pass the test:\n\n``` fennel\n(assert-is (vector? (vector)))\n```") end)
    v_33_auto = vector_3f0
    core["vector?"] = v_33_auto
    vector_3f = v_33_auto
  end
  local set_3f
  do
    local v_33_auto
    local function set_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "set?"))
        else
        end
      end
      local _387_ = getmetatable(x)
      if ((_G.type(_387_) == "table") and ((_387_)["cljlib/type"] == "hash-set")) then
        return true
      elseif true then
        local _ = _387_
        return false
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(set_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check if object is a set.") end)
    v_33_auto = set_3f0
    core["set?"] = v_33_auto
    set_3f = v_33_auto
  end
  local seq_3f
  do
    local v_33_auto
    local function seq_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "seq?"))
        else
        end
      end
      return lazy["seq?"](x)
    end
    pcall(function() require("fennel").metadata:setall(seq_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check if object is a sequence.") end)
    v_33_auto = seq_3f0
    core["seq?"] = v_33_auto
    seq_3f = v_33_auto
  end
  local some_3f
  do
    local v_33_auto
    local function some_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "some?"))
        else
        end
      end
      return (x ~= nil)
    end
    pcall(function() require("fennel").metadata:setall(some_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Returns true if x is not nil, false otherwise.") end)
    v_33_auto = some_3f0
    core["some?"] = v_33_auto
    some_3f = v_33_auto
  end
  local function vec__3etransient(immutable)
    local function _391_(vec)
      local len = #vec
      local function _392_(_, i)
        if (i <= len) then
          return vec[i]
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(_392_, "fnl/arglist", {"_", "i"}) end)
      local function _394_()
        return len
      end
      local function _395_()
        return error("can't `conj` onto transient vector, use `conj!`")
      end
      local function _396_()
        return error("can't `assoc` onto transient vector, use `assoc!`")
      end
      local function _397_()
        return error("can't `dissoc` onto transient vector, use `dissoc!`")
      end
      local function _398_(tvec, v)
        len = (len + 1)
        tvec[len] = v
        return tvec
      end
      pcall(function() require("fennel").metadata:setall(_398_, "fnl/arglist", {"tvec", "v"}) end)
      local function _399_(tvec, ...)
        do
          local len0 = #tvec
          for i = 1, select("#", ...), 2 do
            local k, v = select(i, ...)
            if (1 <= i) and (i <= len0) then
              tvec[i] = v
            else
              error(("index " .. i .. " is out of bounds"))
            end
          end
        end
        return tvec
      end
      pcall(function() require("fennel").metadata:setall(_399_, "fnl/arglist", {"tvec", "..."}) end)
      local function _401_(tvec)
        if (len == 0) then
          return error("transient vector is empty", 2)
        else
          local val = table.remove(tvec)
          len = (len - 1)
          return tvec
        end
      end
      pcall(function() require("fennel").metadata:setall(_401_, "fnl/arglist", {"tvec"}) end)
      local function _403_()
        return error("can't `dissoc!` with a transient vector")
      end
      local function _404_(tvec)
        local v
        do
          local tbl_17_auto = {}
          local i_18_auto = #tbl_17_auto
          for i = 1, len do
            local val_19_auto = tvec[i]
            if (nil ~= val_19_auto) then
              i_18_auto = (i_18_auto + 1)
              do end (tbl_17_auto)[i_18_auto] = val_19_auto
            else
            end
          end
          v = tbl_17_auto
        end
        while (len > 0) do
          table.remove(tvec)
          len = (len - 1)
        end
        local function _406_()
          return error("attempt to use transient after it was persistet")
        end
        local function _407_()
          return error("attempt to use transient after it was persistet")
        end
        setmetatable(tvec, {__index = _406_, __newindex = _407_})
        return immutable(itable(v))
      end
      pcall(function() require("fennel").metadata:setall(_404_, "fnl/arglist", {"tvec"}) end)
      return setmetatable({}, {__index = _392_, __len = _394_, ["cljlib/type"] = "transient", ["cljlib/conj"] = _395_, ["cljlib/assoc"] = _396_, ["cljlib/dissoc"] = _397_, ["cljlib/conj!"] = _398_, ["cljlib/assoc!"] = _399_, ["cljlib/pop!"] = _401_, ["cljlib/dissoc!"] = _403_, ["cljlib/persistent!"] = _404_})
    end
    pcall(function() require("fennel").metadata:setall(_391_, "fnl/arglist", {"vec"}) end)
    return _391_
  end
  pcall(function() require("fennel").metadata:setall(vec__3etransient, "fnl/arglist", {"immutable"}) end)
  local function vec_2a(v, len)
    do
      local _408_ = getmetatable(v)
      if (nil ~= _408_) then
        local mt = _408_
        mt["__len"] = constantly((len or length_2a(v)))
        do end (mt)["cljlib/type"] = "vector"
        mt["cljlib/editable"] = true
        local function _409_(t, v0)
          local len0 = length_2a(t)
          return vec_2a(itable.assoc(t, (len0 + 1), v0), (len0 + 1))
        end
        pcall(function() require("fennel").metadata:setall(_409_, "fnl/arglist", {"t", "v"}) end)
        do end (mt)["cljlib/conj"] = _409_
        local function _410_(t)
          local len0 = (length_2a(t) - 1)
          local coll = {}
          if (len0 < 0) then
            error("can't pop empty vector", 2)
          else
          end
          for i = 1, len0 do
            coll[i] = t[i]
          end
          return vec_2a(itable(coll), len0)
        end
        pcall(function() require("fennel").metadata:setall(_410_, "fnl/arglist", {"t"}) end)
        do end (mt)["cljlib/pop"] = _410_
        local function _412_()
          return vec_2a(itable({}))
        end
        pcall(function() require("fennel").metadata:setall(_412_, "fnl/arglist", {}) end)
        do end (mt)["cljlib/empty"] = _412_
        mt["cljlib/transient"] = vec__3etransient(vec_2a)
        local function _413_(coll, view, inspector, indent)
          if empty_3f(coll) then
            return "[]"
          else
            local lines
            do
              local tbl_17_auto = {}
              local i_18_auto = #tbl_17_auto
              for i = 1, length_2a(coll) do
                local val_19_auto = (" " .. view(coll[i], inspector, indent))
                if (nil ~= val_19_auto) then
                  i_18_auto = (i_18_auto + 1)
                  do end (tbl_17_auto)[i_18_auto] = val_19_auto
                else
                end
              end
              lines = tbl_17_auto
            end
            lines[1] = ("[" .. string.gsub((lines[1] or ""), "^%s+", ""))
            do end (lines)[#lines] = (lines[#lines] .. "]")
            return lines
          end
        end
        pcall(function() require("fennel").metadata:setall(_413_, "fnl/arglist", {"coll", "view", "inspector", "indent"}) end)
        do end (mt)["__fennelview"] = _413_
      elseif (_408_ == nil) then
        vec_2a(setmetatable(v, {}))
      else
      end
    end
    return v
  end
  pcall(function() require("fennel").metadata:setall(vec_2a, "fnl/arglist", {"v", "len"}) end)
  local vec
  do
    local v_33_auto
    local function vec0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "vec"))
        else
        end
      end
      if empty_3f(coll) then
        return vec_2a(itable({}), 0)
      elseif vector_3f(coll) then
        return vec_2a(itable(coll), length_2a(coll))
      elseif "else" then
        local packed = lazy.pack(core.seq(coll))
        local len = packed.n
        local _418_
        do
          packed["n"] = nil
          _418_ = packed
        end
        return vec_2a(itable(_418_, {["fast-index?"] = true}), len)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(vec0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Coerce collection `coll` to a vector.") end)
    v_33_auto = vec0
    core["vec"] = v_33_auto
    vec = v_33_auto
  end
  local vector
  do
    local v_33_auto
    local function vector0(...)
      local core_48_auto = require("init")
      local _let_420_ = core_48_auto.list(...)
      local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_420_, 1)
      return vec(args)
    end
    pcall(function() require("fennel").metadata:setall(vector0, "fnl/arglist", {"[& args]"}, "fnl/docstring", "Constructs sequential table out of its arguments.\n\nSets additional metadata for function `vector?` to work.\n\n# Examples\n\n``` fennel\n(def :private v (vector 1 2 3 4))\n(assert-eq v [1 2 3 4])\n```") end)
    v_33_auto = vector0
    core["vector"] = v_33_auto
    vector = v_33_auto
  end
  local nth
  do
    local v_33_auto
    local function nth0(...)
      local _422_ = select("#", ...)
      if (_422_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "nth"))
      elseif (_422_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "nth"))
      elseif (_422_ == 2) then
        local coll, i = ...
        if vector_3f(coll) then
          if ((i < 1) or (length_2a(coll) < i)) then
            return error(string.format("index %d is out of bounds", i))
          else
            return coll[i]
          end
        elseif string_3f(coll) then
          return nth0(vec(coll), i)
        elseif seq_3f(coll) then
          return nth0(vec(coll), i)
        elseif "else" then
          return error("expected an indexed collection")
        else
          return nil
        end
      elseif (_422_ == 3) then
        local coll, i, not_found = ...
        assert(int_3f(i), "expected an integer key")
        if vector_3f(coll) then
          return (coll[i] or not_found)
        elseif string_3f(coll) then
          return nth0(vec(coll), i, not_found)
        elseif seq_3f(coll) then
          return nth0(vec(coll), i, not_found)
        elseif "else" then
          return error("expected an indexed collection")
        else
          return nil
        end
      elseif true then
        local _ = _422_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "nth"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(nth0, "fnl/arglist", {"([coll i])", "([coll i not-found])"}, "fnl/docstring", "Returns the value at the `index`. `get` returns `nil` if `index` out\nof bounds, `nth` raises an error unless `not-found` is supplied.\n`nth` also works for strings and sequences.") end)
    v_33_auto = nth0
    core["nth"] = v_33_auto
    nth = v_33_auto
  end
  local seq_2a
  local function seq_2a0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "seq*"))
      else
      end
    end
    do
      local _428_ = getmetatable(x)
      if (nil ~= _428_) then
        local mt = _428_
        mt["cljlib/type"] = "seq"
        local function _429_(s, v)
          return core.cons(v, s)
        end
        pcall(function() require("fennel").metadata:setall(_429_, "fnl/arglist", {"s", "v"}) end)
        do end (mt)["cljlib/conj"] = _429_
        local function _430_()
          return core.list()
        end
        mt["cljlib/empty"] = _430_
      else
      end
    end
    return x
  end
  pcall(function() require("fennel").metadata:setall(seq_2a0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Add cljlib sequence meta-info.") end)
  seq_2a = seq_2a0
  local seq
  do
    local v_33_auto
    local function seq0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "seq"))
        else
        end
      end
      local function _434_(...)
        local _433_ = getmetatable(coll)
        if ((_G.type(_433_) == "table") and (nil ~= (_433_)["cljlib/seq"])) then
          local f = (_433_)["cljlib/seq"]
          return f(coll)
        elseif true then
          local _ = _433_
          if lazy["seq?"](coll) then
            return lazy.seq(coll)
          elseif map_3f(coll) then
            return lazy.map(vec, coll)
          elseif "else" then
            return lazy.seq(coll)
          else
            return nil
          end
        else
          return nil
        end
      end
      return seq_2a(_434_(...))
    end
    pcall(function() require("fennel").metadata:setall(seq0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Construct a sequence from the given collection `coll`.  If `coll` is\nan associative table, returns sequence of vectors with key and value.\nIf `col` is sequential table, returns its shallow copy.  If `col` is\nstring, return sequential table of its codepoints.\n\n# Examples\nSequential tables are transformed to sequences:\n\n``` fennel\n(seq [1 2 3 4]) ;; @seq(1 2 3 4)\n```\n\nAssociative tables are transformed to format like this `[[key1 value1]\n... [keyN valueN]]` and order is non-deterministic:\n\n``` fennel\n(seq {:a 1 :b 2 :c 3}) ;; @seq([:b 2] [:a 1] [:c 3])\n```") end)
    v_33_auto = seq0
    core["seq"] = v_33_auto
    seq = v_33_auto
  end
  local rseq
  do
    local v_33_auto
    local function rseq0(...)
      local rev = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "rseq"))
        else
        end
      end
      return seq_2a(lazy.rseq(rev))
    end
    pcall(function() require("fennel").metadata:setall(rseq0, "fnl/arglist", {"[rev]"}, "fnl/docstring", "Returns, in possibly-constant time, a seq of the items in `rev` in reverse order.\nInput must be traversable with `ipairs`.  Doesn't work in constant\ntime if `rev` implements a linear-time `__len` metamethod, or invoking\nLua `#` operator on `rev` takes linar time.  If `t` is empty returns\n`nil`.\n\n# Examples\n\n``` fennel\n(def :private v [1 2 3])\n(def :private r (rseq v))\n\n(assert-eq (reverse v) r)\n```") end)
    v_33_auto = rseq0
    core["rseq"] = v_33_auto
    rseq = v_33_auto
  end
  local lazy_seq
  do
    local v_33_auto
    local function lazy_seq0(...)
      local f = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "lazy-seq"))
        else
        end
      end
      return seq_2a(lazy["lazy-seq"](f))
    end
    pcall(function() require("fennel").metadata:setall(lazy_seq0, "fnl/arglist", {"[f]"}, "fnl/docstring", "Create lazy sequence from the result of calling a function `f`.\nDelays execution of `f` until sequence is consumed.  `f` must return a\nsequence or a vector.") end)
    v_33_auto = lazy_seq0
    core["lazy-seq"] = v_33_auto
    lazy_seq = v_33_auto
  end
  local first
  do
    local v_33_auto
    local function first0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "first"))
        else
        end
      end
      return lazy.first(seq(coll))
    end
    pcall(function() require("fennel").metadata:setall(first0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Return first element of a `coll`. Calls `seq` on its argument.") end)
    v_33_auto = first0
    core["first"] = v_33_auto
    first = v_33_auto
  end
  local rest
  do
    local v_33_auto
    local function rest0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "rest"))
        else
        end
      end
      return seq_2a(lazy.rest(seq(coll)))
    end
    pcall(function() require("fennel").metadata:setall(rest0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a sequence of all elements of a `coll` but the first one.\nCalls `seq` on its argument.") end)
    v_33_auto = rest0
    core["rest"] = v_33_auto
    rest = v_33_auto
  end
  local next_2a
  local function next_2a0(...)
    local s = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "next*"))
      else
      end
    end
    return seq_2a(lazy.next(s))
  end
  pcall(function() require("fennel").metadata:setall(next_2a0, "fnl/arglist", {"[s]"}, "fnl/docstring", "Return the tail of a sequence.\n\nIf the sequence is empty, returns nil.") end)
  next_2a = next_2a0
  do
    core["next"] = next_2a
  end
  local count
  do
    local v_33_auto
    local function count0(...)
      local s = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "count"))
        else
        end
      end
      local _443_ = getmetatable(s)
      if ((_G.type(_443_) == "table") and ((_443_)["cljlib/type"] == "vector")) then
        return length_2a(s)
      elseif true then
        local _ = _443_
        return lazy.count(s)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(count0, "fnl/arglist", {"[s]"}, "fnl/docstring", "Count amount of elements in the sequence.") end)
    v_33_auto = count0
    core["count"] = v_33_auto
    count = v_33_auto
  end
  local cons
  do
    local v_33_auto
    local function cons0(...)
      local head, tail = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "cons"))
        else
        end
      end
      return seq_2a(lazy.cons(head, tail))
    end
    pcall(function() require("fennel").metadata:setall(cons0, "fnl/arglist", {"[head tail]"}, "fnl/docstring", "Construct a cons cell.\nPrepends new `head` to a `tail`, which must be either a table,\nsequence, or nil.\n\n# Examples\n\n``` fennel\n(assert-eq [0 1] (cons 0 [1]))\n(assert-eq (list 0 1 2 3) (cons 0 (cons 1 (list 2 3))))\n```") end)
    v_33_auto = cons0
    core["cons"] = v_33_auto
    cons = v_33_auto
  end
  local function list(...)
    return seq_2a(lazy.list(...))
  end
  pcall(function() require("fennel").metadata:setall(list, "fnl/arglist", {"..."}, "fnl/docstring", "Create eager sequence of provided values.\n\n# Examples\n\n``` fennel\n(local l (list 1 2 3 4 5))\n(assert-eq [1 2 3 4 5] l)\n```") end)
  core.list = list
  local list_2a
  do
    local v_33_auto
    local function list_2a0(...)
      local core_48_auto = require("init")
      local _let_446_ = core_48_auto.list(...)
      local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_446_, 1)
      return seq_2a(apply(lazy["list*"], args))
    end
    pcall(function() require("fennel").metadata:setall(list_2a0, "fnl/arglist", {"[& args]"}, "fnl/docstring", "Creates a new sequence containing the items prepended to the rest,\nthe last of which will be treated as a sequence.\n\n# Examples\n\n``` fennel\n(local l (list* 1 2 3 [4 5]))\n(assert-eq [1 2 3 4 5] l)\n```") end)
    v_33_auto = list_2a0
    core["list*"] = v_33_auto
    list_2a = v_33_auto
  end
  local last
  do
    local v_33_auto
    local function last0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "last"))
        else
        end
      end
      local _448_ = next_2a(coll)
      if (nil ~= _448_) then
        local coll_2a = _448_
        return last0(coll_2a)
      elseif true then
        local _ = _448_
        return first(coll)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(last0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns the last element of a `coll`. Calls `seq` on its argument.") end)
    v_33_auto = last0
    core["last"] = v_33_auto
    last = v_33_auto
  end
  local butlast
  do
    local v_33_auto
    local function butlast0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "butlast"))
        else
        end
      end
      return seq(lazy["drop-last"](coll))
    end
    pcall(function() require("fennel").metadata:setall(butlast0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns everything but the last element of the `coll` as a new\n  sequence.  Calls `seq` on its argument.") end)
    v_33_auto = butlast0
    core["butlast"] = v_33_auto
    butlast = v_33_auto
  end
  local map
  do
    local v_33_auto
    local function map0(...)
      local _451_ = select("#", ...)
      if (_451_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "map"))
      elseif (_451_ == 1) then
        local f = ...
        local function fn_452_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_452_"))
            else
            end
          end
          local function fn_454_(...)
            local _455_ = select("#", ...)
            if (_455_ == 0) then
              return rf()
            elseif (_455_ == 1) then
              local result = ...
              return rf(result)
            elseif (_455_ == 2) then
              local result, input = ...
              return rf(result, f(input))
            elseif true then
              local _ = _455_
              local core_48_auto = require("init")
              local _let_456_ = core_48_auto.list(...)
              local result = _let_456_[1]
              local input = _let_456_[2]
              local inputs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_456_, 3)
              return rf(result, apply(f, input, inputs))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_454_, "fnl/arglist", {"([])", "([result])", "([result input])", "([result input & inputs])"}) end)
          return fn_454_
        end
        pcall(function() require("fennel").metadata:setall(fn_452_, "fnl/arglist", {"[rf]"}) end)
        return fn_452_
      elseif (_451_ == 2) then
        local f, coll = ...
        return seq_2a(lazy.map(f, coll))
      elseif true then
        local _ = _451_
        local core_48_auto = require("init")
        local _let_458_ = core_48_auto.list(...)
        local f = _let_458_[1]
        local coll = _let_458_[2]
        local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_458_, 3)
        return seq_2a(apply(lazy.map, f, coll, colls))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(map0, "fnl/arglist", {"([f])", "([f coll])", "([f coll & colls])"}, "fnl/docstring", "Returns a lazy sequence consisting of the result of applying `f` to\nthe set of first items of each `coll`, followed by applying `f` to the\nset of second items in each `coll`, until any one of the `colls` is\nexhausted.  Any remaining items in other `colls` are ignored. Function\n`f` should accept number-of-colls arguments. Returns a transducer when\nno collection is provided.\n\n# Examples\n\n``` fennel\n(map #(+ $ 1) [1 2 3]) ;; => @seq(2 3 4)\n(map #(+ $1 $2) [1 2 3] [4 5 6]) ;; => @seq(5 7 9)\n(def :private res (map #(+ $ 1) [:a :b :c])) ;; will raise an error only when realized\n```") end)
    v_33_auto = map0
    core["map"] = v_33_auto
    map = v_33_auto
  end
  local mapv
  do
    local v_33_auto
    local function mapv0(...)
      local _461_ = select("#", ...)
      if (_461_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "mapv"))
      elseif (_461_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "mapv"))
      elseif (_461_ == 2) then
        local f, coll = ...
        return core["persistent!"](core.transduce(map(f), core["conj!"], core.transient(vector()), coll))
      elseif true then
        local _ = _461_
        local core_48_auto = require("init")
        local _let_462_ = core_48_auto.list(...)
        local f = _let_462_[1]
        local coll = _let_462_[2]
        local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_462_, 3)
        return vec(apply(map, f, coll, colls))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(mapv0, "fnl/arglist", {"([f coll])", "([f coll & colls])"}, "fnl/docstring", "Returns a vector consisting of the result of applying `f` to the\nset of first items of each `coll`, followed by applying `f` to the set\nof second items in each coll, until any one of the `colls` is\nexhausted.  Any remaining items in other collections are ignored.\nFunction `f` should accept number-of-colls arguments.") end)
    v_33_auto = mapv0
    core["mapv"] = v_33_auto
    mapv = v_33_auto
  end
  local map_indexed
  do
    local v_33_auto
    local function map_indexed0(...)
      local _464_ = select("#", ...)
      if (_464_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "map-indexed"))
      elseif (_464_ == 1) then
        local f = ...
        local function fn_465_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_465_"))
            else
            end
          end
          local i = -1
          local function fn_467_(...)
            local _468_ = select("#", ...)
            if (_468_ == 0) then
              return rf()
            elseif (_468_ == 1) then
              local result = ...
              return rf(result)
            elseif (_468_ == 2) then
              local result, input = ...
              i = (i + 1)
              return rf(result, f(i, input))
            elseif true then
              local _ = _468_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_467_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_467_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_467_
        end
        pcall(function() require("fennel").metadata:setall(fn_465_, "fnl/arglist", {"[rf]"}) end)
        return fn_465_
      elseif (_464_ == 2) then
        local f, coll = ...
        return seq_2a(lazy["map-indexed"](f, coll))
      elseif true then
        local _ = _464_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "map-indexed"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(map_indexed0, "fnl/arglist", {"([f])", "([f coll])"}, "fnl/docstring", "Returns a lazy sequence consisting of the result of applying `f` to 1\nand the first item of `coll`, followed by applying `f` to 2 and the\nsecond item in `coll`, etc., until `coll` is exhausted.  Returns a\ntransducer when no collection is provided.") end)
    v_33_auto = map_indexed0
    core["map-indexed"] = v_33_auto
    map_indexed = v_33_auto
  end
  local mapcat
  do
    local v_33_auto
    local function mapcat0(...)
      local _471_ = select("#", ...)
      if (_471_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "mapcat"))
      elseif (_471_ == 1) then
        local f = ...
        return comp(map(f), core.cat)
      elseif true then
        local _ = _471_
        local core_48_auto = require("init")
        local _let_472_ = core_48_auto.list(...)
        local f = _let_472_[1]
        local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_472_, 2)
        return seq_2a(apply(lazy.mapcat, f, colls))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(mapcat0, "fnl/arglist", {"([f])", "([f & colls])"}, "fnl/docstring", "Apply `concat` to the result of calling `map` with `f` and\ncollections `colls`. Returns a transducer when no collection is\nprovided.") end)
    v_33_auto = mapcat0
    core["mapcat"] = v_33_auto
    mapcat = v_33_auto
  end
  local filter
  do
    local v_33_auto
    local function filter0(...)
      local _474_ = select("#", ...)
      if (_474_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "filter"))
      elseif (_474_ == 1) then
        local pred = ...
        local function fn_475_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_475_"))
            else
            end
          end
          local function fn_477_(...)
            local _478_ = select("#", ...)
            if (_478_ == 0) then
              return rf()
            elseif (_478_ == 1) then
              local result = ...
              return rf(result)
            elseif (_478_ == 2) then
              local result, input = ...
              if pred(input) then
                return rf(result, input)
              else
                return result
              end
            elseif true then
              local _ = _478_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_477_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_477_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_477_
        end
        pcall(function() require("fennel").metadata:setall(fn_475_, "fnl/arglist", {"[rf]"}) end)
        return fn_475_
      elseif (_474_ == 2) then
        local pred, coll = ...
        return seq_2a(lazy.filter(pred, coll))
      elseif true then
        local _ = _474_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "filter"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(filter0, "fnl/arglist", {"([pred])", "([pred coll])"}, "fnl/docstring", "Returns a lazy sequence of the items in `coll` for which\n`pred` returns logical true. Returns a transducer when no collection\nis provided.") end)
    v_33_auto = filter0
    core["filter"] = v_33_auto
    filter = v_33_auto
  end
  local filterv
  do
    local v_33_auto
    local function filterv0(...)
      local pred, coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "filterv"))
        else
        end
      end
      return vec(filter(pred, coll))
    end
    pcall(function() require("fennel").metadata:setall(filterv0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Returns a vector of the items in `coll` for which\n`pred` returns logical true.") end)
    v_33_auto = filterv0
    core["filterv"] = v_33_auto
    filterv = v_33_auto
  end
  local every_3f
  do
    local v_33_auto
    local function every_3f0(...)
      local pred, coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "every?"))
        else
        end
      end
      return lazy["every?"](pred, coll)
    end
    pcall(function() require("fennel").metadata:setall(every_3f0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Test if every item in `coll` satisfies the `pred`.") end)
    v_33_auto = every_3f0
    core["every?"] = v_33_auto
    every_3f = v_33_auto
  end
  local some
  do
    local v_33_auto
    local function some0(...)
      local pred, coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "some"))
        else
        end
      end
      return lazy["some?"](pred, coll)
    end
    pcall(function() require("fennel").metadata:setall(some0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Test if any item in `coll` satisfies the `pred`.") end)
    v_33_auto = some0
    core["some"] = v_33_auto
    some = v_33_auto
  end
  local not_any_3f
  do
    local v_33_auto
    local function not_any_3f0(...)
      local pred, coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "not-any?"))
        else
        end
      end
      local function _486_(_241)
        return not pred(_241)
      end
      return some(_486_, coll)
    end
    pcall(function() require("fennel").metadata:setall(not_any_3f0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Test if no item in `coll` satisfy the `pred`.") end)
    v_33_auto = not_any_3f0
    core["not-any?"] = v_33_auto
    not_any_3f = v_33_auto
  end
  local range
  do
    local v_33_auto
    local function range0(...)
      local _487_ = select("#", ...)
      if (_487_ == 0) then
        return seq_2a(lazy.range())
      elseif (_487_ == 1) then
        local upper = ...
        return seq_2a(lazy.range(upper))
      elseif (_487_ == 2) then
        local lower, upper = ...
        return seq_2a(lazy.range(lower, upper))
      elseif (_487_ == 3) then
        local lower, upper, step = ...
        return seq_2a(lazy.range(lower, upper, step))
      elseif true then
        local _ = _487_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "range"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(range0, "fnl/arglist", {"([])", "([upper])", "([lower upper])", "([lower upper step])"}, "fnl/docstring", "Returns lazy sequence of numbers from `lower` to `upper` with optional\n`step`.") end)
    v_33_auto = range0
    core["range"] = v_33_auto
    range = v_33_auto
  end
  local concat
  do
    local v_33_auto
    local function concat0(...)
      local core_48_auto = require("init")
      local _let_489_ = core_48_auto.list(...)
      local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_489_, 1)
      return seq_2a(apply(lazy.concat, colls))
    end
    pcall(function() require("fennel").metadata:setall(concat0, "fnl/arglist", {"[& colls]"}, "fnl/docstring", "Return a lazy sequence of concatenated `colls`.") end)
    v_33_auto = concat0
    core["concat"] = v_33_auto
    concat = v_33_auto
  end
  local reverse
  do
    local v_33_auto
    local function reverse0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "reverse"))
        else
        end
      end
      return seq_2a(lazy.reverse(coll))
    end
    pcall(function() require("fennel").metadata:setall(reverse0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a lazy sequence with same items as in `coll` but in reverse order.") end)
    v_33_auto = reverse0
    core["reverse"] = v_33_auto
    reverse = v_33_auto
  end
  local take
  do
    local v_33_auto
    local function take0(...)
      local _491_ = select("#", ...)
      if (_491_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "take"))
      elseif (_491_ == 1) then
        local n = ...
        local function fn_492_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_492_"))
            else
            end
          end
          local n0 = n
          local function fn_494_(...)
            local _495_ = select("#", ...)
            if (_495_ == 0) then
              return rf()
            elseif (_495_ == 1) then
              local result = ...
              return rf(result)
            elseif (_495_ == 2) then
              local result, input = ...
              local result0
              if (0 < n0) then
                result0 = rf(result, input)
              else
                result0 = result
              end
              n0 = (n0 - 1)
              if not (0 < n0) then
                return core["ensure-reduced"](result0)
              else
                return result0
              end
            elseif true then
              local _ = _495_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_494_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_494_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_494_
        end
        pcall(function() require("fennel").metadata:setall(fn_492_, "fnl/arglist", {"[rf]"}) end)
        return fn_492_
      elseif (_491_ == 2) then
        local n, coll = ...
        return seq_2a(lazy.take(n, coll))
      elseif true then
        local _ = _491_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "take"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(take0, "fnl/arglist", {"([n])", "([n coll])"}, "fnl/docstring", "Returns a lazy sequence of the first `n` items in `coll`, or all items if\nthere are fewer than `n`.") end)
    v_33_auto = take0
    core["take"] = v_33_auto
    take = v_33_auto
  end
  local take_while
  do
    local v_33_auto
    local function take_while0(...)
      local _500_ = select("#", ...)
      if (_500_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "take-while"))
      elseif (_500_ == 1) then
        local pred = ...
        local function fn_501_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_501_"))
            else
            end
          end
          local function fn_503_(...)
            local _504_ = select("#", ...)
            if (_504_ == 0) then
              return rf()
            elseif (_504_ == 1) then
              local result = ...
              return rf(result)
            elseif (_504_ == 2) then
              local result, input = ...
              if pred(input) then
                return rf(result, input)
              else
                return core.reduced(result)
              end
            elseif true then
              local _ = _504_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_503_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_503_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_503_
        end
        pcall(function() require("fennel").metadata:setall(fn_501_, "fnl/arglist", {"[rf]"}) end)
        return fn_501_
      elseif (_500_ == 2) then
        local pred, coll = ...
        return seq_2a(lazy["take-while"](pred, coll))
      elseif true then
        local _ = _500_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "take-while"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(take_while0, "fnl/arglist", {"([pred])", "([pred coll])"}, "fnl/docstring", "Take the elements from the collection `coll` until `pred` returns logical\nfalse for any of the elemnts.  Returns a lazy sequence. Returns a\ntransducer when no collection is provided.") end)
    v_33_auto = take_while0
    core["take-while"] = v_33_auto
    take_while = v_33_auto
  end
  local drop
  do
    local v_33_auto
    local function drop0(...)
      local _508_ = select("#", ...)
      if (_508_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "drop"))
      elseif (_508_ == 1) then
        local n = ...
        local function fn_509_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_509_"))
            else
            end
          end
          local nv = n
          local function fn_511_(...)
            local _512_ = select("#", ...)
            if (_512_ == 0) then
              return rf()
            elseif (_512_ == 1) then
              local result = ...
              return rf(result)
            elseif (_512_ == 2) then
              local result, input = ...
              local n0 = nv
              nv = (nv - 1)
              if pos_3f(n0) then
                return result
              else
                return rf(result, input)
              end
            elseif true then
              local _ = _512_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_511_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_511_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_511_
        end
        pcall(function() require("fennel").metadata:setall(fn_509_, "fnl/arglist", {"[rf]"}) end)
        return fn_509_
      elseif (_508_ == 2) then
        local n, coll = ...
        return seq_2a(lazy.drop(n, coll))
      elseif true then
        local _ = _508_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "drop"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(drop0, "fnl/arglist", {"([n])", "([n coll])"}, "fnl/docstring", "Drop `n` elements from collection `coll`, returning a lazy sequence\nof remaining elements. Returns a transducer when no collection is\nprovided.") end)
    v_33_auto = drop0
    core["drop"] = v_33_auto
    drop = v_33_auto
  end
  local drop_while
  do
    local v_33_auto
    local function drop_while0(...)
      local _516_ = select("#", ...)
      if (_516_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "drop-while"))
      elseif (_516_ == 1) then
        local pred = ...
        local function fn_517_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_517_"))
            else
            end
          end
          local dv = true
          local function fn_519_(...)
            local _520_ = select("#", ...)
            if (_520_ == 0) then
              return rf()
            elseif (_520_ == 1) then
              local result = ...
              return rf(result)
            elseif (_520_ == 2) then
              local result, input = ...
              local drop_3f = dv
              if (drop_3f and pred(input)) then
                return result
              else
                dv = nil
                return rf(result, input)
              end
            elseif true then
              local _ = _520_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_519_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_519_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_519_
        end
        pcall(function() require("fennel").metadata:setall(fn_517_, "fnl/arglist", {"[rf]"}) end)
        return fn_517_
      elseif (_516_ == 2) then
        local pred, coll = ...
        return seq_2a(lazy["drop-while"](pred, coll))
      elseif true then
        local _ = _516_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "drop-while"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(drop_while0, "fnl/arglist", {"([pred])", "([pred coll])"}, "fnl/docstring", "Drop the elements from the collection `coll` until `pred` returns logical\nfalse for any of the elemnts.  Returns a lazy sequence. Returns a\ntransducer when no collection is provided.") end)
    v_33_auto = drop_while0
    core["drop-while"] = v_33_auto
    drop_while = v_33_auto
  end
  local drop_last
  do
    local v_33_auto
    local function drop_last0(...)
      local _524_ = select("#", ...)
      if (_524_ == 0) then
        return seq_2a(lazy["drop-last"]())
      elseif (_524_ == 1) then
        local coll = ...
        return seq_2a(lazy["drop-last"](coll))
      elseif (_524_ == 2) then
        local n, coll = ...
        return seq_2a(lazy["drop-last"](n, coll))
      elseif true then
        local _ = _524_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "drop-last"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(drop_last0, "fnl/arglist", {"([])", "([coll])", "([n coll])"}, "fnl/docstring", "Return a lazy sequence from `coll` without last `n` elements.") end)
    v_33_auto = drop_last0
    core["drop-last"] = v_33_auto
    drop_last = v_33_auto
  end
  local take_last
  do
    local v_33_auto
    local function take_last0(...)
      local n, coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "take-last"))
        else
        end
      end
      return seq_2a(lazy["take-last"](n, coll))
    end
    pcall(function() require("fennel").metadata:setall(take_last0, "fnl/arglist", {"[n coll]"}, "fnl/docstring", "Return a sequence of last `n` elements of the `coll`.") end)
    v_33_auto = take_last0
    core["take-last"] = v_33_auto
    take_last = v_33_auto
  end
  local take_nth
  do
    local v_33_auto
    local function take_nth0(...)
      local _527_ = select("#", ...)
      if (_527_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "take-nth"))
      elseif (_527_ == 1) then
        local n = ...
        local function fn_528_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_528_"))
            else
            end
          end
          local iv = -1
          local function fn_530_(...)
            local _531_ = select("#", ...)
            if (_531_ == 0) then
              return rf()
            elseif (_531_ == 1) then
              local result = ...
              return rf(result)
            elseif (_531_ == 2) then
              local result, input = ...
              iv = (iv + 1)
              if (0 == (iv % n)) then
                return rf(result, input)
              else
                return result
              end
            elseif true then
              local _ = _531_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_530_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_530_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_530_
        end
        pcall(function() require("fennel").metadata:setall(fn_528_, "fnl/arglist", {"[rf]"}) end)
        return fn_528_
      elseif (_527_ == 2) then
        local n, coll = ...
        return seq_2a(lazy["take-nth"](n, coll))
      elseif true then
        local _ = _527_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "take-nth"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(take_nth0, "fnl/arglist", {"([n])", "([n coll])"}, "fnl/docstring", "Return a lazy sequence of every `n` item in `coll`. Returns a\ntransducer when no collection is provided.") end)
    v_33_auto = take_nth0
    core["take-nth"] = v_33_auto
    take_nth = v_33_auto
  end
  local split_at
  do
    local v_33_auto
    local function split_at0(...)
      local n, coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "split-at"))
        else
        end
      end
      return vec(lazy["split-at"](n, coll))
    end
    pcall(function() require("fennel").metadata:setall(split_at0, "fnl/arglist", {"[n coll]"}, "fnl/docstring", "Return a table with sequence `coll` being split at `n`") end)
    v_33_auto = split_at0
    core["split-at"] = v_33_auto
    split_at = v_33_auto
  end
  local split_with
  do
    local v_33_auto
    local function split_with0(...)
      local pred, coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "split-with"))
        else
        end
      end
      return vec(lazy["split-with"](pred, coll))
    end
    pcall(function() require("fennel").metadata:setall(split_with0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Return a table with sequence `coll` being split with `pred`") end)
    v_33_auto = split_with0
    core["split-with"] = v_33_auto
    split_with = v_33_auto
  end
  local nthrest
  do
    local v_33_auto
    local function nthrest0(...)
      local coll, n = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "nthrest"))
        else
        end
      end
      return seq_2a(lazy.nthrest(coll, n))
    end
    pcall(function() require("fennel").metadata:setall(nthrest0, "fnl/arglist", {"[coll n]"}, "fnl/docstring", "Returns the nth rest of `coll`, `coll` when `n` is 0.\n\n# Examples\n\n``` fennel\n(assert-eq (nthrest [1 2 3 4] 3) [4])\n(assert-eq (nthrest [1 2 3 4] 2) [3 4])\n(assert-eq (nthrest [1 2 3 4] 1) [2 3 4])\n(assert-eq (nthrest [1 2 3 4] 0) [1 2 3 4])\n```\n") end)
    v_33_auto = nthrest0
    core["nthrest"] = v_33_auto
    nthrest = v_33_auto
  end
  local nthnext
  do
    local v_33_auto
    local function nthnext0(...)
      local coll, n = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "nthnext"))
        else
        end
      end
      return lazy.nthnext(coll, n)
    end
    pcall(function() require("fennel").metadata:setall(nthnext0, "fnl/arglist", {"[coll n]"}, "fnl/docstring", "Returns the nth next of `coll`, (seq coll) when `n` is 0.") end)
    v_33_auto = nthnext0
    core["nthnext"] = v_33_auto
    nthnext = v_33_auto
  end
  local keep
  do
    local v_33_auto
    local function keep0(...)
      local _539_ = select("#", ...)
      if (_539_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "keep"))
      elseif (_539_ == 1) then
        local f = ...
        local function fn_540_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_540_"))
            else
            end
          end
          local function fn_542_(...)
            local _543_ = select("#", ...)
            if (_543_ == 0) then
              return rf()
            elseif (_543_ == 1) then
              local result = ...
              return rf(result)
            elseif (_543_ == 2) then
              local result, input = ...
              local v = f(input)
              if nil_3f(v) then
                return result
              else
                return rf(result, v)
              end
            elseif true then
              local _ = _543_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_542_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_542_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_542_
        end
        pcall(function() require("fennel").metadata:setall(fn_540_, "fnl/arglist", {"[rf]"}) end)
        return fn_540_
      elseif (_539_ == 2) then
        local f, coll = ...
        return seq_2a(lazy.keep(f, coll))
      elseif true then
        local _ = _539_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "keep"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(keep0, "fnl/arglist", {"([f])", "([f coll])"}, "fnl/docstring", "Returns a lazy sequence of the non-nil results of calling `f` on the\nitems of the `coll`. Returns a transducer when no collection is\nprovided.") end)
    v_33_auto = keep0
    core["keep"] = v_33_auto
    keep = v_33_auto
  end
  local keep_indexed
  do
    local v_33_auto
    local function keep_indexed0(...)
      local _547_ = select("#", ...)
      if (_547_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "keep-indexed"))
      elseif (_547_ == 1) then
        local f = ...
        local function fn_548_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_548_"))
            else
            end
          end
          local iv = -1
          local function fn_550_(...)
            local _551_ = select("#", ...)
            if (_551_ == 0) then
              return rf()
            elseif (_551_ == 1) then
              local result = ...
              return rf(result)
            elseif (_551_ == 2) then
              local result, input = ...
              iv = (iv + 1)
              local v = f(iv, input)
              if nil_3f(v) then
                return result
              else
                return rf(result, v)
              end
            elseif true then
              local _ = _551_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_550_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_550_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_550_
        end
        pcall(function() require("fennel").metadata:setall(fn_548_, "fnl/arglist", {"[rf]"}) end)
        return fn_548_
      elseif (_547_ == 2) then
        local f, coll = ...
        return seq_2a(lazy["keep-indexed"](f, coll))
      elseif true then
        local _ = _547_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "keep-indexed"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(keep_indexed0, "fnl/arglist", {"([f])", "([f coll])"}, "fnl/docstring", "Returns a lazy sequence of the non-nil results of (f index item) in\nthe `coll`.  Note, this means false return values will be included.\n`f` must be free of side effects. Returns a transducer when no\ncollection is provided.") end)
    v_33_auto = keep_indexed0
    core["keep-indexed"] = v_33_auto
    keep_indexed = v_33_auto
  end
  local partition
  do
    local v_33_auto
    local function partition0(...)
      local _556_ = select("#", ...)
      if (_556_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "partition"))
      elseif (_556_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "partition"))
      elseif (_556_ == 2) then
        local n, coll = ...
        return map(seq_2a, lazy.partition(n, coll))
      elseif (_556_ == 3) then
        local n, step, coll = ...
        return map(seq_2a, lazy.partition(n, step, coll))
      elseif (_556_ == 4) then
        local n, step, pad, coll = ...
        return map(seq_2a, lazy.partition(n, step, pad, coll))
      elseif true then
        local _ = _556_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "partition"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(partition0, "fnl/arglist", {"([n coll])", "([n step coll])", "([n step pad coll])"}, "fnl/docstring", "Given a collection `coll`, returns a lazy sequence of lists of `n`\nitems each, at offsets `step` apart. If `step` is not supplied,\ndefaults to `n`, i.e. the partitions do not overlap. If a `pad`\ncollection is supplied, use its elements as necessary to complete last\npartition up to `n` items. In case there are not enough padding\nelements, return a partition with less than `n` items.") end)
    v_33_auto = partition0
    core["partition"] = v_33_auto
    partition = v_33_auto
  end
  local function array()
    local len = 0
    local function _558_()
      return len
    end
    local function _559_(self)
      while (0 ~= len) do
        self[len] = nil
        len = (len - 1)
      end
      return nil
    end
    pcall(function() require("fennel").metadata:setall(_559_, "fnl/arglist", {"self"}) end)
    local function _560_(self, val)
      len = (len + 1)
      do end (self)[len] = val
      return self
    end
    pcall(function() require("fennel").metadata:setall(_560_, "fnl/arglist", {"self", "val"}) end)
    return setmetatable({}, {__len = _558_, __index = {clear = _559_, add = _560_}})
  end
  pcall(function() require("fennel").metadata:setall(array, "fnl/arglist", {}) end)
  local partition_by
  do
    local v_33_auto
    local function partition_by0(...)
      local _561_ = select("#", ...)
      if (_561_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "partition-by"))
      elseif (_561_ == 1) then
        local f = ...
        local function fn_562_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_562_"))
            else
            end
          end
          local a = array()
          local none = {}
          local pv = none
          local function fn_564_(...)
            local _565_ = select("#", ...)
            if (_565_ == 0) then
              return rf()
            elseif (_565_ == 1) then
              local result = ...
              local function _566_(...)
                if empty_3f(a) then
                  return result
                else
                  local v = vec(a)
                  a:clear()
                  return core.unreduced(rf(result, v))
                end
              end
              return rf(_566_(...))
            elseif (_565_ == 2) then
              local result, input = ...
              local pval = pv
              local val = f(input)
              pv = val
              if ((pval == none) or (val == pval)) then
                a:add(input)
                return result
              else
                local v = vec(a)
                a:clear()
                local ret = rf(result, v)
                if not core["reduced?"](ret) then
                  a:add(input)
                else
                end
                return ret
              end
            elseif true then
              local _ = _565_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_564_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_564_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_564_
        end
        pcall(function() require("fennel").metadata:setall(fn_562_, "fnl/arglist", {"[rf]"}) end)
        return fn_562_
      elseif (_561_ == 2) then
        local f, coll = ...
        return map(seq_2a, lazy["partition-by"](f, coll))
      elseif true then
        local _ = _561_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "partition-by"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(partition_by0, "fnl/arglist", {"([f])", "([f coll])"}, "fnl/docstring", "Applies `f` to each value in `coll`, splitting it each time `f`\nreturns a new value.  Returns a lazy seq of partitions.  Returns a\ntransducer, if collection is not supplied.") end)
    v_33_auto = partition_by0
    core["partition-by"] = v_33_auto
    partition_by = v_33_auto
  end
  local partition_all
  do
    local v_33_auto
    local function partition_all0(...)
      local _571_ = select("#", ...)
      if (_571_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "partition-all"))
      elseif (_571_ == 1) then
        local n = ...
        local function fn_572_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_572_"))
            else
            end
          end
          local a = array()
          local function fn_574_(...)
            local _575_ = select("#", ...)
            if (_575_ == 0) then
              return rf()
            elseif (_575_ == 1) then
              local result = ...
              local function _576_(...)
                if (0 == #a) then
                  return result
                else
                  local v = vec(a)
                  a:clear()
                  return core.unreduced(rf(result, v))
                end
              end
              return rf(_576_(...))
            elseif (_575_ == 2) then
              local result, input = ...
              a:add(input)
              if (n == #a) then
                local v = vec(a)
                a:clear()
                return rf(result, v)
              else
                return result
              end
            elseif true then
              local _ = _575_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_574_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_574_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_574_
        end
        pcall(function() require("fennel").metadata:setall(fn_572_, "fnl/arglist", {"[rf]"}) end)
        return fn_572_
      elseif (_571_ == 2) then
        local n, coll = ...
        return map(seq_2a, lazy["partition-all"](n, coll))
      elseif (_571_ == 3) then
        local n, step, coll = ...
        return map(seq_2a, lazy["partition-all"](n, step, coll))
      elseif true then
        local _ = _571_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "partition-all"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(partition_all0, "fnl/arglist", {"([n])", "([n coll])", "([n step coll])"}, "fnl/docstring", "Given a collection `coll`, returns a lazy sequence of lists like\n`partition`, but may include partitions with fewer than n items at the\nend. Accepts addiitonal `step` argument, similarly to `partition`.\nReturns a transducer, if collection is not supplied.") end)
    v_33_auto = partition_all0
    core["partition-all"] = v_33_auto
    partition_all = v_33_auto
  end
  local reductions
  do
    local v_33_auto
    local function reductions0(...)
      local _581_ = select("#", ...)
      if (_581_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "reductions"))
      elseif (_581_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "reductions"))
      elseif (_581_ == 2) then
        local f, coll = ...
        return seq_2a(lazy.reductions(f, coll))
      elseif (_581_ == 3) then
        local f, init, coll = ...
        return seq_2a(lazy.reductions(f, init, coll))
      elseif true then
        local _ = _581_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "reductions"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(reductions0, "fnl/arglist", {"([f coll])", "([f init coll])"}, "fnl/docstring", "Returns a lazy seq of the intermediate values of the reduction (as\nper reduce) of `coll` by `f`, starting with `init`.") end)
    v_33_auto = reductions0
    core["reductions"] = v_33_auto
    reductions = v_33_auto
  end
  local contains_3f
  do
    local v_33_auto
    local function contains_3f0(...)
      local coll, elt = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "contains?"))
        else
        end
      end
      return lazy["contains?"](coll, elt)
    end
    pcall(function() require("fennel").metadata:setall(contains_3f0, "fnl/arglist", {"[coll elt]"}, "fnl/docstring", "Test if `elt` is in the `coll`.  It may be a linear search depending\non the type of the collection.") end)
    v_33_auto = contains_3f0
    core["contains?"] = v_33_auto
    contains_3f = v_33_auto
  end
  local distinct
  do
    local v_33_auto
    local function distinct0(...)
      local _584_ = select("#", ...)
      if (_584_ == 0) then
        local function fn_585_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_585_"))
            else
            end
          end
          local seen = setmetatable({}, {__index = deep_index})
          local function fn_587_(...)
            local _588_ = select("#", ...)
            if (_588_ == 0) then
              return rf()
            elseif (_588_ == 1) then
              local result = ...
              return rf(result)
            elseif (_588_ == 2) then
              local result, input = ...
              if seen[input] then
                return result
              else
                seen[input] = true
                return rf(result, input)
              end
            elseif true then
              local _ = _588_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_587_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_587_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_587_
        end
        pcall(function() require("fennel").metadata:setall(fn_585_, "fnl/arglist", {"[rf]"}) end)
        return fn_585_
      elseif (_584_ == 1) then
        local coll = ...
        return seq_2a(lazy.distinct(coll))
      elseif true then
        local _ = _584_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "distinct"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(distinct0, "fnl/arglist", {"([])", "([coll])"}, "fnl/docstring", "Returns a lazy sequence of the elements of the `coll` without\nduplicates.  Comparison is done by equality. Returns a transducer when\nno collection is provided.") end)
    v_33_auto = distinct0
    core["distinct"] = v_33_auto
    distinct = v_33_auto
  end
  local dedupe
  do
    local v_33_auto
    local function dedupe0(...)
      local _592_ = select("#", ...)
      if (_592_ == 0) then
        local function fn_593_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_593_"))
            else
            end
          end
          local none = {}
          local pv = none
          local function fn_595_(...)
            local _596_ = select("#", ...)
            if (_596_ == 0) then
              return rf()
            elseif (_596_ == 1) then
              local result = ...
              return rf(result)
            elseif (_596_ == 2) then
              local result, input = ...
              local prior = pv
              pv = input
              if (prior == input) then
                return result
              else
                return rf(result, input)
              end
            elseif true then
              local _ = _596_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_595_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_595_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_595_
        end
        pcall(function() require("fennel").metadata:setall(fn_593_, "fnl/arglist", {"[rf]"}) end)
        return fn_593_
      elseif (_592_ == 1) then
        local coll = ...
        return core.sequence(dedupe0(), coll)
      elseif true then
        local _ = _592_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "dedupe"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(dedupe0, "fnl/arglist", {"([])", "([coll])"}, "fnl/docstring", "Returns a lazy sequence removing consecutive duplicates in coll.\nReturns a transducer when no collection is provided.") end)
    v_33_auto = dedupe0
    core["dedupe"] = v_33_auto
    dedupe = v_33_auto
  end
  local random_sample
  do
    local v_33_auto
    local function random_sample0(...)
      local _600_ = select("#", ...)
      if (_600_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "random-sample"))
      elseif (_600_ == 1) then
        local prob = ...
        local function _601_()
          return (math.random() < prob)
        end
        pcall(function() require("fennel").metadata:setall(_601_, "fnl/arglist", {}) end)
        return filter(_601_)
      elseif (_600_ == 2) then
        local prob, coll = ...
        local function _602_()
          return (math.random() < prob)
        end
        pcall(function() require("fennel").metadata:setall(_602_, "fnl/arglist", {}) end)
        return filter(_602_, coll)
      elseif true then
        local _ = _600_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "random-sample"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(random_sample0, "fnl/arglist", {"([prob])", "([prob coll])"}, "fnl/docstring", "Returns items from `coll` with random probability of `prob` (0.0 -\n1.0).  Returns a transducer when no collection is provided.") end)
    v_33_auto = random_sample0
    core["random-sample"] = v_33_auto
    random_sample = v_33_auto
  end
  local doall
  do
    local v_33_auto
    local function doall0(...)
      local seq0 = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "doall"))
        else
        end
      end
      return seq_2a(lazy.doall(seq0))
    end
    pcall(function() require("fennel").metadata:setall(doall0, "fnl/arglist", {"[seq]"}, "fnl/docstring", "Realize whole lazy sequence `seq`.\n\nWalks whole sequence, realizing each cell.  Use at your own risk on\ninfinite sequences.") end)
    v_33_auto = doall0
    core["doall"] = v_33_auto
    doall = v_33_auto
  end
  local dorun
  do
    local v_33_auto
    local function dorun0(...)
      local seq0 = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "dorun"))
        else
        end
      end
      return lazy.dorun(seq0)
    end
    pcall(function() require("fennel").metadata:setall(dorun0, "fnl/arglist", {"[seq]"}, "fnl/docstring", "Realize whole sequence `seq` for side effects.\n\nWalks whole sequence, realizing each cell.  Use at your own risk on\ninfinite sequences.") end)
    v_33_auto = dorun0
    core["dorun"] = v_33_auto
    dorun = v_33_auto
  end
  local line_seq
  do
    local v_33_auto
    local function line_seq0(...)
      local file = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "line-seq"))
        else
        end
      end
      return seq_2a(lazy["line-seq"](file))
    end
    pcall(function() require("fennel").metadata:setall(line_seq0, "fnl/arglist", {"[file]"}, "fnl/docstring", "Accepts a `file` handle, and creates a lazy sequence of lines using\n`lines` metamethod.\n\n# Examples\n\nLazy sequence of file lines may seem similar to an iterator over a\nfile, but the main difference is that sequence can be shared onve\nrealized, and iterator can't.  Lazy sequence can be consumed in\niterator style with the `doseq` macro.\n\nBear in mind, that since the sequence is lazy it should be realized or\ntruncated before the file is closed:\n\n``` fennel\n(let [lines (with-open [f (io.open \"init.fnl\" :r)]\n              (line-seq f))]\n  ;; this will error because only first line was realized, but the\n  ;; file was closed before the rest of lines were cached\n  (assert-not (pcall next lines)))\n```\n\nSequence is realized with `doall` before file was closed and can be shared:\n\n``` fennel\n(let [lines (with-open [f (io.open \"init.fnl\" :r)]\n              (doall (line-seq f)))]\n  (assert-is (pcall next lines)))\n```\n\nInfinite files can't be fully realized, but can be partially realized\nwith `take`:\n\n``` fennel\n(let [lines (with-open [f (io.open \"/dev/urandom\" :r)]\n              (doall (take 3 (line-seq f))))]\n  (assert-is (pcall next lines)))\n```") end)
    v_33_auto = line_seq0
    core["line-seq"] = v_33_auto
    line_seq = v_33_auto
  end
  local iterate
  do
    local v_33_auto
    local function iterate0(...)
      local f, x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "iterate"))
        else
        end
      end
      return seq_2a(lazy.iterate(f, x))
    end
    pcall(function() require("fennel").metadata:setall(iterate0, "fnl/arglist", {"[f x]"}, "fnl/docstring", "Returns an infinete lazy sequence of x, (f x), (f (f x)) etc.") end)
    v_33_auto = iterate0
    core["iterate"] = v_33_auto
    iterate = v_33_auto
  end
  local remove
  do
    local v_33_auto
    local function remove0(...)
      local _608_ = select("#", ...)
      if (_608_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "remove"))
      elseif (_608_ == 1) then
        local pred = ...
        return filter(complement(pred))
      elseif (_608_ == 2) then
        local pred, coll = ...
        return seq_2a(lazy.remove(pred, coll))
      elseif true then
        local _ = _608_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "remove"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(remove0, "fnl/arglist", {"([pred])", "([pred coll])"}, "fnl/docstring", "Returns a lazy sequence of the items in the `coll` without elements\nfor wich `pred` returns logical true. Returns a transducer when no\ncollection is provided.") end)
    v_33_auto = remove0
    core["remove"] = v_33_auto
    remove = v_33_auto
  end
  local cycle
  do
    local v_33_auto
    local function cycle0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "cycle"))
        else
        end
      end
      return seq_2a(lazy.cycle(coll))
    end
    pcall(function() require("fennel").metadata:setall(cycle0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Create a lazy infinite sequence of repetitions of the items in the\n`coll`.") end)
    v_33_auto = cycle0
    core["cycle"] = v_33_auto
    cycle = v_33_auto
  end
  local _repeat
  do
    local v_33_auto
    local function _repeat0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "repeat"))
        else
        end
      end
      return seq_2a(lazy["repeat"](x))
    end
    pcall(function() require("fennel").metadata:setall(_repeat0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Takes a value `x` and returns an infinite lazy sequence of this value.\n\n# Examples\n\n``` fennel\n(assert-eq 20 (reduce add (take 10 (repeat 2))))\n```") end)
    v_33_auto = _repeat0
    core["repeat"] = v_33_auto
    _repeat = v_33_auto
  end
  local repeatedly
  do
    local v_33_auto
    local function repeatedly0(...)
      local core_48_auto = require("init")
      local _let_612_ = core_48_auto.list(...)
      local f = _let_612_[1]
      local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_612_, 2)
      return seq_2a(apply(lazy.repeatedly, f, args))
    end
    pcall(function() require("fennel").metadata:setall(repeatedly0, "fnl/arglist", {"[f & args]"}, "fnl/docstring", "Takes a function `f` and returns an infinite lazy sequence of\nfunction applications.  Rest arguments are passed to the function.") end)
    v_33_auto = repeatedly0
    core["repeatedly"] = v_33_auto
    repeatedly = v_33_auto
  end
  local tree_seq
  do
    local v_33_auto
    local function tree_seq0(...)
      local branch_3f, children, root = ...
      do
        local cnt_68_auto = select("#", ...)
        if (3 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "tree-seq"))
        else
        end
      end
      return seq_2a(lazy["tree-seq"](branch_3f, children, root))
    end
    pcall(function() require("fennel").metadata:setall(tree_seq0, "fnl/arglist", {"[branch? children root]"}, "fnl/docstring", "Returns a lazy sequence of the nodes in a tree, via a depth-first walk.\n\n`branch?` must be a function of one arg that returns true if passed a\nnode that can have children (but may not).  `children` must be a\nfunction of one arg that returns a sequence of the children.  Will\nonly be called on nodes for which `branch?` returns true.  `root` is\nthe root node of the tree.\n\n# Examples\n\nFor the given tree `[\"A\" [\"B\" [\"D\"] [\"E\"]] [\"C\" [\"F\"]]]`:\n\n        A\n       / \\\n      B   C\n     / \\   \\\n    D   E   F\n\nCalling `tree-seq` with `next` as the `branch?` and `rest` as the\n`children` returns a flat representation of a tree:\n\n``` fennel\n(assert-eq (map first (tree-seq next rest [\"A\" [\"B\" [\"D\"] [\"E\"]] [\"C\" [\"F\"]]]))\n           [\"A\" \"B\" \"D\" \"E\" \"C\" \"F\"])\n```") end)
    v_33_auto = tree_seq0
    core["tree-seq"] = v_33_auto
    tree_seq = v_33_auto
  end
  local interleave
  do
    local v_33_auto
    local function interleave0(...)
      local _614_ = select("#", ...)
      if (_614_ == 0) then
        return seq_2a(lazy.interleave())
      elseif (_614_ == 1) then
        local s = ...
        return seq_2a(lazy.interleave(s))
      elseif (_614_ == 2) then
        local s1, s2 = ...
        return seq_2a(lazy.interleave(s1, s2))
      elseif true then
        local _ = _614_
        local core_48_auto = require("init")
        local _let_615_ = core_48_auto.list(...)
        local s1 = _let_615_[1]
        local s2 = _let_615_[2]
        local ss = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_615_, 3)
        return seq_2a(apply(lazy.interleave, s1, s2, ss))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(interleave0, "fnl/arglist", {"([])", "([s])", "([s1 s2])", "([s1 s2 & ss])"}, "fnl/docstring", "Returns a lazy sequence of the first item in each sequence, then the\nsecond one, until any sequence exhausts.") end)
    v_33_auto = interleave0
    core["interleave"] = v_33_auto
    interleave = v_33_auto
  end
  local interpose
  do
    local v_33_auto
    local function interpose0(...)
      local _617_ = select("#", ...)
      if (_617_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "interpose"))
      elseif (_617_ == 1) then
        local sep = ...
        local function fn_618_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_618_"))
            else
            end
          end
          local started = false
          local function fn_620_(...)
            local _621_ = select("#", ...)
            if (_621_ == 0) then
              return rf()
            elseif (_621_ == 1) then
              local result = ...
              return rf(result)
            elseif (_621_ == 2) then
              local result, input = ...
              if started then
                local sepr = rf(result, sep)
                if core["reduced?"](sepr) then
                  return sepr
                else
                  return rf(sepr, input)
                end
              else
                started = true
                return rf(result, input)
              end
            elseif true then
              local _ = _621_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_620_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_620_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_620_
        end
        pcall(function() require("fennel").metadata:setall(fn_618_, "fnl/arglist", {"[rf]"}) end)
        return fn_618_
      elseif (_617_ == 2) then
        local separator, coll = ...
        return seq_2a(lazy.interpose(separator, coll))
      elseif true then
        local _ = _617_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "interpose"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(interpose0, "fnl/arglist", {"([sep])", "([separator coll])"}, "fnl/docstring", "Returns a lazy sequence of the elements of `coll` separated by\n`separator`. Returns a transducer when no collection is provided.") end)
    v_33_auto = interpose0
    core["interpose"] = v_33_auto
    interpose = v_33_auto
  end
  local halt_when
  do
    local v_33_auto
    local function halt_when0(...)
      local _626_ = select("#", ...)
      if (_626_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "halt-when"))
      elseif (_626_ == 1) then
        local pred = ...
        return halt_when0(pred, nil)
      elseif (_626_ == 2) then
        local pred, retf = ...
        local function fn_627_(...)
          local rf = ...
          do
            local cnt_68_auto = select("#", ...)
            if (1 ~= cnt_68_auto) then
              error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_627_"))
            else
            end
          end
          local halt
          local function _629_()
            return "#<halt>"
          end
          halt = setmetatable({}, {__fennelview = _629_})
          local function fn_630_(...)
            local _631_ = select("#", ...)
            if (_631_ == 0) then
              return rf()
            elseif (_631_ == 1) then
              local result = ...
              if (map_3f(result) and contains_3f(result, halt)) then
                return result.value
              else
                return rf(result)
              end
            elseif (_631_ == 2) then
              local result, input = ...
              if pred(input) then
                local _633_
                if retf then
                  _633_ = retf(rf(result), input)
                else
                  _633_ = input
                end
                return core.reduced({[halt] = true, value = _633_})
              else
                return rf(result, input)
              end
            elseif true then
              local _ = _631_
              return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_630_"))
            else
              return nil
            end
          end
          pcall(function() require("fennel").metadata:setall(fn_630_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
          return fn_630_
        end
        pcall(function() require("fennel").metadata:setall(fn_627_, "fnl/arglist", {"[rf]"}) end)
        return fn_627_
      elseif true then
        local _ = _626_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "halt-when"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(halt_when0, "fnl/arglist", {"([pred])", "([pred retf])"}, "fnl/docstring", "Returns a transducer that ends transduction when `pred` returns `true`\nfor an input. When `retf` is supplied it must be a `fn` of 2 arguments\n- it will be passed the (completed) result so far and the input that\ntriggered the predicate, and its return value (if it does not throw an\nexception) will be the return value of the transducer. If `retf` is\nnot supplied, the input that triggered the predicate will be\nreturned. If the predicate never returns `true` the transduction is\nunaffected.") end)
    v_33_auto = halt_when0
    core["halt-when"] = v_33_auto
    halt_when = v_33_auto
  end
  local realized_3f
  do
    local v_33_auto
    local function realized_3f0(...)
      local s = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "realized?"))
        else
        end
      end
      return lazy["realized?"](s)
    end
    pcall(function() require("fennel").metadata:setall(realized_3f0, "fnl/arglist", {"[s]"}, "fnl/docstring", "Check if sequence's first element is realized.") end)
    v_33_auto = realized_3f0
    core["realized?"] = v_33_auto
    realized_3f = v_33_auto
  end
  local keys
  do
    local v_33_auto
    local function keys0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "keys"))
        else
        end
      end
      assert((map_3f(coll) or empty_3f(coll)), "expected a map")
      if empty_3f(coll) then
        return lazy.list()
      else
        return lazy.keys(coll)
      end
    end
    pcall(function() require("fennel").metadata:setall(keys0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a sequence of the map's keys, in the same order as `seq`.") end)
    v_33_auto = keys0
    core["keys"] = v_33_auto
    keys = v_33_auto
  end
  local vals
  do
    local v_33_auto
    local function vals0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "vals"))
        else
        end
      end
      assert((map_3f(coll) or empty_3f(coll)), "expected a map")
      if empty_3f(coll) then
        return lazy.list()
      else
        return lazy.vals(coll)
      end
    end
    pcall(function() require("fennel").metadata:setall(vals0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a sequence of the table's values, in the same order as `seq`.") end)
    v_33_auto = vals0
    core["vals"] = v_33_auto
    vals = v_33_auto
  end
  local find
  do
    local v_33_auto
    local function find0(...)
      local coll, key = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "find"))
        else
        end
      end
      assert((map_3f(coll) or empty_3f(coll)), "expected a map")
      local _644_ = coll[key]
      if (nil ~= _644_) then
        local v = _644_
        return {key, v}
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(find0, "fnl/arglist", {"[coll key]"}, "fnl/docstring", "Returns the map entry for `key`, or `nil` if key is not present in\n`coll`.") end)
    v_33_auto = find0
    core["find"] = v_33_auto
    find = v_33_auto
  end
  local sort
  do
    local v_33_auto
    local function sort0(...)
      local _646_ = select("#", ...)
      if (_646_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "sort"))
      elseif (_646_ == 1) then
        local coll = ...
        local _647_ = seq(coll)
        if (nil ~= _647_) then
          local s = _647_
          return seq(itable.sort(vec(s)))
        elseif true then
          local _ = _647_
          return list()
        else
          return nil
        end
      elseif (_646_ == 2) then
        local comparator, coll = ...
        local _649_ = seq(coll)
        if (nil ~= _649_) then
          local s = _649_
          return seq(itable.sort(vec(s), comparator))
        elseif true then
          local _ = _649_
          return list()
        else
          return nil
        end
      elseif true then
        local _ = _646_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "sort"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(sort0, "fnl/arglist", {"([coll])", "([comparator coll])"}, "fnl/docstring", "Returns a sorted sequence of the items in `coll`. If no `comparator`\nis supplied, uses `<`.") end)
    v_33_auto = sort0
    core["sort"] = v_33_auto
    sort = v_33_auto
  end
  local reduce
  do
    local v_33_auto
    local function reduce0(...)
      local _653_ = select("#", ...)
      if (_653_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "reduce"))
      elseif (_653_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "reduce"))
      elseif (_653_ == 2) then
        local f, coll = ...
        return lazy.reduce(f, seq(coll))
      elseif (_653_ == 3) then
        local f, val, coll = ...
        return lazy.reduce(f, val, seq(coll))
      elseif true then
        local _ = _653_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "reduce"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(reduce0, "fnl/arglist", {"([f coll])", "([f val coll])"}, "fnl/docstring", "`f` should be a function of 2 arguments. If `val` is not supplied,\nreturns the result of applying `f` to the first 2 items in `coll`,\nthen applying `f` to that result and the 3rd item, etc. If `coll`\ncontains no items, f must accept no arguments as well, and reduce\nreturns the result of calling `f` with no arguments.  If `coll` has\nonly 1 item, it is returned and `f` is not called.  If `val` is\nsupplied, returns the result of applying `f` to `val` and the first\nitem in `coll`, then applying `f` to that result and the 2nd item,\netc. If `coll` contains no items, returns `val` and `f` is not\ncalled. Early termination is supported via `reduced`.\n\n# Examples\n\n``` fennel\n(defn- add\n  ([] 0)\n  ([a] a)\n  ([a b] (+ a b))\n  ([a b & cs] (apply add (+ a b) cs)))\n;; no initial value\n(assert-eq 10 (reduce add [1 2 3 4]))\n;; initial value\n(assert-eq 10 (reduce add 1 [2 3 4]))\n;; empty collection - function is called with 0 args\n(assert-eq 0 (reduce add []))\n(assert-eq 10.3 (reduce math.floor 10.3 []))\n;; collection with a single element doesn't call a function unless the\n;; initial value is supplied\n(assert-eq 10.3 (reduce math.floor [10.3]))\n(assert-eq 7 (reduce add 3 [4]))\n```") end)
    v_33_auto = reduce0
    core["reduce"] = v_33_auto
    reduce = v_33_auto
  end
  local reduced
  do
    local v_33_auto
    local function reduced0(...)
      local value = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "reduced"))
        else
        end
      end
      local _656_ = lazy.reduced(value)
      local function _657_(_241)
        return _241.value
      end
      getmetatable(_656_)["cljlib/deref"] = _657_
      return _656_
    end
    pcall(function() require("fennel").metadata:setall(reduced0, "fnl/arglist", {"[value]"}, "fnl/docstring", "Terminates the `reduce` early with a given `value`.\n\n# Examples\n\n``` fennel\n(assert-eq :NaN\n           (reduce (fn [acc x]\n                     (if (not= :number (type x))\n                         (reduced :NaN)\n                         (+ acc x)))\n                   [1 2 :3 4 5]))\n```") end)
    v_33_auto = reduced0
    core["reduced"] = v_33_auto
    reduced = v_33_auto
  end
  local reduced_3f
  do
    local v_33_auto
    local function reduced_3f0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "reduced?"))
        else
        end
      end
      return lazy["reduced?"](x)
    end
    pcall(function() require("fennel").metadata:setall(reduced_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Returns true if `x` is the result of a call to reduced") end)
    v_33_auto = reduced_3f0
    core["reduced?"] = v_33_auto
    reduced_3f = v_33_auto
  end
  local unreduced
  do
    local v_33_auto
    local function unreduced0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "unreduced"))
        else
        end
      end
      if reduced_3f(x) then
        return deref(x)
      else
        return x
      end
    end
    pcall(function() require("fennel").metadata:setall(unreduced0, "fnl/arglist", {"[x]"}, "fnl/docstring", "If `x` is `reduced?`, returns `(deref x)`, else returns `x`.") end)
    v_33_auto = unreduced0
    core["unreduced"] = v_33_auto
    unreduced = v_33_auto
  end
  local ensure_reduced
  do
    local v_33_auto
    local function ensure_reduced0(...)
      local x = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "ensure-reduced"))
        else
        end
      end
      if reduced_3f(x) then
        return x
      else
        return reduced(x)
      end
    end
    pcall(function() require("fennel").metadata:setall(ensure_reduced0, "fnl/arglist", {"[x]"}, "fnl/docstring", "If x is already reduced?, returns it, else returns (reduced x)") end)
    v_33_auto = ensure_reduced0
    core["ensure-reduced"] = v_33_auto
    ensure_reduced = v_33_auto
  end
  local preserving_reduced
  local function preserving_reduced0(...)
    local rf = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "preserving-reduced"))
      else
      end
    end
    local function fn_664_(...)
      local a, b = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_664_"))
        else
        end
      end
      local ret = rf(a, b)
      if reduced_3f(ret) then
        return reduced(ret)
      else
        return ret
      end
    end
    pcall(function() require("fennel").metadata:setall(fn_664_, "fnl/arglist", {"[a b]"}) end)
    return fn_664_
  end
  pcall(function() require("fennel").metadata:setall(preserving_reduced0, "fnl/arglist", {"[rf]"}) end)
  preserving_reduced = preserving_reduced0
  local cat
  do
    local v_33_auto
    local function cat0(...)
      local rf = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "cat"))
        else
        end
      end
      local rrf = preserving_reduced(rf)
      local function fn_668_(...)
        local _669_ = select("#", ...)
        if (_669_ == 0) then
          return rf()
        elseif (_669_ == 1) then
          local result = ...
          return rf(result)
        elseif (_669_ == 2) then
          local result, input = ...
          return reduce(rrf, result, input)
        elseif true then
          local _ = _669_
          return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_668_"))
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(fn_668_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
      return fn_668_
    end
    pcall(function() require("fennel").metadata:setall(cat0, "fnl/arglist", {"[rf]"}, "fnl/docstring", "A transducer which concatenates the contents of each input, which must be a\n  collection, into the reduction. Accepts the reducing function `rf`.") end)
    v_33_auto = cat0
    core["cat"] = v_33_auto
    cat = v_33_auto
  end
  local reduce_kv
  do
    local v_33_auto
    local function reduce_kv0(...)
      local f, val, s = ...
      do
        local cnt_68_auto = select("#", ...)
        if (3 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "reduce-kv"))
        else
        end
      end
      if map_3f(s) then
        local function _674_(res, _672_)
          local _arg_673_ = _672_
          local k = _arg_673_[1]
          local v = _arg_673_[2]
          return f(res, k, v)
        end
        pcall(function() require("fennel").metadata:setall(_674_, "fnl/arglist", {"res", "[k v]"}) end)
        return reduce(_674_, val, seq(s))
      else
        local function _677_(res, _675_)
          local _arg_676_ = _675_
          local k = _arg_676_[1]
          local v = _arg_676_[2]
          return f(res, k, v)
        end
        pcall(function() require("fennel").metadata:setall(_677_, "fnl/arglist", {"res", "[k v]"}) end)
        return reduce(_677_, val, map(vector, drop(1, range()), seq(s)))
      end
    end
    pcall(function() require("fennel").metadata:setall(reduce_kv0, "fnl/arglist", {"[f val s]"}, "fnl/docstring", "Reduces an associative table using function `f` and initial value `val`.\n\n`f` should be a function of 3 arguments.  Returns the result of\napplying `f` to `val`, the first key and the first value in `tbl`,\nthen applying `f` to that result and the 2nd key and value, etc.  If\n`tbl` contains no entries, returns `val` and `f` is not called.  Note\nthat `reduce-kv` is supported on sequential tables and strings, where\nthe keys will be the ordinals.\n\nEarly termination is possible with the use of `reduced`\nfunction.\n\n# Examples\nReduce associative table by adding values from all keys:\n\n``` fennel\n(local t {:a1 1\n          :b1 2\n          :a2 2\n          :b2 3})\n\n(reduce-kv #(+ $1 $3) 0 t)\n;; => 8\n```\n\nReduce table by adding values from keys that start with letter `a`:\n\n``` fennel\n(local t {:a1 1\n          :b1 2\n          :a2 2\n          :b2 3})\n\n(reduce-kv (fn [res k v] (if (= (string.sub k 1 1) :a) (+ res v) res))\n           0 t)\n;; => 3\n```") end)
    v_33_auto = reduce_kv0
    core["reduce-kv"] = v_33_auto
    reduce_kv = v_33_auto
  end
  local completing
  do
    local v_33_auto
    local function completing0(...)
      local _679_ = select("#", ...)
      if (_679_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "completing"))
      elseif (_679_ == 1) then
        local f = ...
        return completing0(f, identity)
      elseif (_679_ == 2) then
        local f, cf = ...
        local function fn_680_(...)
          local _681_ = select("#", ...)
          if (_681_ == 0) then
            return f()
          elseif (_681_ == 1) then
            local x = ...
            return cf(x)
          elseif (_681_ == 2) then
            local x, y = ...
            return f(x, y)
          elseif true then
            local _ = _681_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_680_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_680_, "fnl/arglist", {"([])", "([x])", "([x y])"}) end)
        return fn_680_
      elseif true then
        local _ = _679_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "completing"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(completing0, "fnl/arglist", {"([f])", "([f cf])"}, "fnl/docstring", "Takes a reducing function `f` of 2 args and returns a function\nsuitable for transduce by adding an arity-1 signature that calls\n`cf` (default - `identity`) on the result argument.") end)
    v_33_auto = completing0
    core["completing"] = v_33_auto
    completing = v_33_auto
  end
  local transduce
  do
    local v_33_auto
    local function transduce0(...)
      local _687_ = select("#", ...)
      if (_687_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "transduce"))
      elseif (_687_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "transduce"))
      elseif (_687_ == 2) then
        return error(("Wrong number of args (%s) passed to %s"):format(2, "transduce"))
      elseif (_687_ == 3) then
        local xform, f, coll = ...
        return transduce0(xform, f, f(), coll)
      elseif (_687_ == 4) then
        local xform, f, init, coll = ...
        local f0 = xform(f)
        return f0(reduce(f0, init, seq(coll)))
      elseif true then
        local _ = _687_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "transduce"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(transduce0, "fnl/arglist", {"([xform f coll])", "([xform f init coll])"}, "fnl/docstring", "`reduce` with a transformation of `f` (`xform`). If `init` is not\nsupplied, `f` will be called to produce it. `f` should be a reducing\nstep function that accepts both 1 and 2 arguments, if it accepts only\n2 you can add the arity-1 with `completing`. Returns the result of\napplying (the transformed) `xform` to `init` and the first item in\n`coll`, then applying `xform` to that result and the 2nd item, etc. If\n`coll` contains no items, returns `init` and `f` is not called. Note\nthat certain transforms may inject or skip items.") end)
    v_33_auto = transduce0
    core["transduce"] = v_33_auto
    transduce = v_33_auto
  end
  local sequence
  do
    local v_33_auto
    local function sequence0(...)
      local _689_ = select("#", ...)
      if (_689_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "sequence"))
      elseif (_689_ == 1) then
        local coll = ...
        if seq_3f(coll) then
          return coll
        else
          return (seq(coll) or list())
        end
      elseif (_689_ == 2) then
        local xform, coll = ...
        local f
        local function _691_(_241, _242)
          return cons(_242, _241)
        end
        f = xform(completing(_691_))
        local function step(coll0)
          local val_106_auto = seq(coll0)
          if (nil ~= val_106_auto) then
            local s = val_106_auto
            local res = f(nil, first(s))
            if reduced_3f(res) then
              return f(deref(res))
            elseif seq_3f(res) then
              local function _692_()
                return step(rest(s))
              end
              return concat(res, lazy_seq(_692_))
            elseif "else" then
              return step(rest(s))
            else
              return nil
            end
          else
            return f(nil)
          end
        end
        pcall(function() require("fennel").metadata:setall(step, "fnl/arglist", {"coll"}) end)
        return (step(coll) or list())
      elseif true then
        local _ = _689_
        local core_48_auto = require("init")
        local _let_695_ = core_48_auto.list(...)
        local xform = _let_695_[1]
        local coll = _let_695_[2]
        local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_695_, 3)
        local f
        local function _696_(_241, _242)
          return cons(_242, _241)
        end
        f = xform(completing(_696_))
        local function step(colls0)
          if every_3f(seq, colls0) then
            local res = apply(f, nil, map(first, colls0))
            if reduced_3f(res) then
              return f(deref(res))
            elseif seq_3f(res) then
              local function _697_()
                return step(map(rest, colls0))
              end
              return concat(res, lazy_seq(_697_))
            elseif "else" then
              return step(map(rest, colls0))
            else
              return nil
            end
          else
            return f(nil)
          end
        end
        pcall(function() require("fennel").metadata:setall(step, "fnl/arglist", {"colls"}) end)
        return (step(cons(coll, colls)) or list())
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(sequence0, "fnl/arglist", {"([coll])", "([xform coll])", "([xform coll & colls])"}, "fnl/docstring", "Coerces coll to a (possibly empty) sequence, if it is not already\none. Will not force a lazy seq. `(sequence nil)` yields an empty list,\nWhen a transducer `xform` is supplied, returns a lazy sequence of\napplications of the transform to the items in `coll`, i.e. to the set\nof first items of each `coll`, followed by the set of second items in\neach `coll`, until any one of the `colls` is exhausted.  Any remaining\nitems in other `colls` are ignored. The transform should accept\nnumber-of-colls arguments") end)
    v_33_auto = sequence0
    core["sequence"] = v_33_auto
    sequence = v_33_auto
  end
  local function map__3etransient(immutable)
    local function _701_(map0)
      local removed = setmetatable({}, {__index = deep_index})
      local function _702_(_, k)
        if not removed[k] then
          return (map0)[k]
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(_702_, "fnl/arglist", {"_", "k"}) end)
      local function _704_()
        return error("can't `conj` onto transient map, use `conj!`")
      end
      local function _705_()
        return error("can't `assoc` onto transient map, use `assoc!`")
      end
      local function _706_()
        return error("can't `dissoc` onto transient map, use `dissoc!`")
      end
      local function _709_(tmap, _707_)
        local _arg_708_ = _707_
        local k = _arg_708_[1]
        local v = _arg_708_[2]
        if (nil == v) then
          removed[k] = true
        else
          removed[k] = nil
        end
        tmap[k] = v
        return tmap
      end
      pcall(function() require("fennel").metadata:setall(_709_, "fnl/arglist", {"tmap", "[k v]"}) end)
      local function _711_(tmap, ...)
        for i = 1, select("#", ...), 2 do
          local k, v = select(i, ...)
          do end (tmap)[k] = v
          if (nil == v) then
            removed[k] = true
          else
            removed[k] = nil
          end
        end
        return tmap
      end
      pcall(function() require("fennel").metadata:setall(_711_, "fnl/arglist", {"tmap", "..."}) end)
      local function _713_(tmap, ...)
        for i = 1, select("#", ...) do
          local k = select(i, ...)
          do end (tmap)[k] = nil
          removed[k] = true
        end
        return tmap
      end
      pcall(function() require("fennel").metadata:setall(_713_, "fnl/arglist", {"tmap", "..."}) end)
      local function _714_(tmap)
        local t
        do
          local tbl_14_auto
          do
            local tbl_14_auto0 = {}
            for k, v in pairs(map0) do
              local k_15_auto, v_16_auto = k, v
              if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
                tbl_14_auto0[k_15_auto] = v_16_auto
              else
              end
            end
            tbl_14_auto = tbl_14_auto0
          end
          for k, v in pairs(tmap) do
            local k_15_auto, v_16_auto = k, v
            if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
              tbl_14_auto[k_15_auto] = v_16_auto
            else
            end
          end
          t = tbl_14_auto
        end
        for k in pairs(removed) do
          t[k] = nil
        end
        local function _717_()
          local tbl_17_auto = {}
          local i_18_auto = #tbl_17_auto
          for k in pairs_2a(tmap) do
            local val_19_auto = k
            if (nil ~= val_19_auto) then
              i_18_auto = (i_18_auto + 1)
              do end (tbl_17_auto)[i_18_auto] = val_19_auto
            else
            end
          end
          return tbl_17_auto
        end
        for _, k in ipairs(_717_()) do
          tmap[k] = nil
        end
        local function _719_()
          return error("attempt to use transient after it was persistet")
        end
        local function _720_()
          return error("attempt to use transient after it was persistet")
        end
        setmetatable(tmap, {__index = _719_, __newindex = _720_})
        return immutable(itable(t))
      end
      pcall(function() require("fennel").metadata:setall(_714_, "fnl/arglist", {"tmap"}) end)
      return setmetatable({}, {__index = _702_, ["cljlib/type"] = "transient", ["cljlib/conj"] = _704_, ["cljlib/assoc"] = _705_, ["cljlib/dissoc"] = _706_, ["cljlib/conj!"] = _709_, ["cljlib/assoc!"] = _711_, ["cljlib/dissoc!"] = _713_, ["cljlib/persistent!"] = _714_})
    end
    pcall(function() require("fennel").metadata:setall(_701_, "fnl/arglist", {"map"}) end)
    return _701_
  end
  pcall(function() require("fennel").metadata:setall(map__3etransient, "fnl/arglist", {"immutable"}) end)
  local function hash_map_2a(x)
    do
      local _721_ = getmetatable(x)
      if (nil ~= _721_) then
        local mt = _721_
        mt["cljlib/type"] = "hash-map"
        mt["cljlib/editable"] = true
        local function _724_(t, _722_, ...)
          local _arg_723_ = _722_
          local k = _arg_723_[1]
          local v = _arg_723_[2]
          local function _725_(...)
            local kvs = {}
            for _, _726_ in ipairs_2a({...}) do
              local _each_727_ = _726_
              local k0 = _each_727_[1]
              local v0 = _each_727_[2]
              table.insert(kvs, k0)
              table.insert(kvs, v0)
              kvs = kvs
            end
            return kvs
          end
          return apply(core.assoc, t, k, v, _725_(...))
        end
        pcall(function() require("fennel").metadata:setall(_724_, "fnl/arglist", {"t", "[k v]", "..."}) end)
        do end (mt)["cljlib/conj"] = _724_
        mt["cljlib/transient"] = map__3etransient(hash_map_2a)
        local function _728_()
          return hash_map_2a(itable({}))
        end
        mt["cljlib/empty"] = _728_
      elseif true then
        local _ = _721_
        hash_map_2a(setmetatable(x, {}))
      else
      end
    end
    return x
  end
  pcall(function() require("fennel").metadata:setall(hash_map_2a, "fnl/arglist", {"x"}, "fnl/docstring", "Add cljlib hash-map meta-info.") end)
  local assoc
  do
    local v_33_auto
    local function assoc0(...)
      local _732_ = select("#", ...)
      if (_732_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "assoc"))
      elseif (_732_ == 1) then
        local tbl = ...
        return hash_map_2a(itable({}))
      elseif (_732_ == 2) then
        return error(("Wrong number of args (%s) passed to %s"):format(2, "assoc"))
      elseif (_732_ == 3) then
        local tbl, k, v = ...
        assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map")
        assert(not nil_3f(k), "attempt to use nil as key")
        return hash_map_2a(itable.assoc((tbl or {}), k, v))
      elseif true then
        local _ = _732_
        local core_48_auto = require("init")
        local _let_733_ = core_48_auto.list(...)
        local tbl = _let_733_[1]
        local k = _let_733_[2]
        local v = _let_733_[3]
        local kvs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_733_, 4)
        assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map")
        assert(not nil_3f(k), "attempt to use nil as key")
        return hash_map_2a(apply(itable.assoc, (tbl or {}), k, v, kvs))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(assoc0, "fnl/arglist", {"([tbl])", "([tbl k v])", "([tbl k v & kvs])"}, "fnl/docstring", "Associate `val` under a `key`.\nAccepts extra keys and values.\n\n# Examples\n\n``` fennel\n(assert-eq {:a 1 :b 2} (assoc {:a 1} :b 2))\n(assert-eq {:a 1 :b 2} (assoc {:a 1 :b 1} :b 2))\n(assert-eq {:a 1 :b 2 :c 3} (assoc {:a 1 :b 1} :b 2 :c 3))\n```") end)
    v_33_auto = assoc0
    core["assoc"] = v_33_auto
    assoc = v_33_auto
  end
  local assoc_in
  do
    local v_33_auto
    local function assoc_in0(...)
      local tbl, key_seq, val = ...
      do
        local cnt_68_auto = select("#", ...)
        if (3 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "assoc-in"))
        else
        end
      end
      assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map or nil")
      return hash_map_2a(itable["assoc-in"](tbl, key_seq, val))
    end
    pcall(function() require("fennel").metadata:setall(assoc_in0, "fnl/arglist", {"[tbl key-seq val]"}, "fnl/docstring", "Associate `val` into set of immutable nested tables `t`, via given `key-seq`.\nReturns a new immutable table.  Returns a new immutable table.\n\n# Examples\n\nReplace value under nested keys:\n\n``` fennel\n(assert-eq\n {:a {:b {:c 1}}}\n (assoc-in {:a {:b {:c 0}}} [:a :b :c] 1))\n```\n\nCreate new entries as you go:\n\n``` fennel\n(assert-eq\n {:a {:b {:c 1}} :e 2}\n (assoc-in {:e 2} [:a :b :c] 1))\n```") end)
    v_33_auto = assoc_in0
    core["assoc-in"] = v_33_auto
    assoc_in = v_33_auto
  end
  local update
  do
    local v_33_auto
    local function update0(...)
      local tbl, key, f = ...
      do
        local cnt_68_auto = select("#", ...)
        if (3 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "update"))
        else
        end
      end
      assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map")
      return hash_map_2a(itable.update(tbl, key, f))
    end
    pcall(function() require("fennel").metadata:setall(update0, "fnl/arglist", {"[tbl key f]"}, "fnl/docstring", "Update table value stored under `key` by calling a function `f` on\nthat value. `f` must take one argument, which will be a value stored\nunder the key in the table.\n\n# Examples\n\nSame as `assoc` but accepts function to produce new value based on key value.\n\n``` fennel\n(assert-eq\n {:data \"THIS SHOULD BE UPPERCASE\"}\n (update {:data \"this should be uppercase\"} :data string.upper))\n```") end)
    v_33_auto = update0
    core["update"] = v_33_auto
    update = v_33_auto
  end
  local update_in
  do
    local v_33_auto
    local function update_in0(...)
      local tbl, key_seq, f = ...
      do
        local cnt_68_auto = select("#", ...)
        if (3 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "update-in"))
        else
        end
      end
      assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map or nil")
      return hash_map_2a(itable["update-in"](tbl, key_seq, f))
    end
    pcall(function() require("fennel").metadata:setall(update_in0, "fnl/arglist", {"[tbl key-seq f]"}, "fnl/docstring", "Update table value stored under set of immutable nested tables, via\ngiven `key-seq` by calling a function `f` on the value stored under the\nlast key.  `f` must take one argument, which will be a value stored\nunder the key in the table.  Returns a new immutable table.\n\n# Examples\n\nSame as `assoc-in` but accepts function to produce new value based on key value.\n\n``` fennel\n(fn capitalize-words [s]\n  (pick-values 1\n    (s:gsub \"(%a)([%w_`]*)\" #(.. ($1:upper) ($2:lower)))))\n\n(assert-eq\n {:user {:name \"John Doe\"}}\n (update-in {:user {:name \"john doe\"}} [:user :name] capitalize-words))\n```") end)
    v_33_auto = update_in0
    core["update-in"] = v_33_auto
    update_in = v_33_auto
  end
  local hash_map
  do
    local v_33_auto
    local function hash_map0(...)
      local core_48_auto = require("init")
      local _let_738_ = core_48_auto.list(...)
      local kvs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_738_, 1)
      return apply(assoc, {}, kvs)
    end
    pcall(function() require("fennel").metadata:setall(hash_map0, "fnl/arglist", {"[& kvs]"}, "fnl/docstring", "Create associative table from `kvs` represented as sequence of keys\nand values") end)
    v_33_auto = hash_map0
    core["hash-map"] = v_33_auto
    hash_map = v_33_auto
  end
  local get
  do
    local v_33_auto
    local function get0(...)
      local _740_ = select("#", ...)
      if (_740_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "get"))
      elseif (_740_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "get"))
      elseif (_740_ == 2) then
        local tbl, key = ...
        return get0(tbl, key, nil)
      elseif (_740_ == 3) then
        local tbl, key, not_found = ...
        assert((map_3f(tbl) or empty_3f(tbl)), "expected a map")
        return (tbl[key] or not_found)
      elseif true then
        local _ = _740_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "get"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(get0, "fnl/arglist", {"([tbl key])", "([tbl key not-found])"}, "fnl/docstring", "Get value from the table by accessing it with a `key`.\nAccepts additional `not-found` as a marker to return if value wasn't\nfound in the table.") end)
    v_33_auto = get0
    core["get"] = v_33_auto
    get = v_33_auto
  end
  local get_in
  do
    local v_33_auto
    local function get_in0(...)
      local _743_ = select("#", ...)
      if (_743_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "get-in"))
      elseif (_743_ == 1) then
        return error(("Wrong number of args (%s) passed to %s"):format(1, "get-in"))
      elseif (_743_ == 2) then
        local tbl, keys0 = ...
        return get_in0(tbl, keys0, nil)
      elseif (_743_ == 3) then
        local tbl, keys0, not_found = ...
        assert((map_3f(tbl) or empty_3f(tbl)), "expected a map")
        local res, t, done = tbl, tbl, nil
        for _, k in ipairs_2a(keys0) do
          if done then break end
          local _744_ = t[k]
          if (nil ~= _744_) then
            local v = _744_
            res, t = v, v
          elseif true then
            local _0 = _744_
            res, done = not_found, true
          else
          end
        end
        return res
      elseif true then
        local _ = _743_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "get-in"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(get_in0, "fnl/arglist", {"([tbl keys])", "([tbl keys not-found])"}, "fnl/docstring", "Get value from nested set of tables by providing key sequence.\nAccepts additional `not-found` as a marker to return if value wasn't\nfound in the table.") end)
    v_33_auto = get_in0
    core["get-in"] = v_33_auto
    get_in = v_33_auto
  end
  local dissoc
  do
    local v_33_auto
    local function dissoc0(...)
      local _747_ = select("#", ...)
      if (_747_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "dissoc"))
      elseif (_747_ == 1) then
        local tbl = ...
        return tbl
      elseif (_747_ == 2) then
        local tbl, key = ...
        assert((map_3f(tbl) or empty_3f(tbl)), "expected a map")
        local function _748_(...)
          tbl[key] = nil
          return tbl
        end
        return hash_map_2a(_748_(...))
      elseif true then
        local _ = _747_
        local core_48_auto = require("init")
        local _let_749_ = core_48_auto.list(...)
        local tbl = _let_749_[1]
        local key = _let_749_[2]
        local keys0 = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_749_, 3)
        return apply(dissoc0, dissoc0(tbl, key), keys0)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(dissoc0, "fnl/arglist", {"([tbl])", "([tbl key])", "([tbl key & keys])"}, "fnl/docstring", "Remove `key` from table `tbl`.  Optionally takes more `keys`.") end)
    v_33_auto = dissoc0
    core["dissoc"] = v_33_auto
    dissoc = v_33_auto
  end
  local merge
  do
    local v_33_auto
    local function merge0(...)
      local core_48_auto = require("init")
      local _let_751_ = core_48_auto.list(...)
      local maps = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_751_, 1)
      if some(identity, maps) then
        local function _752_(a, b)
          local tbl_14_auto = a
          for k, v in pairs_2a(b) do
            local k_15_auto, v_16_auto = k, v
            if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
              tbl_14_auto[k_15_auto] = v_16_auto
            else
            end
          end
          return tbl_14_auto
        end
        pcall(function() require("fennel").metadata:setall(_752_, "fnl/arglist", {"a", "b"}) end)
        return hash_map_2a(itable(reduce(_752_, {}, maps)))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(merge0, "fnl/arglist", {"[& maps]"}, "fnl/docstring", "Merge `maps` rght to left into a single hash-map.") end)
    v_33_auto = merge0
    core["merge"] = v_33_auto
    merge = v_33_auto
  end
  local frequencies
  do
    local v_33_auto
    local function frequencies0(...)
      local t = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "frequencies"))
        else
        end
      end
      return hash_map_2a(itable.frequencies(t))
    end
    pcall(function() require("fennel").metadata:setall(frequencies0, "fnl/arglist", {"[t]"}, "fnl/docstring", "Return a table of unique entries from table `t` associated to amount\nof their appearances.\n\n# Examples\n\nCount each entry of a random letter:\n\n``` fennel\n(let [fruits [:banana :banana :apple :strawberry :apple :banana]]\n  (assert-eq (frequencies fruits)\n             {:banana 3\n              :apple 2\n              :strawberry 1}))\n```") end)
    v_33_auto = frequencies0
    core["frequencies"] = v_33_auto
    frequencies = v_33_auto
  end
  local group_by
  do
    local v_33_auto
    local function group_by0(...)
      local f, t = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "group-by"))
        else
        end
      end
      local function _758_(...)
        local _757_ = itable["group-by"](f, t)
        return _757_
      end
      return hash_map_2a(_758_(...))
    end
    pcall(function() require("fennel").metadata:setall(group_by0, "fnl/arglist", {"[f t]"}, "fnl/docstring", "Group table items in an associative table under the keys that are\nresults of calling `f` on each element of sequential table `t`.\nElements that the function call resulted in `nil` returned in a\nseparate table.\n\n# Examples\n\nGroup rows by their date:\n\n``` fennel\n(local rows\n  [{:date \"2007-03-03\" :product \"pineapple\"}\n   {:date \"2007-03-04\" :product \"pizza\"}\n   {:date \"2007-03-04\" :product \"pineapple pizza\"}\n   {:date \"2007-03-05\" :product \"bananas\"}])\n\n(assert-eq (group-by #(. $ :date) rows)\n           {\"2007-03-03\"\n            [{:date \"2007-03-03\" :product \"pineapple\"}]\n            \"2007-03-04\"\n            [{:date \"2007-03-04\" :product \"pizza\"}\n             {:date \"2007-03-04\" :product \"pineapple pizza\"}]\n            \"2007-03-05\"\n            [{:date \"2007-03-05\" :product \"bananas\"}]})\n```") end)
    v_33_auto = group_by0
    core["group-by"] = v_33_auto
    group_by = v_33_auto
  end
  local zipmap
  do
    local v_33_auto
    local function zipmap0(...)
      local keys0, vals0 = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "zipmap"))
        else
        end
      end
      return hash_map_2a(itable(lazy.zipmap(keys0, vals0)))
    end
    pcall(function() require("fennel").metadata:setall(zipmap0, "fnl/arglist", {"[keys vals]"}, "fnl/docstring", "Return an associative table with the `keys` mapped to the\ncorresponding `vals`.") end)
    v_33_auto = zipmap0
    core["zipmap"] = v_33_auto
    zipmap = v_33_auto
  end
  local replace
  do
    local v_33_auto
    local function replace0(...)
      local _760_ = select("#", ...)
      if (_760_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "replace"))
      elseif (_760_ == 1) then
        local smap = ...
        local function _761_(_241)
          local val_100_auto = find(smap, _241)
          if val_100_auto then
            local e = val_100_auto
            return e[2]
          else
            return _241
          end
        end
        return map(_761_)
      elseif (_760_ == 2) then
        local smap, coll = ...
        if vector_3f(coll) then
          local function _763_(res, v)
            local val_100_auto = find(smap, v)
            if val_100_auto then
              local e = val_100_auto
              table.insert(res, e[2])
              return res
            else
              table.insert(res, v)
              return res
            end
          end
          pcall(function() require("fennel").metadata:setall(_763_, "fnl/arglist", {"res", "v"}) end)
          return vec_2a(itable(reduce(_763_, {}, coll)))
        else
          local function _765_(_241)
            local val_100_auto = find(smap, _241)
            if val_100_auto then
              local e = val_100_auto
              return e[2]
            else
              return _241
            end
          end
          return map(_765_, coll)
        end
      elseif true then
        local _ = _760_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "replace"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(replace0, "fnl/arglist", {"([smap])", "([smap coll])"}, "fnl/docstring", "Given a map of replacement pairs and a vector/collection `coll`,\nreturns a vector/seq with any elements `=` a key in `smap` replaced\nwith the corresponding `val` in `smap`.  Returns a transducer when no\ncollection is provided.") end)
    v_33_auto = replace0
    core["replace"] = v_33_auto
    replace = v_33_auto
  end
  local conj
  do
    local v_33_auto
    local function conj0(...)
      local _769_ = select("#", ...)
      if (_769_ == 0) then
        return vector()
      elseif (_769_ == 1) then
        local s = ...
        return s
      elseif (_769_ == 2) then
        local s, x = ...
        local _770_ = getmetatable(s)
        if ((_G.type(_770_) == "table") and (nil ~= (_770_)["cljlib/conj"])) then
          local f = (_770_)["cljlib/conj"]
          return f(s, x)
        elseif true then
          local _ = _770_
          if vector_3f(s) then
            return vec_2a(itable.insert(s, x))
          elseif map_3f(s) then
            return apply(assoc, s, x)
          elseif nil_3f(s) then
            return cons(x, s)
          elseif empty_3f(s) then
            return vector(x)
          else
            return error("expected collection, got", type(s))
          end
        else
          return nil
        end
      elseif true then
        local _ = _769_
        local core_48_auto = require("init")
        local _let_773_ = core_48_auto.list(...)
        local s = _let_773_[1]
        local x = _let_773_[2]
        local xs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_773_, 3)
        return apply(conj0, conj0(s, x), xs)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(conj0, "fnl/arglist", {"([])", "([s])", "([s x])", "([s x & xs])"}, "fnl/docstring", "Insert `x` as a last element of a table `tbl`.\n\nIf `tbl` is a sequential table or empty table, inserts `x` and\noptional `xs` as final element in the table.\n\nIf `tbl` is an associative table, that satisfies `map?` test,\ninsert `[key value]` pair into the table.\n\nMutates `tbl`.\n\n# Examples\nAdding to sequential tables:\n\n``` fennel\n(conj [] 1 2 3 4)\n;; => [1 2 3 4]\n(conj [1 2 3] 4 5)\n;; => [1 2 3 4 5]\n```\n\nAdding to associative tables:\n\n``` fennel\n(conj {:a 1} [:b 2] [:c 3])\n;; => {:a 1 :b 2 :c 3}\n```\n\nNote, that passing literal empty associative table `{}` will not work:\n\n``` fennel\n(conj {} [:a 1] [:b 2])\n;; => [[:a 1] [:b 2]]\n(conj (hash-map) [:a 1] [:b 2])\n;; => {:a 1 :b 2}\n```\n\nSee `hash-map` for creating empty associative tables.") end)
    v_33_auto = conj0
    core["conj"] = v_33_auto
    conj = v_33_auto
  end
  local disj
  do
    local v_33_auto
    local function disj0(...)
      local _775_ = select("#", ...)
      if (_775_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "disj"))
      elseif (_775_ == 1) then
        local Set = ...
        return Set
      elseif (_775_ == 2) then
        local Set, key = ...
        local _776_ = getmetatable(Set)
        if ((_G.type(_776_) == "table") and ((_776_)["cljlib/type"] == "hash-set") and (nil ~= (_776_)["cljlib/disj"])) then
          local f = (_776_)["cljlib/disj"]
          return f(Set, key)
        elseif true then
          local _ = _776_
          return error(("disj is not supported on " .. class(Set)), 2)
        else
          return nil
        end
      elseif true then
        local _ = _775_
        local core_48_auto = require("init")
        local _let_778_ = core_48_auto.list(...)
        local Set = _let_778_[1]
        local key = _let_778_[2]
        local keys0 = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_778_, 3)
        local _779_ = getmetatable(Set)
        if ((_G.type(_779_) == "table") and ((_779_)["cljlib/type"] == "hash-set") and (nil ~= (_779_)["cljlib/disj"])) then
          local f = (_779_)["cljlib/disj"]
          return apply(f, Set, key, keys0)
        elseif true then
          local _0 = _779_
          return error(("disj is not supported on " .. class(Set)), 2)
        else
          return nil
        end
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(disj0, "fnl/arglist", {"([Set])", "([Set key])", "([Set key & keys])"}, "fnl/docstring", "Returns a new set type, that does not contain the\nspecified `key` or `keys`.") end)
    v_33_auto = disj0
    core["disj"] = v_33_auto
    disj = v_33_auto
  end
  local pop
  do
    local v_33_auto
    local function pop0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "pop"))
        else
        end
      end
      local _783_ = getmetatable(coll)
      if ((_G.type(_783_) == "table") and ((_783_)["cljlib/type"] == "seq")) then
        local _784_ = seq(coll)
        if (nil ~= _784_) then
          local s = _784_
          return drop(1, s)
        elseif true then
          local _ = _784_
          return error("can't pop empty list", 2)
        else
          return nil
        end
      elseif ((_G.type(_783_) == "table") and (nil ~= (_783_)["cljlib/pop"])) then
        local f = (_783_)["cljlib/pop"]
        return f(coll)
      elseif true then
        local _ = _783_
        return error(("pop is not supported on " .. class(coll)), 2)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(pop0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "If `coll` is a list returns a new list without the first\nitem. If `coll` is a vector, returns a new vector without the last\nitem. If the collection is empty, raises an error. Not the same as\n`next` or `butlast`.") end)
    v_33_auto = pop0
    core["pop"] = v_33_auto
    pop = v_33_auto
  end
  local transient
  do
    local v_33_auto
    local function transient0(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "transient"))
        else
        end
      end
      local _788_ = getmetatable(coll)
      if ((_G.type(_788_) == "table") and ((_788_)["cljlib/editable"] == true) and (nil ~= (_788_)["cljlib/transient"])) then
        local f = (_788_)["cljlib/transient"]
        return f(coll)
      elseif true then
        local _ = _788_
        return error("expected editable collection", 2)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(transient0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a new, transient version of the collection.") end)
    v_33_auto = transient0
    core["transient"] = v_33_auto
    transient = v_33_auto
  end
  local conj_21
  do
    local v_33_auto
    local function conj_210(...)
      local _790_ = select("#", ...)
      if (_790_ == 0) then
        return transient(vec_2a({}))
      elseif (_790_ == 1) then
        local coll = ...
        return coll
      elseif (_790_ == 2) then
        local coll, x = ...
        do
          local _791_ = getmetatable(coll)
          if ((_G.type(_791_) == "table") and ((_791_)["cljlib/type"] == "transient") and (nil ~= (_791_)["cljlib/conj!"])) then
            local f = (_791_)["cljlib/conj!"]
            f(coll, x)
          elseif ((_G.type(_791_) == "table") and ((_791_)["cljlib/type"] == "transient")) then
            error("unsupported transient operation", 2)
          elseif true then
            local _ = _791_
            error("expected transient collection", 2)
          else
          end
        end
        return coll
      elseif true then
        local _ = _790_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "conj!"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(conj_210, "fnl/arglist", {"([])", "([coll])", "([coll x])"}, "fnl/docstring", "Adds `x` to the transient collection, and return `coll`.") end)
    v_33_auto = conj_210
    core["conj!"] = v_33_auto
    conj_21 = v_33_auto
  end
  local assoc_21
  do
    local v_33_auto
    local function assoc_210(...)
      local core_48_auto = require("init")
      local _let_794_ = core_48_auto.list(...)
      local map0 = _let_794_[1]
      local k = _let_794_[2]
      local ks = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_794_, 3)
      do
        local _795_ = getmetatable(map0)
        if ((_G.type(_795_) == "table") and ((_795_)["cljlib/type"] == "transient") and (nil ~= (_795_)["cljlib/dissoc!"])) then
          local f = (_795_)["cljlib/dissoc!"]
          apply(f, map0, k, ks)
        elseif ((_G.type(_795_) == "table") and ((_795_)["cljlib/type"] == "transient")) then
          error("unsupported transient operation", 2)
        elseif true then
          local _ = _795_
          error("expected transient collection", 2)
        else
        end
      end
      return map0
    end
    pcall(function() require("fennel").metadata:setall(assoc_210, "fnl/arglist", {"[map k & ks]"}, "fnl/docstring", "Remove `k`from transient map, and return `map`.") end)
    v_33_auto = assoc_210
    core["assoc!"] = v_33_auto
    assoc_21 = v_33_auto
  end
  local dissoc_21
  do
    local v_33_auto
    local function dissoc_210(...)
      local core_48_auto = require("init")
      local _let_797_ = core_48_auto.list(...)
      local map0 = _let_797_[1]
      local k = _let_797_[2]
      local ks = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_797_, 3)
      do
        local _798_ = getmetatable(map0)
        if ((_G.type(_798_) == "table") and ((_798_)["cljlib/type"] == "transient") and (nil ~= (_798_)["cljlib/dissoc!"])) then
          local f = (_798_)["cljlib/dissoc!"]
          apply(f, map0, k, ks)
        elseif ((_G.type(_798_) == "table") and ((_798_)["cljlib/type"] == "transient")) then
          error("unsupported transient operation", 2)
        elseif true then
          local _ = _798_
          error("expected transient collection", 2)
        else
        end
      end
      return map0
    end
    pcall(function() require("fennel").metadata:setall(dissoc_210, "fnl/arglist", {"[map k & ks]"}, "fnl/docstring", "Remove `k`from transient map, and return `map`.") end)
    v_33_auto = dissoc_210
    core["dissoc!"] = v_33_auto
    dissoc_21 = v_33_auto
  end
  local disj_21
  do
    local v_33_auto
    local function disj_210(...)
      local _800_ = select("#", ...)
      if (_800_ == 0) then
        return error(("Wrong number of args (%s) passed to %s"):format(0, "disj!"))
      elseif (_800_ == 1) then
        local Set = ...
        return Set
      elseif true then
        local _ = _800_
        local core_48_auto = require("init")
        local _let_801_ = core_48_auto.list(...)
        local Set = _let_801_[1]
        local key = _let_801_[2]
        local ks = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_801_, 3)
        local _802_ = getmetatable(Set)
        if ((_G.type(_802_) == "table") and ((_802_)["cljlib/type"] == "transient") and (nil ~= (_802_)["cljlib/disj!"])) then
          local f = (_802_)["cljlib/disj!"]
          return apply(f, Set, key, ks)
        elseif ((_G.type(_802_) == "table") and ((_802_)["cljlib/type"] == "transient")) then
          return error("unsupported transient operation", 2)
        elseif true then
          local _0 = _802_
          return error("expected transient collection", 2)
        else
          return nil
        end
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(disj_210, "fnl/arglist", {"([Set])", "([Set key & ks])"}, "fnl/docstring", "disj[oin]. Returns a transient set of the same type, that does not\ncontain `key`.") end)
    v_33_auto = disj_210
    core["disj!"] = v_33_auto
    disj_21 = v_33_auto
  end
  local pop_21
  do
    local v_33_auto
    local function pop_210(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "pop!"))
        else
        end
      end
      local _806_ = getmetatable(coll)
      if ((_G.type(_806_) == "table") and ((_806_)["cljlib/type"] == "transient") and (nil ~= (_806_)["cljlib/pop!"])) then
        local f = (_806_)["cljlib/pop!"]
        return f(coll)
      elseif ((_G.type(_806_) == "table") and ((_806_)["cljlib/type"] == "transient")) then
        return error("unsupported transient operation", 2)
      elseif true then
        local _ = _806_
        return error("expected transient collection", 2)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(pop_210, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Removes the last item from a transient vector. If the collection is\nempty, raises an error Returns coll") end)
    v_33_auto = pop_210
    core["pop!"] = v_33_auto
    pop_21 = v_33_auto
  end
  local persistent_21
  do
    local v_33_auto
    local function persistent_210(...)
      local coll = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "persistent!"))
        else
        end
      end
      local _809_ = getmetatable(coll)
      if ((_G.type(_809_) == "table") and ((_809_)["cljlib/type"] == "transient") and (nil ~= (_809_)["cljlib/persistent!"])) then
        local f = (_809_)["cljlib/persistent!"]
        return f(coll)
      elseif true then
        local _ = _809_
        return error("expected transient collection", 2)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(persistent_210, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a new, persistent version of the transient collection. The\ntransient collection cannot be used after this call, any such use will\nraise an error.") end)
    v_33_auto = persistent_210
    core["persistent!"] = v_33_auto
    persistent_21 = v_33_auto
  end
  local into
  do
    local v_33_auto
    local function into0(...)
      local _811_ = select("#", ...)
      if (_811_ == 0) then
        return vector()
      elseif (_811_ == 1) then
        local to = ...
        return to
      elseif (_811_ == 2) then
        local to, from = ...
        local _812_ = getmetatable(to)
        if ((_G.type(_812_) == "table") and ((_812_)["cljlib/editable"] == true)) then
          return persistent_21(reduce(conj_21, transient(to), from))
        elseif true then
          local _ = _812_
          return reduce(conj, to, from)
        else
          return nil
        end
      elseif (_811_ == 3) then
        local to, xform, from = ...
        local _814_ = getmetatable(to)
        if ((_G.type(_814_) == "table") and ((_814_)["cljlib/editable"] == true)) then
          return persistent_21(transduce(xform, conj_21, transient(to), from))
        elseif true then
          local _ = _814_
          return transduce(xform, conj, to, from)
        else
          return nil
        end
      elseif true then
        local _ = _811_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "into"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(into0, "fnl/arglist", {"([])", "([to])", "([to from])", "([to xform from])"}, "fnl/docstring", "Returns a new coll consisting of `to` with all of the items of `from`\nconjoined. A transducer `xform` may be supplied.\n\n# Examples\n\nInsert items of one collection into another collection:\n\n```fennel\n(assert-eq [1 2 3 :a :b :c] (into [1 2 3] \"abc\"))\n(assert-eq {:a 2 :b 3} (into {:a 1} {:a 2 :b 3}))\n```\n\nTransform a hash-map into a sequence of key-value pairs:\n\n``` fennel\n(assert-eq [[:a 1]] (into (vector) {:a 1}))\n```\n\nYou can also construct a hash-map from a sequence of key-value pairs:\n\n``` fennel\n(assert-eq {:a 1 :b 2 :c 3}\n           (into (hash-map) [[:a 1] [:b 2] [:c 3]]))\n```") end)
    v_33_auto = into0
    core["into"] = v_33_auto
    into = v_33_auto
  end
  local function viewset(Set, view, inspector, indent)
    if inspector.seen[Set] then
      return ("@set" .. inspector.seen[Set] .. "{...}")
    else
      local prefix
      local function _817_()
        if inspector["visible-cycle?"](Set) then
          return inspector.seen[Set]
        else
          return ""
        end
      end
      prefix = ("@set" .. _817_() .. "{")
      local set_indent = #prefix
      local indent_str = string.rep(" ", set_indent)
      local lines
      do
        local tbl_17_auto = {}
        local i_18_auto = #tbl_17_auto
        for v in pairs_2a(Set) do
          local val_19_auto = (indent_str .. view(v, inspector, (indent + set_indent), true))
          if (nil ~= val_19_auto) then
            i_18_auto = (i_18_auto + 1)
            do end (tbl_17_auto)[i_18_auto] = val_19_auto
          else
          end
        end
        lines = tbl_17_auto
      end
      lines[1] = (prefix .. string.gsub((lines[1] or ""), "^%s+", ""))
      do end (lines)[#lines] = (lines[#lines] .. "}")
      return lines
    end
  end
  pcall(function() require("fennel").metadata:setall(viewset, "fnl/arglist", {"Set", "view", "inspector", "indent"}) end)
  local function hash_set__3etransient(immutable)
    local function _820_(hset)
      local removed = setmetatable({}, {__index = deep_index})
      local function _821_(_, k)
        if not removed[k] then
          return hset[k]
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(_821_, "fnl/arglist", {"_", "k"}) end)
      local function _823_()
        return error("can't `conj` onto transient set, use `conj!`")
      end
      local function _824_()
        return error("can't `disj` a transient set, use `disj!`")
      end
      local function _825_()
        return error("can't `assoc` onto transient set, use `assoc!`")
      end
      local function _826_()
        return error("can't `dissoc` onto transient set, use `dissoc!`")
      end
      local function _827_(thset, v)
        if (nil == v) then
          removed[v] = true
        else
          removed[v] = nil
        end
        thset[v] = v
        return thset
      end
      pcall(function() require("fennel").metadata:setall(_827_, "fnl/arglist", {"thset", "v"}) end)
      local function _829_()
        return error("can't `dissoc!` a transient set")
      end
      local function _830_(thset, ...)
        for i = 1, select("#", ...) do
          local k = select(i, ...)
          do end (thset)[k] = nil
          removed[k] = true
        end
        return thset
      end
      pcall(function() require("fennel").metadata:setall(_830_, "fnl/arglist", {"thset", "..."}) end)
      local function _831_(thset)
        local t
        do
          local tbl_14_auto
          do
            local tbl_14_auto0 = {}
            for k, v in pairs(hset) do
              local k_15_auto, v_16_auto = k, v
              if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
                tbl_14_auto0[k_15_auto] = v_16_auto
              else
              end
            end
            tbl_14_auto = tbl_14_auto0
          end
          for k, v in pairs(thset) do
            local k_15_auto, v_16_auto = k, v
            if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
              tbl_14_auto[k_15_auto] = v_16_auto
            else
            end
          end
          t = tbl_14_auto
        end
        for k in pairs(removed) do
          t[k] = nil
        end
        local function _834_()
          local tbl_17_auto = {}
          local i_18_auto = #tbl_17_auto
          for k in pairs_2a(thset) do
            local val_19_auto = k
            if (nil ~= val_19_auto) then
              i_18_auto = (i_18_auto + 1)
              do end (tbl_17_auto)[i_18_auto] = val_19_auto
            else
            end
          end
          return tbl_17_auto
        end
        for _, k in ipairs(_834_()) do
          thset[k] = nil
        end
        local function _836_()
          return error("attempt to use transient after it was persistet")
        end
        local function _837_()
          return error("attempt to use transient after it was persistet")
        end
        setmetatable(thset, {__index = _836_, __newindex = _837_})
        return immutable(itable(t))
      end
      pcall(function() require("fennel").metadata:setall(_831_, "fnl/arglist", {"thset"}) end)
      return setmetatable({}, {__index = _821_, ["cljlib/type"] = "transient", ["cljlib/conj"] = _823_, ["cljlib/disj"] = _824_, ["cljlib/assoc"] = _825_, ["cljlib/dissoc"] = _826_, ["cljlib/conj!"] = _827_, ["cljlib/assoc!"] = _829_, ["cljlib/disj!"] = _830_, ["cljlib/persistent!"] = _831_})
    end
    pcall(function() require("fennel").metadata:setall(_820_, "fnl/arglist", {"hset"}) end)
    return _820_
  end
  pcall(function() require("fennel").metadata:setall(hash_set__3etransient, "fnl/arglist", {"immutable"}) end)
  local function hash_set_2a(x)
    do
      local _838_ = getmetatable(x)
      if (nil ~= _838_) then
        local mt = _838_
        mt["cljlib/type"] = "hash-set"
        local function _839_(s, v, ...)
          local function _840_(...)
            local res = {}
            for _, v0 in ipairs({...}) do
              table.insert(res, v0)
              table.insert(res, v0)
            end
            return res
          end
          return hash_set_2a(itable.assoc(s, v, v, unpack_2a(_840_(...))))
        end
        pcall(function() require("fennel").metadata:setall(_839_, "fnl/arglist", {"s", "v", "..."}) end)
        do end (mt)["cljlib/conj"] = _839_
        local function _841_(s, k, ...)
          local to_remove
          do
            local tbl_14_auto = setmetatable({[k] = true}, {__index = deep_index})
            for _, k0 in ipairs({...}) do
              local k_15_auto, v_16_auto = k0, true
              if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
                tbl_14_auto[k_15_auto] = v_16_auto
              else
              end
            end
            to_remove = tbl_14_auto
          end
          local function _843_(...)
            local res = {}
            for _, v in pairs(s) do
              if not to_remove[v] then
                table.insert(res, v)
                table.insert(res, v)
              else
              end
            end
            return res
          end
          return hash_set_2a(itable.assoc({}, unpack_2a(_843_(...))))
        end
        pcall(function() require("fennel").metadata:setall(_841_, "fnl/arglist", {"s", "k", "..."}) end)
        do end (mt)["cljlib/disj"] = _841_
        local function _845_()
          return hash_set_2a(itable({}))
        end
        mt["cljlib/empty"] = _845_
        mt["cljlib/editable"] = true
        mt["cljlib/transient"] = hash_set__3etransient(hash_set_2a)
        local function _846_(s)
          local function _847_(_241)
            if vector_3f(_241) then
              return (_241)[1]
            else
              return _241
            end
          end
          return map(_847_, s)
        end
        pcall(function() require("fennel").metadata:setall(_846_, "fnl/arglist", {"s"}) end)
        do end (mt)["cljlib/seq"] = _846_
        mt["__fennelview"] = viewset
        local function _849_(s, i)
          local j = 1
          local vals0 = {}
          for v in pairs_2a(s) do
            if (j >= i) then
              table.insert(vals0, v)
            else
              j = (j + 1)
            end
          end
          return core["hash-set"](unpack_2a(vals0))
        end
        pcall(function() require("fennel").metadata:setall(_849_, "fnl/arglist", {"s", "i"}) end)
        do end (mt)["__fennelrest"] = _849_
      elseif true then
        local _ = _838_
        hash_set_2a(setmetatable(x, {}))
      else
      end
    end
    return x
  end
  pcall(function() require("fennel").metadata:setall(hash_set_2a, "fnl/arglist", {"x"}) end)
  local hash_set
  do
    local v_33_auto
    local function hash_set0(...)
      local core_48_auto = require("init")
      local _let_852_ = core_48_auto.list(...)
      local xs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_852_, 1)
      local Set
      do
        local tbl_14_auto = setmetatable({}, {__newindex = deep_newindex})
        for _, val in pairs_2a(xs) do
          local k_15_auto, v_16_auto = val, val
          if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
            tbl_14_auto[k_15_auto] = v_16_auto
          else
          end
        end
        Set = tbl_14_auto
      end
      return hash_set_2a(itable(Set))
    end
    pcall(function() require("fennel").metadata:setall(hash_set0, "fnl/arglist", {"[& xs]"}, "fnl/docstring", "Create hash set.\n\nSet is a collection of unique elements, which sore purpose is only to\ntell you if something is in the set or not.") end)
    v_33_auto = hash_set0
    core["hash-set"] = v_33_auto
    hash_set = v_33_auto
  end
  local multifn_3f
  do
    local v_33_auto
    local function multifn_3f0(...)
      local mf = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "multifn?"))
        else
        end
      end
      local _855_ = getmetatable(mf)
      if ((_G.type(_855_) == "table") and ((_855_)["cljlib/type"] == "multifn")) then
        return true
      elseif true then
        local _ = _855_
        return false
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(multifn_3f0, "fnl/arglist", {"[mf]"}, "fnl/docstring", "Test if `mf' is an instance of `multifn'.\n\n`multifn' is a special kind of table, created with `defmulti' macros\nfrom `macros.fnl'.") end)
    v_33_auto = multifn_3f0
    core["multifn?"] = v_33_auto
    multifn_3f = v_33_auto
  end
  local remove_method
  do
    local v_33_auto
    local function remove_method0(...)
      local multimethod, dispatch_value = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "remove-method"))
        else
        end
      end
      if multifn_3f(multimethod) then
        multimethod[dispatch_value] = nil
      else
        error((tostring(multimethod) .. " is not a multifn"), 2)
      end
      return multimethod
    end
    pcall(function() require("fennel").metadata:setall(remove_method0, "fnl/arglist", {"[multimethod dispatch-value]"}, "fnl/docstring", "Remove method from `multimethod' for given `dispatch-value'.") end)
    v_33_auto = remove_method0
    core["remove-method"] = v_33_auto
    remove_method = v_33_auto
  end
  local remove_all_methods
  do
    local v_33_auto
    local function remove_all_methods0(...)
      local multimethod = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "remove-all-methods"))
        else
        end
      end
      if multifn_3f(multimethod) then
        for k, _ in pairs(multimethod) do
          multimethod[k] = nil
        end
      else
        error((tostring(multimethod) .. " is not a multifn"), 2)
      end
      return multimethod
    end
    pcall(function() require("fennel").metadata:setall(remove_all_methods0, "fnl/arglist", {"[multimethod]"}, "fnl/docstring", "Removes all methods of `multimethod'") end)
    v_33_auto = remove_all_methods0
    core["remove-all-methods"] = v_33_auto
    remove_all_methods = v_33_auto
  end
  local methods
  do
    local v_33_auto
    local function methods0(...)
      local multimethod = ...
      do
        local cnt_68_auto = select("#", ...)
        if (1 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "methods"))
        else
        end
      end
      if multifn_3f(multimethod) then
        local m = {}
        for k, v in pairs(multimethod) do
          m[k] = v
        end
        return m
      else
        return error((tostring(multimethod) .. " is not a multifn"), 2)
      end
    end
    pcall(function() require("fennel").metadata:setall(methods0, "fnl/arglist", {"[multimethod]"}, "fnl/docstring", "Given a `multimethod', returns a map of dispatch values -> dispatch fns") end)
    v_33_auto = methods0
    core["methods"] = v_33_auto
    methods = v_33_auto
  end
  local get_method
  do
    local v_33_auto
    local function get_method0(...)
      local multimethod, dispatch_value = ...
      do
        local cnt_68_auto = select("#", ...)
        if (2 ~= cnt_68_auto) then
          error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "get-method"))
        else
        end
      end
      if multifn_3f(multimethod) then
        return (multimethod[dispatch_value] or multimethod.default)
      else
        return error((tostring(multimethod) .. " is not a multifn"), 2)
      end
    end
    pcall(function() require("fennel").metadata:setall(get_method0, "fnl/arglist", {"[multimethod dispatch-value]"}, "fnl/docstring", "Given a `multimethod' and a `dispatch-value', returns the dispatch\n`fn' that would apply to that value, or `nil' if none apply and no\ndefault.") end)
    v_33_auto = get_method0
    core["get-method"] = v_33_auto
    get_method = v_33_auto
  end
  return core
end
do
  local v_33_auto
  local function apply0(...)
    local _253_ = select("#", ...)
    if (_253_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "apply"))
    elseif (_253_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "apply"))
    elseif (_253_ == 2) then
      local f, args = ...
      return f(unpack_2a(args))
    elseif (_253_ == 3) then
      local f, a, args = ...
      return f(a, unpack_2a(args))
    elseif (_253_ == 4) then
      local f, a, b, args = ...
      return f(a, b, unpack_2a(args))
    elseif (_253_ == 5) then
      local f, a, b, c, args = ...
      return f(a, b, c, unpack_2a(args))
    elseif true then
      local _ = _253_
      local core_48_auto = require("init")
      local _let_865_ = core_48_auto.list(...)
      local f = _let_865_[1]
      local a = _let_865_[2]
      local b = _let_865_[3]
      local c = _let_865_[4]
      local d = _let_865_[5]
      local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_865_, 6)
      local flat_args = {}
      local len = (length_2a(args) - 1)
      for i = 1, len do
        flat_args[i] = args[i]
      end
      for i, a0 in pairs_2a(args[(len + 1)]) do
        flat_args[(i + len)] = a0
      end
      return f(a, b, c, d, unpack_2a(flat_args))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(apply0, "fnl/arglist", {"([f args])", "([f a args])", "([f a b args])", "([f a b c args])", "([f a b c d & args])"}, "fnl/docstring", "Apply `f` to the argument list formed by prepending intervening\narguments to `args`, and `f` must support variadic amount of\narguments.\n\n# Examples\nApplying `add` to different amount of arguments:\n\n``` fennel\n(assert-eq (apply add [1 2 3 4]) 10)\n(assert-eq (apply add 1 [2 3 4]) 10)\n(assert-eq (apply add 1 2 3 4 5 6 [7 8 9]) 45)\n```") end)
  v_33_auto = apply0
  core["apply"] = v_33_auto
  apply = v_33_auto
end
local add
do
  local v_33_auto
  local function add0(...)
    local _867_ = select("#", ...)
    if (_867_ == 0) then
      return 0
    elseif (_867_ == 1) then
      local a = ...
      return a
    elseif (_867_ == 2) then
      local a, b = ...
      return (a + b)
    elseif (_867_ == 3) then
      local a, b, c = ...
      return (a + b + c)
    elseif (_867_ == 4) then
      local a, b, c, d = ...
      return (a + b + c + d)
    elseif true then
      local _ = _867_
      local core_48_auto = require("init")
      local _let_868_ = core_48_auto.list(...)
      local a = _let_868_[1]
      local b = _let_868_[2]
      local c = _let_868_[3]
      local d = _let_868_[4]
      local rest = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_868_, 5)
      return apply(add0, (a + b + c + d), rest)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(add0, "fnl/arglist", {"([])", "([a])", "([a b])", "([a b c])", "([a b c d])", "([a b c d & rest])"}, "fnl/docstring", "Sum arbitrary amount of numbers.") end)
  v_33_auto = add0
  core["add"] = v_33_auto
  add = v_33_auto
end
local sub
do
  local v_33_auto
  local function sub0(...)
    local _870_ = select("#", ...)
    if (_870_ == 0) then
      return 0
    elseif (_870_ == 1) then
      local a = ...
      return ( - a)
    elseif (_870_ == 2) then
      local a, b = ...
      return (a - b)
    elseif (_870_ == 3) then
      local a, b, c = ...
      return (a - b - c)
    elseif (_870_ == 4) then
      local a, b, c, d = ...
      return (a - b - c - d)
    elseif true then
      local _ = _870_
      local core_48_auto = require("init")
      local _let_871_ = core_48_auto.list(...)
      local a = _let_871_[1]
      local b = _let_871_[2]
      local c = _let_871_[3]
      local d = _let_871_[4]
      local rest = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_871_, 5)
      return apply(sub0, (a - b - c - d), rest)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(sub0, "fnl/arglist", {"([])", "([a])", "([a b])", "([a b c])", "([a b c d])", "([a b c d & rest])"}, "fnl/docstring", "Subtract arbitrary amount of numbers.") end)
  v_33_auto = sub0
  core["sub"] = v_33_auto
  sub = v_33_auto
end
local mul
do
  local v_33_auto
  local function mul0(...)
    local _873_ = select("#", ...)
    if (_873_ == 0) then
      return 1
    elseif (_873_ == 1) then
      local a = ...
      return a
    elseif (_873_ == 2) then
      local a, b = ...
      return (a * b)
    elseif (_873_ == 3) then
      local a, b, c = ...
      return (a * b * c)
    elseif (_873_ == 4) then
      local a, b, c, d = ...
      return (a * b * c * d)
    elseif true then
      local _ = _873_
      local core_48_auto = require("init")
      local _let_874_ = core_48_auto.list(...)
      local a = _let_874_[1]
      local b = _let_874_[2]
      local c = _let_874_[3]
      local d = _let_874_[4]
      local rest = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_874_, 5)
      return apply(mul0, (a * b * c * d), rest)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(mul0, "fnl/arglist", {"([])", "([a])", "([a b])", "([a b c])", "([a b c d])", "([a b c d & rest])"}, "fnl/docstring", "Multiply arbitrary amount of numbers.") end)
  v_33_auto = mul0
  core["mul"] = v_33_auto
  mul = v_33_auto
end
local div
do
  local v_33_auto
  local function div0(...)
    local _876_ = select("#", ...)
    if (_876_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "div"))
    elseif (_876_ == 1) then
      local a = ...
      return (1 / a)
    elseif (_876_ == 2) then
      local a, b = ...
      return (a / b)
    elseif (_876_ == 3) then
      local a, b, c = ...
      return (a / b / c)
    elseif (_876_ == 4) then
      local a, b, c, d = ...
      return (a / b / c / d)
    elseif true then
      local _ = _876_
      local core_48_auto = require("init")
      local _let_877_ = core_48_auto.list(...)
      local a = _let_877_[1]
      local b = _let_877_[2]
      local c = _let_877_[3]
      local d = _let_877_[4]
      local rest = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_877_, 5)
      return apply(div0, (a / b / c / d), rest)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(div0, "fnl/arglist", {"([a])", "([a b])", "([a b c])", "([a b c d])", "([a b c d & rest])"}, "fnl/docstring", "Divide arbitrary amount of numbers.") end)
  v_33_auto = div0
  core["div"] = v_33_auto
  div = v_33_auto
end
local le
do
  local v_33_auto
  local function le0(...)
    local _879_ = select("#", ...)
    if (_879_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "le"))
    elseif (_879_ == 1) then
      local a = ...
      return true
    elseif (_879_ == 2) then
      local a, b = ...
      return (a <= b)
    elseif true then
      local _ = _879_
      local core_48_auto = require("init")
      local _let_880_ = core_48_auto.list(...)
      local a = _let_880_[1]
      local b = _let_880_[2]
      local _let_881_ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_880_, 3)
      local c = _let_881_[1]
      local d = _let_881_[2]
      local more = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_881_, 3)
      if (a <= b) then
        if d then
          return apply(le0, b, c, d, more)
        else
          return (b <= c)
        end
      else
        return false
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(le0, "fnl/arglist", {"([a])", "([a b])", "([a b & [c d & more]])"}, "fnl/docstring", "Returns true if nums are in monotonically non-decreasing order") end)
  v_33_auto = le0
  core["le"] = v_33_auto
  le = v_33_auto
end
local lt
do
  local v_33_auto
  local function lt0(...)
    local _885_ = select("#", ...)
    if (_885_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "lt"))
    elseif (_885_ == 1) then
      local a = ...
      return true
    elseif (_885_ == 2) then
      local a, b = ...
      return (a < b)
    elseif true then
      local _ = _885_
      local core_48_auto = require("init")
      local _let_886_ = core_48_auto.list(...)
      local a = _let_886_[1]
      local b = _let_886_[2]
      local _let_887_ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_886_, 3)
      local c = _let_887_[1]
      local d = _let_887_[2]
      local more = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_887_, 3)
      if (a < b) then
        if d then
          return apply(lt0, b, c, d, more)
        else
          return (b < c)
        end
      else
        return false
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(lt0, "fnl/arglist", {"([a])", "([a b])", "([a b & [c d & more]])"}, "fnl/docstring", "Returns true if nums are in monotonically decreasing order") end)
  v_33_auto = lt0
  core["lt"] = v_33_auto
  lt = v_33_auto
end
local ge
do
  local v_33_auto
  local function ge0(...)
    local _891_ = select("#", ...)
    if (_891_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "ge"))
    elseif (_891_ == 1) then
      local a = ...
      return true
    elseif (_891_ == 2) then
      local a, b = ...
      return (a >= b)
    elseif true then
      local _ = _891_
      local core_48_auto = require("init")
      local _let_892_ = core_48_auto.list(...)
      local a = _let_892_[1]
      local b = _let_892_[2]
      local _let_893_ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_892_, 3)
      local c = _let_893_[1]
      local d = _let_893_[2]
      local more = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_893_, 3)
      if (a >= b) then
        if d then
          return apply(ge0, b, c, d, more)
        else
          return (b >= c)
        end
      else
        return false
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(ge0, "fnl/arglist", {"([a])", "([a b])", "([a b & [c d & more]])"}, "fnl/docstring", "Returns true if nums are in monotonically non-increasing order") end)
  v_33_auto = ge0
  core["ge"] = v_33_auto
  ge = v_33_auto
end
local gt
do
  local v_33_auto
  local function gt0(...)
    local _897_ = select("#", ...)
    if (_897_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "gt"))
    elseif (_897_ == 1) then
      local a = ...
      return true
    elseif (_897_ == 2) then
      local a, b = ...
      return (a > b)
    elseif true then
      local _ = _897_
      local core_48_auto = require("init")
      local _let_898_ = core_48_auto.list(...)
      local a = _let_898_[1]
      local b = _let_898_[2]
      local _let_899_ = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_898_, 3)
      local c = _let_899_[1]
      local d = _let_899_[2]
      local more = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_899_, 3)
      if (a > b) then
        if d then
          return apply(gt0, b, c, d, more)
        else
          return (b > c)
        end
      else
        return false
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(gt0, "fnl/arglist", {"([a])", "([a b])", "([a b & [c d & more]])"}, "fnl/docstring", "Returns true if nums are in monotonically increasing order") end)
  v_33_auto = gt0
  core["gt"] = v_33_auto
  gt = v_33_auto
end
local inc
do
  local v_33_auto
  local function inc0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "inc"))
      else
      end
    end
    return (x + 1)
  end
  pcall(function() require("fennel").metadata:setall(inc0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Increase number `x` by one") end)
  v_33_auto = inc0
  core["inc"] = v_33_auto
  inc = v_33_auto
end
local dec
do
  local v_33_auto
  local function dec0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "dec"))
      else
      end
    end
    return (x - 1)
  end
  pcall(function() require("fennel").metadata:setall(dec0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Decrease number `x` by one") end)
  v_33_auto = dec0
  core["dec"] = v_33_auto
  dec = v_33_auto
end
local class
do
  local v_33_auto
  local function class0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "class"))
      else
      end
    end
    local _906_ = type(x)
    if (_906_ == "table") then
      local _907_ = getmetatable(x)
      if ((_G.type(_907_) == "table") and (nil ~= (_907_)["cljlib/type"])) then
        local t = (_907_)["cljlib/type"]
        return t
      elseif true then
        local _ = _907_
        return "table"
      else
        return nil
      end
    elseif (nil ~= _906_) then
      local t = _906_
      return t
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(class0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Return cljlib type of the `x`, or lua type.") end)
  v_33_auto = class0
  core["class"] = v_33_auto
  class = v_33_auto
end
local constantly
do
  local v_33_auto
  local function constantly0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "constantly"))
      else
      end
    end
    local function _911_()
      return x
    end
    pcall(function() require("fennel").metadata:setall(_911_, "fnl/arglist", {}) end)
    return _911_
  end
  pcall(function() require("fennel").metadata:setall(constantly0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Returns a function that takes any number of arguments and returns `x`.") end)
  v_33_auto = constantly0
  core["constantly"] = v_33_auto
  constantly = v_33_auto
end
local complement
do
  local v_33_auto
  local function complement0(...)
    local f = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "complement"))
      else
      end
    end
    local function fn_913_(...)
      local _914_ = select("#", ...)
      if (_914_ == 0) then
        return not f()
      elseif (_914_ == 1) then
        local a = ...
        return not f(a)
      elseif (_914_ == 2) then
        local a, b = ...
        return not f(a, b)
      elseif true then
        local _ = _914_
        local core_48_auto = require("init")
        local _let_915_ = core_48_auto.list(...)
        local a = _let_915_[1]
        local b = _let_915_[2]
        local cs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_915_, 3)
        return not apply(f, a, b, cs)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(fn_913_, "fnl/arglist", {"([])", "([a])", "([a b])", "([a b & cs])"}) end)
    return fn_913_
  end
  pcall(function() require("fennel").metadata:setall(complement0, "fnl/arglist", {"[f]"}, "fnl/docstring", "Takes a function `f` and returns the function that takes the same\namount of arguments as `f`, has the same effect, and returns the\nopposite truth value.") end)
  v_33_auto = complement0
  core["complement"] = v_33_auto
  complement = v_33_auto
end
local identity
do
  local v_33_auto
  local function identity0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "identity"))
      else
      end
    end
    return x
  end
  pcall(function() require("fennel").metadata:setall(identity0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Returns its argument.") end)
  v_33_auto = identity0
  core["identity"] = v_33_auto
  identity = v_33_auto
end
local comp
do
  local v_33_auto
  local function comp0(...)
    local _918_ = select("#", ...)
    if (_918_ == 0) then
      return identity
    elseif (_918_ == 1) then
      local f = ...
      return f
    elseif (_918_ == 2) then
      local f, g = ...
      local function fn_919_(...)
        local _920_ = select("#", ...)
        if (_920_ == 0) then
          return f(g())
        elseif (_920_ == 1) then
          local x = ...
          return f(g(x))
        elseif (_920_ == 2) then
          local x, y = ...
          return f(g(x, y))
        elseif (_920_ == 3) then
          local x, y, z = ...
          return f(g(x, y, z))
        elseif true then
          local _ = _920_
          local core_48_auto = require("init")
          local _let_921_ = core_48_auto.list(...)
          local x = _let_921_[1]
          local y = _let_921_[2]
          local z = _let_921_[3]
          local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_921_, 4)
          return f(apply(g, x, y, z, args))
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(fn_919_, "fnl/arglist", {"([])", "([x])", "([x y])", "([x y z])", "([x y z & args])"}) end)
      return fn_919_
    elseif true then
      local _ = _918_
      local core_48_auto = require("init")
      local _let_923_ = core_48_auto.list(...)
      local f = _let_923_[1]
      local g = _let_923_[2]
      local fs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_923_, 3)
      return core.reduce(comp0, core.cons(f, core.cons(g, fs)))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(comp0, "fnl/arglist", {"([])", "([f])", "([f g])", "([f g & fs])"}, "fnl/docstring", "Compose functions.") end)
  v_33_auto = comp0
  core["comp"] = v_33_auto
  comp = v_33_auto
end
local eq
do
  local v_33_auto
  local function eq0(...)
    local _925_ = select("#", ...)
    if (_925_ == 0) then
      return true
    elseif (_925_ == 1) then
      local _ = ...
      return true
    elseif (_925_ == 2) then
      local a, b = ...
      if ((a == b) and (b == a)) then
        return true
      elseif (function(_926_,_927_,_928_) return (_926_ == _927_) and (_927_ == _928_) end)("table",type(a),type(b)) then
        local res, count_a = true, 0
        for k, v in pairs_2a(a) do
          if not res then break end
          local function _929_(...)
            local res0, done = nil, nil
            for k_2a, v0 in pairs_2a(b) do
              if done then break end
              if eq0(k_2a, k) then
                res0, done = v0, true
              else
              end
            end
            return res0
          end
          res = eq0(v, _929_(...))
          count_a = (count_a + 1)
        end
        if res then
          local count_b
          do
            local res0 = 0
            for _, _0 in pairs_2a(b) do
              res0 = (res0 + 1)
            end
            count_b = res0
          end
          res = (count_a == count_b)
        else
        end
        return res
      else
        return false
      end
    elseif true then
      local _ = _925_
      local core_48_auto = require("init")
      local _let_933_ = core_48_auto.list(...)
      local a = _let_933_[1]
      local b = _let_933_[2]
      local cs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_933_, 3)
      return (eq0(a, b) and apply(eq0, b, cs))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(eq0, "fnl/arglist", {"([])", "([_])", "([a b])", "([a b & cs])"}, "fnl/docstring", "Comparison function.\n\nAccepts arbitrary amount of values, and does the deep comparison.  If\nvalues implement `__eq` metamethod, tries to use it, by checking if\nfirst value is equal to second value, and the second value is equal to\nthe first value.  If values are not equal and are tables does the deep\ncomparison.  Tables as keys are supported.") end)
  v_33_auto = eq0
  core["eq"] = v_33_auto
  eq = v_33_auto
end
local function deep_index(tbl, key)
  local res = nil
  for k, v in pairs_2a(tbl) do
    if res then break end
    if eq(k, key) then
      res = v
    else
      res = nil
    end
  end
  return res
end
pcall(function() require("fennel").metadata:setall(deep_index, "fnl/arglist", {"tbl", "key"}, "fnl/docstring", "This function uses the `eq` function to compare keys of the given\ntable `tbl` and the given `key`.  Several other functions also reuse\nthis indexing method, such as sets.") end)
local function deep_newindex(tbl, key, val)
  local done = false
  if ("table" == type(key)) then
    for k, _ in pairs_2a(tbl) do
      if done then break end
      if eq(k, key) then
        rawset(tbl, k, val)
        done = true
      else
      end
    end
  else
  end
  if not done then
    return rawset(tbl, key, val)
  else
    return nil
  end
end
pcall(function() require("fennel").metadata:setall(deep_newindex, "fnl/arglist", {"tbl", "key", "val"}, "fnl/docstring", "This function uses the `eq` function to compare keys of the given\ntable `tbl` and the given `key`. If the key is found it's being\nset, if not a new key is set.") end)
local memoize
do
  local v_33_auto
  local function memoize0(...)
    local f = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "memoize"))
      else
      end
    end
    local memo = setmetatable({}, {__index = deep_index})
    local function fn_940_(...)
      local core_48_auto = require("init")
      local _let_941_ = core_48_auto.list(...)
      local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_941_, 1)
      local _942_ = memo[args]
      if (nil ~= _942_) then
        local res = _942_
        return unpack_2a(res, 1, res.n)
      elseif true then
        local _ = _942_
        local res = pack_2a(f(...))
        do end (memo)[args] = res
        return unpack_2a(res, 1, res.n)
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(fn_940_, "fnl/arglist", {"[& args]"}) end)
    return fn_940_
  end
  pcall(function() require("fennel").metadata:setall(memoize0, "fnl/arglist", {"[f]"}, "fnl/docstring", "Returns a memoized version of a referentially transparent function.\nThe memoized version of the function keeps a cache of the mapping from\narguments to results and, when calls with the same arguments are\nrepeated often, has higher performance at the expense of higher memory\nuse.") end)
  v_33_auto = memoize0
  core["memoize"] = v_33_auto
  memoize = v_33_auto
end
local deref
do
  local v_33_auto
  local function deref0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "deref"))
      else
      end
    end
    local _945_ = getmetatable(x)
    if ((_G.type(_945_) == "table") and (nil ~= (_945_)["cljlib/deref"])) then
      local f = (_945_)["cljlib/deref"]
      return f(x)
    elseif true then
      local _ = _945_
      return error("object doesn't implement cljlib/deref metamethod", 2)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(deref0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Dereference an object.") end)
  v_33_auto = deref0
  core["deref"] = v_33_auto
  deref = v_33_auto
end
local empty
do
  local v_33_auto
  local function empty0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "empty"))
      else
      end
    end
    local _948_ = getmetatable(x)
    if ((_G.type(_948_) == "table") and (nil ~= (_948_)["cljlib/empty"])) then
      local f = (_948_)["cljlib/empty"]
      return f()
    elseif true then
      local _ = _948_
      local _949_ = type(x)
      if (_949_ == "table") then
        return {}
      elseif (_949_ == "string") then
        return ""
      elseif true then
        local _0 = _949_
        return error(("don't know how to create empty variant of type " .. _0))
      else
        return nil
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(empty0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Get an empty variant of a given collection.") end)
  v_33_auto = empty0
  core["empty"] = v_33_auto
  empty = v_33_auto
end
local nil_3f
do
  local v_33_auto
  local function nil_3f0(...)
    local _952_ = select("#", ...)
    if (_952_ == 0) then
      return true
    elseif (_952_ == 1) then
      local x = ...
      return (x == nil)
    elseif true then
      local _ = _952_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "nil?"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(nil_3f0, "fnl/arglist", {"([])", "([x])"}, "fnl/docstring", "Test if `x` is nil.") end)
  v_33_auto = nil_3f0
  core["nil?"] = v_33_auto
  nil_3f = v_33_auto
end
local zero_3f
do
  local v_33_auto
  local function zero_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "zero?"))
      else
      end
    end
    return (x == 0)
  end
  pcall(function() require("fennel").metadata:setall(zero_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is equal to zero.") end)
  v_33_auto = zero_3f0
  core["zero?"] = v_33_auto
  zero_3f = v_33_auto
end
local pos_3f
do
  local v_33_auto
  local function pos_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "pos?"))
      else
      end
    end
    return (x > 0)
  end
  pcall(function() require("fennel").metadata:setall(pos_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is greater than zero.") end)
  v_33_auto = pos_3f0
  core["pos?"] = v_33_auto
  pos_3f = v_33_auto
end
local neg_3f
do
  local v_33_auto
  local function neg_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "neg?"))
      else
      end
    end
    return (x < 0)
  end
  pcall(function() require("fennel").metadata:setall(neg_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is less than zero.") end)
  v_33_auto = neg_3f0
  core["neg?"] = v_33_auto
  neg_3f = v_33_auto
end
local even_3f
do
  local v_33_auto
  local function even_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "even?"))
      else
      end
    end
    return ((x % 2) == 0)
  end
  pcall(function() require("fennel").metadata:setall(even_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is even.") end)
  v_33_auto = even_3f0
  core["even?"] = v_33_auto
  even_3f = v_33_auto
end
local odd_3f
do
  local v_33_auto
  local function odd_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "odd?"))
      else
      end
    end
    return not even_3f(x)
  end
  pcall(function() require("fennel").metadata:setall(odd_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is odd.") end)
  v_33_auto = odd_3f0
  core["odd?"] = v_33_auto
  odd_3f = v_33_auto
end
local string_3f
do
  local v_33_auto
  local function string_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "string?"))
      else
      end
    end
    return (type(x) == "string")
  end
  pcall(function() require("fennel").metadata:setall(string_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a string.") end)
  v_33_auto = string_3f0
  core["string?"] = v_33_auto
  string_3f = v_33_auto
end
local boolean_3f
do
  local v_33_auto
  local function boolean_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "boolean?"))
      else
      end
    end
    return (type(x) == "boolean")
  end
  pcall(function() require("fennel").metadata:setall(boolean_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a Boolean") end)
  v_33_auto = boolean_3f0
  core["boolean?"] = v_33_auto
  boolean_3f = v_33_auto
end
local true_3f
do
  local v_33_auto
  local function true_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "true?"))
      else
      end
    end
    return (x == true)
  end
  pcall(function() require("fennel").metadata:setall(true_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is `true`") end)
  v_33_auto = true_3f0
  core["true?"] = v_33_auto
  true_3f = v_33_auto
end
local false_3f
do
  local v_33_auto
  local function false_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "false?"))
      else
      end
    end
    return (x == false)
  end
  pcall(function() require("fennel").metadata:setall(false_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is `false`") end)
  v_33_auto = false_3f0
  core["false?"] = v_33_auto
  false_3f = v_33_auto
end
local int_3f
do
  local v_33_auto
  local function int_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "int?"))
      else
      end
    end
    return ((type(x) == "number") and (x == math.floor(x)))
  end
  pcall(function() require("fennel").metadata:setall(int_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a number without floating point data.\n\nNumber is rounded with `math.floor` and compared with original number.") end)
  v_33_auto = int_3f0
  core["int?"] = v_33_auto
  int_3f = v_33_auto
end
local pos_int_3f
do
  local v_33_auto
  local function pos_int_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "pos-int?"))
      else
      end
    end
    return (int_3f(x) and pos_3f(x))
  end
  pcall(function() require("fennel").metadata:setall(pos_int_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a positive integer.") end)
  v_33_auto = pos_int_3f0
  core["pos-int?"] = v_33_auto
  pos_int_3f = v_33_auto
end
local neg_int_3f
do
  local v_33_auto
  local function neg_int_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "neg-int?"))
      else
      end
    end
    return (int_3f(x) and neg_3f(x))
  end
  pcall(function() require("fennel").metadata:setall(neg_int_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a negative integer.") end)
  v_33_auto = neg_int_3f0
  core["neg-int?"] = v_33_auto
  neg_int_3f = v_33_auto
end
local double_3f
do
  local v_33_auto
  local function double_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "double?"))
      else
      end
    end
    return ((type(x) == "number") and (x ~= math.floor(x)))
  end
  pcall(function() require("fennel").metadata:setall(double_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Test if `x` is a number with floating point data.") end)
  v_33_auto = double_3f0
  core["double?"] = v_33_auto
  double_3f = v_33_auto
end
local empty_3f
do
  local v_33_auto
  local function empty_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "empty?"))
      else
      end
    end
    local _968_ = type(x)
    if (_968_ == "table") then
      local _969_ = getmetatable(x)
      if ((_G.type(_969_) == "table") and ((_969_)["cljlib/type"] == "seq")) then
        return nil_3f(core.seq(x))
      elseif ((_969_ == nil) or ((_G.type(_969_) == "table") and ((_969_)["cljlib/type"] == nil))) then
        local next_2a = pairs_2a(x)
        return (next_2a(x) == nil)
      else
        return nil
      end
    elseif (_968_ == "string") then
      return (x == "")
    elseif (_968_ == "nil") then
      return true
    elseif true then
      local _ = _968_
      return error("empty?: unsupported collection")
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(empty_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check if collection is empty.") end)
  v_33_auto = empty_3f0
  core["empty?"] = v_33_auto
  empty_3f = v_33_auto
end
local not_empty
do
  local v_33_auto
  local function not_empty0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "not-empty"))
      else
      end
    end
    if not empty_3f(x) then
      return x
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(not_empty0, "fnl/arglist", {"[x]"}, "fnl/docstring", "If `x` is empty, returns `nil`, otherwise `x`.") end)
  v_33_auto = not_empty0
  core["not-empty"] = v_33_auto
  not_empty = v_33_auto
end
local map_3f
do
  local v_33_auto
  local function map_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "map?"))
      else
      end
    end
    if ("table" == type(x)) then
      local _975_ = getmetatable(x)
      if ((_G.type(_975_) == "table") and ((_975_)["cljlib/type"] == "hash-map")) then
        return true
      elseif ((_G.type(_975_) == "table") and ((_975_)["cljlib/type"] == "sorted-map")) then
        return true
      elseif ((_975_ == nil) or ((_G.type(_975_) == "table") and ((_975_)["cljlib/type"] == nil))) then
        local len = length_2a(x)
        local nxt, t, k = pairs_2a(x)
        local function _976_(...)
          if (len == 0) then
            return k
          else
            return len
          end
        end
        return (nil ~= nxt(t, _976_(...)))
      elseif true then
        local _ = _975_
        return false
      else
        return nil
      end
    else
      return false
    end
  end
  pcall(function() require("fennel").metadata:setall(map_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check whether `x` is an associative table.\n\nNon-empty tables are tested by calling `next`. If the length of the\ntable is greater than zero, the last integer key is passed to the\n`next`, and if `next` returns a key, the table is considered\nassociative. If the length is zero, `next` is called with what `paris`\nreturns for the table, and if the key is returned, table is considered\nassociative.\n\nEmpty tables can't be analyzed with this method, and `map?` will\nalways return `false`.  If you need this test pass for empty table,\nsee `hash-map` for creating tables that have additional metadata\nattached for this test to work.\n\n# Examples\nNon-empty map:\n\n``` fennel\n(assert-is (map? {:a 1 :b 2}))\n```\n\nEmpty tables don't pass the test:\n\n``` fennel\n(assert-not (map? {}))\n```\n\nEmpty tables created with `hash-map` will pass the test:\n\n``` fennel\n(assert-is (map? (hash-map)))\n```") end)
  v_33_auto = map_3f0
  core["map?"] = v_33_auto
  map_3f = v_33_auto
end
local vector_3f
do
  local v_33_auto
  local function vector_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "vector?"))
      else
      end
    end
    if ("table" == type(x)) then
      local _980_ = getmetatable(x)
      if ((_G.type(_980_) == "table") and ((_980_)["cljlib/type"] == "vector")) then
        return true
      elseif ((_980_ == nil) or ((_G.type(_980_) == "table") and ((_980_)["cljlib/type"] == nil))) then
        local len = length_2a(x)
        local nxt, t, k = pairs_2a(x)
        local function _981_(...)
          if (len == 0) then
            return k
          else
            return len
          end
        end
        if (nil ~= nxt(t, _981_(...))) then
          return false
        elseif (len > 0) then
          return true
        else
          return false
        end
      elseif true then
        local _ = _980_
        return false
      else
        return nil
      end
    else
      return false
    end
  end
  pcall(function() require("fennel").metadata:setall(vector_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check whether `tbl` is a sequential table.\n\nNon-empty sequential tables are tested for two things:\n- `next` returns the key-value pair,\n- key, that is returned by the `next` is equal to `1`.\n\nEmpty tables can't be analyzed with this method, and `vector?` will\nalways return `false`.  If you need this test pass for empty table,\nsee `vector` for creating tables that have additional\nmetadata attached for this test to work.\n\n# Examples\nNon-empty vector:\n\n``` fennel\n(assert-is (vector? [1 2 3 4]))\n```\n\nEmpty tables don't pass the test:\n\n``` fennel\n(assert-not (vector? []))\n```\n\nEmpty tables created with `vector` will pass the test:\n\n``` fennel\n(assert-is (vector? (vector)))\n```") end)
  v_33_auto = vector_3f0
  core["vector?"] = v_33_auto
  vector_3f = v_33_auto
end
local set_3f
do
  local v_33_auto
  local function set_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "set?"))
      else
      end
    end
    local _986_ = getmetatable(x)
    if ((_G.type(_986_) == "table") and ((_986_)["cljlib/type"] == "hash-set")) then
      return true
    elseif true then
      local _ = _986_
      return false
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(set_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check if object is a set.") end)
  v_33_auto = set_3f0
  core["set?"] = v_33_auto
  set_3f = v_33_auto
end
local seq_3f
do
  local v_33_auto
  local function seq_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "seq?"))
      else
      end
    end
    return lazy["seq?"](x)
  end
  pcall(function() require("fennel").metadata:setall(seq_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Check if object is a sequence.") end)
  v_33_auto = seq_3f0
  core["seq?"] = v_33_auto
  seq_3f = v_33_auto
end
local some_3f
do
  local v_33_auto
  local function some_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "some?"))
      else
      end
    end
    return (x ~= nil)
  end
  pcall(function() require("fennel").metadata:setall(some_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Returns true if x is not nil, false otherwise.") end)
  v_33_auto = some_3f0
  core["some?"] = v_33_auto
  some_3f = v_33_auto
end
local function vec__3etransient(immutable)
  local function _990_(vec)
    local len = #vec
    local function _991_(_, i)
      if (i <= len) then
        return vec[i]
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(_991_, "fnl/arglist", {"_", "i"}) end)
    local function _993_()
      return len
    end
    local function _994_()
      return error("can't `conj` onto transient vector, use `conj!`")
    end
    local function _995_()
      return error("can't `assoc` onto transient vector, use `assoc!`")
    end
    local function _996_()
      return error("can't `dissoc` onto transient vector, use `dissoc!`")
    end
    local function _997_(tvec, v)
      len = (len + 1)
      tvec[len] = v
      return tvec
    end
    pcall(function() require("fennel").metadata:setall(_997_, "fnl/arglist", {"tvec", "v"}) end)
    local function _998_(tvec, ...)
      do
        local len0 = #tvec
        for i = 1, select("#", ...), 2 do
          local k, v = select(i, ...)
          if (1 <= i) and (i <= len0) then
            tvec[i] = v
          else
            error(("index " .. i .. " is out of bounds"))
          end
        end
      end
      return tvec
    end
    pcall(function() require("fennel").metadata:setall(_998_, "fnl/arglist", {"tvec", "..."}) end)
    local function _1000_(tvec)
      if (len == 0) then
        return error("transient vector is empty", 2)
      else
        local val = table.remove(tvec)
        len = (len - 1)
        return tvec
      end
    end
    pcall(function() require("fennel").metadata:setall(_1000_, "fnl/arglist", {"tvec"}) end)
    local function _1002_()
      return error("can't `dissoc!` with a transient vector")
    end
    local function _1003_(tvec)
      local v
      do
        local tbl_17_auto = {}
        local i_18_auto = #tbl_17_auto
        for i = 1, len do
          local val_19_auto = tvec[i]
          if (nil ~= val_19_auto) then
            i_18_auto = (i_18_auto + 1)
            do end (tbl_17_auto)[i_18_auto] = val_19_auto
          else
          end
        end
        v = tbl_17_auto
      end
      while (len > 0) do
        table.remove(tvec)
        len = (len - 1)
      end
      local function _1005_()
        return error("attempt to use transient after it was persistet")
      end
      local function _1006_()
        return error("attempt to use transient after it was persistet")
      end
      setmetatable(tvec, {__index = _1005_, __newindex = _1006_})
      return immutable(itable(v))
    end
    pcall(function() require("fennel").metadata:setall(_1003_, "fnl/arglist", {"tvec"}) end)
    return setmetatable({}, {__index = _991_, __len = _993_, ["cljlib/type"] = "transient", ["cljlib/conj"] = _994_, ["cljlib/assoc"] = _995_, ["cljlib/dissoc"] = _996_, ["cljlib/conj!"] = _997_, ["cljlib/assoc!"] = _998_, ["cljlib/pop!"] = _1000_, ["cljlib/dissoc!"] = _1002_, ["cljlib/persistent!"] = _1003_})
  end
  pcall(function() require("fennel").metadata:setall(_990_, "fnl/arglist", {"vec"}) end)
  return _990_
end
pcall(function() require("fennel").metadata:setall(vec__3etransient, "fnl/arglist", {"immutable"}) end)
local function vec_2a(v, len)
  do
    local _1007_ = getmetatable(v)
    if (nil ~= _1007_) then
      local mt = _1007_
      mt["__len"] = constantly((len or length_2a(v)))
      do end (mt)["cljlib/type"] = "vector"
      mt["cljlib/editable"] = true
      local function _1008_(t, v0)
        local len0 = length_2a(t)
        return vec_2a(itable.assoc(t, (len0 + 1), v0), (len0 + 1))
      end
      pcall(function() require("fennel").metadata:setall(_1008_, "fnl/arglist", {"t", "v"}) end)
      do end (mt)["cljlib/conj"] = _1008_
      local function _1009_(t)
        local len0 = (length_2a(t) - 1)
        local coll = {}
        if (len0 < 0) then
          error("can't pop empty vector", 2)
        else
        end
        for i = 1, len0 do
          coll[i] = t[i]
        end
        return vec_2a(itable(coll), len0)
      end
      pcall(function() require("fennel").metadata:setall(_1009_, "fnl/arglist", {"t"}) end)
      do end (mt)["cljlib/pop"] = _1009_
      local function _1011_()
        return vec_2a(itable({}))
      end
      pcall(function() require("fennel").metadata:setall(_1011_, "fnl/arglist", {}) end)
      do end (mt)["cljlib/empty"] = _1011_
      mt["cljlib/transient"] = vec__3etransient(vec_2a)
      local function _1012_(coll, view, inspector, indent)
        if empty_3f(coll) then
          return "[]"
        else
          local lines
          do
            local tbl_17_auto = {}
            local i_18_auto = #tbl_17_auto
            for i = 1, length_2a(coll) do
              local val_19_auto = (" " .. view(coll[i], inspector, indent))
              if (nil ~= val_19_auto) then
                i_18_auto = (i_18_auto + 1)
                do end (tbl_17_auto)[i_18_auto] = val_19_auto
              else
              end
            end
            lines = tbl_17_auto
          end
          lines[1] = ("[" .. string.gsub((lines[1] or ""), "^%s+", ""))
          do end (lines)[#lines] = (lines[#lines] .. "]")
          return lines
        end
      end
      pcall(function() require("fennel").metadata:setall(_1012_, "fnl/arglist", {"coll", "view", "inspector", "indent"}) end)
      do end (mt)["__fennelview"] = _1012_
    elseif (_1007_ == nil) then
      vec_2a(setmetatable(v, {}))
    else
    end
  end
  return v
end
pcall(function() require("fennel").metadata:setall(vec_2a, "fnl/arglist", {"v", "len"}) end)
local vec
do
  local v_33_auto
  local function vec0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "vec"))
      else
      end
    end
    if empty_3f(coll) then
      return vec_2a(itable({}), 0)
    elseif vector_3f(coll) then
      return vec_2a(itable(coll), length_2a(coll))
    elseif "else" then
      local packed = lazy.pack(core.seq(coll))
      local len = packed.n
      local _1017_
      do
        packed["n"] = nil
        _1017_ = packed
      end
      return vec_2a(itable(_1017_, {["fast-index?"] = true}), len)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(vec0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Coerce collection `coll` to a vector.") end)
  v_33_auto = vec0
  core["vec"] = v_33_auto
  vec = v_33_auto
end
local vector
do
  local v_33_auto
  local function vector0(...)
    local core_48_auto = require("init")
    local _let_1019_ = core_48_auto.list(...)
    local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1019_, 1)
    return vec(args)
  end
  pcall(function() require("fennel").metadata:setall(vector0, "fnl/arglist", {"[& args]"}, "fnl/docstring", "Constructs sequential table out of its arguments.\n\nSets additional metadata for function `vector?` to work.\n\n# Examples\n\n``` fennel\n(def :private v (vector 1 2 3 4))\n(assert-eq v [1 2 3 4])\n```") end)
  v_33_auto = vector0
  core["vector"] = v_33_auto
  vector = v_33_auto
end
local nth
do
  local v_33_auto
  local function nth0(...)
    local _1021_ = select("#", ...)
    if (_1021_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "nth"))
    elseif (_1021_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "nth"))
    elseif (_1021_ == 2) then
      local coll, i = ...
      if vector_3f(coll) then
        if ((i < 1) or (length_2a(coll) < i)) then
          return error(string.format("index %d is out of bounds", i))
        else
          return coll[i]
        end
      elseif string_3f(coll) then
        return nth0(vec(coll), i)
      elseif seq_3f(coll) then
        return nth0(vec(coll), i)
      elseif "else" then
        return error("expected an indexed collection")
      else
        return nil
      end
    elseif (_1021_ == 3) then
      local coll, i, not_found = ...
      assert(int_3f(i), "expected an integer key")
      if vector_3f(coll) then
        return (coll[i] or not_found)
      elseif string_3f(coll) then
        return nth0(vec(coll), i, not_found)
      elseif seq_3f(coll) then
        return nth0(vec(coll), i, not_found)
      elseif "else" then
        return error("expected an indexed collection")
      else
        return nil
      end
    elseif true then
      local _ = _1021_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "nth"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(nth0, "fnl/arglist", {"([coll i])", "([coll i not-found])"}, "fnl/docstring", "Returns the value at the `index`. `get` returns `nil` if `index` out\nof bounds, `nth` raises an error unless `not-found` is supplied.\n`nth` also works for strings and sequences.") end)
  v_33_auto = nth0
  core["nth"] = v_33_auto
  nth = v_33_auto
end
local seq_2a
local function seq_2a0(...)
  local x = ...
  do
    local cnt_68_auto = select("#", ...)
    if (1 ~= cnt_68_auto) then
      error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "seq*"))
    else
    end
  end
  do
    local _1027_ = getmetatable(x)
    if (nil ~= _1027_) then
      local mt = _1027_
      mt["cljlib/type"] = "seq"
      local function _1028_(s, v)
        return core.cons(v, s)
      end
      pcall(function() require("fennel").metadata:setall(_1028_, "fnl/arglist", {"s", "v"}) end)
      do end (mt)["cljlib/conj"] = _1028_
      local function _1029_()
        return core.list()
      end
      mt["cljlib/empty"] = _1029_
    else
    end
  end
  return x
end
pcall(function() require("fennel").metadata:setall(seq_2a0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Add cljlib sequence meta-info.") end)
seq_2a = seq_2a0
local seq
do
  local v_33_auto
  local function seq0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "seq"))
      else
      end
    end
    local function _1033_(...)
      local _1032_ = getmetatable(coll)
      if ((_G.type(_1032_) == "table") and (nil ~= (_1032_)["cljlib/seq"])) then
        local f = (_1032_)["cljlib/seq"]
        return f(coll)
      elseif true then
        local _ = _1032_
        if lazy["seq?"](coll) then
          return lazy.seq(coll)
        elseif map_3f(coll) then
          return lazy.map(vec, coll)
        elseif "else" then
          return lazy.seq(coll)
        else
          return nil
        end
      else
        return nil
      end
    end
    return seq_2a(_1033_(...))
  end
  pcall(function() require("fennel").metadata:setall(seq0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Construct a sequence from the given collection `coll`.  If `coll` is\nan associative table, returns sequence of vectors with key and value.\nIf `col` is sequential table, returns its shallow copy.  If `col` is\nstring, return sequential table of its codepoints.\n\n# Examples\nSequential tables are transformed to sequences:\n\n``` fennel\n(seq [1 2 3 4]) ;; @seq(1 2 3 4)\n```\n\nAssociative tables are transformed to format like this `[[key1 value1]\n... [keyN valueN]]` and order is non-deterministic:\n\n``` fennel\n(seq {:a 1 :b 2 :c 3}) ;; @seq([:b 2] [:a 1] [:c 3])\n```") end)
  v_33_auto = seq0
  core["seq"] = v_33_auto
  seq = v_33_auto
end
local rseq
do
  local v_33_auto
  local function rseq0(...)
    local rev = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "rseq"))
      else
      end
    end
    return seq_2a(lazy.rseq(rev))
  end
  pcall(function() require("fennel").metadata:setall(rseq0, "fnl/arglist", {"[rev]"}, "fnl/docstring", "Returns, in possibly-constant time, a seq of the items in `rev` in reverse order.\nInput must be traversable with `ipairs`.  Doesn't work in constant\ntime if `rev` implements a linear-time `__len` metamethod, or invoking\nLua `#` operator on `rev` takes linar time.  If `t` is empty returns\n`nil`.\n\n# Examples\n\n``` fennel\n(def :private v [1 2 3])\n(def :private r (rseq v))\n\n(assert-eq (reverse v) r)\n```") end)
  v_33_auto = rseq0
  core["rseq"] = v_33_auto
  rseq = v_33_auto
end
local lazy_seq
do
  local v_33_auto
  local function lazy_seq0(...)
    local f = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "lazy-seq"))
      else
      end
    end
    return seq_2a(lazy["lazy-seq"](f))
  end
  pcall(function() require("fennel").metadata:setall(lazy_seq0, "fnl/arglist", {"[f]"}, "fnl/docstring", "Create lazy sequence from the result of calling a function `f`.\nDelays execution of `f` until sequence is consumed.  `f` must return a\nsequence or a vector.") end)
  v_33_auto = lazy_seq0
  core["lazy-seq"] = v_33_auto
  lazy_seq = v_33_auto
end
local first
do
  local v_33_auto
  local function first0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "first"))
      else
      end
    end
    return lazy.first(seq(coll))
  end
  pcall(function() require("fennel").metadata:setall(first0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Return first element of a `coll`. Calls `seq` on its argument.") end)
  v_33_auto = first0
  core["first"] = v_33_auto
  first = v_33_auto
end
local rest
do
  local v_33_auto
  local function rest0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "rest"))
      else
      end
    end
    return seq_2a(lazy.rest(seq(coll)))
  end
  pcall(function() require("fennel").metadata:setall(rest0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a sequence of all elements of a `coll` but the first one.\nCalls `seq` on its argument.") end)
  v_33_auto = rest0
  core["rest"] = v_33_auto
  rest = v_33_auto
end
local next_2a
local function next_2a0(...)
  local s = ...
  do
    local cnt_68_auto = select("#", ...)
    if (1 ~= cnt_68_auto) then
      error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "next*"))
    else
    end
  end
  return seq_2a(lazy.next(s))
end
pcall(function() require("fennel").metadata:setall(next_2a0, "fnl/arglist", {"[s]"}, "fnl/docstring", "Return the tail of a sequence.\n\nIf the sequence is empty, returns nil.") end)
next_2a = next_2a0
do
  core["next"] = next_2a
end
local count
do
  local v_33_auto
  local function count0(...)
    local s = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "count"))
      else
      end
    end
    local _1042_ = getmetatable(s)
    if ((_G.type(_1042_) == "table") and ((_1042_)["cljlib/type"] == "vector")) then
      return length_2a(s)
    elseif true then
      local _ = _1042_
      return lazy.count(s)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(count0, "fnl/arglist", {"[s]"}, "fnl/docstring", "Count amount of elements in the sequence.") end)
  v_33_auto = count0
  core["count"] = v_33_auto
  count = v_33_auto
end
local cons
do
  local v_33_auto
  local function cons0(...)
    local head, tail = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "cons"))
      else
      end
    end
    return seq_2a(lazy.cons(head, tail))
  end
  pcall(function() require("fennel").metadata:setall(cons0, "fnl/arglist", {"[head tail]"}, "fnl/docstring", "Construct a cons cell.\nPrepends new `head` to a `tail`, which must be either a table,\nsequence, or nil.\n\n# Examples\n\n``` fennel\n(assert-eq [0 1] (cons 0 [1]))\n(assert-eq (list 0 1 2 3) (cons 0 (cons 1 (list 2 3))))\n```") end)
  v_33_auto = cons0
  core["cons"] = v_33_auto
  cons = v_33_auto
end
local function list(...)
  return seq_2a(lazy.list(...))
end
pcall(function() require("fennel").metadata:setall(list, "fnl/arglist", {"..."}, "fnl/docstring", "Create eager sequence of provided values.\n\n# Examples\n\n``` fennel\n(local l (list 1 2 3 4 5))\n(assert-eq [1 2 3 4 5] l)\n```") end)
core.list = list
local list_2a
do
  local v_33_auto
  local function list_2a0(...)
    local core_48_auto = require("init")
    local _let_1045_ = core_48_auto.list(...)
    local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1045_, 1)
    return seq_2a(apply(lazy["list*"], args))
  end
  pcall(function() require("fennel").metadata:setall(list_2a0, "fnl/arglist", {"[& args]"}, "fnl/docstring", "Creates a new sequence containing the items prepended to the rest,\nthe last of which will be treated as a sequence.\n\n# Examples\n\n``` fennel\n(local l (list* 1 2 3 [4 5]))\n(assert-eq [1 2 3 4 5] l)\n```") end)
  v_33_auto = list_2a0
  core["list*"] = v_33_auto
  list_2a = v_33_auto
end
local last
do
  local v_33_auto
  local function last0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "last"))
      else
      end
    end
    local _1047_ = next_2a(coll)
    if (nil ~= _1047_) then
      local coll_2a = _1047_
      return last0(coll_2a)
    elseif true then
      local _ = _1047_
      return first(coll)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(last0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns the last element of a `coll`. Calls `seq` on its argument.") end)
  v_33_auto = last0
  core["last"] = v_33_auto
  last = v_33_auto
end
local butlast
do
  local v_33_auto
  local function butlast0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "butlast"))
      else
      end
    end
    return seq(lazy["drop-last"](coll))
  end
  pcall(function() require("fennel").metadata:setall(butlast0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns everything but the last element of the `coll` as a new\n  sequence.  Calls `seq` on its argument.") end)
  v_33_auto = butlast0
  core["butlast"] = v_33_auto
  butlast = v_33_auto
end
local map
do
  local v_33_auto
  local function map0(...)
    local _1050_ = select("#", ...)
    if (_1050_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "map"))
    elseif (_1050_ == 1) then
      local f = ...
      local function fn_1051_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1051_"))
          else
          end
        end
        local function fn_1053_(...)
          local _1054_ = select("#", ...)
          if (_1054_ == 0) then
            return rf()
          elseif (_1054_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1054_ == 2) then
            local result, input = ...
            return rf(result, f(input))
          elseif true then
            local _ = _1054_
            local core_48_auto = require("init")
            local _let_1055_ = core_48_auto.list(...)
            local result = _let_1055_[1]
            local input = _let_1055_[2]
            local inputs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1055_, 3)
            return rf(result, apply(f, input, inputs))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1053_, "fnl/arglist", {"([])", "([result])", "([result input])", "([result input & inputs])"}) end)
        return fn_1053_
      end
      pcall(function() require("fennel").metadata:setall(fn_1051_, "fnl/arglist", {"[rf]"}) end)
      return fn_1051_
    elseif (_1050_ == 2) then
      local f, coll = ...
      return seq_2a(lazy.map(f, coll))
    elseif true then
      local _ = _1050_
      local core_48_auto = require("init")
      local _let_1057_ = core_48_auto.list(...)
      local f = _let_1057_[1]
      local coll = _let_1057_[2]
      local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1057_, 3)
      return seq_2a(apply(lazy.map, f, coll, colls))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(map0, "fnl/arglist", {"([f])", "([f coll])", "([f coll & colls])"}, "fnl/docstring", "Returns a lazy sequence consisting of the result of applying `f` to\nthe set of first items of each `coll`, followed by applying `f` to the\nset of second items in each `coll`, until any one of the `colls` is\nexhausted.  Any remaining items in other `colls` are ignored. Function\n`f` should accept number-of-colls arguments. Returns a transducer when\nno collection is provided.\n\n# Examples\n\n``` fennel\n(map #(+ $ 1) [1 2 3]) ;; => @seq(2 3 4)\n(map #(+ $1 $2) [1 2 3] [4 5 6]) ;; => @seq(5 7 9)\n(def :private res (map #(+ $ 1) [:a :b :c])) ;; will raise an error only when realized\n```") end)
  v_33_auto = map0
  core["map"] = v_33_auto
  map = v_33_auto
end
local mapv
do
  local v_33_auto
  local function mapv0(...)
    local _1060_ = select("#", ...)
    if (_1060_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "mapv"))
    elseif (_1060_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "mapv"))
    elseif (_1060_ == 2) then
      local f, coll = ...
      return core["persistent!"](core.transduce(map(f), core["conj!"], core.transient(vector()), coll))
    elseif true then
      local _ = _1060_
      local core_48_auto = require("init")
      local _let_1061_ = core_48_auto.list(...)
      local f = _let_1061_[1]
      local coll = _let_1061_[2]
      local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1061_, 3)
      return vec(apply(map, f, coll, colls))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(mapv0, "fnl/arglist", {"([f coll])", "([f coll & colls])"}, "fnl/docstring", "Returns a vector consisting of the result of applying `f` to the\nset of first items of each `coll`, followed by applying `f` to the set\nof second items in each coll, until any one of the `colls` is\nexhausted.  Any remaining items in other collections are ignored.\nFunction `f` should accept number-of-colls arguments.") end)
  v_33_auto = mapv0
  core["mapv"] = v_33_auto
  mapv = v_33_auto
end
local map_indexed
do
  local v_33_auto
  local function map_indexed0(...)
    local _1063_ = select("#", ...)
    if (_1063_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "map-indexed"))
    elseif (_1063_ == 1) then
      local f = ...
      local function fn_1064_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1064_"))
          else
          end
        end
        local i = -1
        local function fn_1066_(...)
          local _1067_ = select("#", ...)
          if (_1067_ == 0) then
            return rf()
          elseif (_1067_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1067_ == 2) then
            local result, input = ...
            i = (i + 1)
            return rf(result, f(i, input))
          elseif true then
            local _ = _1067_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1066_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1066_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1066_
      end
      pcall(function() require("fennel").metadata:setall(fn_1064_, "fnl/arglist", {"[rf]"}) end)
      return fn_1064_
    elseif (_1063_ == 2) then
      local f, coll = ...
      return seq_2a(lazy["map-indexed"](f, coll))
    elseif true then
      local _ = _1063_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "map-indexed"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(map_indexed0, "fnl/arglist", {"([f])", "([f coll])"}, "fnl/docstring", "Returns a lazy sequence consisting of the result of applying `f` to 1\nand the first item of `coll`, followed by applying `f` to 2 and the\nsecond item in `coll`, etc., until `coll` is exhausted.  Returns a\ntransducer when no collection is provided.") end)
  v_33_auto = map_indexed0
  core["map-indexed"] = v_33_auto
  map_indexed = v_33_auto
end
local mapcat
do
  local v_33_auto
  local function mapcat0(...)
    local _1070_ = select("#", ...)
    if (_1070_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "mapcat"))
    elseif (_1070_ == 1) then
      local f = ...
      return comp(map(f), core.cat)
    elseif true then
      local _ = _1070_
      local core_48_auto = require("init")
      local _let_1071_ = core_48_auto.list(...)
      local f = _let_1071_[1]
      local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1071_, 2)
      return seq_2a(apply(lazy.mapcat, f, colls))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(mapcat0, "fnl/arglist", {"([f])", "([f & colls])"}, "fnl/docstring", "Apply `concat` to the result of calling `map` with `f` and\ncollections `colls`. Returns a transducer when no collection is\nprovided.") end)
  v_33_auto = mapcat0
  core["mapcat"] = v_33_auto
  mapcat = v_33_auto
end
local filter
do
  local v_33_auto
  local function filter0(...)
    local _1073_ = select("#", ...)
    if (_1073_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "filter"))
    elseif (_1073_ == 1) then
      local pred = ...
      local function fn_1074_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1074_"))
          else
          end
        end
        local function fn_1076_(...)
          local _1077_ = select("#", ...)
          if (_1077_ == 0) then
            return rf()
          elseif (_1077_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1077_ == 2) then
            local result, input = ...
            if pred(input) then
              return rf(result, input)
            else
              return result
            end
          elseif true then
            local _ = _1077_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1076_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1076_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1076_
      end
      pcall(function() require("fennel").metadata:setall(fn_1074_, "fnl/arglist", {"[rf]"}) end)
      return fn_1074_
    elseif (_1073_ == 2) then
      local pred, coll = ...
      return seq_2a(lazy.filter(pred, coll))
    elseif true then
      local _ = _1073_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "filter"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(filter0, "fnl/arglist", {"([pred])", "([pred coll])"}, "fnl/docstring", "Returns a lazy sequence of the items in `coll` for which\n`pred` returns logical true. Returns a transducer when no collection\nis provided.") end)
  v_33_auto = filter0
  core["filter"] = v_33_auto
  filter = v_33_auto
end
local filterv
do
  local v_33_auto
  local function filterv0(...)
    local pred, coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "filterv"))
      else
      end
    end
    return vec(filter(pred, coll))
  end
  pcall(function() require("fennel").metadata:setall(filterv0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Returns a vector of the items in `coll` for which\n`pred` returns logical true.") end)
  v_33_auto = filterv0
  core["filterv"] = v_33_auto
  filterv = v_33_auto
end
local every_3f
do
  local v_33_auto
  local function every_3f0(...)
    local pred, coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "every?"))
      else
      end
    end
    return lazy["every?"](pred, coll)
  end
  pcall(function() require("fennel").metadata:setall(every_3f0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Test if every item in `coll` satisfies the `pred`.") end)
  v_33_auto = every_3f0
  core["every?"] = v_33_auto
  every_3f = v_33_auto
end
local some
do
  local v_33_auto
  local function some0(...)
    local pred, coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "some"))
      else
      end
    end
    return lazy["some?"](pred, coll)
  end
  pcall(function() require("fennel").metadata:setall(some0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Test if any item in `coll` satisfies the `pred`.") end)
  v_33_auto = some0
  core["some"] = v_33_auto
  some = v_33_auto
end
local not_any_3f
do
  local v_33_auto
  local function not_any_3f0(...)
    local pred, coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "not-any?"))
      else
      end
    end
    local function _1085_(_241)
      return not pred(_241)
    end
    return some(_1085_, coll)
  end
  pcall(function() require("fennel").metadata:setall(not_any_3f0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Test if no item in `coll` satisfy the `pred`.") end)
  v_33_auto = not_any_3f0
  core["not-any?"] = v_33_auto
  not_any_3f = v_33_auto
end
local range
do
  local v_33_auto
  local function range0(...)
    local _1086_ = select("#", ...)
    if (_1086_ == 0) then
      return seq_2a(lazy.range())
    elseif (_1086_ == 1) then
      local upper = ...
      return seq_2a(lazy.range(upper))
    elseif (_1086_ == 2) then
      local lower, upper = ...
      return seq_2a(lazy.range(lower, upper))
    elseif (_1086_ == 3) then
      local lower, upper, step = ...
      return seq_2a(lazy.range(lower, upper, step))
    elseif true then
      local _ = _1086_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "range"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(range0, "fnl/arglist", {"([])", "([upper])", "([lower upper])", "([lower upper step])"}, "fnl/docstring", "Returns lazy sequence of numbers from `lower` to `upper` with optional\n`step`.") end)
  v_33_auto = range0
  core["range"] = v_33_auto
  range = v_33_auto
end
local concat
do
  local v_33_auto
  local function concat0(...)
    local core_48_auto = require("init")
    local _let_1088_ = core_48_auto.list(...)
    local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1088_, 1)
    return seq_2a(apply(lazy.concat, colls))
  end
  pcall(function() require("fennel").metadata:setall(concat0, "fnl/arglist", {"[& colls]"}, "fnl/docstring", "Return a lazy sequence of concatenated `colls`.") end)
  v_33_auto = concat0
  core["concat"] = v_33_auto
  concat = v_33_auto
end
local reverse
do
  local v_33_auto
  local function reverse0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "reverse"))
      else
      end
    end
    return seq_2a(lazy.reverse(coll))
  end
  pcall(function() require("fennel").metadata:setall(reverse0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a lazy sequence with same items as in `coll` but in reverse order.") end)
  v_33_auto = reverse0
  core["reverse"] = v_33_auto
  reverse = v_33_auto
end
local take
do
  local v_33_auto
  local function take0(...)
    local _1090_ = select("#", ...)
    if (_1090_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "take"))
    elseif (_1090_ == 1) then
      local n = ...
      local function fn_1091_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1091_"))
          else
          end
        end
        local n0 = n
        local function fn_1093_(...)
          local _1094_ = select("#", ...)
          if (_1094_ == 0) then
            return rf()
          elseif (_1094_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1094_ == 2) then
            local result, input = ...
            local result0
            if (0 < n0) then
              result0 = rf(result, input)
            else
              result0 = result
            end
            n0 = (n0 - 1)
            if not (0 < n0) then
              return core["ensure-reduced"](result0)
            else
              return result0
            end
          elseif true then
            local _ = _1094_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1093_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1093_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1093_
      end
      pcall(function() require("fennel").metadata:setall(fn_1091_, "fnl/arglist", {"[rf]"}) end)
      return fn_1091_
    elseif (_1090_ == 2) then
      local n, coll = ...
      return seq_2a(lazy.take(n, coll))
    elseif true then
      local _ = _1090_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "take"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(take0, "fnl/arglist", {"([n])", "([n coll])"}, "fnl/docstring", "Returns a lazy sequence of the first `n` items in `coll`, or all items if\nthere are fewer than `n`.") end)
  v_33_auto = take0
  core["take"] = v_33_auto
  take = v_33_auto
end
local take_while
do
  local v_33_auto
  local function take_while0(...)
    local _1099_ = select("#", ...)
    if (_1099_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "take-while"))
    elseif (_1099_ == 1) then
      local pred = ...
      local function fn_1100_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1100_"))
          else
          end
        end
        local function fn_1102_(...)
          local _1103_ = select("#", ...)
          if (_1103_ == 0) then
            return rf()
          elseif (_1103_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1103_ == 2) then
            local result, input = ...
            if pred(input) then
              return rf(result, input)
            else
              return core.reduced(result)
            end
          elseif true then
            local _ = _1103_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1102_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1102_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1102_
      end
      pcall(function() require("fennel").metadata:setall(fn_1100_, "fnl/arglist", {"[rf]"}) end)
      return fn_1100_
    elseif (_1099_ == 2) then
      local pred, coll = ...
      return seq_2a(lazy["take-while"](pred, coll))
    elseif true then
      local _ = _1099_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "take-while"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(take_while0, "fnl/arglist", {"([pred])", "([pred coll])"}, "fnl/docstring", "Take the elements from the collection `coll` until `pred` returns logical\nfalse for any of the elemnts.  Returns a lazy sequence. Returns a\ntransducer when no collection is provided.") end)
  v_33_auto = take_while0
  core["take-while"] = v_33_auto
  take_while = v_33_auto
end
local drop
do
  local v_33_auto
  local function drop0(...)
    local _1107_ = select("#", ...)
    if (_1107_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "drop"))
    elseif (_1107_ == 1) then
      local n = ...
      local function fn_1108_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1108_"))
          else
          end
        end
        local nv = n
        local function fn_1110_(...)
          local _1111_ = select("#", ...)
          if (_1111_ == 0) then
            return rf()
          elseif (_1111_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1111_ == 2) then
            local result, input = ...
            local n0 = nv
            nv = (nv - 1)
            if pos_3f(n0) then
              return result
            else
              return rf(result, input)
            end
          elseif true then
            local _ = _1111_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1110_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1110_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1110_
      end
      pcall(function() require("fennel").metadata:setall(fn_1108_, "fnl/arglist", {"[rf]"}) end)
      return fn_1108_
    elseif (_1107_ == 2) then
      local n, coll = ...
      return seq_2a(lazy.drop(n, coll))
    elseif true then
      local _ = _1107_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "drop"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(drop0, "fnl/arglist", {"([n])", "([n coll])"}, "fnl/docstring", "Drop `n` elements from collection `coll`, returning a lazy sequence\nof remaining elements. Returns a transducer when no collection is\nprovided.") end)
  v_33_auto = drop0
  core["drop"] = v_33_auto
  drop = v_33_auto
end
local drop_while
do
  local v_33_auto
  local function drop_while0(...)
    local _1115_ = select("#", ...)
    if (_1115_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "drop-while"))
    elseif (_1115_ == 1) then
      local pred = ...
      local function fn_1116_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1116_"))
          else
          end
        end
        local dv = true
        local function fn_1118_(...)
          local _1119_ = select("#", ...)
          if (_1119_ == 0) then
            return rf()
          elseif (_1119_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1119_ == 2) then
            local result, input = ...
            local drop_3f = dv
            if (drop_3f and pred(input)) then
              return result
            else
              dv = nil
              return rf(result, input)
            end
          elseif true then
            local _ = _1119_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1118_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1118_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1118_
      end
      pcall(function() require("fennel").metadata:setall(fn_1116_, "fnl/arglist", {"[rf]"}) end)
      return fn_1116_
    elseif (_1115_ == 2) then
      local pred, coll = ...
      return seq_2a(lazy["drop-while"](pred, coll))
    elseif true then
      local _ = _1115_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "drop-while"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(drop_while0, "fnl/arglist", {"([pred])", "([pred coll])"}, "fnl/docstring", "Drop the elements from the collection `coll` until `pred` returns logical\nfalse for any of the elemnts.  Returns a lazy sequence. Returns a\ntransducer when no collection is provided.") end)
  v_33_auto = drop_while0
  core["drop-while"] = v_33_auto
  drop_while = v_33_auto
end
local drop_last
do
  local v_33_auto
  local function drop_last0(...)
    local _1123_ = select("#", ...)
    if (_1123_ == 0) then
      return seq_2a(lazy["drop-last"]())
    elseif (_1123_ == 1) then
      local coll = ...
      return seq_2a(lazy["drop-last"](coll))
    elseif (_1123_ == 2) then
      local n, coll = ...
      return seq_2a(lazy["drop-last"](n, coll))
    elseif true then
      local _ = _1123_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "drop-last"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(drop_last0, "fnl/arglist", {"([])", "([coll])", "([n coll])"}, "fnl/docstring", "Return a lazy sequence from `coll` without last `n` elements.") end)
  v_33_auto = drop_last0
  core["drop-last"] = v_33_auto
  drop_last = v_33_auto
end
local take_last
do
  local v_33_auto
  local function take_last0(...)
    local n, coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "take-last"))
      else
      end
    end
    return seq_2a(lazy["take-last"](n, coll))
  end
  pcall(function() require("fennel").metadata:setall(take_last0, "fnl/arglist", {"[n coll]"}, "fnl/docstring", "Return a sequence of last `n` elements of the `coll`.") end)
  v_33_auto = take_last0
  core["take-last"] = v_33_auto
  take_last = v_33_auto
end
local take_nth
do
  local v_33_auto
  local function take_nth0(...)
    local _1126_ = select("#", ...)
    if (_1126_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "take-nth"))
    elseif (_1126_ == 1) then
      local n = ...
      local function fn_1127_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1127_"))
          else
          end
        end
        local iv = -1
        local function fn_1129_(...)
          local _1130_ = select("#", ...)
          if (_1130_ == 0) then
            return rf()
          elseif (_1130_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1130_ == 2) then
            local result, input = ...
            iv = (iv + 1)
            if (0 == (iv % n)) then
              return rf(result, input)
            else
              return result
            end
          elseif true then
            local _ = _1130_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1129_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1129_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1129_
      end
      pcall(function() require("fennel").metadata:setall(fn_1127_, "fnl/arglist", {"[rf]"}) end)
      return fn_1127_
    elseif (_1126_ == 2) then
      local n, coll = ...
      return seq_2a(lazy["take-nth"](n, coll))
    elseif true then
      local _ = _1126_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "take-nth"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(take_nth0, "fnl/arglist", {"([n])", "([n coll])"}, "fnl/docstring", "Return a lazy sequence of every `n` item in `coll`. Returns a\ntransducer when no collection is provided.") end)
  v_33_auto = take_nth0
  core["take-nth"] = v_33_auto
  take_nth = v_33_auto
end
local split_at
do
  local v_33_auto
  local function split_at0(...)
    local n, coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "split-at"))
      else
      end
    end
    return vec(lazy["split-at"](n, coll))
  end
  pcall(function() require("fennel").metadata:setall(split_at0, "fnl/arglist", {"[n coll]"}, "fnl/docstring", "Return a table with sequence `coll` being split at `n`") end)
  v_33_auto = split_at0
  core["split-at"] = v_33_auto
  split_at = v_33_auto
end
local split_with
do
  local v_33_auto
  local function split_with0(...)
    local pred, coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "split-with"))
      else
      end
    end
    return vec(lazy["split-with"](pred, coll))
  end
  pcall(function() require("fennel").metadata:setall(split_with0, "fnl/arglist", {"[pred coll]"}, "fnl/docstring", "Return a table with sequence `coll` being split with `pred`") end)
  v_33_auto = split_with0
  core["split-with"] = v_33_auto
  split_with = v_33_auto
end
local nthrest
do
  local v_33_auto
  local function nthrest0(...)
    local coll, n = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "nthrest"))
      else
      end
    end
    return seq_2a(lazy.nthrest(coll, n))
  end
  pcall(function() require("fennel").metadata:setall(nthrest0, "fnl/arglist", {"[coll n]"}, "fnl/docstring", "Returns the nth rest of `coll`, `coll` when `n` is 0.\n\n# Examples\n\n``` fennel\n(assert-eq (nthrest [1 2 3 4] 3) [4])\n(assert-eq (nthrest [1 2 3 4] 2) [3 4])\n(assert-eq (nthrest [1 2 3 4] 1) [2 3 4])\n(assert-eq (nthrest [1 2 3 4] 0) [1 2 3 4])\n```\n") end)
  v_33_auto = nthrest0
  core["nthrest"] = v_33_auto
  nthrest = v_33_auto
end
local nthnext
do
  local v_33_auto
  local function nthnext0(...)
    local coll, n = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "nthnext"))
      else
      end
    end
    return lazy.nthnext(coll, n)
  end
  pcall(function() require("fennel").metadata:setall(nthnext0, "fnl/arglist", {"[coll n]"}, "fnl/docstring", "Returns the nth next of `coll`, (seq coll) when `n` is 0.") end)
  v_33_auto = nthnext0
  core["nthnext"] = v_33_auto
  nthnext = v_33_auto
end
local keep
do
  local v_33_auto
  local function keep0(...)
    local _1138_ = select("#", ...)
    if (_1138_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "keep"))
    elseif (_1138_ == 1) then
      local f = ...
      local function fn_1139_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1139_"))
          else
          end
        end
        local function fn_1141_(...)
          local _1142_ = select("#", ...)
          if (_1142_ == 0) then
            return rf()
          elseif (_1142_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1142_ == 2) then
            local result, input = ...
            local v = f(input)
            if nil_3f(v) then
              return result
            else
              return rf(result, v)
            end
          elseif true then
            local _ = _1142_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1141_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1141_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1141_
      end
      pcall(function() require("fennel").metadata:setall(fn_1139_, "fnl/arglist", {"[rf]"}) end)
      return fn_1139_
    elseif (_1138_ == 2) then
      local f, coll = ...
      return seq_2a(lazy.keep(f, coll))
    elseif true then
      local _ = _1138_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "keep"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(keep0, "fnl/arglist", {"([f])", "([f coll])"}, "fnl/docstring", "Returns a lazy sequence of the non-nil results of calling `f` on the\nitems of the `coll`. Returns a transducer when no collection is\nprovided.") end)
  v_33_auto = keep0
  core["keep"] = v_33_auto
  keep = v_33_auto
end
local keep_indexed
do
  local v_33_auto
  local function keep_indexed0(...)
    local _1146_ = select("#", ...)
    if (_1146_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "keep-indexed"))
    elseif (_1146_ == 1) then
      local f = ...
      local function fn_1147_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1147_"))
          else
          end
        end
        local iv = -1
        local function fn_1149_(...)
          local _1150_ = select("#", ...)
          if (_1150_ == 0) then
            return rf()
          elseif (_1150_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1150_ == 2) then
            local result, input = ...
            iv = (iv + 1)
            local v = f(iv, input)
            if nil_3f(v) then
              return result
            else
              return rf(result, v)
            end
          elseif true then
            local _ = _1150_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1149_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1149_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1149_
      end
      pcall(function() require("fennel").metadata:setall(fn_1147_, "fnl/arglist", {"[rf]"}) end)
      return fn_1147_
    elseif (_1146_ == 2) then
      local f, coll = ...
      return seq_2a(lazy["keep-indexed"](f, coll))
    elseif true then
      local _ = _1146_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "keep-indexed"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(keep_indexed0, "fnl/arglist", {"([f])", "([f coll])"}, "fnl/docstring", "Returns a lazy sequence of the non-nil results of (f index item) in\nthe `coll`.  Note, this means false return values will be included.\n`f` must be free of side effects. Returns a transducer when no\ncollection is provided.") end)
  v_33_auto = keep_indexed0
  core["keep-indexed"] = v_33_auto
  keep_indexed = v_33_auto
end
local partition
do
  local v_33_auto
  local function partition0(...)
    local _1155_ = select("#", ...)
    if (_1155_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "partition"))
    elseif (_1155_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "partition"))
    elseif (_1155_ == 2) then
      local n, coll = ...
      return map(seq_2a, lazy.partition(n, coll))
    elseif (_1155_ == 3) then
      local n, step, coll = ...
      return map(seq_2a, lazy.partition(n, step, coll))
    elseif (_1155_ == 4) then
      local n, step, pad, coll = ...
      return map(seq_2a, lazy.partition(n, step, pad, coll))
    elseif true then
      local _ = _1155_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "partition"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(partition0, "fnl/arglist", {"([n coll])", "([n step coll])", "([n step pad coll])"}, "fnl/docstring", "Given a collection `coll`, returns a lazy sequence of lists of `n`\nitems each, at offsets `step` apart. If `step` is not supplied,\ndefaults to `n`, i.e. the partitions do not overlap. If a `pad`\ncollection is supplied, use its elements as necessary to complete last\npartition up to `n` items. In case there are not enough padding\nelements, return a partition with less than `n` items.") end)
  v_33_auto = partition0
  core["partition"] = v_33_auto
  partition = v_33_auto
end
local function array()
  local len = 0
  local function _1157_()
    return len
  end
  local function _1158_(self)
    while (0 ~= len) do
      self[len] = nil
      len = (len - 1)
    end
    return nil
  end
  pcall(function() require("fennel").metadata:setall(_1158_, "fnl/arglist", {"self"}) end)
  local function _1159_(self, val)
    len = (len + 1)
    do end (self)[len] = val
    return self
  end
  pcall(function() require("fennel").metadata:setall(_1159_, "fnl/arglist", {"self", "val"}) end)
  return setmetatable({}, {__len = _1157_, __index = {clear = _1158_, add = _1159_}})
end
pcall(function() require("fennel").metadata:setall(array, "fnl/arglist", {}) end)
local partition_by
do
  local v_33_auto
  local function partition_by0(...)
    local _1160_ = select("#", ...)
    if (_1160_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "partition-by"))
    elseif (_1160_ == 1) then
      local f = ...
      local function fn_1161_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1161_"))
          else
          end
        end
        local a = array()
        local none = {}
        local pv = none
        local function fn_1163_(...)
          local _1164_ = select("#", ...)
          if (_1164_ == 0) then
            return rf()
          elseif (_1164_ == 1) then
            local result = ...
            local function _1165_(...)
              if empty_3f(a) then
                return result
              else
                local v = vec(a)
                a:clear()
                return core.unreduced(rf(result, v))
              end
            end
            return rf(_1165_(...))
          elseif (_1164_ == 2) then
            local result, input = ...
            local pval = pv
            local val = f(input)
            pv = val
            if ((pval == none) or (val == pval)) then
              a:add(input)
              return result
            else
              local v = vec(a)
              a:clear()
              local ret = rf(result, v)
              if not core["reduced?"](ret) then
                a:add(input)
              else
              end
              return ret
            end
          elseif true then
            local _ = _1164_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1163_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1163_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1163_
      end
      pcall(function() require("fennel").metadata:setall(fn_1161_, "fnl/arglist", {"[rf]"}) end)
      return fn_1161_
    elseif (_1160_ == 2) then
      local f, coll = ...
      return map(seq_2a, lazy["partition-by"](f, coll))
    elseif true then
      local _ = _1160_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "partition-by"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(partition_by0, "fnl/arglist", {"([f])", "([f coll])"}, "fnl/docstring", "Applies `f` to each value in `coll`, splitting it each time `f`\nreturns a new value.  Returns a lazy seq of partitions.  Returns a\ntransducer, if collection is not supplied.") end)
  v_33_auto = partition_by0
  core["partition-by"] = v_33_auto
  partition_by = v_33_auto
end
local partition_all
do
  local v_33_auto
  local function partition_all0(...)
    local _1170_ = select("#", ...)
    if (_1170_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "partition-all"))
    elseif (_1170_ == 1) then
      local n = ...
      local function fn_1171_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1171_"))
          else
          end
        end
        local a = array()
        local function fn_1173_(...)
          local _1174_ = select("#", ...)
          if (_1174_ == 0) then
            return rf()
          elseif (_1174_ == 1) then
            local result = ...
            local function _1175_(...)
              if (0 == #a) then
                return result
              else
                local v = vec(a)
                a:clear()
                return core.unreduced(rf(result, v))
              end
            end
            return rf(_1175_(...))
          elseif (_1174_ == 2) then
            local result, input = ...
            a:add(input)
            if (n == #a) then
              local v = vec(a)
              a:clear()
              return rf(result, v)
            else
              return result
            end
          elseif true then
            local _ = _1174_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1173_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1173_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1173_
      end
      pcall(function() require("fennel").metadata:setall(fn_1171_, "fnl/arglist", {"[rf]"}) end)
      return fn_1171_
    elseif (_1170_ == 2) then
      local n, coll = ...
      return map(seq_2a, lazy["partition-all"](n, coll))
    elseif (_1170_ == 3) then
      local n, step, coll = ...
      return map(seq_2a, lazy["partition-all"](n, step, coll))
    elseif true then
      local _ = _1170_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "partition-all"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(partition_all0, "fnl/arglist", {"([n])", "([n coll])", "([n step coll])"}, "fnl/docstring", "Given a collection `coll`, returns a lazy sequence of lists like\n`partition`, but may include partitions with fewer than n items at the\nend. Accepts addiitonal `step` argument, similarly to `partition`.\nReturns a transducer, if collection is not supplied.") end)
  v_33_auto = partition_all0
  core["partition-all"] = v_33_auto
  partition_all = v_33_auto
end
local reductions
do
  local v_33_auto
  local function reductions0(...)
    local _1180_ = select("#", ...)
    if (_1180_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "reductions"))
    elseif (_1180_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "reductions"))
    elseif (_1180_ == 2) then
      local f, coll = ...
      return seq_2a(lazy.reductions(f, coll))
    elseif (_1180_ == 3) then
      local f, init, coll = ...
      return seq_2a(lazy.reductions(f, init, coll))
    elseif true then
      local _ = _1180_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "reductions"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(reductions0, "fnl/arglist", {"([f coll])", "([f init coll])"}, "fnl/docstring", "Returns a lazy seq of the intermediate values of the reduction (as\nper reduce) of `coll` by `f`, starting with `init`.") end)
  v_33_auto = reductions0
  core["reductions"] = v_33_auto
  reductions = v_33_auto
end
local contains_3f
do
  local v_33_auto
  local function contains_3f0(...)
    local coll, elt = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "contains?"))
      else
      end
    end
    return lazy["contains?"](coll, elt)
  end
  pcall(function() require("fennel").metadata:setall(contains_3f0, "fnl/arglist", {"[coll elt]"}, "fnl/docstring", "Test if `elt` is in the `coll`.  It may be a linear search depending\non the type of the collection.") end)
  v_33_auto = contains_3f0
  core["contains?"] = v_33_auto
  contains_3f = v_33_auto
end
local distinct
do
  local v_33_auto
  local function distinct0(...)
    local _1183_ = select("#", ...)
    if (_1183_ == 0) then
      local function fn_1184_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1184_"))
          else
          end
        end
        local seen = setmetatable({}, {__index = deep_index})
        local function fn_1186_(...)
          local _1187_ = select("#", ...)
          if (_1187_ == 0) then
            return rf()
          elseif (_1187_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1187_ == 2) then
            local result, input = ...
            if seen[input] then
              return result
            else
              seen[input] = true
              return rf(result, input)
            end
          elseif true then
            local _ = _1187_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1186_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1186_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1186_
      end
      pcall(function() require("fennel").metadata:setall(fn_1184_, "fnl/arglist", {"[rf]"}) end)
      return fn_1184_
    elseif (_1183_ == 1) then
      local coll = ...
      return seq_2a(lazy.distinct(coll))
    elseif true then
      local _ = _1183_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "distinct"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(distinct0, "fnl/arglist", {"([])", "([coll])"}, "fnl/docstring", "Returns a lazy sequence of the elements of the `coll` without\nduplicates.  Comparison is done by equality. Returns a transducer when\nno collection is provided.") end)
  v_33_auto = distinct0
  core["distinct"] = v_33_auto
  distinct = v_33_auto
end
local dedupe
do
  local v_33_auto
  local function dedupe0(...)
    local _1191_ = select("#", ...)
    if (_1191_ == 0) then
      local function fn_1192_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1192_"))
          else
          end
        end
        local none = {}
        local pv = none
        local function fn_1194_(...)
          local _1195_ = select("#", ...)
          if (_1195_ == 0) then
            return rf()
          elseif (_1195_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1195_ == 2) then
            local result, input = ...
            local prior = pv
            pv = input
            if (prior == input) then
              return result
            else
              return rf(result, input)
            end
          elseif true then
            local _ = _1195_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1194_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1194_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1194_
      end
      pcall(function() require("fennel").metadata:setall(fn_1192_, "fnl/arglist", {"[rf]"}) end)
      return fn_1192_
    elseif (_1191_ == 1) then
      local coll = ...
      return core.sequence(dedupe0(), coll)
    elseif true then
      local _ = _1191_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "dedupe"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(dedupe0, "fnl/arglist", {"([])", "([coll])"}, "fnl/docstring", "Returns a lazy sequence removing consecutive duplicates in coll.\nReturns a transducer when no collection is provided.") end)
  v_33_auto = dedupe0
  core["dedupe"] = v_33_auto
  dedupe = v_33_auto
end
local random_sample
do
  local v_33_auto
  local function random_sample0(...)
    local _1199_ = select("#", ...)
    if (_1199_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "random-sample"))
    elseif (_1199_ == 1) then
      local prob = ...
      local function _1200_()
        return (math.random() < prob)
      end
      pcall(function() require("fennel").metadata:setall(_1200_, "fnl/arglist", {}) end)
      return filter(_1200_)
    elseif (_1199_ == 2) then
      local prob, coll = ...
      local function _1201_()
        return (math.random() < prob)
      end
      pcall(function() require("fennel").metadata:setall(_1201_, "fnl/arglist", {}) end)
      return filter(_1201_, coll)
    elseif true then
      local _ = _1199_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "random-sample"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(random_sample0, "fnl/arglist", {"([prob])", "([prob coll])"}, "fnl/docstring", "Returns items from `coll` with random probability of `prob` (0.0 -\n1.0).  Returns a transducer when no collection is provided.") end)
  v_33_auto = random_sample0
  core["random-sample"] = v_33_auto
  random_sample = v_33_auto
end
local doall
do
  local v_33_auto
  local function doall0(...)
    local seq0 = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "doall"))
      else
      end
    end
    return seq_2a(lazy.doall(seq0))
  end
  pcall(function() require("fennel").metadata:setall(doall0, "fnl/arglist", {"[seq]"}, "fnl/docstring", "Realize whole lazy sequence `seq`.\n\nWalks whole sequence, realizing each cell.  Use at your own risk on\ninfinite sequences.") end)
  v_33_auto = doall0
  core["doall"] = v_33_auto
  doall = v_33_auto
end
local dorun
do
  local v_33_auto
  local function dorun0(...)
    local seq0 = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "dorun"))
      else
      end
    end
    return lazy.dorun(seq0)
  end
  pcall(function() require("fennel").metadata:setall(dorun0, "fnl/arglist", {"[seq]"}, "fnl/docstring", "Realize whole sequence `seq` for side effects.\n\nWalks whole sequence, realizing each cell.  Use at your own risk on\ninfinite sequences.") end)
  v_33_auto = dorun0
  core["dorun"] = v_33_auto
  dorun = v_33_auto
end
local line_seq
do
  local v_33_auto
  local function line_seq0(...)
    local file = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "line-seq"))
      else
      end
    end
    return seq_2a(lazy["line-seq"](file))
  end
  pcall(function() require("fennel").metadata:setall(line_seq0, "fnl/arglist", {"[file]"}, "fnl/docstring", "Accepts a `file` handle, and creates a lazy sequence of lines using\n`lines` metamethod.\n\n# Examples\n\nLazy sequence of file lines may seem similar to an iterator over a\nfile, but the main difference is that sequence can be shared onve\nrealized, and iterator can't.  Lazy sequence can be consumed in\niterator style with the `doseq` macro.\n\nBear in mind, that since the sequence is lazy it should be realized or\ntruncated before the file is closed:\n\n``` fennel\n(let [lines (with-open [f (io.open \"init.fnl\" :r)]\n              (line-seq f))]\n  ;; this will error because only first line was realized, but the\n  ;; file was closed before the rest of lines were cached\n  (assert-not (pcall next lines)))\n```\n\nSequence is realized with `doall` before file was closed and can be shared:\n\n``` fennel\n(let [lines (with-open [f (io.open \"init.fnl\" :r)]\n              (doall (line-seq f)))]\n  (assert-is (pcall next lines)))\n```\n\nInfinite files can't be fully realized, but can be partially realized\nwith `take`:\n\n``` fennel\n(let [lines (with-open [f (io.open \"/dev/urandom\" :r)]\n              (doall (take 3 (line-seq f))))]\n  (assert-is (pcall next lines)))\n```") end)
  v_33_auto = line_seq0
  core["line-seq"] = v_33_auto
  line_seq = v_33_auto
end
local iterate
do
  local v_33_auto
  local function iterate0(...)
    local f, x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "iterate"))
      else
      end
    end
    return seq_2a(lazy.iterate(f, x))
  end
  pcall(function() require("fennel").metadata:setall(iterate0, "fnl/arglist", {"[f x]"}, "fnl/docstring", "Returns an infinete lazy sequence of x, (f x), (f (f x)) etc.") end)
  v_33_auto = iterate0
  core["iterate"] = v_33_auto
  iterate = v_33_auto
end
local remove
do
  local v_33_auto
  local function remove0(...)
    local _1207_ = select("#", ...)
    if (_1207_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "remove"))
    elseif (_1207_ == 1) then
      local pred = ...
      return filter(complement(pred))
    elseif (_1207_ == 2) then
      local pred, coll = ...
      return seq_2a(lazy.remove(pred, coll))
    elseif true then
      local _ = _1207_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "remove"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(remove0, "fnl/arglist", {"([pred])", "([pred coll])"}, "fnl/docstring", "Returns a lazy sequence of the items in the `coll` without elements\nfor wich `pred` returns logical true. Returns a transducer when no\ncollection is provided.") end)
  v_33_auto = remove0
  core["remove"] = v_33_auto
  remove = v_33_auto
end
local cycle
do
  local v_33_auto
  local function cycle0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "cycle"))
      else
      end
    end
    return seq_2a(lazy.cycle(coll))
  end
  pcall(function() require("fennel").metadata:setall(cycle0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Create a lazy infinite sequence of repetitions of the items in the\n`coll`.") end)
  v_33_auto = cycle0
  core["cycle"] = v_33_auto
  cycle = v_33_auto
end
local _repeat
do
  local v_33_auto
  local function _repeat0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "repeat"))
      else
      end
    end
    return seq_2a(lazy["repeat"](x))
  end
  pcall(function() require("fennel").metadata:setall(_repeat0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Takes a value `x` and returns an infinite lazy sequence of this value.\n\n# Examples\n\n``` fennel\n(assert-eq 20 (reduce add (take 10 (repeat 2))))\n```") end)
  v_33_auto = _repeat0
  core["repeat"] = v_33_auto
  _repeat = v_33_auto
end
local repeatedly
do
  local v_33_auto
  local function repeatedly0(...)
    local core_48_auto = require("init")
    local _let_1211_ = core_48_auto.list(...)
    local f = _let_1211_[1]
    local args = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1211_, 2)
    return seq_2a(apply(lazy.repeatedly, f, args))
  end
  pcall(function() require("fennel").metadata:setall(repeatedly0, "fnl/arglist", {"[f & args]"}, "fnl/docstring", "Takes a function `f` and returns an infinite lazy sequence of\nfunction applications.  Rest arguments are passed to the function.") end)
  v_33_auto = repeatedly0
  core["repeatedly"] = v_33_auto
  repeatedly = v_33_auto
end
local tree_seq
do
  local v_33_auto
  local function tree_seq0(...)
    local branch_3f, children, root = ...
    do
      local cnt_68_auto = select("#", ...)
      if (3 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "tree-seq"))
      else
      end
    end
    return seq_2a(lazy["tree-seq"](branch_3f, children, root))
  end
  pcall(function() require("fennel").metadata:setall(tree_seq0, "fnl/arglist", {"[branch? children root]"}, "fnl/docstring", "Returns a lazy sequence of the nodes in a tree, via a depth-first walk.\n\n`branch?` must be a function of one arg that returns true if passed a\nnode that can have children (but may not).  `children` must be a\nfunction of one arg that returns a sequence of the children.  Will\nonly be called on nodes for which `branch?` returns true.  `root` is\nthe root node of the tree.\n\n# Examples\n\nFor the given tree `[\"A\" [\"B\" [\"D\"] [\"E\"]] [\"C\" [\"F\"]]]`:\n\n        A\n       / \\\n      B   C\n     / \\   \\\n    D   E   F\n\nCalling `tree-seq` with `next` as the `branch?` and `rest` as the\n`children` returns a flat representation of a tree:\n\n``` fennel\n(assert-eq (map first (tree-seq next rest [\"A\" [\"B\" [\"D\"] [\"E\"]] [\"C\" [\"F\"]]]))\n           [\"A\" \"B\" \"D\" \"E\" \"C\" \"F\"])\n```") end)
  v_33_auto = tree_seq0
  core["tree-seq"] = v_33_auto
  tree_seq = v_33_auto
end
local interleave
do
  local v_33_auto
  local function interleave0(...)
    local _1213_ = select("#", ...)
    if (_1213_ == 0) then
      return seq_2a(lazy.interleave())
    elseif (_1213_ == 1) then
      local s = ...
      return seq_2a(lazy.interleave(s))
    elseif (_1213_ == 2) then
      local s1, s2 = ...
      return seq_2a(lazy.interleave(s1, s2))
    elseif true then
      local _ = _1213_
      local core_48_auto = require("init")
      local _let_1214_ = core_48_auto.list(...)
      local s1 = _let_1214_[1]
      local s2 = _let_1214_[2]
      local ss = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1214_, 3)
      return seq_2a(apply(lazy.interleave, s1, s2, ss))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(interleave0, "fnl/arglist", {"([])", "([s])", "([s1 s2])", "([s1 s2 & ss])"}, "fnl/docstring", "Returns a lazy sequence of the first item in each sequence, then the\nsecond one, until any sequence exhausts.") end)
  v_33_auto = interleave0
  core["interleave"] = v_33_auto
  interleave = v_33_auto
end
local interpose
do
  local v_33_auto
  local function interpose0(...)
    local _1216_ = select("#", ...)
    if (_1216_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "interpose"))
    elseif (_1216_ == 1) then
      local sep = ...
      local function fn_1217_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1217_"))
          else
          end
        end
        local started = false
        local function fn_1219_(...)
          local _1220_ = select("#", ...)
          if (_1220_ == 0) then
            return rf()
          elseif (_1220_ == 1) then
            local result = ...
            return rf(result)
          elseif (_1220_ == 2) then
            local result, input = ...
            if started then
              local sepr = rf(result, sep)
              if core["reduced?"](sepr) then
                return sepr
              else
                return rf(sepr, input)
              end
            else
              started = true
              return rf(result, input)
            end
          elseif true then
            local _ = _1220_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1219_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1219_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1219_
      end
      pcall(function() require("fennel").metadata:setall(fn_1217_, "fnl/arglist", {"[rf]"}) end)
      return fn_1217_
    elseif (_1216_ == 2) then
      local separator, coll = ...
      return seq_2a(lazy.interpose(separator, coll))
    elseif true then
      local _ = _1216_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "interpose"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(interpose0, "fnl/arglist", {"([sep])", "([separator coll])"}, "fnl/docstring", "Returns a lazy sequence of the elements of `coll` separated by\n`separator`. Returns a transducer when no collection is provided.") end)
  v_33_auto = interpose0
  core["interpose"] = v_33_auto
  interpose = v_33_auto
end
local halt_when
do
  local v_33_auto
  local function halt_when0(...)
    local _1225_ = select("#", ...)
    if (_1225_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "halt-when"))
    elseif (_1225_ == 1) then
      local pred = ...
      return halt_when0(pred, nil)
    elseif (_1225_ == 2) then
      local pred, retf = ...
      local function fn_1226_(...)
        local rf = ...
        do
          local cnt_68_auto = select("#", ...)
          if (1 ~= cnt_68_auto) then
            error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1226_"))
          else
          end
        end
        local halt
        local function _1228_()
          return "#<halt>"
        end
        halt = setmetatable({}, {__fennelview = _1228_})
        local function fn_1229_(...)
          local _1230_ = select("#", ...)
          if (_1230_ == 0) then
            return rf()
          elseif (_1230_ == 1) then
            local result = ...
            if (map_3f(result) and contains_3f(result, halt)) then
              return result.value
            else
              return rf(result)
            end
          elseif (_1230_ == 2) then
            local result, input = ...
            if pred(input) then
              local _1232_
              if retf then
                _1232_ = retf(rf(result), input)
              else
                _1232_ = input
              end
              return core.reduced({[halt] = true, value = _1232_})
            else
              return rf(result, input)
            end
          elseif true then
            local _ = _1230_
            return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1229_"))
          else
            return nil
          end
        end
        pcall(function() require("fennel").metadata:setall(fn_1229_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
        return fn_1229_
      end
      pcall(function() require("fennel").metadata:setall(fn_1226_, "fnl/arglist", {"[rf]"}) end)
      return fn_1226_
    elseif true then
      local _ = _1225_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "halt-when"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(halt_when0, "fnl/arglist", {"([pred])", "([pred retf])"}, "fnl/docstring", "Returns a transducer that ends transduction when `pred` returns `true`\nfor an input. When `retf` is supplied it must be a `fn` of 2 arguments\n- it will be passed the (completed) result so far and the input that\ntriggered the predicate, and its return value (if it does not throw an\nexception) will be the return value of the transducer. If `retf` is\nnot supplied, the input that triggered the predicate will be\nreturned. If the predicate never returns `true` the transduction is\nunaffected.") end)
  v_33_auto = halt_when0
  core["halt-when"] = v_33_auto
  halt_when = v_33_auto
end
local realized_3f
do
  local v_33_auto
  local function realized_3f0(...)
    local s = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "realized?"))
      else
      end
    end
    return lazy["realized?"](s)
  end
  pcall(function() require("fennel").metadata:setall(realized_3f0, "fnl/arglist", {"[s]"}, "fnl/docstring", "Check if sequence's first element is realized.") end)
  v_33_auto = realized_3f0
  core["realized?"] = v_33_auto
  realized_3f = v_33_auto
end
local keys
do
  local v_33_auto
  local function keys0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "keys"))
      else
      end
    end
    assert((map_3f(coll) or empty_3f(coll)), "expected a map")
    if empty_3f(coll) then
      return lazy.list()
    else
      return lazy.keys(coll)
    end
  end
  pcall(function() require("fennel").metadata:setall(keys0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a sequence of the map's keys, in the same order as `seq`.") end)
  v_33_auto = keys0
  core["keys"] = v_33_auto
  keys = v_33_auto
end
local vals
do
  local v_33_auto
  local function vals0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "vals"))
      else
      end
    end
    assert((map_3f(coll) or empty_3f(coll)), "expected a map")
    if empty_3f(coll) then
      return lazy.list()
    else
      return lazy.vals(coll)
    end
  end
  pcall(function() require("fennel").metadata:setall(vals0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a sequence of the table's values, in the same order as `seq`.") end)
  v_33_auto = vals0
  core["vals"] = v_33_auto
  vals = v_33_auto
end
local find
do
  local v_33_auto
  local function find0(...)
    local coll, key = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "find"))
      else
      end
    end
    assert((map_3f(coll) or empty_3f(coll)), "expected a map")
    local _1243_ = coll[key]
    if (nil ~= _1243_) then
      local v = _1243_
      return {key, v}
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(find0, "fnl/arglist", {"[coll key]"}, "fnl/docstring", "Returns the map entry for `key`, or `nil` if key is not present in\n`coll`.") end)
  v_33_auto = find0
  core["find"] = v_33_auto
  find = v_33_auto
end
local sort
do
  local v_33_auto
  local function sort0(...)
    local _1245_ = select("#", ...)
    if (_1245_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "sort"))
    elseif (_1245_ == 1) then
      local coll = ...
      local _1246_ = seq(coll)
      if (nil ~= _1246_) then
        local s = _1246_
        return seq(itable.sort(vec(s)))
      elseif true then
        local _ = _1246_
        return list()
      else
        return nil
      end
    elseif (_1245_ == 2) then
      local comparator, coll = ...
      local _1248_ = seq(coll)
      if (nil ~= _1248_) then
        local s = _1248_
        return seq(itable.sort(vec(s), comparator))
      elseif true then
        local _ = _1248_
        return list()
      else
        return nil
      end
    elseif true then
      local _ = _1245_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "sort"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(sort0, "fnl/arglist", {"([coll])", "([comparator coll])"}, "fnl/docstring", "Returns a sorted sequence of the items in `coll`. If no `comparator`\nis supplied, uses `<`.") end)
  v_33_auto = sort0
  core["sort"] = v_33_auto
  sort = v_33_auto
end
local reduce
do
  local v_33_auto
  local function reduce0(...)
    local _1252_ = select("#", ...)
    if (_1252_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "reduce"))
    elseif (_1252_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "reduce"))
    elseif (_1252_ == 2) then
      local f, coll = ...
      return lazy.reduce(f, seq(coll))
    elseif (_1252_ == 3) then
      local f, val, coll = ...
      return lazy.reduce(f, val, seq(coll))
    elseif true then
      local _ = _1252_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "reduce"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(reduce0, "fnl/arglist", {"([f coll])", "([f val coll])"}, "fnl/docstring", "`f` should be a function of 2 arguments. If `val` is not supplied,\nreturns the result of applying `f` to the first 2 items in `coll`,\nthen applying `f` to that result and the 3rd item, etc. If `coll`\ncontains no items, f must accept no arguments as well, and reduce\nreturns the result of calling `f` with no arguments.  If `coll` has\nonly 1 item, it is returned and `f` is not called.  If `val` is\nsupplied, returns the result of applying `f` to `val` and the first\nitem in `coll`, then applying `f` to that result and the 2nd item,\netc. If `coll` contains no items, returns `val` and `f` is not\ncalled. Early termination is supported via `reduced`.\n\n# Examples\n\n``` fennel\n(defn- add\n  ([] 0)\n  ([a] a)\n  ([a b] (+ a b))\n  ([a b & cs] (apply add (+ a b) cs)))\n;; no initial value\n(assert-eq 10 (reduce add [1 2 3 4]))\n;; initial value\n(assert-eq 10 (reduce add 1 [2 3 4]))\n;; empty collection - function is called with 0 args\n(assert-eq 0 (reduce add []))\n(assert-eq 10.3 (reduce math.floor 10.3 []))\n;; collection with a single element doesn't call a function unless the\n;; initial value is supplied\n(assert-eq 10.3 (reduce math.floor [10.3]))\n(assert-eq 7 (reduce add 3 [4]))\n```") end)
  v_33_auto = reduce0
  core["reduce"] = v_33_auto
  reduce = v_33_auto
end
local reduced
do
  local v_33_auto
  local function reduced0(...)
    local value = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "reduced"))
      else
      end
    end
    local _1255_ = lazy.reduced(value)
    local function _1256_(_241)
      return _241.value
    end
    getmetatable(_1255_)["cljlib/deref"] = _1256_
    return _1255_
  end
  pcall(function() require("fennel").metadata:setall(reduced0, "fnl/arglist", {"[value]"}, "fnl/docstring", "Terminates the `reduce` early with a given `value`.\n\n# Examples\n\n``` fennel\n(assert-eq :NaN\n           (reduce (fn [acc x]\n                     (if (not= :number (type x))\n                         (reduced :NaN)\n                         (+ acc x)))\n                   [1 2 :3 4 5]))\n```") end)
  v_33_auto = reduced0
  core["reduced"] = v_33_auto
  reduced = v_33_auto
end
local reduced_3f
do
  local v_33_auto
  local function reduced_3f0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "reduced?"))
      else
      end
    end
    return lazy["reduced?"](x)
  end
  pcall(function() require("fennel").metadata:setall(reduced_3f0, "fnl/arglist", {"[x]"}, "fnl/docstring", "Returns true if `x` is the result of a call to reduced") end)
  v_33_auto = reduced_3f0
  core["reduced?"] = v_33_auto
  reduced_3f = v_33_auto
end
local unreduced
do
  local v_33_auto
  local function unreduced0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "unreduced"))
      else
      end
    end
    if reduced_3f(x) then
      return deref(x)
    else
      return x
    end
  end
  pcall(function() require("fennel").metadata:setall(unreduced0, "fnl/arglist", {"[x]"}, "fnl/docstring", "If `x` is `reduced?`, returns `(deref x)`, else returns `x`.") end)
  v_33_auto = unreduced0
  core["unreduced"] = v_33_auto
  unreduced = v_33_auto
end
local ensure_reduced
do
  local v_33_auto
  local function ensure_reduced0(...)
    local x = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "ensure-reduced"))
      else
      end
    end
    if reduced_3f(x) then
      return x
    else
      return reduced(x)
    end
  end
  pcall(function() require("fennel").metadata:setall(ensure_reduced0, "fnl/arglist", {"[x]"}, "fnl/docstring", "If x is already reduced?, returns it, else returns (reduced x)") end)
  v_33_auto = ensure_reduced0
  core["ensure-reduced"] = v_33_auto
  ensure_reduced = v_33_auto
end
local preserving_reduced
local function preserving_reduced0(...)
  local rf = ...
  do
    local cnt_68_auto = select("#", ...)
    if (1 ~= cnt_68_auto) then
      error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "preserving-reduced"))
    else
    end
  end
  local function fn_1263_(...)
    local a, b = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "fn_1263_"))
      else
      end
    end
    local ret = rf(a, b)
    if reduced_3f(ret) then
      return reduced(ret)
    else
      return ret
    end
  end
  pcall(function() require("fennel").metadata:setall(fn_1263_, "fnl/arglist", {"[a b]"}) end)
  return fn_1263_
end
pcall(function() require("fennel").metadata:setall(preserving_reduced0, "fnl/arglist", {"[rf]"}) end)
preserving_reduced = preserving_reduced0
local cat
do
  local v_33_auto
  local function cat0(...)
    local rf = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "cat"))
      else
      end
    end
    local rrf = preserving_reduced(rf)
    local function fn_1267_(...)
      local _1268_ = select("#", ...)
      if (_1268_ == 0) then
        return rf()
      elseif (_1268_ == 1) then
        local result = ...
        return rf(result)
      elseif (_1268_ == 2) then
        local result, input = ...
        return reduce(rrf, result, input)
      elseif true then
        local _ = _1268_
        return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1267_"))
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(fn_1267_, "fnl/arglist", {"([])", "([result])", "([result input])"}) end)
    return fn_1267_
  end
  pcall(function() require("fennel").metadata:setall(cat0, "fnl/arglist", {"[rf]"}, "fnl/docstring", "A transducer which concatenates the contents of each input, which must be a\n  collection, into the reduction. Accepts the reducing function `rf`.") end)
  v_33_auto = cat0
  core["cat"] = v_33_auto
  cat = v_33_auto
end
local reduce_kv
do
  local v_33_auto
  local function reduce_kv0(...)
    local f, val, s = ...
    do
      local cnt_68_auto = select("#", ...)
      if (3 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "reduce-kv"))
      else
      end
    end
    if map_3f(s) then
      local function _1273_(res, _1271_)
        local _arg_1272_ = _1271_
        local k = _arg_1272_[1]
        local v = _arg_1272_[2]
        return f(res, k, v)
      end
      pcall(function() require("fennel").metadata:setall(_1273_, "fnl/arglist", {"res", "[k v]"}) end)
      return reduce(_1273_, val, seq(s))
    else
      local function _1276_(res, _1274_)
        local _arg_1275_ = _1274_
        local k = _arg_1275_[1]
        local v = _arg_1275_[2]
        return f(res, k, v)
      end
      pcall(function() require("fennel").metadata:setall(_1276_, "fnl/arglist", {"res", "[k v]"}) end)
      return reduce(_1276_, val, map(vector, drop(1, range()), seq(s)))
    end
  end
  pcall(function() require("fennel").metadata:setall(reduce_kv0, "fnl/arglist", {"[f val s]"}, "fnl/docstring", "Reduces an associative table using function `f` and initial value `val`.\n\n`f` should be a function of 3 arguments.  Returns the result of\napplying `f` to `val`, the first key and the first value in `tbl`,\nthen applying `f` to that result and the 2nd key and value, etc.  If\n`tbl` contains no entries, returns `val` and `f` is not called.  Note\nthat `reduce-kv` is supported on sequential tables and strings, where\nthe keys will be the ordinals.\n\nEarly termination is possible with the use of `reduced`\nfunction.\n\n# Examples\nReduce associative table by adding values from all keys:\n\n``` fennel\n(local t {:a1 1\n          :b1 2\n          :a2 2\n          :b2 3})\n\n(reduce-kv #(+ $1 $3) 0 t)\n;; => 8\n```\n\nReduce table by adding values from keys that start with letter `a`:\n\n``` fennel\n(local t {:a1 1\n          :b1 2\n          :a2 2\n          :b2 3})\n\n(reduce-kv (fn [res k v] (if (= (string.sub k 1 1) :a) (+ res v) res))\n           0 t)\n;; => 3\n```") end)
  v_33_auto = reduce_kv0
  core["reduce-kv"] = v_33_auto
  reduce_kv = v_33_auto
end
local completing
do
  local v_33_auto
  local function completing0(...)
    local _1278_ = select("#", ...)
    if (_1278_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "completing"))
    elseif (_1278_ == 1) then
      local f = ...
      return completing0(f, identity)
    elseif (_1278_ == 2) then
      local f, cf = ...
      local function fn_1279_(...)
        local _1280_ = select("#", ...)
        if (_1280_ == 0) then
          return f()
        elseif (_1280_ == 1) then
          local x = ...
          return cf(x)
        elseif (_1280_ == 2) then
          local x, y = ...
          return f(x, y)
        elseif true then
          local _ = _1280_
          return error(("Wrong number of args (%s) passed to %s"):format(_, "fn_1279_"))
        else
          return nil
        end
      end
      pcall(function() require("fennel").metadata:setall(fn_1279_, "fnl/arglist", {"([])", "([x])", "([x y])"}) end)
      return fn_1279_
    elseif true then
      local _ = _1278_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "completing"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(completing0, "fnl/arglist", {"([f])", "([f cf])"}, "fnl/docstring", "Takes a reducing function `f` of 2 args and returns a function\nsuitable for transduce by adding an arity-1 signature that calls\n`cf` (default - `identity`) on the result argument.") end)
  v_33_auto = completing0
  core["completing"] = v_33_auto
  completing = v_33_auto
end
local transduce
do
  local v_33_auto
  local function transduce0(...)
    local _1286_ = select("#", ...)
    if (_1286_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "transduce"))
    elseif (_1286_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "transduce"))
    elseif (_1286_ == 2) then
      return error(("Wrong number of args (%s) passed to %s"):format(2, "transduce"))
    elseif (_1286_ == 3) then
      local xform, f, coll = ...
      return transduce0(xform, f, f(), coll)
    elseif (_1286_ == 4) then
      local xform, f, init, coll = ...
      local f0 = xform(f)
      return f0(reduce(f0, init, seq(coll)))
    elseif true then
      local _ = _1286_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "transduce"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(transduce0, "fnl/arglist", {"([xform f coll])", "([xform f init coll])"}, "fnl/docstring", "`reduce` with a transformation of `f` (`xform`). If `init` is not\nsupplied, `f` will be called to produce it. `f` should be a reducing\nstep function that accepts both 1 and 2 arguments, if it accepts only\n2 you can add the arity-1 with `completing`. Returns the result of\napplying (the transformed) `xform` to `init` and the first item in\n`coll`, then applying `xform` to that result and the 2nd item, etc. If\n`coll` contains no items, returns `init` and `f` is not called. Note\nthat certain transforms may inject or skip items.") end)
  v_33_auto = transduce0
  core["transduce"] = v_33_auto
  transduce = v_33_auto
end
local sequence
do
  local v_33_auto
  local function sequence0(...)
    local _1288_ = select("#", ...)
    if (_1288_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "sequence"))
    elseif (_1288_ == 1) then
      local coll = ...
      if seq_3f(coll) then
        return coll
      else
        return (seq(coll) or list())
      end
    elseif (_1288_ == 2) then
      local xform, coll = ...
      local f
      local function _1290_(_241, _242)
        return cons(_242, _241)
      end
      f = xform(completing(_1290_))
      local function step(coll0)
        local val_106_auto = seq(coll0)
        if (nil ~= val_106_auto) then
          local s = val_106_auto
          local res = f(nil, first(s))
          if reduced_3f(res) then
            return f(deref(res))
          elseif seq_3f(res) then
            local function _1291_()
              return step(rest(s))
            end
            return concat(res, lazy_seq(_1291_))
          elseif "else" then
            return step(rest(s))
          else
            return nil
          end
        else
          return f(nil)
        end
      end
      pcall(function() require("fennel").metadata:setall(step, "fnl/arglist", {"coll"}) end)
      return (step(coll) or list())
    elseif true then
      local _ = _1288_
      local core_48_auto = require("init")
      local _let_1294_ = core_48_auto.list(...)
      local xform = _let_1294_[1]
      local coll = _let_1294_[2]
      local colls = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1294_, 3)
      local f
      local function _1295_(_241, _242)
        return cons(_242, _241)
      end
      f = xform(completing(_1295_))
      local function step(colls0)
        if every_3f(seq, colls0) then
          local res = apply(f, nil, map(first, colls0))
          if reduced_3f(res) then
            return f(deref(res))
          elseif seq_3f(res) then
            local function _1296_()
              return step(map(rest, colls0))
            end
            return concat(res, lazy_seq(_1296_))
          elseif "else" then
            return step(map(rest, colls0))
          else
            return nil
          end
        else
          return f(nil)
        end
      end
      pcall(function() require("fennel").metadata:setall(step, "fnl/arglist", {"colls"}) end)
      return (step(cons(coll, colls)) or list())
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(sequence0, "fnl/arglist", {"([coll])", "([xform coll])", "([xform coll & colls])"}, "fnl/docstring", "Coerces coll to a (possibly empty) sequence, if it is not already\none. Will not force a lazy seq. `(sequence nil)` yields an empty list,\nWhen a transducer `xform` is supplied, returns a lazy sequence of\napplications of the transform to the items in `coll`, i.e. to the set\nof first items of each `coll`, followed by the set of second items in\neach `coll`, until any one of the `colls` is exhausted.  Any remaining\nitems in other `colls` are ignored. The transform should accept\nnumber-of-colls arguments") end)
  v_33_auto = sequence0
  core["sequence"] = v_33_auto
  sequence = v_33_auto
end
local function map__3etransient(immutable)
  local function _1300_(map0)
    local removed = setmetatable({}, {__index = deep_index})
    local function _1301_(_, k)
      if not removed[k] then
        return (map0)[k]
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(_1301_, "fnl/arglist", {"_", "k"}) end)
    local function _1303_()
      return error("can't `conj` onto transient map, use `conj!`")
    end
    local function _1304_()
      return error("can't `assoc` onto transient map, use `assoc!`")
    end
    local function _1305_()
      return error("can't `dissoc` onto transient map, use `dissoc!`")
    end
    local function _1308_(tmap, _1306_)
      local _arg_1307_ = _1306_
      local k = _arg_1307_[1]
      local v = _arg_1307_[2]
      if (nil == v) then
        removed[k] = true
      else
        removed[k] = nil
      end
      tmap[k] = v
      return tmap
    end
    pcall(function() require("fennel").metadata:setall(_1308_, "fnl/arglist", {"tmap", "[k v]"}) end)
    local function _1310_(tmap, ...)
      for i = 1, select("#", ...), 2 do
        local k, v = select(i, ...)
        do end (tmap)[k] = v
        if (nil == v) then
          removed[k] = true
        else
          removed[k] = nil
        end
      end
      return tmap
    end
    pcall(function() require("fennel").metadata:setall(_1310_, "fnl/arglist", {"tmap", "..."}) end)
    local function _1312_(tmap, ...)
      for i = 1, select("#", ...) do
        local k = select(i, ...)
        do end (tmap)[k] = nil
        removed[k] = true
      end
      return tmap
    end
    pcall(function() require("fennel").metadata:setall(_1312_, "fnl/arglist", {"tmap", "..."}) end)
    local function _1313_(tmap)
      local t
      do
        local tbl_14_auto
        do
          local tbl_14_auto0 = {}
          for k, v in pairs(map0) do
            local k_15_auto, v_16_auto = k, v
            if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
              tbl_14_auto0[k_15_auto] = v_16_auto
            else
            end
          end
          tbl_14_auto = tbl_14_auto0
        end
        for k, v in pairs(tmap) do
          local k_15_auto, v_16_auto = k, v
          if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
            tbl_14_auto[k_15_auto] = v_16_auto
          else
          end
        end
        t = tbl_14_auto
      end
      for k in pairs(removed) do
        t[k] = nil
      end
      local function _1316_()
        local tbl_17_auto = {}
        local i_18_auto = #tbl_17_auto
        for k in pairs_2a(tmap) do
          local val_19_auto = k
          if (nil ~= val_19_auto) then
            i_18_auto = (i_18_auto + 1)
            do end (tbl_17_auto)[i_18_auto] = val_19_auto
          else
          end
        end
        return tbl_17_auto
      end
      for _, k in ipairs(_1316_()) do
        tmap[k] = nil
      end
      local function _1318_()
        return error("attempt to use transient after it was persistet")
      end
      local function _1319_()
        return error("attempt to use transient after it was persistet")
      end
      setmetatable(tmap, {__index = _1318_, __newindex = _1319_})
      return immutable(itable(t))
    end
    pcall(function() require("fennel").metadata:setall(_1313_, "fnl/arglist", {"tmap"}) end)
    return setmetatable({}, {__index = _1301_, ["cljlib/type"] = "transient", ["cljlib/conj"] = _1303_, ["cljlib/assoc"] = _1304_, ["cljlib/dissoc"] = _1305_, ["cljlib/conj!"] = _1308_, ["cljlib/assoc!"] = _1310_, ["cljlib/dissoc!"] = _1312_, ["cljlib/persistent!"] = _1313_})
  end
  pcall(function() require("fennel").metadata:setall(_1300_, "fnl/arglist", {"map"}) end)
  return _1300_
end
pcall(function() require("fennel").metadata:setall(map__3etransient, "fnl/arglist", {"immutable"}) end)
local function hash_map_2a(x)
  do
    local _1320_ = getmetatable(x)
    if (nil ~= _1320_) then
      local mt = _1320_
      mt["cljlib/type"] = "hash-map"
      mt["cljlib/editable"] = true
      local function _1323_(t, _1321_, ...)
        local _arg_1322_ = _1321_
        local k = _arg_1322_[1]
        local v = _arg_1322_[2]
        local function _1324_(...)
          local kvs = {}
          for _, _1325_ in ipairs_2a({...}) do
            local _each_1326_ = _1325_
            local k0 = _each_1326_[1]
            local v0 = _each_1326_[2]
            table.insert(kvs, k0)
            table.insert(kvs, v0)
            kvs = kvs
          end
          return kvs
        end
        return apply(core.assoc, t, k, v, _1324_(...))
      end
      pcall(function() require("fennel").metadata:setall(_1323_, "fnl/arglist", {"t", "[k v]", "..."}) end)
      do end (mt)["cljlib/conj"] = _1323_
      mt["cljlib/transient"] = map__3etransient(hash_map_2a)
      local function _1327_()
        return hash_map_2a(itable({}))
      end
      mt["cljlib/empty"] = _1327_
    elseif true then
      local _ = _1320_
      hash_map_2a(setmetatable(x, {}))
    else
    end
  end
  return x
end
pcall(function() require("fennel").metadata:setall(hash_map_2a, "fnl/arglist", {"x"}, "fnl/docstring", "Add cljlib hash-map meta-info.") end)
local assoc
do
  local v_33_auto
  local function assoc0(...)
    local _1331_ = select("#", ...)
    if (_1331_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "assoc"))
    elseif (_1331_ == 1) then
      local tbl = ...
      return hash_map_2a(itable({}))
    elseif (_1331_ == 2) then
      return error(("Wrong number of args (%s) passed to %s"):format(2, "assoc"))
    elseif (_1331_ == 3) then
      local tbl, k, v = ...
      assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map")
      assert(not nil_3f(k), "attempt to use nil as key")
      return hash_map_2a(itable.assoc((tbl or {}), k, v))
    elseif true then
      local _ = _1331_
      local core_48_auto = require("init")
      local _let_1332_ = core_48_auto.list(...)
      local tbl = _let_1332_[1]
      local k = _let_1332_[2]
      local v = _let_1332_[3]
      local kvs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1332_, 4)
      assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map")
      assert(not nil_3f(k), "attempt to use nil as key")
      return hash_map_2a(apply(itable.assoc, (tbl or {}), k, v, kvs))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(assoc0, "fnl/arglist", {"([tbl])", "([tbl k v])", "([tbl k v & kvs])"}, "fnl/docstring", "Associate `val` under a `key`.\nAccepts extra keys and values.\n\n# Examples\n\n``` fennel\n(assert-eq {:a 1 :b 2} (assoc {:a 1} :b 2))\n(assert-eq {:a 1 :b 2} (assoc {:a 1 :b 1} :b 2))\n(assert-eq {:a 1 :b 2 :c 3} (assoc {:a 1 :b 1} :b 2 :c 3))\n```") end)
  v_33_auto = assoc0
  core["assoc"] = v_33_auto
  assoc = v_33_auto
end
local assoc_in
do
  local v_33_auto
  local function assoc_in0(...)
    local tbl, key_seq, val = ...
    do
      local cnt_68_auto = select("#", ...)
      if (3 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "assoc-in"))
      else
      end
    end
    assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map or nil")
    return hash_map_2a(itable["assoc-in"](tbl, key_seq, val))
  end
  pcall(function() require("fennel").metadata:setall(assoc_in0, "fnl/arglist", {"[tbl key-seq val]"}, "fnl/docstring", "Associate `val` into set of immutable nested tables `t`, via given `key-seq`.\nReturns a new immutable table.  Returns a new immutable table.\n\n# Examples\n\nReplace value under nested keys:\n\n``` fennel\n(assert-eq\n {:a {:b {:c 1}}}\n (assoc-in {:a {:b {:c 0}}} [:a :b :c] 1))\n```\n\nCreate new entries as you go:\n\n``` fennel\n(assert-eq\n {:a {:b {:c 1}} :e 2}\n (assoc-in {:e 2} [:a :b :c] 1))\n```") end)
  v_33_auto = assoc_in0
  core["assoc-in"] = v_33_auto
  assoc_in = v_33_auto
end
local update
do
  local v_33_auto
  local function update0(...)
    local tbl, key, f = ...
    do
      local cnt_68_auto = select("#", ...)
      if (3 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "update"))
      else
      end
    end
    assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map")
    return hash_map_2a(itable.update(tbl, key, f))
  end
  pcall(function() require("fennel").metadata:setall(update0, "fnl/arglist", {"[tbl key f]"}, "fnl/docstring", "Update table value stored under `key` by calling a function `f` on\nthat value. `f` must take one argument, which will be a value stored\nunder the key in the table.\n\n# Examples\n\nSame as `assoc` but accepts function to produce new value based on key value.\n\n``` fennel\n(assert-eq\n {:data \"THIS SHOULD BE UPPERCASE\"}\n (update {:data \"this should be uppercase\"} :data string.upper))\n```") end)
  v_33_auto = update0
  core["update"] = v_33_auto
  update = v_33_auto
end
local update_in
do
  local v_33_auto
  local function update_in0(...)
    local tbl, key_seq, f = ...
    do
      local cnt_68_auto = select("#", ...)
      if (3 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "update-in"))
      else
      end
    end
    assert((nil_3f(tbl) or map_3f(tbl) or empty_3f(tbl)), "expected a map or nil")
    return hash_map_2a(itable["update-in"](tbl, key_seq, f))
  end
  pcall(function() require("fennel").metadata:setall(update_in0, "fnl/arglist", {"[tbl key-seq f]"}, "fnl/docstring", "Update table value stored under set of immutable nested tables, via\ngiven `key-seq` by calling a function `f` on the value stored under the\nlast key.  `f` must take one argument, which will be a value stored\nunder the key in the table.  Returns a new immutable table.\n\n# Examples\n\nSame as `assoc-in` but accepts function to produce new value based on key value.\n\n``` fennel\n(fn capitalize-words [s]\n  (pick-values 1\n    (s:gsub \"(%a)([%w_`]*)\" #(.. ($1:upper) ($2:lower)))))\n\n(assert-eq\n {:user {:name \"John Doe\"}}\n (update-in {:user {:name \"john doe\"}} [:user :name] capitalize-words))\n```") end)
  v_33_auto = update_in0
  core["update-in"] = v_33_auto
  update_in = v_33_auto
end
local hash_map
do
  local v_33_auto
  local function hash_map0(...)
    local core_48_auto = require("init")
    local _let_1337_ = core_48_auto.list(...)
    local kvs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1337_, 1)
    return apply(assoc, {}, kvs)
  end
  pcall(function() require("fennel").metadata:setall(hash_map0, "fnl/arglist", {"[& kvs]"}, "fnl/docstring", "Create associative table from `kvs` represented as sequence of keys\nand values") end)
  v_33_auto = hash_map0
  core["hash-map"] = v_33_auto
  hash_map = v_33_auto
end
local get
do
  local v_33_auto
  local function get0(...)
    local _1339_ = select("#", ...)
    if (_1339_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "get"))
    elseif (_1339_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "get"))
    elseif (_1339_ == 2) then
      local tbl, key = ...
      return get0(tbl, key, nil)
    elseif (_1339_ == 3) then
      local tbl, key, not_found = ...
      assert((map_3f(tbl) or empty_3f(tbl)), "expected a map")
      return (tbl[key] or not_found)
    elseif true then
      local _ = _1339_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "get"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(get0, "fnl/arglist", {"([tbl key])", "([tbl key not-found])"}, "fnl/docstring", "Get value from the table by accessing it with a `key`.\nAccepts additional `not-found` as a marker to return if value wasn't\nfound in the table.") end)
  v_33_auto = get0
  core["get"] = v_33_auto
  get = v_33_auto
end
local get_in
do
  local v_33_auto
  local function get_in0(...)
    local _1342_ = select("#", ...)
    if (_1342_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "get-in"))
    elseif (_1342_ == 1) then
      return error(("Wrong number of args (%s) passed to %s"):format(1, "get-in"))
    elseif (_1342_ == 2) then
      local tbl, keys0 = ...
      return get_in0(tbl, keys0, nil)
    elseif (_1342_ == 3) then
      local tbl, keys0, not_found = ...
      assert((map_3f(tbl) or empty_3f(tbl)), "expected a map")
      local res, t, done = tbl, tbl, nil
      for _, k in ipairs_2a(keys0) do
        if done then break end
        local _1343_ = t[k]
        if (nil ~= _1343_) then
          local v = _1343_
          res, t = v, v
        elseif true then
          local _0 = _1343_
          res, done = not_found, true
        else
        end
      end
      return res
    elseif true then
      local _ = _1342_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "get-in"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(get_in0, "fnl/arglist", {"([tbl keys])", "([tbl keys not-found])"}, "fnl/docstring", "Get value from nested set of tables by providing key sequence.\nAccepts additional `not-found` as a marker to return if value wasn't\nfound in the table.") end)
  v_33_auto = get_in0
  core["get-in"] = v_33_auto
  get_in = v_33_auto
end
local dissoc
do
  local v_33_auto
  local function dissoc0(...)
    local _1346_ = select("#", ...)
    if (_1346_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "dissoc"))
    elseif (_1346_ == 1) then
      local tbl = ...
      return tbl
    elseif (_1346_ == 2) then
      local tbl, key = ...
      assert((map_3f(tbl) or empty_3f(tbl)), "expected a map")
      local function _1347_(...)
        tbl[key] = nil
        return tbl
      end
      return hash_map_2a(_1347_(...))
    elseif true then
      local _ = _1346_
      local core_48_auto = require("init")
      local _let_1348_ = core_48_auto.list(...)
      local tbl = _let_1348_[1]
      local key = _let_1348_[2]
      local keys0 = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1348_, 3)
      return apply(dissoc0, dissoc0(tbl, key), keys0)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(dissoc0, "fnl/arglist", {"([tbl])", "([tbl key])", "([tbl key & keys])"}, "fnl/docstring", "Remove `key` from table `tbl`.  Optionally takes more `keys`.") end)
  v_33_auto = dissoc0
  core["dissoc"] = v_33_auto
  dissoc = v_33_auto
end
local merge
do
  local v_33_auto
  local function merge0(...)
    local core_48_auto = require("init")
    local _let_1350_ = core_48_auto.list(...)
    local maps = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1350_, 1)
    if some(identity, maps) then
      local function _1351_(a, b)
        local tbl_14_auto = a
        for k, v in pairs_2a(b) do
          local k_15_auto, v_16_auto = k, v
          if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
            tbl_14_auto[k_15_auto] = v_16_auto
          else
          end
        end
        return tbl_14_auto
      end
      pcall(function() require("fennel").metadata:setall(_1351_, "fnl/arglist", {"a", "b"}) end)
      return hash_map_2a(itable(reduce(_1351_, {}, maps)))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(merge0, "fnl/arglist", {"[& maps]"}, "fnl/docstring", "Merge `maps` rght to left into a single hash-map.") end)
  v_33_auto = merge0
  core["merge"] = v_33_auto
  merge = v_33_auto
end
local frequencies
do
  local v_33_auto
  local function frequencies0(...)
    local t = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "frequencies"))
      else
      end
    end
    return hash_map_2a(itable.frequencies(t))
  end
  pcall(function() require("fennel").metadata:setall(frequencies0, "fnl/arglist", {"[t]"}, "fnl/docstring", "Return a table of unique entries from table `t` associated to amount\nof their appearances.\n\n# Examples\n\nCount each entry of a random letter:\n\n``` fennel\n(let [fruits [:banana :banana :apple :strawberry :apple :banana]]\n  (assert-eq (frequencies fruits)\n             {:banana 3\n              :apple 2\n              :strawberry 1}))\n```") end)
  v_33_auto = frequencies0
  core["frequencies"] = v_33_auto
  frequencies = v_33_auto
end
local group_by
do
  local v_33_auto
  local function group_by0(...)
    local f, t = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "group-by"))
      else
      end
    end
    local function _1357_(...)
      local _1356_ = itable["group-by"](f, t)
      return _1356_
    end
    return hash_map_2a(_1357_(...))
  end
  pcall(function() require("fennel").metadata:setall(group_by0, "fnl/arglist", {"[f t]"}, "fnl/docstring", "Group table items in an associative table under the keys that are\nresults of calling `f` on each element of sequential table `t`.\nElements that the function call resulted in `nil` returned in a\nseparate table.\n\n# Examples\n\nGroup rows by their date:\n\n``` fennel\n(local rows\n  [{:date \"2007-03-03\" :product \"pineapple\"}\n   {:date \"2007-03-04\" :product \"pizza\"}\n   {:date \"2007-03-04\" :product \"pineapple pizza\"}\n   {:date \"2007-03-05\" :product \"bananas\"}])\n\n(assert-eq (group-by #(. $ :date) rows)\n           {\"2007-03-03\"\n            [{:date \"2007-03-03\" :product \"pineapple\"}]\n            \"2007-03-04\"\n            [{:date \"2007-03-04\" :product \"pizza\"}\n             {:date \"2007-03-04\" :product \"pineapple pizza\"}]\n            \"2007-03-05\"\n            [{:date \"2007-03-05\" :product \"bananas\"}]})\n```") end)
  v_33_auto = group_by0
  core["group-by"] = v_33_auto
  group_by = v_33_auto
end
local zipmap
do
  local v_33_auto
  local function zipmap0(...)
    local keys0, vals0 = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "zipmap"))
      else
      end
    end
    return hash_map_2a(itable(lazy.zipmap(keys0, vals0)))
  end
  pcall(function() require("fennel").metadata:setall(zipmap0, "fnl/arglist", {"[keys vals]"}, "fnl/docstring", "Return an associative table with the `keys` mapped to the\ncorresponding `vals`.") end)
  v_33_auto = zipmap0
  core["zipmap"] = v_33_auto
  zipmap = v_33_auto
end
local replace
do
  local v_33_auto
  local function replace0(...)
    local _1359_ = select("#", ...)
    if (_1359_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "replace"))
    elseif (_1359_ == 1) then
      local smap = ...
      local function _1360_(_241)
        local val_100_auto = find(smap, _241)
        if val_100_auto then
          local e = val_100_auto
          return e[2]
        else
          return _241
        end
      end
      return map(_1360_)
    elseif (_1359_ == 2) then
      local smap, coll = ...
      if vector_3f(coll) then
        local function _1362_(res, v)
          local val_100_auto = find(smap, v)
          if val_100_auto then
            local e = val_100_auto
            table.insert(res, e[2])
            return res
          else
            table.insert(res, v)
            return res
          end
        end
        pcall(function() require("fennel").metadata:setall(_1362_, "fnl/arglist", {"res", "v"}) end)
        return vec_2a(itable(reduce(_1362_, {}, coll)))
      else
        local function _1364_(_241)
          local val_100_auto = find(smap, _241)
          if val_100_auto then
            local e = val_100_auto
            return e[2]
          else
            return _241
          end
        end
        return map(_1364_, coll)
      end
    elseif true then
      local _ = _1359_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "replace"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(replace0, "fnl/arglist", {"([smap])", "([smap coll])"}, "fnl/docstring", "Given a map of replacement pairs and a vector/collection `coll`,\nreturns a vector/seq with any elements `=` a key in `smap` replaced\nwith the corresponding `val` in `smap`.  Returns a transducer when no\ncollection is provided.") end)
  v_33_auto = replace0
  core["replace"] = v_33_auto
  replace = v_33_auto
end
local conj
do
  local v_33_auto
  local function conj0(...)
    local _1368_ = select("#", ...)
    if (_1368_ == 0) then
      return vector()
    elseif (_1368_ == 1) then
      local s = ...
      return s
    elseif (_1368_ == 2) then
      local s, x = ...
      local _1369_ = getmetatable(s)
      if ((_G.type(_1369_) == "table") and (nil ~= (_1369_)["cljlib/conj"])) then
        local f = (_1369_)["cljlib/conj"]
        return f(s, x)
      elseif true then
        local _ = _1369_
        if vector_3f(s) then
          return vec_2a(itable.insert(s, x))
        elseif map_3f(s) then
          return apply(assoc, s, x)
        elseif nil_3f(s) then
          return cons(x, s)
        elseif empty_3f(s) then
          return vector(x)
        else
          return error("expected collection, got", type(s))
        end
      else
        return nil
      end
    elseif true then
      local _ = _1368_
      local core_48_auto = require("init")
      local _let_1372_ = core_48_auto.list(...)
      local s = _let_1372_[1]
      local x = _let_1372_[2]
      local xs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1372_, 3)
      return apply(conj0, conj0(s, x), xs)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(conj0, "fnl/arglist", {"([])", "([s])", "([s x])", "([s x & xs])"}, "fnl/docstring", "Insert `x` as a last element of a table `tbl`.\n\nIf `tbl` is a sequential table or empty table, inserts `x` and\noptional `xs` as final element in the table.\n\nIf `tbl` is an associative table, that satisfies `map?` test,\ninsert `[key value]` pair into the table.\n\nMutates `tbl`.\n\n# Examples\nAdding to sequential tables:\n\n``` fennel\n(conj [] 1 2 3 4)\n;; => [1 2 3 4]\n(conj [1 2 3] 4 5)\n;; => [1 2 3 4 5]\n```\n\nAdding to associative tables:\n\n``` fennel\n(conj {:a 1} [:b 2] [:c 3])\n;; => {:a 1 :b 2 :c 3}\n```\n\nNote, that passing literal empty associative table `{}` will not work:\n\n``` fennel\n(conj {} [:a 1] [:b 2])\n;; => [[:a 1] [:b 2]]\n(conj (hash-map) [:a 1] [:b 2])\n;; => {:a 1 :b 2}\n```\n\nSee `hash-map` for creating empty associative tables.") end)
  v_33_auto = conj0
  core["conj"] = v_33_auto
  conj = v_33_auto
end
local disj
do
  local v_33_auto
  local function disj0(...)
    local _1374_ = select("#", ...)
    if (_1374_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "disj"))
    elseif (_1374_ == 1) then
      local Set = ...
      return Set
    elseif (_1374_ == 2) then
      local Set, key = ...
      local _1375_ = getmetatable(Set)
      if ((_G.type(_1375_) == "table") and ((_1375_)["cljlib/type"] == "hash-set") and (nil ~= (_1375_)["cljlib/disj"])) then
        local f = (_1375_)["cljlib/disj"]
        return f(Set, key)
      elseif true then
        local _ = _1375_
        return error(("disj is not supported on " .. class(Set)), 2)
      else
        return nil
      end
    elseif true then
      local _ = _1374_
      local core_48_auto = require("init")
      local _let_1377_ = core_48_auto.list(...)
      local Set = _let_1377_[1]
      local key = _let_1377_[2]
      local keys0 = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1377_, 3)
      local _1378_ = getmetatable(Set)
      if ((_G.type(_1378_) == "table") and ((_1378_)["cljlib/type"] == "hash-set") and (nil ~= (_1378_)["cljlib/disj"])) then
        local f = (_1378_)["cljlib/disj"]
        return apply(f, Set, key, keys0)
      elseif true then
        local _0 = _1378_
        return error(("disj is not supported on " .. class(Set)), 2)
      else
        return nil
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(disj0, "fnl/arglist", {"([Set])", "([Set key])", "([Set key & keys])"}, "fnl/docstring", "Returns a new set type, that does not contain the\nspecified `key` or `keys`.") end)
  v_33_auto = disj0
  core["disj"] = v_33_auto
  disj = v_33_auto
end
local pop
do
  local v_33_auto
  local function pop0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "pop"))
      else
      end
    end
    local _1382_ = getmetatable(coll)
    if ((_G.type(_1382_) == "table") and ((_1382_)["cljlib/type"] == "seq")) then
      local _1383_ = seq(coll)
      if (nil ~= _1383_) then
        local s = _1383_
        return drop(1, s)
      elseif true then
        local _ = _1383_
        return error("can't pop empty list", 2)
      else
        return nil
      end
    elseif ((_G.type(_1382_) == "table") and (nil ~= (_1382_)["cljlib/pop"])) then
      local f = (_1382_)["cljlib/pop"]
      return f(coll)
    elseif true then
      local _ = _1382_
      return error(("pop is not supported on " .. class(coll)), 2)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(pop0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "If `coll` is a list returns a new list without the first\nitem. If `coll` is a vector, returns a new vector without the last\nitem. If the collection is empty, raises an error. Not the same as\n`next` or `butlast`.") end)
  v_33_auto = pop0
  core["pop"] = v_33_auto
  pop = v_33_auto
end
local transient
do
  local v_33_auto
  local function transient0(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "transient"))
      else
      end
    end
    local _1387_ = getmetatable(coll)
    if ((_G.type(_1387_) == "table") and ((_1387_)["cljlib/editable"] == true) and (nil ~= (_1387_)["cljlib/transient"])) then
      local f = (_1387_)["cljlib/transient"]
      return f(coll)
    elseif true then
      local _ = _1387_
      return error("expected editable collection", 2)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(transient0, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a new, transient version of the collection.") end)
  v_33_auto = transient0
  core["transient"] = v_33_auto
  transient = v_33_auto
end
local conj_21
do
  local v_33_auto
  local function conj_210(...)
    local _1389_ = select("#", ...)
    if (_1389_ == 0) then
      return transient(vec_2a({}))
    elseif (_1389_ == 1) then
      local coll = ...
      return coll
    elseif (_1389_ == 2) then
      local coll, x = ...
      do
        local _1390_ = getmetatable(coll)
        if ((_G.type(_1390_) == "table") and ((_1390_)["cljlib/type"] == "transient") and (nil ~= (_1390_)["cljlib/conj!"])) then
          local f = (_1390_)["cljlib/conj!"]
          f(coll, x)
        elseif ((_G.type(_1390_) == "table") and ((_1390_)["cljlib/type"] == "transient")) then
          error("unsupported transient operation", 2)
        elseif true then
          local _ = _1390_
          error("expected transient collection", 2)
        else
        end
      end
      return coll
    elseif true then
      local _ = _1389_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "conj!"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(conj_210, "fnl/arglist", {"([])", "([coll])", "([coll x])"}, "fnl/docstring", "Adds `x` to the transient collection, and return `coll`.") end)
  v_33_auto = conj_210
  core["conj!"] = v_33_auto
  conj_21 = v_33_auto
end
local assoc_21
do
  local v_33_auto
  local function assoc_210(...)
    local core_48_auto = require("init")
    local _let_1393_ = core_48_auto.list(...)
    local map0 = _let_1393_[1]
    local k = _let_1393_[2]
    local ks = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1393_, 3)
    do
      local _1394_ = getmetatable(map0)
      if ((_G.type(_1394_) == "table") and ((_1394_)["cljlib/type"] == "transient") and (nil ~= (_1394_)["cljlib/dissoc!"])) then
        local f = (_1394_)["cljlib/dissoc!"]
        apply(f, map0, k, ks)
      elseif ((_G.type(_1394_) == "table") and ((_1394_)["cljlib/type"] == "transient")) then
        error("unsupported transient operation", 2)
      elseif true then
        local _ = _1394_
        error("expected transient collection", 2)
      else
      end
    end
    return map0
  end
  pcall(function() require("fennel").metadata:setall(assoc_210, "fnl/arglist", {"[map k & ks]"}, "fnl/docstring", "Remove `k`from transient map, and return `map`.") end)
  v_33_auto = assoc_210
  core["assoc!"] = v_33_auto
  assoc_21 = v_33_auto
end
local dissoc_21
do
  local v_33_auto
  local function dissoc_210(...)
    local core_48_auto = require("init")
    local _let_1396_ = core_48_auto.list(...)
    local map0 = _let_1396_[1]
    local k = _let_1396_[2]
    local ks = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1396_, 3)
    do
      local _1397_ = getmetatable(map0)
      if ((_G.type(_1397_) == "table") and ((_1397_)["cljlib/type"] == "transient") and (nil ~= (_1397_)["cljlib/dissoc!"])) then
        local f = (_1397_)["cljlib/dissoc!"]
        apply(f, map0, k, ks)
      elseif ((_G.type(_1397_) == "table") and ((_1397_)["cljlib/type"] == "transient")) then
        error("unsupported transient operation", 2)
      elseif true then
        local _ = _1397_
        error("expected transient collection", 2)
      else
      end
    end
    return map0
  end
  pcall(function() require("fennel").metadata:setall(dissoc_210, "fnl/arglist", {"[map k & ks]"}, "fnl/docstring", "Remove `k`from transient map, and return `map`.") end)
  v_33_auto = dissoc_210
  core["dissoc!"] = v_33_auto
  dissoc_21 = v_33_auto
end
local disj_21
do
  local v_33_auto
  local function disj_210(...)
    local _1399_ = select("#", ...)
    if (_1399_ == 0) then
      return error(("Wrong number of args (%s) passed to %s"):format(0, "disj!"))
    elseif (_1399_ == 1) then
      local Set = ...
      return Set
    elseif true then
      local _ = _1399_
      local core_48_auto = require("init")
      local _let_1400_ = core_48_auto.list(...)
      local Set = _let_1400_[1]
      local key = _let_1400_[2]
      local ks = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1400_, 3)
      local _1401_ = getmetatable(Set)
      if ((_G.type(_1401_) == "table") and ((_1401_)["cljlib/type"] == "transient") and (nil ~= (_1401_)["cljlib/disj!"])) then
        local f = (_1401_)["cljlib/disj!"]
        return apply(f, Set, key, ks)
      elseif ((_G.type(_1401_) == "table") and ((_1401_)["cljlib/type"] == "transient")) then
        return error("unsupported transient operation", 2)
      elseif true then
        local _0 = _1401_
        return error("expected transient collection", 2)
      else
        return nil
      end
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(disj_210, "fnl/arglist", {"([Set])", "([Set key & ks])"}, "fnl/docstring", "disj[oin]. Returns a transient set of the same type, that does not\ncontain `key`.") end)
  v_33_auto = disj_210
  core["disj!"] = v_33_auto
  disj_21 = v_33_auto
end
local pop_21
do
  local v_33_auto
  local function pop_210(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "pop!"))
      else
      end
    end
    local _1405_ = getmetatable(coll)
    if ((_G.type(_1405_) == "table") and ((_1405_)["cljlib/type"] == "transient") and (nil ~= (_1405_)["cljlib/pop!"])) then
      local f = (_1405_)["cljlib/pop!"]
      return f(coll)
    elseif ((_G.type(_1405_) == "table") and ((_1405_)["cljlib/type"] == "transient")) then
      return error("unsupported transient operation", 2)
    elseif true then
      local _ = _1405_
      return error("expected transient collection", 2)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(pop_210, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Removes the last item from a transient vector. If the collection is\nempty, raises an error Returns coll") end)
  v_33_auto = pop_210
  core["pop!"] = v_33_auto
  pop_21 = v_33_auto
end
local persistent_21
do
  local v_33_auto
  local function persistent_210(...)
    local coll = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "persistent!"))
      else
      end
    end
    local _1408_ = getmetatable(coll)
    if ((_G.type(_1408_) == "table") and ((_1408_)["cljlib/type"] == "transient") and (nil ~= (_1408_)["cljlib/persistent!"])) then
      local f = (_1408_)["cljlib/persistent!"]
      return f(coll)
    elseif true then
      local _ = _1408_
      return error("expected transient collection", 2)
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(persistent_210, "fnl/arglist", {"[coll]"}, "fnl/docstring", "Returns a new, persistent version of the transient collection. The\ntransient collection cannot be used after this call, any such use will\nraise an error.") end)
  v_33_auto = persistent_210
  core["persistent!"] = v_33_auto
  persistent_21 = v_33_auto
end
local into
do
  local v_33_auto
  local function into0(...)
    local _1410_ = select("#", ...)
    if (_1410_ == 0) then
      return vector()
    elseif (_1410_ == 1) then
      local to = ...
      return to
    elseif (_1410_ == 2) then
      local to, from = ...
      local _1411_ = getmetatable(to)
      if ((_G.type(_1411_) == "table") and ((_1411_)["cljlib/editable"] == true)) then
        return persistent_21(reduce(conj_21, transient(to), from))
      elseif true then
        local _ = _1411_
        return reduce(conj, to, from)
      else
        return nil
      end
    elseif (_1410_ == 3) then
      local to, xform, from = ...
      local _1413_ = getmetatable(to)
      if ((_G.type(_1413_) == "table") and ((_1413_)["cljlib/editable"] == true)) then
        return persistent_21(transduce(xform, conj_21, transient(to), from))
      elseif true then
        local _ = _1413_
        return transduce(xform, conj, to, from)
      else
        return nil
      end
    elseif true then
      local _ = _1410_
      return error(("Wrong number of args (%s) passed to %s"):format(_, "into"))
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(into0, "fnl/arglist", {"([])", "([to])", "([to from])", "([to xform from])"}, "fnl/docstring", "Returns a new coll consisting of `to` with all of the items of `from`\nconjoined. A transducer `xform` may be supplied.\n\n# Examples\n\nInsert items of one collection into another collection:\n\n```fennel\n(assert-eq [1 2 3 :a :b :c] (into [1 2 3] \"abc\"))\n(assert-eq {:a 2 :b 3} (into {:a 1} {:a 2 :b 3}))\n```\n\nTransform a hash-map into a sequence of key-value pairs:\n\n``` fennel\n(assert-eq [[:a 1]] (into (vector) {:a 1}))\n```\n\nYou can also construct a hash-map from a sequence of key-value pairs:\n\n``` fennel\n(assert-eq {:a 1 :b 2 :c 3}\n           (into (hash-map) [[:a 1] [:b 2] [:c 3]]))\n```") end)
  v_33_auto = into0
  core["into"] = v_33_auto
  into = v_33_auto
end
local function viewset(Set, view, inspector, indent)
  if inspector.seen[Set] then
    return ("@set" .. inspector.seen[Set] .. "{...}")
  else
    local prefix
    local function _1416_()
      if inspector["visible-cycle?"](Set) then
        return inspector.seen[Set]
      else
        return ""
      end
    end
    prefix = ("@set" .. _1416_() .. "{")
    local set_indent = #prefix
    local indent_str = string.rep(" ", set_indent)
    local lines
    do
      local tbl_17_auto = {}
      local i_18_auto = #tbl_17_auto
      for v in pairs_2a(Set) do
        local val_19_auto = (indent_str .. view(v, inspector, (indent + set_indent), true))
        if (nil ~= val_19_auto) then
          i_18_auto = (i_18_auto + 1)
          do end (tbl_17_auto)[i_18_auto] = val_19_auto
        else
        end
      end
      lines = tbl_17_auto
    end
    lines[1] = (prefix .. string.gsub((lines[1] or ""), "^%s+", ""))
    do end (lines)[#lines] = (lines[#lines] .. "}")
    return lines
  end
end
pcall(function() require("fennel").metadata:setall(viewset, "fnl/arglist", {"Set", "view", "inspector", "indent"}) end)
local function hash_set__3etransient(immutable)
  local function _1419_(hset)
    local removed = setmetatable({}, {__index = deep_index})
    local function _1420_(_, k)
      if not removed[k] then
        return hset[k]
      else
        return nil
      end
    end
    pcall(function() require("fennel").metadata:setall(_1420_, "fnl/arglist", {"_", "k"}) end)
    local function _1422_()
      return error("can't `conj` onto transient set, use `conj!`")
    end
    local function _1423_()
      return error("can't `disj` a transient set, use `disj!`")
    end
    local function _1424_()
      return error("can't `assoc` onto transient set, use `assoc!`")
    end
    local function _1425_()
      return error("can't `dissoc` onto transient set, use `dissoc!`")
    end
    local function _1426_(thset, v)
      if (nil == v) then
        removed[v] = true
      else
        removed[v] = nil
      end
      thset[v] = v
      return thset
    end
    pcall(function() require("fennel").metadata:setall(_1426_, "fnl/arglist", {"thset", "v"}) end)
    local function _1428_()
      return error("can't `dissoc!` a transient set")
    end
    local function _1429_(thset, ...)
      for i = 1, select("#", ...) do
        local k = select(i, ...)
        do end (thset)[k] = nil
        removed[k] = true
      end
      return thset
    end
    pcall(function() require("fennel").metadata:setall(_1429_, "fnl/arglist", {"thset", "..."}) end)
    local function _1430_(thset)
      local t
      do
        local tbl_14_auto
        do
          local tbl_14_auto0 = {}
          for k, v in pairs(hset) do
            local k_15_auto, v_16_auto = k, v
            if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
              tbl_14_auto0[k_15_auto] = v_16_auto
            else
            end
          end
          tbl_14_auto = tbl_14_auto0
        end
        for k, v in pairs(thset) do
          local k_15_auto, v_16_auto = k, v
          if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
            tbl_14_auto[k_15_auto] = v_16_auto
          else
          end
        end
        t = tbl_14_auto
      end
      for k in pairs(removed) do
        t[k] = nil
      end
      local function _1433_()
        local tbl_17_auto = {}
        local i_18_auto = #tbl_17_auto
        for k in pairs_2a(thset) do
          local val_19_auto = k
          if (nil ~= val_19_auto) then
            i_18_auto = (i_18_auto + 1)
            do end (tbl_17_auto)[i_18_auto] = val_19_auto
          else
          end
        end
        return tbl_17_auto
      end
      for _, k in ipairs(_1433_()) do
        thset[k] = nil
      end
      local function _1435_()
        return error("attempt to use transient after it was persistet")
      end
      local function _1436_()
        return error("attempt to use transient after it was persistet")
      end
      setmetatable(thset, {__index = _1435_, __newindex = _1436_})
      return immutable(itable(t))
    end
    pcall(function() require("fennel").metadata:setall(_1430_, "fnl/arglist", {"thset"}) end)
    return setmetatable({}, {__index = _1420_, ["cljlib/type"] = "transient", ["cljlib/conj"] = _1422_, ["cljlib/disj"] = _1423_, ["cljlib/assoc"] = _1424_, ["cljlib/dissoc"] = _1425_, ["cljlib/conj!"] = _1426_, ["cljlib/assoc!"] = _1428_, ["cljlib/disj!"] = _1429_, ["cljlib/persistent!"] = _1430_})
  end
  pcall(function() require("fennel").metadata:setall(_1419_, "fnl/arglist", {"hset"}) end)
  return _1419_
end
pcall(function() require("fennel").metadata:setall(hash_set__3etransient, "fnl/arglist", {"immutable"}) end)
local function hash_set_2a(x)
  do
    local _1437_ = getmetatable(x)
    if (nil ~= _1437_) then
      local mt = _1437_
      mt["cljlib/type"] = "hash-set"
      local function _1438_(s, v, ...)
        local function _1439_(...)
          local res = {}
          for _, v0 in ipairs({...}) do
            table.insert(res, v0)
            table.insert(res, v0)
          end
          return res
        end
        return hash_set_2a(itable.assoc(s, v, v, unpack_2a(_1439_(...))))
      end
      pcall(function() require("fennel").metadata:setall(_1438_, "fnl/arglist", {"s", "v", "..."}) end)
      do end (mt)["cljlib/conj"] = _1438_
      local function _1440_(s, k, ...)
        local to_remove
        do
          local tbl_14_auto = setmetatable({[k] = true}, {__index = deep_index})
          for _, k0 in ipairs({...}) do
            local k_15_auto, v_16_auto = k0, true
            if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
              tbl_14_auto[k_15_auto] = v_16_auto
            else
            end
          end
          to_remove = tbl_14_auto
        end
        local function _1442_(...)
          local res = {}
          for _, v in pairs(s) do
            if not to_remove[v] then
              table.insert(res, v)
              table.insert(res, v)
            else
            end
          end
          return res
        end
        return hash_set_2a(itable.assoc({}, unpack_2a(_1442_(...))))
      end
      pcall(function() require("fennel").metadata:setall(_1440_, "fnl/arglist", {"s", "k", "..."}) end)
      do end (mt)["cljlib/disj"] = _1440_
      local function _1444_()
        return hash_set_2a(itable({}))
      end
      mt["cljlib/empty"] = _1444_
      mt["cljlib/editable"] = true
      mt["cljlib/transient"] = hash_set__3etransient(hash_set_2a)
      local function _1445_(s)
        local function _1446_(_241)
          if vector_3f(_241) then
            return (_241)[1]
          else
            return _241
          end
        end
        return map(_1446_, s)
      end
      pcall(function() require("fennel").metadata:setall(_1445_, "fnl/arglist", {"s"}) end)
      do end (mt)["cljlib/seq"] = _1445_
      mt["__fennelview"] = viewset
      local function _1448_(s, i)
        local j = 1
        local vals0 = {}
        for v in pairs_2a(s) do
          if (j >= i) then
            table.insert(vals0, v)
          else
            j = (j + 1)
          end
        end
        return core["hash-set"](unpack_2a(vals0))
      end
      pcall(function() require("fennel").metadata:setall(_1448_, "fnl/arglist", {"s", "i"}) end)
      do end (mt)["__fennelrest"] = _1448_
    elseif true then
      local _ = _1437_
      hash_set_2a(setmetatable(x, {}))
    else
    end
  end
  return x
end
pcall(function() require("fennel").metadata:setall(hash_set_2a, "fnl/arglist", {"x"}) end)
local hash_set
do
  local v_33_auto
  local function hash_set0(...)
    local core_48_auto = require("init")
    local _let_1451_ = core_48_auto.list(...)
    local xs = (function (t, k, e) local mt = getmetatable(t) if 'table' == type(mt) and mt.__fennelrest then return mt.__fennelrest(t, k) elseif e then local rest = {} for k, v in pairs(t) do if not e[k] then rest[k] = v end end return rest else return {(table.unpack or unpack)(t, k)} end end)(_let_1451_, 1)
    local Set
    do
      local tbl_14_auto = setmetatable({}, {__newindex = deep_newindex})
      for _, val in pairs_2a(xs) do
        local k_15_auto, v_16_auto = val, val
        if ((k_15_auto ~= nil) and (v_16_auto ~= nil)) then
          tbl_14_auto[k_15_auto] = v_16_auto
        else
        end
      end
      Set = tbl_14_auto
    end
    return hash_set_2a(itable(Set))
  end
  pcall(function() require("fennel").metadata:setall(hash_set0, "fnl/arglist", {"[& xs]"}, "fnl/docstring", "Create hash set.\n\nSet is a collection of unique elements, which sore purpose is only to\ntell you if something is in the set or not.") end)
  v_33_auto = hash_set0
  core["hash-set"] = v_33_auto
  hash_set = v_33_auto
end
local multifn_3f
do
  local v_33_auto
  local function multifn_3f0(...)
    local mf = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "multifn?"))
      else
      end
    end
    local _1454_ = getmetatable(mf)
    if ((_G.type(_1454_) == "table") and ((_1454_)["cljlib/type"] == "multifn")) then
      return true
    elseif true then
      local _ = _1454_
      return false
    else
      return nil
    end
  end
  pcall(function() require("fennel").metadata:setall(multifn_3f0, "fnl/arglist", {"[mf]"}, "fnl/docstring", "Test if `mf' is an instance of `multifn'.\n\n`multifn' is a special kind of table, created with `defmulti' macros\nfrom `macros.fnl'.") end)
  v_33_auto = multifn_3f0
  core["multifn?"] = v_33_auto
  multifn_3f = v_33_auto
end
local remove_method
do
  local v_33_auto
  local function remove_method0(...)
    local multimethod, dispatch_value = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "remove-method"))
      else
      end
    end
    if multifn_3f(multimethod) then
      multimethod[dispatch_value] = nil
    else
      error((tostring(multimethod) .. " is not a multifn"), 2)
    end
    return multimethod
  end
  pcall(function() require("fennel").metadata:setall(remove_method0, "fnl/arglist", {"[multimethod dispatch-value]"}, "fnl/docstring", "Remove method from `multimethod' for given `dispatch-value'.") end)
  v_33_auto = remove_method0
  core["remove-method"] = v_33_auto
  remove_method = v_33_auto
end
local remove_all_methods
do
  local v_33_auto
  local function remove_all_methods0(...)
    local multimethod = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "remove-all-methods"))
      else
      end
    end
    if multifn_3f(multimethod) then
      for k, _ in pairs(multimethod) do
        multimethod[k] = nil
      end
    else
      error((tostring(multimethod) .. " is not a multifn"), 2)
    end
    return multimethod
  end
  pcall(function() require("fennel").metadata:setall(remove_all_methods0, "fnl/arglist", {"[multimethod]"}, "fnl/docstring", "Removes all methods of `multimethod'") end)
  v_33_auto = remove_all_methods0
  core["remove-all-methods"] = v_33_auto
  remove_all_methods = v_33_auto
end
local methods
do
  local v_33_auto
  local function methods0(...)
    local multimethod = ...
    do
      local cnt_68_auto = select("#", ...)
      if (1 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "methods"))
      else
      end
    end
    if multifn_3f(multimethod) then
      local m = {}
      for k, v in pairs(multimethod) do
        m[k] = v
      end
      return m
    else
      return error((tostring(multimethod) .. " is not a multifn"), 2)
    end
  end
  pcall(function() require("fennel").metadata:setall(methods0, "fnl/arglist", {"[multimethod]"}, "fnl/docstring", "Given a `multimethod', returns a map of dispatch values -> dispatch fns") end)
  v_33_auto = methods0
  core["methods"] = v_33_auto
  methods = v_33_auto
end
local get_method
do
  local v_33_auto
  local function get_method0(...)
    local multimethod, dispatch_value = ...
    do
      local cnt_68_auto = select("#", ...)
      if (2 ~= cnt_68_auto) then
        error(("Wrong number of args (%s) passed to %s"):format(cnt_68_auto, "get-method"))
      else
      end
    end
    if multifn_3f(multimethod) then
      return (multimethod[dispatch_value] or multimethod.default)
    else
      return error((tostring(multimethod) .. " is not a multifn"), 2)
    end
  end
  pcall(function() require("fennel").metadata:setall(get_method0, "fnl/arglist", {"[multimethod dispatch-value]"}, "fnl/docstring", "Given a `multimethod' and a `dispatch-value', returns the dispatch\n`fn' that would apply to that value, or `nil' if none apply and no\ndefault.") end)
  v_33_auto = get_method0
  core["get-method"] = v_33_auto
  get_method = v_33_auto
end
return core
