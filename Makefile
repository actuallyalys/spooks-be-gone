VERSION=0.2.2023-09-03-1
LOVE_VERSION=11.3
NAME=spooks-be-gone-llc
ITCH_ACCOUNT=actuallyalys
URL=https://gitlab.com/alexjgriffith/min-love2d-fennel
AUTHOR="actuallyalys"
DESCRIPTION="fight off spooks as an independent contractor"
SLUG="-afterjam"
GITHUB_USERNAME := $(shell grep GITHUB_USERNAME credentials.private | cut -d= -f2)
GITHUB_PAT := $(shell grep GITHUB_PAT credentials.private | cut -d= -f2)
LIBS := $(wildcard lib/*)
LUA := $(wildcard *.lua)
SRC := $(wildcard *.fnl src/*.fnl)
CONTENT := $(wildcard content/*)

run: ; love .

count: ; cloc *.fnl

clean: ; rm -rf releases/*

LOVEFILE=releases/$(NAME)-$(VERSION)$(SLUG).love

$(LOVEFILE): $(LUA) $(SRC) $(LIBS) $(CONTENT)
	mkdir -p releases/
	find $^ -type f | LC_ALL=C sort | env TZ=UTC zip -r -q -9 -X $@ -@

love: $(LOVEFILE)

# platform-specific distributables

REL=$(PWD)/buildtools/love-release.sh # https://p.hagelb.org/love-release.sh
FLAGS=-a "$(AUTHOR)" --description $(DESCRIPTION) \
	--love $(LOVE_VERSION) --url $(URL) --version $(VERSION)$(SLUG) --lovefile $(LOVEFILE)

releases/$(NAME)-$(VERSION)$(SLUG)-x86_64.AppImage: $(LOVEFILE)
	cd buildtools/appimage && \
	./build.sh $(LOVE_VERSION) $(PWD)/$(LOVEFILE) $(GITHUB_USERNAME) $(GITHUB_PAT)
	mv buildtools/appimage/game-x86_64.AppImage $@

releases/$(NAME)-$(VERSION)$(SLUG)-macos.zip: $(LOVEFILE)
	$(REL) $(FLAGS) -M
	mv releases/$(NAME)-macos.zip $@

releases/$(NAME)-$(VERSION)$(SLUG)-win.zip: $(LOVEFILE)
	$(REL) $(FLAGS) -W32
	mv releases/$(NAME)-win32.zip $@

releases/$(NAME)-$(VERSION)$(SLUG)-web.zip: $(LOVEFILE)
	buildtools/love-js/love-js.sh releases/$(NAME)-$(VERSION)$(SLUG).love $(NAME) -v=$(VERSION)$(SLUG) -a=$(AUTHOR) -o=releases -c="0,0,0"

releases/$(NAME)-$(VERSION)$(SLUG)-source.zip: $(LOVEFILE)
	hg archive -t zip releases/$(NAME)-$(VERSION)$(SLUG)-source.zip

linux: releases/$(NAME)-$(VERSION)$(SLUG)-x86_64.AppImage
mac: releases/$(NAME)-$(VERSION)$(SLUG)-macos.zip
windows: releases/$(NAME)-$(VERSION)$(SLUG)-win.zip
web: releases/$(NAME)-$(VERSION)$(SLUG)-web.zip
source: releases/$(NAME)-$(VERSION)$(SLUG)-source.zip


runweb: $(LOVEFILE)
	buildtools/love-js/love-js.sh $(LOVEFILE) $(NAME) -v=$(VERSION)$(SLUG) -a=$(AUTHOR) -o=releases -r -n
# If you release on itch.io, you should install butler:
# https://itch.io/docs/butler/installing.html

uploadlinux: releases/$(NAME)-$(VERSION)$(SLUG)-x86_64.AppImage
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):linux$(SLUG) --userversion $(VERSION)$(SLUG) 
uploadmac: releases/$(NAME)-$(VERSION)$(SLUG)-macos.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):mac$(SLUG) --userversion $(VERSION)$(SLUG) 
uploadwindows: releases/$(NAME)-$(VERSION)$(SLUG)-win.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):windows$(SLUG) --userversion $(VERSION)$(SLUG) 
uploadweb: releases/$(NAME)-$(VERSION)$(SLUG)-web.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):web$(SLUG) --userversion $(VERSION)$(SLUG) 
uploadlove: $(LOVEFILE)
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):linux-mac-windows-love$(SLUG) --userversion $(VERSION)$(SLUG) 
uploadsource:  releases/$(NAME)-$(VERSION)$(SLUG)-source.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):source$(SLUG) --userversion $(VERSION)$(SLUG) 

upload: uploadlinux uploadmac uploadwindows uploadlove uploadsource

release: linux mac windows upload cleansrc
