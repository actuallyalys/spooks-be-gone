(local fennel (require :lib.fennel))
(local repl (require :lib.stdio))
(local canvas (let [(w h) (love.window.getMode)]
                (love.graphics.newCanvas w h)))


(var scale 1)

(var state {})

(fn set-state [new-val]
  (set state (or new-val {}))
  new-val)

;; set the first mode
(var (mode mode-name) nil)

(fn mode-module [mode] (.. "src.mode-" mode))

(fn set-mode [new-mode-name ...]
  (set (mode mode-name) (values (require (mode-module new-mode-name)) new-mode-name))
  (when mode.activate
    (match (pcall mode.activate state ...)
      (true new-state) (set-state new-state)
      (false msg) (print mode-name "activate error" msg))))

(fn love.load [args]
  (set-mode :intro)
  (canvas:setFilter "nearest" "nearest")
  (when (~= :web (. args 1)) (repl.start)))

(fn safely [f]
  (xpcall f  ;#(set-mode :error mode-name $ (fennel.traceback))
          #(do 
             (print $)
             (print (fennel.traceback))
             (set-mode :error mode-name $ (fennel.traceback)))
          ))

(fn love.draw []
  ;; the canvas allows you to get sharp pixel-art style scaling; if you
  ;; don't want that, just skip that and call mode.draw directly.
  (love.graphics.setLineStyle :rough)
  (love.graphics.setCanvas canvas)
  (love.graphics.clear)
  (love.graphics.setColor 1 1 1)
  (if mode.fun-draw
    (safely #(mode.fun-draw state))
    (safely mode.draw))
  (love.graphics.setCanvas)
  (love.graphics.setColor 1 1 1)
  (love.graphics.draw canvas 0 0 0 scale scale))

(fn love.update [dt]
  (if mode.fun-update
    (let [(success val) (safely #(mode.fun-update state dt set-mode))]
      (when success 
        (set-state val)))
    (when mode.update
      (safely #(mode.update dt set-mode)))))

(fn love.mousemoved [x y dx dy]
  (if mode.fun-mousemoved
    (let [(success val) (safely #(mode.fun-mousemoved state x y dx dy set-mode))]
      (when success 
        (set-state val)))
    (when mode.mousemoved
      (safely #(mode.mousemoved x y dx dy)))))

(fn love.mousepressed [x y button istouch presses]
  (if mode.fun-mousepressed
    (let [(success val) (safely #(mode.fun-mousepressed state x y button istouch presses set-mode))]
          (when success 
            (set-state val)))
    (when mode.mousepressed
      (safely #(mode.mousepressed x y button istouch presses)))))

(fn love.keypressed [key]
  (if (and (love.keyboard.isDown "lctrl" "rctrl" "capslock") (= key "q"))
      (love.event.quit)
      ;; add what each keypress should do in each mode
      (if mode.fun-keypressed
        (let [(success val) (safely #(mode.fun-keypressed state key set-mode))]
          (when success 
            (set-state val)))
       (safely #(mode.keypressed key set-mode)))))
