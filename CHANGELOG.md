# unreleased 

* Draw a highlight with the user mouses over a piece of correspondence.
* Added level 4. Includes a job that comes in over email.
* Player now starts with $100
* Ensure audio stops correctly.
* Multiple fixes to collision detection. Enemy hit boxes would be in the wrong
    place when the enemy was facing left. Also, the player's hit box wouldn't
    move when the player moved.
* Some improvements to the debug code.
* Fix bug where clicking the very edge of a piece of correspondence wouldn't
    select the job. 

# 0.1.2023-06-11-1 #

* When selecting a job for the third level, previously only one level would show up.
* Hitting space on the intro screen no longer crashes the game.
* There were some additional graphical glitches when displaying the various jobs.
* I started to fix the rotation bug. Sprites would move around instead of flipping in place like normal. For this release, I fixed the player character's.
* As a bit of a break from bug fixing, I added an email format, so you can get jobs by email, too. You won't actually get jobs that use it yet, however.
